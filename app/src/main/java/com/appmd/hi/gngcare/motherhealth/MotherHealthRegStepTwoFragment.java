package com.appmd.hi.gngcare.motherhealth;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.BluetoothManager;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_hedctdata;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.JsonLogPrint;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.util.TextWatcherUtil;
import com.appmd.hi.gngcare.intro.JunMemberCertifi2Activity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.setting.SettingActivity;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public class MotherHealthRegStepTwoFragment extends BaseFragment {

    private MotherHealthRegActivity activity;
    private View view;

    private LinearLayout mActBtn01;
    private LinearLayout mActBtn02;
    private LinearLayout mActBtn03;

    private LinearLayout mEditBefLy;
    private LinearLayout mEditAftLy;

    private Button mSaveBtn;

    private EditText befCmEdTxt; //임신 전 키
    private EditText befKgEdTxt; //임신 전 몸무게
    private EditText curKgEdTxt; //현재 몸무게
    private EditText fulKgEdTxt; //만삭 몸무게
    private TextView birthTxt; //출산 예정일
    private TextView chgDateTxt;  //
    private Spinner kindSpinner;  //임신 종류
    private Spinner milkSpinner;  //모유수유 종류

    private String actType = "1";

    private String chlKindType = "1";
    private String chlMilkType = "Y";

    private int curYear;
    private int curMonth;
    private int curDay;

    private int selYear;
    private int selMonth;
    private int selDay;
    private String today;


    private String beforerCm;
    private String beforerKg;
    private String mberKg;

    private String birth_chl_yn;
    private String mber_birth_due_de;
    private String mber_chl_typ;

    private String mber_term_kg;
    private String mber_chl_birth_de;
    private String mber_milk_yn;
    CommonData commonData = CommonData.getInstance();

    private CustomAlertDialog mDialog;

    private TextView mActBtnTv1,mActBtnTv1_1,mActBtnTv2,mActBtnTv2_1,mActBtnTv3,mActBtnTv3_1;

    private LinearLayout mbirthbefore_lv,mbirthday_lv;

    private int TabNum = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        activity = (MotherHealthRegActivity) getActivity();
        view = inflater.inflate(R.layout.mother_health_reg_step_two_fragment, null);

        initLayout(view);
        setEvent();
        setLayout();
        initCurDate();
        Logger.i("activity.getMatherBeforeFlag()","activity.getMatherBeforeFlag():"+activity.getMatherBeforeFlag());

        Bundle bundle = getArguments();
//        if(bundle != null)
//            TabNum = bundle.getInt("TabNum",0);


        return view;

    }


    public void initCurDate() {
            /* 날짜 초기화 */
        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        Date currentTime = new Date();
        //   String mTime = mSimpleDateFormat.format(currentTime);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        String curDate = mSimpleDateFormat.format(calendar.getTime());
        String[] curDateStrArr = curDate.split("-");

        //ssshin 수정 20190110
        //calendar.add(Calendar.MONTH, -1);

        String pDate = mSimpleDateFormat.format(calendar.getTime());
        String[] pDateStrArr = pDate.split("-");

        curYear = Integer.valueOf(pDateStrArr[0]);
        //ssshin 수정 20190110
        //curMonth = Integer.valueOf(pDateStrArr[1]) + 1;
        curMonth = Integer.valueOf(pDateStrArr[1]);
        curDay = Integer.valueOf(pDateStrArr[2]);

        selYear = Integer.valueOf(pDateStrArr[0]);
        //ssshin 수정 20190110
        //selMonth = Integer.valueOf(pDateStrArr[1]) + 1;
        selMonth = Integer.valueOf(pDateStrArr[1]);
        selDay = Integer.valueOf(pDateStrArr[2]);

        birthTxt.setText("");
        chgDateTxt.setText("");
        befCmEdTxt.setText("");
        befKgEdTxt.setText("");
        curKgEdTxt.setText("");
        fulKgEdTxt.setText("");

        // 2018.05.30 김영준 과장 요청으로 넣음
        CommonData commonData = CommonData.getInstance();
        String bithDe = commonData.getMberBirthDueDe();
        Logger.i("", "bithDe="+bithDe);
        if(commonData.getMberGrad().equals("10")) {
            if (TextUtils.isEmpty(bithDe) == false && bithDe.length() >= 8) {
                String str = bithDe.substring(0, 4);
                str += "-" + bithDe.substring(4, 6);
                str += "-" + bithDe.substring(6, 8);
                chgDateTxt.setText(str);
                mbirthbefore_lv.setVisibility(View.VISIBLE);
            }
        }else{
            mbirthbefore_lv.setVisibility(View.GONE);
            final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
            int year = mDate.getYear() + 1900;
            int month = mDate.getMonth() + 1;
            int day = mDate.getDate();

            String bithDeJun = String.format("%04d",year) +  String.format("%02d",month)  +  String.format("%02d",day);
            if (TextUtils.isEmpty(bithDe) == false && bithDe.length() >= 8) {
                String str = bithDe.substring(0, 4);
                str += "-" + bithDe.substring(4, 6);
                str += "-" + bithDe.substring(6, 8);
                chgDateTxt.setText(str);
            }else{
                String str =  bithDeJun.substring(0, 4);
                str += "-" + bithDeJun.substring(4, 6);
                str += "-" + bithDeJun.substring(6, 8);
                chgDateTxt.setText(str);
            }
        }

        recvMonJinData();

        if("2".equals(Util.getSharedPreference(getContext(), "MonJin_actqy"))){
            mActBtnTv1.setSelected(false);
            mActBtnTv1_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv2.setSelected(true);
            mActBtnTv2_1.setTextColor(getResources().getColor(R.color.color_F2199D));
            mActBtnTv3.setSelected(false);
            mActBtnTv3_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            actType = "2";
        } else if("3".equals(Util.getSharedPreference(getContext(), "MonJin_actqy"))){
            mActBtnTv1.setSelected(false);
            mActBtnTv1_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv2.setSelected(false);
            mActBtnTv2_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv3.setSelected(true);
            mActBtnTv3_1.setTextColor(getResources().getColor(R.color.color_FC1263));
            actType = "3";
        }else{
            mActBtnTv1.setSelected(true);
            mActBtnTv1_1.setTextColor(getResources().getColor(R.color.color_FDB92C));
            mActBtnTv2.setSelected(false);
            mActBtnTv2_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv3.setSelected(false);
            mActBtnTv3_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            actType = "1";
        }



        setSaveBtnEnable();
    }


//    private TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            setSaveBtnEnable();
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//
//
//
//            String strNumber = String.format("%.2f", nNumber);
//
//        }
//    };


    public void setSaveBtnEnable() {
        if (activity.getMatherBeforeFlag()) { //임신 중
            if (befCmEdTxt.getText().toString().length() != 0        //공통
                    && befKgEdTxt.getText().toString().length() != 0    //공통
                    && curKgEdTxt.getText().toString().length() != 0    //공통
                    && chgDateTxt.getText().toString().length() != 0 //출산예정일
                    ) {
                mSaveBtn.setEnabled(true);
                mSaveBtn.setBackgroundColor(getResources().getColor(R.color.color_fd0ea2));
            } else {
                mSaveBtn.setEnabled(false);
                mSaveBtn.setBackgroundColor(getResources().getColor(R.color.color_fec0e7));
            }
        } else {                              //출산 후
            if (befCmEdTxt.getText().toString().length() != 0
                    && befKgEdTxt.getText().toString().length() != 0
                    && curKgEdTxt.getText().toString().length() != 0
                    && fulKgEdTxt.getText().toString().length() != 0 //만삭
                    && birthTxt.getText().toString().length() != 0 //출산일
                    ) {
                mSaveBtn.setEnabled(true);
                mSaveBtn.setBackgroundColor(getResources().getColor(R.color.color_fd0ea2));
            } else {
                mSaveBtn.setEnabled(false);
                mSaveBtn.setBackgroundColor(getResources().getColor(R.color.color_fec0e7));
            }
        }
    }

    public void initLayout(View view) {

        mActBtn01 = (LinearLayout) view.findViewById(R.id.actBtn01);
        mActBtn02 = (LinearLayout) view.findViewById(R.id.actBtn02);
        mActBtn03 = (LinearLayout) view.findViewById(R.id.actBtn03);
        mEditBefLy = (LinearLayout) view.findViewById(R.id.editBefLy);
        mEditAftLy = (LinearLayout) view.findViewById(R.id.editAftLy);

        mbirthbefore_lv = view.findViewById(R.id.birthbefore_lv);
        mbirthday_lv = view.findViewById(R.id.birthday_lv);


        mSaveBtn = (Button) view.findViewById(R.id.saveBtn);
        mSaveBtn.setEnabled(false);
        mSaveBtn.setBackgroundColor(getResources().getColor(R.color.color_fec0e7));

        befCmEdTxt = (EditText) view.findViewById(R.id.befCmEdTxt); //임신 전 키
        befKgEdTxt = (EditText) view.findViewById(R.id.befKgEdTxt); //임신 전 몸무게
        curKgEdTxt = (EditText) view.findViewById(R.id.curKgEdTxt);
        fulKgEdTxt = (EditText) view.findViewById(R.id.fulKgEdTxt);
        birthTxt = (TextView) view.findViewById(R.id.birthTxt);
        chgDateTxt = (TextView) view.findViewById(R.id.chgDateTxt);
        kindSpinner = (Spinner) view.findViewById(R.id.kindSpinner);
        milkSpinner = (Spinner) view.findViewById(R.id.milkSpinner);

        new TextWatcherUtil().setTextWatcher(befCmEdTxt, 210, 2);
        new TextWatcherUtil().setTextWatcher(befKgEdTxt, 130, 2);
        new TextWatcherUtil().setTextWatcher(curKgEdTxt, 130, 2);
        new TextWatcherUtil().setTextWatcher(fulKgEdTxt, 130, 2);

        mActBtnTv1 = (TextView) view.findViewById(R.id.actBtnTv1);
        mActBtnTv1_1 = (TextView) view.findViewById(R.id.actBtnTv1_1);
        mActBtnTv2 = (TextView) view.findViewById(R.id.actBtnTv2);
        mActBtnTv2_1 = (TextView) view.findViewById(R.id.actBtnTv2_1);
        mActBtnTv3 = (TextView) view.findViewById(R.id.actBtnTv3);
        mActBtnTv3_1 = (TextView) view.findViewById(R.id.actBtnTv3_1);

    }

    private DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            selYear = year;
            selMonth = (monthOfYear + 1);
            selDay = dayOfMonth;

            String selDateStr = getDateStr(selYear, selMonth, selDay);
            String curDateStr = getDateStr(curYear, curMonth, curDay);

            int selDateInt = Integer.valueOf(selDateStr.replaceAll("-", ""));
            int curDateInt = Integer.valueOf(curDateStr.replaceAll("-", ""));

            if (activity.getMatherBeforeFlag()) { //임신 중
                if (selDateInt < curDateInt) {
                    Toast.makeText(getActivity(), "출산예정일은 미래 날짜로 입력해주세요.", Toast.LENGTH_SHORT).show();
                    selYear = curYear;
                    selMonth = curMonth;
                    selDay = curDay;
                    return;
                }

                if(selDateInt > StringUtil.getIntVal(CDateUtil.addDay(CDateUtil.getToday_yyyy_MM_dd(),280))){
                    Toast.makeText(getActivity(), "출산예정일은 40주를 넘길 수 없습니다.", Toast.LENGTH_SHORT).show();
                    selYear = curYear;
                    selMonth = curMonth;
                    selDay = curDay;
                    return;
                }
            } else {                              //출산 후
                if (selDateInt > curDateInt) {
                    Toast.makeText(getActivity(), "출산일은 미래일 수 없습니다.", Toast.LENGTH_SHORT).show();
                    selYear = curYear;
                    selMonth = curMonth;
                    selDay = curDay;
                    return;
                }
            }

            birthTxt.setText(getDateStr(year, (monthOfYear + 1), dayOfMonth));
            chgDateTxt.setText(getDateStr(year, (monthOfYear + 1), dayOfMonth));
            setSaveBtnEnable();
        }
    };

    public String getDateStr(int y, int m, int d) {
        String result = getIntToStringForDate(y)
                + "-" + getIntToStringForDate(m)
                + "-" + getIntToStringForDate(d);
        return result;
    }

    public String getIntToStringForDate(int tmp) {
        String result = String.valueOf(tmp);
        if (result.length() == 1) {
            result = "0" + result;
        }
        return result;
    }

    public boolean checkBeforeSave() {

        if (befCmEdTxt.getText().length() == 0
                || befKgEdTxt.getText().length() == 0
                || curKgEdTxt.getText().length() == 0) {
            Toast.makeText(getActivity(), "정보를 입력해주세요.", Toast.LENGTH_SHORT).show();
            return false;
        }


        float tmp = Float.valueOf(befCmEdTxt.getText().toString()); //신장
        if (tmp < 120 || 210 < tmp) {
            Toast.makeText(getActivity(), "키는 120~210cm까지 입력가능합니다.", Toast.LENGTH_SHORT).show();
            return false;
        }
        tmp = Float.valueOf(befKgEdTxt.getText().toString()); //임신 전 몸무게
        if (tmp < 30 || 130 < tmp) {
            Toast.makeText(getActivity(), "몸무게는 30~130kg까지 입력가능합니다.", Toast.LENGTH_SHORT).show();
            return false;
        }

        tmp = Float.valueOf(curKgEdTxt.getText().toString()); //현재 몸무게
        if (tmp < 30 || 130 < tmp) {
            Toast.makeText(getActivity(), "몸무게는 30~130kg까지 입력가능합니다.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (activity.getMatherBeforeFlag()) { //임신 중
            if (chgDateTxt.getText().toString().length() == 0) {  //출산 예정일
                Toast.makeText(getActivity(), "출산 예정일을 선택해주세요.", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {                              //출산 후
            tmp = Float.valueOf(fulKgEdTxt.getText().toString()); //만삭 몸무게
            if (fulKgEdTxt.getText().length() == 0) {
                Toast.makeText(getActivity(), "만삭 시 몸무게를 기재해주세요.", Toast.LENGTH_SHORT).show();
                return false;
            } else if (tmp < 30 || 130 < tmp) {
                Toast.makeText(getActivity(), "몸무게는 30~130kg까지 입력가능합니다.", Toast.LENGTH_SHORT).show();
                return false;
            } else if (birthTxt.getText().toString().length() == 0) { //출산일
                Toast.makeText(getActivity(), "출산일을 선택해주세요.", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    public void recvMonJinData(){
      befCmEdTxt.setText(TextUtils.isEmpty(Util.getSharedPreference(getContext(),"MonJin_bef_cm"))?"":Util.getSharedPreference(getContext(),"MonJin_bef_cm"));
      befKgEdTxt.setText(TextUtils.isEmpty(Util.getSharedPreference(getContext(),"MonJin_bef_kg"))?"":Util.getSharedPreference(getContext(),"MonJin_bef_kg"));
      curKgEdTxt.setText(TextUtils.isEmpty(Util.getSharedPreference(getContext(),"MonJin_mber_kg"))?"":Util.getSharedPreference(getContext(),"MonJin_mber_kg"));
      fulKgEdTxt.setText(TextUtils.isEmpty(Util.getSharedPreference(getContext(),"MonJin_mber_term_kg"))?"":Util.getSharedPreference(getContext(),"MonJin_mber_term_kg"));

      if(commonData.getMberGrad().equals("10")) {
          mbirthday_lv.setVisibility(View.VISIBLE);
          if (TextUtils.isEmpty(Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de")) == false && Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").length() >= 8) {
              String str = Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(0, 4);
              str += "-" + Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(4, 6);
              str += "-" + Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(6, 8);
              birthTxt.setText(str);
          } else {
              birthTxt.setText("");
          }
      }else{
          mbirthday_lv.setVisibility(View.GONE);
          final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
          int year = mDate.getYear() + 1900;
          int month = mDate.getMonth() + 1;
          int day = mDate.getDate();

          String bith = String.format("%04d",year) +  String.format("%02d",month)  +  String.format("%02d",day);
          if (TextUtils.isEmpty(Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de")) == false && Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").length() >= 8) {
              String str = Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(0, 4);
              str += "-" + Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(4, 6);
              str += "-" + Util.getSharedPreference(getContext(), "MonJin_mber_chl_birth_de").substring(6, 8);
              birthTxt.setText(str);
          } else {
              String str = bith.substring(0, 4);
              str += "-" + bith.substring(4, 6);
              str += "-" + bith.substring(6, 8);
              birthTxt.setText(str);
          }
      }

        if(Util.getSharedPreference(getContext(),"MonJin_mber_milk_yn").equals("Y")) {
            milkSpinner.setSelection(0);
            chlMilkType = "Y";
        }
        else{
            milkSpinner.setSelection(1);
            chlMilkType = "N";
        }
        chgDateTxt.setText(TextUtils.isEmpty(chgDateTxt.getText().toString())?Util.getSharedPreference(getContext(),"mber_birth_due_de"):chgDateTxt.getText().toString());

        kindSpinner.setSelection(StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ"))!=0?StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ"))-1:StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ")));
        chlKindType = String.valueOf(StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ"))!=0?StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ")):StringUtil.getIntVal(Util.getSharedPreference(getContext(),"MonJin_mber_chl_typ"))+1);

    }



    public void requestRegMatherData() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put("api_code", activity.getApiCode());
            object.put("insures_code", activity.getInsuresCode());
            object.put("mber_sn", activity.getMberSn());

            CommonData commonData = CommonData.getInstance();

            beforerCm = befCmEdTxt.getText().toString().trim();
            beforerKg = befKgEdTxt.getText().toString().trim();
            mberKg = curKgEdTxt.getText().toString().trim();

            /* 공통 */
            object.put("actqy", actType);           //활동 타입
            object.put("bef_cm", beforerCm);   //임신전 키
            object.put("bef_kg", beforerKg);   //임신전 몸무게
            object.put("mber_kg", mberKg);  //몸무게

            if (activity.getMatherBeforeFlag()) { //임신 중
                object.put("birth_chl_yn", "N");                                   //임신 중                 //임신 종류
                object.put("mber_birth_due_de", chgDateTxt.getText().toString().trim().replaceAll("-", "")); //출산일 변경
                object.put("mber_chl_typ", chlKindType); //출산일 변경

                object.put("mber_term_kg", "");      //공백 전송
                object.put("mber_chl_birth_de", "");  //공백 전송
                object.put("mber_milk_yn", "");       //공백 전송

                birth_chl_yn = "N";
                mber_birth_due_de = chgDateTxt.getText().toString().trim().replaceAll("-", "");
                mber_chl_typ = chlKindType;

                mber_term_kg = "";
                mber_chl_birth_de = "";
                mber_milk_yn = "";

            } else {  //출산 후
                object.put("birth_chl_yn", "Y");
                object.put("mber_term_kg", fulKgEdTxt.getText().toString().trim());     //만삭
                object.put("mber_chl_birth_de", birthTxt.getText().toString().trim().replaceAll("-", ""));  //출산일
                object.put("mber_milk_yn", chlMilkType);                                //모유 수유

                object.put("mber_birth_due_de", "");    //공백 전송
                object.put("mber_chl_typ", "");         //공백 전송

                birth_chl_yn = "N";
                mber_term_kg = fulKgEdTxt.getText().toString().trim();     //만삭
                mber_chl_birth_de = birthTxt.getText().toString().trim().replaceAll("-", "");  //출산일
                mber_milk_yn = chlMilkType;

                mber_birth_due_de = "";
                mber_chl_typ = "";
            }

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));
            RequestApi.requestApi(getActivity(), NetworkConst.NET_PSY_CHECK_M, NetworkConst.getInstance().getDefDomain(), networkListener, params, activity.getProgressLayout());
        } catch (Exception e) {
            Log.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            hideProgress();
            switch (type) {
                case NetworkConst.NET_PSY_CHECK_M:             // 음원 메인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                //    Toast.makeText(getActivity(), resultData.toString(), Toast.LENGTH_LONG).show();
                                if (resultData.getString("reg_yn").compareTo("Y") == 0) {


                                    commonData.setActqy(actType);
                                    commonData.setBefCm(beforerCm);
                                    commonData.setBefKg(beforerKg);

                                    commonData.setMberKg(mberKg);
                                    commonData.setMotherWeight(mberKg);
                                    commonData.setbirth_chl_yn(birth_chl_yn);
                                    commonData.setMberBirthDueDe(mber_birth_due_de);
                                    commonData.setMberChlBirthDe(mber_chl_birth_de);

                                    uploadWeight(curKgEdTxt.getText().toString(),today);


                                } else {
                                    Toast.makeText(getActivity(), "다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                    //코드 에러
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type,
                                   int httpResultCode, CustomAlertDialog dialog) {
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog
                dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };


    public void setLayout() {
        if (activity.getMatherBeforeFlag()) { //임신 중
            mEditBefLy.setVisibility(View.VISIBLE);
            mEditAftLy.setVisibility(View.GONE);
        } else {                              //출산 후
            mEditBefLy.setVisibility(View.GONE);
            mEditAftLy.setVisibility(View.VISIBLE);
        }
    }

    public void setEvent() {
        mActBtn01.setOnClickListener(actBtnListener);
        mActBtn02.setOnClickListener(actBtnListener);
        mActBtn03.setOnClickListener(actBtnListener);

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBeforeSave()) {
                    today = CDateUtil.getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
                    Log.i("MotherRegTwo", "today :" + today);
                    requestRegMatherData();
                }
            }
        });

        kindSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chlKindType = String.valueOf(i + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        milkSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    chlMilkType = "Y";
                } else {
                    chlMilkType = "N";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        birthTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), dateListener, selYear, selMonth - 1, selDay); //1탭

                dialog.show();
                dialog.getDatePicker().getTouchables().get(0).performClick();
            }
        });

        chgDateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), dateListener, selYear, selMonth - 1, selDay); //1탭
                dialog.show();
            }
        });

        befCmEdTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setSaveBtnEnable();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String tmp = befCmEdTxt.getText().toString();
                if (befCmEdTxt.getText().toString().length() != 0 && befCmEdTxt.getText().toString().contains(".")) {
                    String st = tmp.substring(tmp.indexOf("."), tmp.length());
                    if (st.length() > 3) {
                        String hTmp = tmp.substring(0, tmp.indexOf("."));
                        String tTmp = st;
                        befCmEdTxt.setText(hTmp + tTmp.substring(0, 3));
                    }
                }
            }
        });
        befKgEdTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setSaveBtnEnable();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String tmp = befKgEdTxt.getText().toString();
                if (befKgEdTxt.getText().toString().length() != 0 && befKgEdTxt.getText().toString().contains(".")) {
                    String st = tmp.substring(tmp.indexOf("."), tmp.length());
                    if (st.length() > 3) {
                        String hTmp = tmp.substring(0, tmp.indexOf("."));
                        String tTmp = st;
                        befKgEdTxt.setText(hTmp + tTmp.substring(0, 3));
                    }
                }
            }
        });
        curKgEdTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setSaveBtnEnable();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String tmp = curKgEdTxt.getText().toString();
                if (curKgEdTxt.getText().toString().length() != 0 && curKgEdTxt.getText().toString().contains(".")) {
                    String st = tmp.substring(tmp.indexOf("."), tmp.length());
                    if (st.length() > 3) {
                        String hTmp = tmp.substring(0, tmp.indexOf("."));
                        String tTmp = st;
                        curKgEdTxt.setText(hTmp + tTmp.substring(0, 3));
                    }
                }
            }
        });
        fulKgEdTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setSaveBtnEnable();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String tmp = fulKgEdTxt.getText().toString();
                if (fulKgEdTxt.getText().toString().length() != 0 && fulKgEdTxt.getText().toString().contains(".")) {
                    String st = tmp.substring(tmp.indexOf("."), tmp.length());
                    if (st.length() > 3) {
                        String hTmp = tmp.substring(0, tmp.indexOf("."));
                        String tTmp = st;
                        fulKgEdTxt.setText(hTmp + tTmp.substring(0, 3));

                    }
                }
            }
        });

    }

    /**
     * 체중 데이터 입력
     *
     */
    public void uploadWeight(String weight, String regDate) {
        showProgress();

        CommonData commonData = CommonData.getInstance();

        Tr_get_hedctdata.DataList data = new Tr_get_hedctdata.DataList();
        data.bmr = "0";
        data.bodywater = "0"     ;
        data.bone = "0"          ;
        data.fat = "0"           ;
        data.heartrate = "0"     ;
        data.muscle = "0"        ;
        data.obesity = "0"       ;
        data.weight = "" + weight;
        data.bdwgh_goal = "" + commonData.getMotherGoalWeight();//weightModel.getBdwgh_goal();


        data.idx = CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis()));
        data.regtype = "U";//weightEt.getTag().toString();
        data.reg_de = regDate;

        List<Tr_get_hedctdata.DataList> datas = new ArrayList<>();
        datas.add(data);

        new DeviceDataUtil().uploadWeight(this, datas, new BluetoothManager.IBluetoothResult() {
            @Override
            public void onResult(boolean isSuccess) {
                Logger.i("MotherRegTwo", "DeviceDataUtil().uploadWeight.weight="+weight);
                hideProgress();

                commonData.setMotherWeight(weight);


                if(isSuccess){
                    mDialog = new CustomAlertDialog(getActivity(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.mother_health_reg_dialog_title));
                    mDialog.setContent(getString(R.string.mother_health_reg_dialog_contents));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (cDialog, button) -> {
                        Intent intent = new Intent(getActivity(), MotherHealthMainActivity.class);
                        intent.putExtra("Num",TabNum);
                        startActivity(intent);
                        getActivity().finish();
                        commonData.setHpMjYn("Y");
                        commonData.setHpMjYnJun(true);
                        Util.setSharedPreference(getContext(), "MonJin_bef_cm", "");
                        Util.setSharedPreference(getContext(), "MonJin_bef_kg", "");
                        Util.setSharedPreference(getContext(), "MonJin_mber_kg","");
                        Util.setSharedPreference(getContext(), "MonJin_mber_term_kg", "");
                        Util.setSharedPreference(getContext(), "MonJin_mber_chl_birth_de", "");
                        Util.setSharedPreference(getContext(), "MonJin_mber_milk_yn", "");
                        Util.setSharedPreference(getContext(), "MonJin_mber_birth_due_de", "");
                        Util.setSharedPreference(getContext(), "MonJin_mber_chl_typ", "");
                        Util.setSharedPreference(getContext(), "MonJin_actqy", "");

                        cDialog.dismiss();
                    });
                    mDialog.show();
                }
                else{
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.noti));
                    mDialog.setContent("다시 시도해주세요.");
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                        @Override
                        public void onClick(CustomAlertDialog dialog, Button button) {
                            dialog.dismiss();
                        }
                    });
                    mDialog.show();
                }

            }
        });

    }

    public View.OnClickListener actBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActBtnTv1.setSelected(false);
            mActBtnTv1_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv2.setSelected(false);
            mActBtnTv2_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            mActBtnTv3.setSelected(false);
            mActBtnTv3_1.setTextColor(getResources().getColor(R.color.color_B3B3B3));
            if (v.getId() == R.id.actBtn01) {
                mActBtnTv1.setSelected(true);
                mActBtnTv1_1.setTextColor(getResources().getColor(R.color.color_FDB92C));
                actType = "1";
            } else if (v.getId() == R.id.actBtn02) {
                mActBtnTv2.setSelected(true);
                mActBtnTv2_1.setTextColor(getResources().getColor(R.color.color_F2199D));
                actType = "2";
            } else if (v.getId() == R.id.actBtn03) {
                mActBtnTv3.setSelected(true);
                mActBtnTv3_1.setTextColor(getResources().getColor(R.color.color_FC1263));
                actType = "3";
            }

        }
    };

}