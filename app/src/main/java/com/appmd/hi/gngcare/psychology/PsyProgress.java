package com.appmd.hi.gngcare.psychology;

import android.content.Context;
import android.os.Build;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;

import java.util.Arrays;


public class PsyProgress extends LinearLayout {
    private final String TAG = getClass().getSimpleName();

    public String[] MODE_위험부족양호우수_4;        // 대인관계, 두뇌건강
    public String[] MODE_낮음보통높음_3;            // 자아정체감, 집중도
    public String[] MODE_낮음보통아주높음_4;        // 학습욕구
    public String[] MODE_소아우울증;
    public String[] MODE_스마트폰중독;

    public String[] MODE_교육강박검사;
    public String[] MODE_우울증검사;
    public String[] MODE_상황판단검사;

    private LinearLayout mBottomLabelLayout;

    private ProgressBar mProgress;

    private TextView mLableTv1;
    private TextView mLableTv2;
    private TextView mLableTv3;
    private TextView mLableTv4;
    private TextView[] mLables;

    public PsyProgress(Context context) {
        super(context);
        initView();
    }

    public PsyProgress(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PsyProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void initView() {
        createLabelTextView();

        View progressView = inflate(getContext(), R.layout.child_mind_progress, null);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        addView(progressView, params);

        mProgress = progressView.findViewById(R.id.child_progress_bar);
        mProgress.setMax(100);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mProgress.setMin(0);
        }
        mBottomLabelLayout = progressView.findViewById(R.id.child_progress_bottom_layout);

        mLableTv1 = progressView.findViewById(R.id.child_progress_label_1);
        mLableTv2 = progressView.findViewById(R.id.child_progress_label_2);
        mLableTv3 = progressView.findViewById(R.id.child_progress_label_3);
        mLableTv4 = progressView.findViewById(R.id.child_progress_label_4);
        mLables = new TextView[]{mLableTv1, mLableTv2, mLableTv3, mLableTv4};
    }

    /**
     * 라벨에 따라 프로그래스 하단 Text 세팅
     * @param mName
     */
    public void setProgress(String mName, int level) {
        String label = replaceSpaceStr(mName);

        if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_01_01_label)).equals(label) || "대인관계".equals(mName)) {
            setBottomLabels(MODE_위험부족양호우수_4);
            mProgress.setMax(50);
            if(level < (int)((mProgress.getMax() / MODE_위험부족양호우수_4.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_위험부족양호우수_4.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_01_03_label)).equals(label) || "자아정체감".equals(mName)) {
            setBottomLabels(MODE_낮음보통높음_3);
            mProgress.setMax(80);
            if(level < (int)((mProgress.getMax() / MODE_낮음보통높음_3.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_낮음보통높음_3.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_01_04_label)).equals(label) || "소아 우울증".equals(mName)) {
            setBottomLabels(MODE_소아우울증);
            mProgress.setMax(60);
            level = mProgress.getMax() -level;
            if(level < (int)((mProgress.getMax() / MODE_소아우울증.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_소아우울증.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_02_01_label)).equals(label) || "집중도".equals(mName)  ) {
            setBottomLabels(MODE_낮음보통높음_3);
            mProgress.setMax(90);
            if(level < (int)((mProgress.getMax() / MODE_낮음보통높음_3.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_낮음보통높음_3.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_02_02_label)).equals(label) || "학습 욕구".equals(mName)) {
            setBottomLabels(MODE_낮음보통아주높음_4);
            mProgress.setMax(10);
            if(level < (int)((mProgress.getMax() / MODE_낮음보통아주높음_4.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_낮음보통아주높음_4.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_02_03_label)).equals(label) || "두뇌건강".equals(mName)  ) {
            setBottomLabels(MODE_위험부족양호우수_4);
            mProgress.setMax(50);
            if(level < (int)((mProgress.getMax() / MODE_위험부족양호우수_4.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_위험부족양호우수_4.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_02_04_label)).equals(label) || "스마트폰 중독".equals(mName)) {
            setBottomLabels(MODE_스마트폰중독);
            mProgress.setMax(10);
            level = mProgress.getMax() -level;
            if(level < (int)((mProgress.getMax() / MODE_스마트폰중독.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_스마트폰중독.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_03_01)).equals(label) || "교육 강박 검사".equals(mName)) {
            setBottomLabels(MODE_교육강박검사);
            mProgress.setMax(24);
            level = mProgress.getMax() -level;
            if(level < (int)((mProgress.getMax() / MODE_교육강박검사.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_교육강박검사.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_03_02)).equals(label) || "엄마 우울증 검사".equals(mName)) {
            setBottomLabels(MODE_우울증검사);
            mProgress.setMax(60);
            level = mProgress.getMax() -level;
            if(level < (int)((mProgress.getMax() / MODE_우울증검사.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_우울증검사.length) * 0.5);
            }
        } else if (replaceSpaceStr(getContext().getString(R.string.psy_btn_text_tab_03_03)).equals(label) || "상황 판단 검사".equals(mName)) {
            setBottomLabels(MODE_상황판단검사);
            mProgress.setMax(60);
            level = mProgress.getMax() -level;
            if(level < (int)((mProgress.getMax() / MODE_상황판단검사.length) * 0.5)){
                level = (int)((mProgress.getMax() / MODE_상황판단검사.length) * 0.5);
            }
        }


        mProgress.setProgress(level);


        Log.i(TAG, "mProgress.getMax="+mProgress.getMax()+", progress="+mProgress.getProgress()+", level="+level);

    }

    /**
     * 전문에서 들어오는 공백이 space가 아닌듯
     * @param str
     * @return
     */
    private String replaceSpaceStr(String str) {
        Log.i(TAG, "replaceSpaceStr.str="+str);
        if (TextUtils.isEmpty(str))
            return "";

        str = TextUtils.isEmpty(str) ? str : str.trim();
        str = str.replaceAll(" ", "");
        return str;
    }

    /**
     * 라벨 세팅 3개냐 4개냐 따라 다른 처리
     * @param labels
     */
    private void setBottomLabels(String... labels) {
        Log.i(TAG, "setBottomLabels="+ Arrays.toString(labels));
        try {
            if(labels.length <=3){
                mLables[3].setVisibility(View.GONE);
            }
            for (int i = 0; i < labels.length; i++) {
                mLables[i].setText(labels[i]);
                mLables[i].setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 프로그래스 하단 사용되는 라벨 표시
     */
    private void createLabelTextView() {
        MODE_위험부족양호우수_4 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_낮음보통높음_3 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_낮음보통아주높음_4 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_소아우울증 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_스마트폰중독 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};

        MODE_교육강박검사 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_우울증검사 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
        MODE_상황판단검사 = new String[]{getContext().getString(R.string.Bad), getContext().getString(R.string.Nomal), getContext().getString(R.string.Good)};
    }
}
