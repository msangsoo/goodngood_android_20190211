package com.appmd.hi.gngcare.collection;

/**
 * Created by MobileDoctor on 2017-03-21.
 */

public class FeverResultItem {
    private String mFeverReSn;
    private String mInputDe;
    private String mInputCode;

    public String getmFeverReSn() {
        return mFeverReSn;
    }

    public void setmFeverReSn(String mFeverReSn) {
        this.mFeverReSn = mFeverReSn;
    }

    public String getmInputDe() {
        return mInputDe;
    }

    public void setmInputDe(String mInputDe) {
        this.mInputDe = mInputDe;
    }

    public String getmInputCode() {
        return mInputCode;
    }

    public void setmInputCode(String mInputCode) {
        this.mInputCode = mInputCode;
    }
}
