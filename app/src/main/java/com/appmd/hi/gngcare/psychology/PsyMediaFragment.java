package com.appmd.hi.gngcare.psychology;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.PsyMediaAdapter;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.collection.PsyMediaItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;

import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-04-18.
 *
 * @since 0, 1
 */
public class PsyMediaFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, PsyMainActivity.onKeyBackPressedListener {

    private PsyMainActivity activity;
    private LinearLayout mVisibleLayout;
    private Button tabStrBtn;
    private Button tabConBtn;
    private View view;
    private PullToRefreshListView mRefreshListView;
    private PsyMediaAdapter mAdapter;
    private ArrayList<PsyMediaItem> mItem;
    private MediaPlayer mPlayer;

    // 현재 보고있는 페이지
    private int mPageId = 1;

    // 전체 페이지
    private int mTotalPage = 1;

    // header view
    private View mHeaderView;
    private TextView mHeaderTitleTv, mHeaderEmptyTv;
    private String mType = CommonData.STRING_ONE;


    private ImageView prevBtn;
    private int preIndex;
    private ImageView playBtn;
    private int playIndex;

    private Handler playHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            activity.getProgressLayout().setVisibility(View.VISIBLE);

            int position = msg.what;

            if(prevBtn != null ) {
                prevBtn.setImageResource(R.drawable.psy_play_on);
            }
            playBtn = (ImageView) msg.obj;

            if(prevBtn != null && prevBtn == playBtn){
                activity.hideProgress();
                prevBtn.setImageResource(R.drawable.psy_play_on);
                mPlayer.reset();
                prevBtn = null;
                return false;
            }
            playBtn.setImageResource(R.drawable.psy_play_off);

            PsyMediaItem data = mItem.get(position);
            data.getmUrl();

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    requestPlayAudio(data.getmUrl());
                }
            });
            thread.start();

            prevBtn = playBtn;
            preIndex = msg.what;

            activity.hideProgress();

            return false;
        }
    });


    public void requestPlayAudio(String mUrl){



        // Set the media player audio stream type
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //Try to play music/audio from url
        try{

            if(mPlayer.isPlaying())
                mPlayer.reset();
                    /*
                        void setDataSource (String path)
                            Sets the data source (file-path or http/rtsp URL) to use.

                        Parameters
                            path String : the path of the file, or the http/rtsp URL of the stream you want to play

                        Throws
                            IllegalStateException : if it is called in an invalid state

                                When path refers to a local file, the file may actually be opened by a
                                process other than the calling application. This implies that the
                                pathname should be an absolute path (as any other process runs with
                                unspecified current working directory), and that the pathname should
                                reference a world-readable file. As an alternative, the application
                                could first open the file for reading, and then use the file
                                descriptor form setDataSource(FileDescriptor).

                            IOException
                            IllegalArgumentException
                            SecurityException
                    */
            // Set the audio data source
            mPlayer.setDataSource(mUrl);

                    /*
                        void prepare ()
                            Prepares the player for playback, synchronously. After setting the
                            datasource and the display surface, you need to either call prepare()
                            or prepareAsync(). For files, it is OK to call prepare(), which blocks
                            until MediaPlayer is ready for playback.

                        Throws
                            IllegalStateException : if it is called in an invalid state
                            IOException
                    */
            // Prepare the media player
            mPlayer.prepare();

            // Start playing audio from http url
            mPlayer.start();

        }catch (IOException e){
            // Catch the exception
            e.printStackTrace();
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (SecurityException e){
            e.printStackTrace();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }

        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                if(mediaPlayer.isPlaying()){
                    activity.hideProgress();
                }
            }
        });

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (getContext() != null) {
                    Toast.makeText(getContext(),"End",Toast.LENGTH_SHORT).show();
                    playBtn.setEnabled(true);
                }
            }
        });
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (PsyMainActivity)getActivity();
        view = inflater.inflate(R.layout.psy_media_fragment, null);
        init(view);
        setEvent();
        mPlayer = new MediaPlayer();
        // 헤더뷰
        addHeaderView();
        requestPsyMediaData(mType);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitleTxt("심 리 음 원");
    }

    @Override
    public void onStop(){
//        mPlayer.reset();
        super.onStop();
    }


    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {

        mRefreshListView = (PullToRefreshListView) view.findViewById(R.id.psy_media_listview);
        mVisibleLayout = (LinearLayout) view.findViewById(R.id.visible_layout);
        tabStrBtn = (Button) view.findViewById(R.id.tabStrBtn);
        tabConBtn = (Button) view.findViewById(R.id.tabConBtn);
        tabStrBtn.setSelected(true);
        tabConBtn.setSelected(false);

        mItem = new ArrayList<PsyMediaItem>();
//        mListView.setDividerHeight(BleUtil.pixelFromDP(getActivity(), 29));

    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {

        tabStrBtn.setOnClickListener(this);
        tabConBtn.setOnClickListener(this);

        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view, getContext());

        //아이 심리
        tabStrBtn.setOnTouchListener(mClickListener);
        tabConBtn.setOnTouchListener(mClickListener);

        //코드 부여(아이 심리)
        tabStrBtn.setContentDescription(getString(R.string.tabStrBtn));
        tabConBtn.setContentDescription(getString(R.string.tabConBtn));


        // 상단에서 내리면 새로고침 동작
        mRefreshListView.setOnRefreshListener(refreshView -> {
            // TODO Auto-generated method stub
            mItem.clear();

            mPageId = 1;

            GLog.i("상단에서 내릴때--->", "dd");
            requestPsyMediaData(mType);

        });

        // 리스트 마지막 아이템일 경우
        mRefreshListView.setOnLastItemVisibleListener(() -> {
            // TODO Auto-generated method stub
//            switch (mMode) {
//                case MODE_NORMAL: // 기본모드
//                    if (mPageId != 1 && mPageId <= mTotalPage) {    // 현재 페이지가 1이 아니거나, 전체 페이지수보다 적을경우 호출
//                    //    requestFaqList(mSearchEdit.getText().toString());
//                    }
//                    break;
//
//            }
        });

    }

    /**
     * 리스트뷰 연결
     */
    public void setListAdapter() {
        try {
            mAdapter = new PsyMediaAdapter((PsyMainActivity) getActivity(), mItem, playHandler);
            mRefreshListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 성장 FAQ headerView
     */
    public void addHeaderView() {

        mHeaderView = mLayoutInflater.inflate(R.layout.child_care_note_header_view, null, false);
        mHeaderTitleTv = (TextView) mHeaderView.findViewById(R.id.title_tv);
        mHeaderEmptyTv = (TextView) mHeaderView.findViewById(R.id.empty_tv);

        mHeaderTitleTv.setText(getString(R.string.faq_desc));
        mHeaderEmptyTv.setVisibility(View.GONE);

    }

    public void requestPsyMediaData(String type) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        if(mPlayer != null)
            mPlayer.reset();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP001);
            object.put(CommonData.JSON_ML_MCODE_F, type);
            object.put(CommonData.JSON_PGSIZE_F, "15");
            object.put(CommonData.JSON_NOWPG_F, mPageId);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(getActivity(), NetworkConst.NET_PSY_MEDIA, NetworkConst.getInstance().getPsyDomain(), networkListener, params,  activity.getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.search_del_btn:   // 검색 삭제
                GLog.i("search_del_btn", "dd");
                //   CommonView.getInstance().setClearEditText(mSearchEdit);
                break;
            case R.id.tabStrBtn:
                tabStrBtn.setSelected(true);
                tabConBtn.setSelected(false);

                mPageId = 1;

                prevBtn = null;
                playBtn = null;
                mPlayer.reset();

                mAdapter.clear();
                mItem.clear();
                mType = CommonData.STRING_ONE;
                requestPsyMediaData(mType);

                break;
            case R.id.tabConBtn:
                tabStrBtn.setSelected(false);
                tabConBtn.setSelected(true);

                mPageId = 1;

                prevBtn = null;
                playBtn = null;
                mPlayer.reset();

                mAdapter.clear();
                mItem.clear();
                mType = CommonData.STRING_TWO;
                requestPsyMediaData(mType);

                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch (id) {
            case R.id.search_edit:    // 검색
                //     CommonView.getInstance().setClearImageBt(mSearchDelBtn, hasFocus);
                break;
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            activity.hideProgress();
            switch (type) {
                case NetworkConst.NET_PSY_MEDIA:

                    mRefreshListView.onRefreshComplete();

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:

                            try {
                                if (resultData.getString("RESULT_CODE").compareTo("0000") == 0) {
                                    JSONArray jsonArr = resultData.getJSONArray("DATA");

                                    mTotalPage = resultData.getInt("DATA_LENGTH");
                                    GLog.i("mTotalPage = " + mTotalPage, "dd");
                                    //    GLog.i("mPageId = " + mPageId);

                                            if (mPageId == 1) {
                                                mItem.clear();
                                            }

                                    if (jsonArr.length() > 0) {    // 리스트가 있을 경우
                                        mHeaderEmptyTv.setVisibility(View.GONE);
                                        for (int i = 0; i < jsonArr.length(); i++) {
                                            JSONObject object = jsonArr.getJSONObject(i);

                                            PsyMediaItem item = new PsyMediaItem(object.getString("ML_SN"),
                                                    object.getString("IMG"),
                                                    object.getString("IURL"),
                                                    object.getString("TIT"),
                                                    object.getString("CON"),
                                                    object.getString("WR"),
                                                    object.getString("TOTPG"));
                                            mItem.add(item);
                                        }
                                    } else {  // 리스트 데이터가 없는 경우
                                        if (mPageId == 1) {
                                            mHeaderEmptyTv.setText(getString(R.string.child_care_note_empty));
                                            mHeaderEmptyTv.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    if (mPageId == 1) {   // 첫번쨰 리스트 호출
                                        setListAdapter();
                                        mRefreshListView.onRefreshComplete();
                                    } else {
                                        mAdapter.notifyDataSetChanged();
                                    }

                                    mPageId++;  // 페이지 증가

//                                    if (mItem.size() > 0 && mItem.size() <= CommonData.PAGE_MAX_COUNT) {
                                        Util.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
//                                    }
                                } else {
                                    //코드 에러
                                    activity.hideProgress();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                activity.hideProgress();
                            }
                            mRefreshListView.onRefreshComplete();
                            activity.hideProgress();

                            break;
                    }

            }
            activity.hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            activity.hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            activity.hideProgress();
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            GLog.i("s = " + s.toString() + "\n start = " + start + "\n before = " + before + "\n count = " + count, "dd");

//            if(mSearchEdit.getText().toString().length() == 0){  // 검색어 입력 후, 텍스트 삭제시 기본 화면으로 원복
//                mPageId = 1;
//                mSearchPageId = 1;
//
//                requestFaqList("");
//            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((PsyMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        }

        PsyMainActivity activity = (PsyMainActivity) getActivity();
        switchFragment(new PsyMainFragment());
    }
}
