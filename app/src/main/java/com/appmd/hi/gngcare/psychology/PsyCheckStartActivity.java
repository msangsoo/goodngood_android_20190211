package com.appmd.hi.gngcare.psychology;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.main.BabyInfoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by CP on 2018. 4. 6..
 */

public class PsyCheckStartActivity extends PsyBaseActivity {
    private final String TAG = getClass().getSimpleName();

    public static Activity PSY_CHECK_START_ACTIVITY;
    public LayoutInflater mLayoutInflater;
    public Fragment mContentFragment;
    public FrameLayout mFrameContainer;
    private Toolbar toolbar;


    boolean mNoShowHelp = false;

    // 네비바
    public RelativeLayout mBgActionBar;
    private ImageButton mLefeBtn;
    private RelativeLayout mRightLayout;
    private FrameLayout mBgBabyFace;
    private ImageView mRightImg;
    private TextView mTitleTv;


    private int pageCnt = 0;
    private int curPosition = 0;
    private JSONArray jDataArr = null;

    private ArrayList<String> selArray = new ArrayList<>();

    private ViewPager viewPager;
    private PsyCheckStartActivity.ViewPagerAdapter viewPagerAdapter;
    private ImageView prevBtn;
    private ImageView nextBtn;
    private TextView pageNumTxt;


    private Button selOneBtn;
    private Button selTwoBtn;
    private Button selThreeBtn;
    private Button selFourBtn;
    private Button selFiveBtn;

    private Button reqResultBtn;

    private String mCode;
    private String mTitle;

    private boolean continueFlag = false;
//    ("LK03001".equals(mCode) || "LK03002".equals(mCode) || "LK03003".equals(mCode) ) {



    private Intent intent;
    ArrayList<Button> btnArr = new ArrayList<>();

    private View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.psy_check_start_activity);
        PSY_CHECK_START_ACTIVITY = PsyCheckStartActivity.this;
        mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init();
        setEvent();

        intent = getIntent();

        mCode = intent.getStringExtra(CommonData.JSON_M_CODE_F);
        mTitle = intent.getStringExtra(CommonData.EXTRA_TITLE);

        if (Util.getSharedPreference(PsyCheckStartActivity.this, "psyCheckDataCode").compareTo("") == 0) {
            continueFlag = false;
        } else {
            continueFlag = true;

            mCode = Util.getSharedPreference(PsyCheckStartActivity.this, "psyCheckDataCode");
            mTitle = Util.getSharedPreference(PsyCheckStartActivity.this, "psyCheckDataTitle");
        }


        /* 임시 */
        setTitleTxt(mTitle);

        prevBtn.setVisibility(View.GONE);
        nextBtn.setVisibility(View.GONE);


        selOneBtn.setVisibility(View.GONE);
        selTwoBtn.setVisibility(View.GONE);
        selThreeBtn.setVisibility(View.GONE);
        selFourBtn.setVisibility(View.GONE);
        selFiveBtn.setVisibility(View.GONE);

        btnArr.add(selOneBtn);
        btnArr.add(selTwoBtn);
        btnArr.add(selThreeBtn);
        btnArr.add(selFourBtn);
        btnArr.add(selFiveBtn);


        // 자녀 데이터가 있는경우 UI 세팅
        if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
            CustomImageLoader.displayImage(PsyCheckStartActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
            mRightLayout.setVisibility(View.VISIBLE);
        } else {
            mRightLayout.setVisibility(View.GONE);
        }
        requestPsyCheckData();
        Log.i(getClass().getSimpleName(), "mCode="+mCode);

        View readyLayout = findViewById(R.id.psy_ready_layout); // 부모심리검사 준비화면
        TextView parentInfoTv = findViewById(R.id.psy_parent_check_info_tv);
        findViewById(R.id.psy_ready_start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readyLayout.setVisibility(View.GONE);
            }
        });

        if ("LK03001".equals(mCode)) {
            // 부모 심리 검사 일때, 설명 창 보여주기
            readyLayout.setVisibility(View.GONE);
            parentInfoTv.setText(R.string.psy_parent_check_info_1);
        } else if ("LK03002".equals(mCode)) {
            readyLayout.setVisibility(View.GONE);
            parentInfoTv.setText(R.string.psy_parent_check_info_2);
        } else if ("LK03003".equals(mCode) ) {
            readyLayout.setVisibility(View.GONE);
            parentInfoTv.setText(R.string.psy_parent_check_info_3);
        } else {
            readyLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 초기화
     */
    public void init() {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_no_main);

        // start custom actionbar leftmargin remove
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        // end custom actionbar leftmargin remove
        mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);

        mBgActionBar = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_action_bar);
        mBgActionBar.setBackgroundColor(getResources().getColor(R.color.h_green));
        mLefeBtn = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.left_btn);
        mRightLayout = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.right_layout);
        mBgBabyFace = (FrameLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_baby_face);
        mRightImg = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.photo_img);
        mBgBabyFace.setVisibility(View.GONE);

        mTitleTv = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.title_tv);

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        pageNumTxt = (TextView) findViewById(R.id.pageNumTxt);

        prevBtn = (ImageView) findViewById(R.id.prevBtn);
        nextBtn = (ImageView) findViewById(R.id.nextBtn);
        prevBtn.setVisibility(View.GONE);

        reqResultBtn = (Button) findViewById(R.id.reqResultBtn);
        reqResultBtn.setVisibility(View.GONE);
        selOneBtn = (Button) findViewById(R.id.selOneBtn);
        selTwoBtn = (Button) findViewById(R.id.selTwoBtn);
        selThreeBtn = (Button) findViewById(R.id.selThreeBtn);
        selFourBtn = (Button) findViewById(R.id.selFourBtn);
        selFiveBtn = (Button) findViewById(R.id.selFiveBtn);

        view = findViewById(R.id.root_view);

    }


    public void requestPsyCheckData() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP007);
            //         object.put(CommonData.JSON_CHL_SN_F, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_M_CODE_F, mCode);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(PsyCheckStartActivity.this, NetworkConst.NET_PSY_CHECK_M, NetworkConst.getInstance().getPsyDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    public void requestResult() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP003);
            object.put(CommonData.JSON_CHL_SN_F, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_M_CODE_F, mCode);

            String pNy = CommonData.getInstance().getIamChild();
            if ("N".equals(pNy)){
                pNy = "Y";
            }else{
                pNy = "N";
            }
            object.put(CommonData.JSON_P_YN, pNy);

            for (int i = 0; i <= pageCnt; i++) {
                object.put("A" + String.valueOf((i + 1)), selArray.get(i));
            }

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(PsyCheckStartActivity.this, NetworkConst.NET_PSY_CHECK_RESULT, NetworkConst.getInstance().getPsyDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            hideProgress();
            switch (type) {
                case NetworkConst.NET_PSY_CHECK_M:             // 음원 메인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                if (resultData.getString("RESULT_CODE").compareTo("0000") == 0) {
                                    jDataArr = resultData.getJSONArray("DATA");
                                    if (jDataArr.length() > 0) {
                                        //  pageCnt = jDataArr.length();

                                        GLog.i("tag", jDataArr.toString());
                                        // 실행할 동작 코딩

                                        viewPagerAdapter = new PsyCheckStartActivity.ViewPagerAdapter(getSupportFragmentManager());
                                        viewPager.setAdapter(viewPagerAdapter);

                                        selArray.clear();

                                        if (continueFlag) {
                                            String sharedData = Util.getSharedPreference(PsyCheckStartActivity.this, "psyCheckData");

                                            JSONArray tmp = new JSONArray(sharedData);

                                            for (int i = 0; i < jDataArr.length(); i++) {
                                                if (tmp.getString(i).compareTo("-1") != 0) {
                                                    selArray.add(tmp.getString(i));
                                                    curPosition = i;
                                                    pageCnt = i;
                                                } else {
                                                    selArray.add("-1");
                                                }
                                            }

                                            JSONObject obj = jDataArr.getJSONObject(curPosition);
                                            int btnCnt = Integer.valueOf(obj.getString("ANS_CNT"));
                                            for (int j = 0; j < btnCnt; j++) {
                                                btnArr.get(j).setVisibility(View.VISIBLE);
                                                String Text = obj.getString("ANS_TITLE" + String.valueOf(j + 1)).replace(" ", "");
                                                if (Text.length() == 5) {
                                                    Text = Text.substring(0, 2) + "\n" + Text.substring(2);
                                                } else if (Text.length() == 6) {
                                                    Text = Text.substring(0, 3) + "\n" + Text.substring(3);
                                                } else if (Text.length() == 7) {
                                                    Text = Text.substring(0, 2) + "\n" + Text.substring(2, 5) + "\n" + Text.substring(5);
                                                }
                                                btnArr.get(j).setText(Text);
                                                //btnArr.get(j).setText(obj.getString("ANS_TITLE" + String.valueOf(j + 1)));
                                            }


                                            pageNumTxt.setText(curPosition + 1 + "/" + jDataArr.length());

                                            viewPager.setCurrentItem(curPosition);
                                            viewPagerAdapter.notifyDataSetChanged();

                                            prevBtn.setVisibility(View.VISIBLE);
                                            nextBtn.setVisibility(View.GONE);

                                        } else {
                                            for (int i = 0; i < jDataArr.length(); i++) {
                                                selArray.add("-1");
                                            }
                                            pageNumTxt.setText(1 + "/" + jDataArr.length());

                                            JSONObject obj = jDataArr.getJSONObject(0);
                                            int btnCnt = Integer.valueOf(obj.getString("ANS_CNT"));
                                            for (int j = 0; j < btnCnt; j++) {
                                                btnArr.get(j).setVisibility(View.VISIBLE);
                                                String Text = obj.getString("ANS_TITLE" + String.valueOf(j + 1)).replace(" ", "");
                                                if (Text.length() == 5) {
                                                    Text = Text.substring(0, 2) + "\n" + Text.substring(2);
                                                } else if (Text.length() == 6) {
                                                    Text = Text.substring(0, 3) + "\n" + Text.substring(3);
                                                } else if (Text.length() == 7) {
                                                    Text = Text.substring(0, 2) + "\n" + Text.substring(2, 5) + "\n" + Text.substring(5);
                                                }
                                                btnArr.get(j).setText(Text);
                                                //btnArr.get(j).setText(obj.getString("ANS_TITLE" + String.valueOf(j + 1)));
                                            }

                                            viewPager.setCurrentItem(0);
                                            viewPagerAdapter.notifyDataSetChanged();
                                        }
                                    } else {

                                    }
                                } else {
                                    //코드 에러
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;

                case NetworkConst.NET_PSY_CHECK_RESULT:             // 음원 메인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                String apiResultCode = resultData.getString("RESULT_CODE");
                                if (apiResultCode.compareTo("0000") == 0) {

                                    Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckDataCode", "");
                                    Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckDataCodeData", "[]");
                                    Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckData", "[]");
                                    String objSeq = resultData.getString("OMJ_SEQ");
                                    Intent intent = new Intent(PsyCheckStartActivity.this, PsyCheckResultActivity.class);
                                    intent.putExtra("objSeq", objSeq);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    //코드 에러
                                    Log.e(TAG, "apiResultCode="+apiResultCode+", 심리결과 오류");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type,
                                   int httpResultCode, CustomAlertDialog dialog) {
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog
                dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };

    /* 타이틀 변경 */
    public void setTitleTxt(String title) {
        mTitleTv.setText(title);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {

        mRightLayout.setOnClickListener(btnListener);
        mLefeBtn.setOnClickListener(btnListener);
        mRightImg.setOnClickListener(btnListener);

        prevBtn.setOnClickListener(btnListener);
        nextBtn.setOnClickListener(btnListener);

        selOneBtn.setOnClickListener(btnListener);
        selTwoBtn.setOnClickListener(btnListener);
        selThreeBtn.setOnClickListener(btnListener);
        selFourBtn.setOnClickListener(btnListener);
        selFiveBtn.setOnClickListener(btnListener);

        reqResultBtn.setOnClickListener(btnListener);

        //click 저장
        OnClickListener mClickListener = new OnClickListener(btnListener,view, PsyCheckStartActivity.this);

        //아이 심리
        reqResultBtn.setOnTouchListener(mClickListener);

        //코드 부여(아이 심리)
        reqResultBtn.setContentDescription(getString(R.string.reqResultBtn));


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                //Toast.makeText(this(), "select position : " + i, 200).show();

                if (selArray.get(i).compareTo("-1") == 0) {
                    //    viewPager.setPagingEnabled(false);
                    nextBtn.setVisibility(View.GONE);
                    reqResultBtn.setVisibility(View.GONE);
                } else {
                    //    viewPager.setPagingEnabled(true);
                    if (i == jDataArr.length() - 1) {
                        reqResultBtn.setVisibility(View.VISIBLE);
                    }
                    nextBtn.setVisibility(View.VISIBLE);
                }


                try {
                    JSONObject obj = jDataArr.getJSONObject(i);

                    int btnCnt = Integer.valueOf(obj.getString("ANS_CNT"));


                    selOneBtn.setVisibility(View.GONE);
                    selTwoBtn.setVisibility(View.GONE);
                    selThreeBtn.setVisibility(View.GONE);
                    selFourBtn.setVisibility(View.GONE);
                    selFiveBtn.setVisibility(View.GONE);

                    for (int j = 0; j < btnCnt; j++) {
                        btnArr.get(j).setVisibility(View.VISIBLE);
                        String Text = obj.getString("ANS_TITLE" + String.valueOf(j + 1)).replace(" ", "");
                        if (Text.length() == 5) {
                            Text = Text.substring(0, 2) + "\n" + Text.substring(2);
                        } else if (Text.length() == 6) {
                            Text = Text.substring(0, 3) + "\n" + Text.substring(3);
                        }
                        btnArr.get(j).setText(Text);
                        //btnArr.get(j).setText(obj.getString("ANS_TITLE" + String.valueOf(j + 1)));


                        curPosition = i;

                        pageNumTxt.setText(curPosition + 1 + "/" + jDataArr.length());

                        if (curPosition == 0) {
                            prevBtn.setVisibility(View.GONE);
                        } else if (curPosition == jDataArr.length() - 1) {
                            nextBtn.setVisibility(View.GONE);
                            //    reqResultBtn.setVisibility(View.VISIBLE);
                        } else {
                            prevBtn.setVisibility(View.VISIBLE);
                            reqResultBtn.setVisibility(View.GONE);
                        }

                        if (selArray.size() != 0) {
                            String selStr = selArray.get(i);
                            if (selStr.compareTo("-1") != 0) {

                                releaseSelBtnSelected();
                                if(selStr.compareTo(obj.getString("ANS_SCR1")) == 0){
                                    selOneBtn.setSelected(true);
                                }else if(selStr.compareTo(obj.getString("ANS_SCR2")) == 0){
                                    selTwoBtn.setSelected(true);
                                }else if(selStr.compareTo(obj.getString("ANS_SCR3")) == 0){
                                    selThreeBtn.setSelected(true);
                                }else if(selStr.compareTo(obj.getString("ANS_SCR4")) == 0){
                                    selFourBtn.setSelected(true);
                                }else if(selStr.compareTo(obj.getString("ANS_SCR5")) == 0){
                                    selFiveBtn.setSelected(true);
                                }

                            } else {
                                releaseSelBtnSelected();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    public void releaseSelBtnSelected() {
        selOneBtn.setSelected(false);
        selTwoBtn.setSelected(false);
        selThreeBtn.setSelected(false);
        selFourBtn.setSelected(false);
        selFiveBtn.setSelected(false);


    }

    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


        GLog.i("onResume", "dd");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GLog.i("onStop", "dd");

    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            String str = "";
            Fragment fragment = null;

            switch (v.getId()) {
                case R.id.left_btn:
                    onBackPressed();
                    break;
                case R.id.photo_img:
                    intent = new Intent(PsyCheckStartActivity.this, BabyInfoActivity.class);
                    startActivityForResult(intent, CommonData.REQUEST_CHILD_MANAGE);
                    Util.BackAnimationStart(PsyCheckStartActivity.this);
                    break;
                case R.id.prevBtn:
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    break;
                case R.id.nextBtn:
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    break;

                case R.id.selOneBtn:
                    releaseSelBtnSelected();


                    selOneBtn.setSelected(true);
                    onSelectedBtnEvent("ANS_SCR1");
                    break;
                case R.id.selTwoBtn:
                    releaseSelBtnSelected();
                    selTwoBtn.setSelected(true);
                    onSelectedBtnEvent("ANS_SCR2");
                    break;
                case R.id.selThreeBtn:
                    releaseSelBtnSelected();
                    selThreeBtn.setSelected(true);
                    onSelectedBtnEvent("ANS_SCR3");
                    break;
                case R.id.selFourBtn:
                    releaseSelBtnSelected();
                    selFourBtn.setSelected(true);
                    onSelectedBtnEvent("ANS_SCR4");
                    break;
                case R.id.selFiveBtn:
                    releaseSelBtnSelected();
                    selFiveBtn.setSelected(true);
                    onSelectedBtnEvent("ANS_SCR5");
                    break;

                case R.id.reqResultBtn:
                    requestResult();
                    break;
            }

            if (!str.equals("")) {
                mTitleTv.setText(str);
            }
        }
    };

    private void onSelectedBtnEvent(String selValue) {

        try {

            JSONObject obj = jDataArr.getJSONObject(curPosition);
            String score = obj.getString(selValue);


            selArray.set(curPosition, score);

            Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckData", selArray.toString());
            Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckDataCode", mCode);
            Util.setSharedPreference(PsyCheckStartActivity.this, "psyCheckDataTitle", mTitle);
            if (curPosition == jDataArr.length() - 1) {
                reqResultBtn.setVisibility(View.VISIBLE);
                return;
            }
            reqResultBtn.setVisibility(View.GONE);
            curPosition++;
            pageCnt = curPosition;
            viewPagerAdapter.notifyDataSetChanged();

            viewPager.setCurrentItem(curPosition);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment f = new PsyCheckViewPageFragment();
            try {

                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                JSONObject obj = obj = jDataArr.getJSONObject(position);
                bundle.putString("title", obj.getString("QUE_NUM") + ". " + obj.getString("QUE_TITLE"));
                f.setArguments(bundle);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return f;
        }

        @Override
        public int getCount() {

            if (pageCnt == jDataArr.length()) {
                return pageCnt;
            }

            return pageCnt + 1;
        }
    }


    @Override
    public void onBackPressed() {
        if (mOnKeyBackPressedListener != null) {
            mOnKeyBackPressedListener.onBack();
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        Util.BackAnimationEnd(PsyCheckStartActivity.this);
    }

    public interface onKeyBackPressedListener {
        void onBack();
    }

    private PsyMainActivity.onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(PsyMainActivity.onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }

    /**
     * Fragment 변경
     *
     * @param fragment 변경할 fragment
     */
    public void switchContent(Fragment fragment) {
        mContentFragment = fragment;

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
        } else {
            GLog.e("Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        if (CommonData.getInstance().getMemberId() == 0) {
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.i("requestCode = " + requestCode, "dd");
        GLog.i("resultCode = " + resultCode, "dd");
        GLog.i("data = " + data, "dd");

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CommonData.REQUEST_CHILD_MANAGE:   // 자녀관리
                GLog.i("REQUEST_CHILD_MANAGE", "dd");
                mFrameContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 자녀 데이터가 있는경우 UI 세팅
                        if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {

                            CustomImageLoader.displayImage(PsyCheckStartActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);

                            mRightLayout.setVisibility(View.VISIBLE);
                        } else {
                            mRightLayout.setVisibility(View.GONE);
                        }

                        setResult(RESULT_OK);
                        switchContent(new PsyMainFragment());

                    }
                }, CommonData.ANI_DELAY_500);
                break;

        }

        if (mContentFragment != null) {
            GLog.i("mContentFragment != null", "dd");
            mContentFragment.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }


}