package com.appmd.hi.gngcare.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by jihoon on 2016-03-30
 * 하단 팝업 클래스
 * @since 0, 1
 */
public class WheelPopupActivity extends BaseActivity implements View.OnClickListener {

    // 상단 레이아웃
//    private LinearLayout mMoveLayout;
//    private ImageButton mLeftBtn, mRightBtn;
    private Button mConfirmBtn;
    private TextView mTitleTv;


    // 생년월일, 키, 몸무게 레이아웃
    private LinearLayout mBirthLayout, mHeightLayout, mWeightLayout , mHeadLayout;
    private WheelView mYearWheel, mMonthWheel, mDayWheel, mHeightWheel_1, mHeightWheel_2, mWeightWheel_1, mWeightWheel_2, mHeadWheel_1, mHeadWheel_2 ;

    // 활동량 수동측정 시간 레이아웃
    private LinearLayout mActiveTimeLayout;
    private WheelView mTimeWheel_1, mTimeWheel_2;

    // 음식사진 등록시간 레이아웃
    private LinearLayout mFoodUpdateTimeLayout;
    private WheelView mFoodUpdateTimeWheel_1, mFoodUpdateTimeWheel_2, mFoodUpdateTimeWheel_3;

    // 걸음 목표 레이아웃
    private LinearLayout mStepGoalLayout;
    private WheelView mStepGoalWheel_1, mStepGoalWheel_2;

    // intent 데이터
    private Intent intent = null;
    private String mCurrentBirth;
    private float mCurrentHeight, mCurrentWeight , mCurrentHead;
    private String mHeightType , mWeightType , mHeadType;
    private int mCurrentHour, mCurrentMinute, mCurrentAmPm;
    private int mCurrentStep;
    private int mType = 0;
    private boolean mIsVisibleBirth, mIsVIsibleHeight, mIsVisibleWeight = false;

    Calendar calendar = Calendar.getInstance();

    // 걸음목표 res
    String[] mStepGoalArr_1000, mStepGoalArr_100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wheel_popup_activity);

        //mStepGoalArr_1000   =   getResources().getStringArray(R.array.step_goal_1000);
        //mStepGoalArr_100   =   getResources().getStringArray(R.array.step_goal_100);

        findViewById(R.id.click_layout).setOnClickListener(this);
        findViewById(R.id.wheel_main_layout).setOnClickListener(this);

        initBirth();
        initHeight();
        initWeight();
        initHead();

        init();
    }

    /**
     * 초기화
     */
    public void init(){
        intent = getIntent();

        int year,month, day;
        int height_1, height_2;
        int weight_1, weight_2;
        int head_1, head_2;
        int step_1, step_2;

        if(intent != null){
            mType = intent.getIntExtra(CommonData.EXTRA_WHEEL_TYPE, 0);

            mTitleTv    =   (TextView)      findViewById(R.id.title_tv);
            mConfirmBtn =   (Button)        findViewById(R.id.confirm_btn);
            mConfirmBtn.setOnClickListener(this);

            switch( mType ){
                case CommonData.WHEEL_TYPE_BIRTHDAY:    // 생년월일
                case CommonData.WHEEL_TYPE_HEIGHT:  // 키
                case CommonData.WHEEL_TYPE_WEIGHT:  // 몸무게
                    mCurrentBirth = intent.getStringExtra(CommonData.EXTRA_BIRTHDAY);
                    year = CommonData.BIRTHDAY_YEAR_START;
                    month = CommonData.BIRTHDAY_MONTH_START;
                    day = CommonData.BIRTHDAY_DAY_START;
                    if(!mCurrentBirth.equals("")){
                        Date mDate = Util.getDateFormat(mCurrentBirth, CommonData.PATTERN_DATE);
                        year = mDate.getYear() + 1900;
                        month = mDate.getMonth() + 1;
                        day = mDate.getDate();
                    }
//                    initBirth();
                    setBirthWheel(year, month, day);

                    mCurrentHeight = intent.getFloatExtra(CommonData.EXTRA_HEIGHT, 0f);
                    height_1 = CommonData.HEIGHT_START;
                    height_2 = 0;

                    if(mCurrentHeight > 0){
                        String heightStr = String.valueOf(mCurrentHeight);
                        StringTokenizer heightToken = new StringTokenizer(heightStr, ".");

                        height_1 = Integer.parseInt(heightToken.nextToken());
                        height_2 = Integer.parseInt(heightToken.nextToken());

                    }
//                    initHeight();
                    setHeightWheel(height_1, height_2);

                    mCurrentWeight = intent.getFloatExtra(CommonData.EXTRA_WEIGHT, 0f);
                    weight_1 = CommonData.WEIGHT_START;
                    weight_2 = 0;

                    if(mCurrentWeight > 0){
                        String weightStr = String.valueOf(mCurrentWeight);
                        StringTokenizer weightToken = new StringTokenizer(weightStr, ".");

                        weight_1 = Integer.parseInt(weightToken.nextToken());
                        weight_2 = Integer.parseInt(weightToken.nextToken());

                    }
//                    initWeight();
                    setWeightWheel(weight_1, weight_2);

                    // intro 에서만 활성화
//                    mMoveLayout =   (LinearLayout)  findViewById(R.id.move_layout);
//                    mLeftBtn    =   (ImageButton)   findViewById(R.id.left_btn);
//                    mRightBtn   =   (ImageButton)   findViewById(R.id.right_btn);
//
//                    mMoveLayout.setVisibility(View.VISIBLE);
//
//                    mLeftBtn.setOnClickListener(this);
//                    mRightBtn.setOnClickListener(this);

                    switch( mType ) {
                        case CommonData.WHEEL_TYPE_BIRTHDAY:    // 생년월일
                            mIsVisibleBirth = true;
                            mTitleTv.setText(getString(R.string.birthday));
                            mBirthLayout.setVisibility(View.VISIBLE);
                            break;
                        case CommonData.WHEEL_TYPE_HEIGHT:  // 키
                            mIsVIsibleHeight = true;
                            mTitleTv.setText(getString(R.string.height));
                            mHeightLayout.setVisibility(View.VISIBLE);
                            break;
                        case CommonData.WHEEL_TYPE_WEIGHT:  // 몸무게
                            mIsVisibleWeight = true;
                            mTitleTv.setText(getString(R.string.weight));
                            mWeightLayout.setVisibility(View.VISIBLE);
                            break;
                    }

                    break;
                case CommonData.WHEEL_TYPE_MYPAGE_BIRTHDAY: // 마이페이지에서 생년월일 수정
                    mCurrentBirth = intent.getStringExtra(CommonData.EXTRA_BIRTHDAY);
                    year = CommonData.BIRTHDAY_YEAR_START;
                    month = CommonData.BIRTHDAY_MONTH_START;
                    day = CommonData.BIRTHDAY_DAY_START;
                    if(!mCurrentBirth.equals("")){
//                        Date mDate = BleUtil.getDateFormat(mCurrentBirth, CommonData.PATTERN_DATE);
                        Date mDate = Util.getDateFormat(mCurrentBirth, CommonData.PATTERN_YYYYMMDD);
                        year = mDate.getYear() + 1900;
                        month = mDate.getMonth() + 1;
                        day = mDate.getDate();
                    }
                    setBirthWheel(year, month, day);
                    mBirthLayout.setVisibility(View.VISIBLE);

                    break;
                case CommonData.WHEEL_TYPE_MYPAGE_HEIGHT:   // 마이페이지에서 키 수정
                    mCurrentHeight = intent.getFloatExtra(CommonData.EXTRA_HEIGHT, 0f);
                    mHeightType = intent.getStringExtra(CommonData.EXTRA_HEIGHTTYPE);

                    height_1 = CommonData.HEIGHT_START;
                    height_2 = 0;

                    if(mCurrentHeight > 0){
                        String heightStr = String.valueOf(mCurrentHeight);
                        StringTokenizer heightToken = new StringTokenizer(heightStr, ".");

                        height_1 = Integer.parseInt(heightToken.nextToken());
                        height_2 = Integer.parseInt(heightToken.nextToken());

                    }
//                    initHeight();
                    setHeightWheel(height_1, height_2);
                    mHeightLayout.setVisibility(View.VISIBLE);

                    break;
                case CommonData.WHEEL_TYPE_MYPAGE_WEIGHT:   // 마이페이지에서 몸무게 수정
                    mCurrentWeight = intent.getFloatExtra(CommonData.EXTRA_WEIGHT, 0f);
                    mWeightType = intent.getStringExtra(CommonData.EXTRA_WEIGHTTYPE);

                    weight_1 = CommonData.WEIGHT_START;
                    weight_2 = 0;

                    if(mCurrentWeight > 0){
                        String weightStr = String.valueOf(mCurrentWeight);
                        StringTokenizer weightToken = new StringTokenizer(weightStr, ".");

                        weight_1 = Integer.parseInt(weightToken.nextToken());
                        weight_2 = Integer.parseInt(weightToken.nextToken());

                    }
//                    initHeight();
                    setWeightWheel(weight_1, weight_2);
                    mWeightLayout.setVisibility(View.VISIBLE);

                    break;
                case CommonData.WHEEL_TYPE_MYPAGE_HEAD:   // 마이페이지에서 머리둘레 수정
                    mCurrentHead = intent.getFloatExtra(CommonData.EXTRA_HEAD, 0f);
                    mHeadType = intent.getStringExtra(CommonData.EXTRA_HEADTYPE);

                    head_1 = CommonData.HEAD_START;
                    head_2 = 0;

                    if(mCurrentHead > 0){
                        String headStr = String.valueOf(mCurrentHead);
                        StringTokenizer headToken = new StringTokenizer(headStr, ".");

                        head_1 = Integer.parseInt(headToken.nextToken());
                        head_2 = Integer.parseInt(headToken.nextToken());

                    }
//                    initHeight();
                    setHeadWheel(head_1, head_2);
                    mHeadLayout.setVisibility(View.VISIBLE);

                    break;

                case CommonData.WHEEL_TYPE_WEIGHT_WEIGHT:   // 웨이트 몸무게 저장
                    mCurrentWeight = intent.getFloatExtra(CommonData.EXTRA_WEIGHT, 0f);
                    weight_1 = CommonData.WEIGHT_START;
                    weight_2 = 0;

                    if(mCurrentWeight > 0){
                        String weightStr = String.valueOf(mCurrentWeight);
                        StringTokenizer weightToken = new StringTokenizer(weightStr, ".");

                        weight_1 = Integer.parseInt(weightToken.nextToken());
                        weight_2 = Integer.parseInt(weightToken.nextToken());

                    }
//                    initHeight();
                    setWeightWheel(weight_1, weight_2);
                    mWeightLayout.setVisibility(View.VISIBLE);

                    break;
            }
        }
    }

    /**
     * 생년월일 wheel 설정
     */
    public void initBirth(){
        mBirthLayout    =   (LinearLayout)    findViewById(R.id.birth_layout);
//        mBirthBtn       =   (Button)            findViewById(R.id.birth_confirm_btn);
        mYearWheel      =   (WheelView)     findViewById(R.id.wheel_year);
        mMonthWheel     =   (WheelView)     findViewById(R.id.wheel_month);
        mDayWheel       =   (WheelView)     findViewById(R.id.wheel_day);

        mYearWheel.setCyclic(true);
        mMonthWheel.setCyclic(true);
        mDayWheel.setCyclic(true);

//        mBirthLayout.setVisibility(View.VISIBLE);

//        mBirthBtn.setOnClickListener(this);
    }

    /**
     * 키 wheel 설정
     */
    public void initHeight(){
        mHeightLayout    =   (LinearLayout)    findViewById(R.id.height_layout);
//        mHeightBtn       =   (Button)            findViewById(R.id.height_confirm_btn);
        mHeightWheel_1  =   (WheelView)     findViewById(R.id.wheel_height_1);
        mHeightWheel_2     =   (WheelView)     findViewById(R.id.wheel_height_2);

        mHeightWheel_1.setCyclic(true);
        mHeightWheel_2.setCyclic(true);

//        mHeightLayout.setVisibility(View.VISIBLE);

//        mHeightBtn.setOnClickListener(this);
    }

    /**
     * 몸무게 wheel 설정
     */
    public void initWeight(){
        mWeightLayout    =   (LinearLayout)    findViewById(R.id.weight_layout);
//        mWeightBtn       =   (Button)            findViewById(R.id.weight_confirm_btn);
        mWeightWheel_1      =   (WheelView)     findViewById(R.id.wheel_weight_1);
        mWeightWheel_2     =   (WheelView)     findViewById(R.id.wheel_weight_2);

        mWeightWheel_1.setCyclic(true);
        mWeightWheel_2.setCyclic(true);

//        mWeightLayout.setVisibility(View.VISIBLE);

//        mWeightBtn.setOnClickListener(this);
    }

    /**
     ** 머리둘레 wheel 설정
     */
    public void initHead(){
        mHeadLayout    =   (LinearLayout)    findViewById(R.id.head_layout);
//        mWeightBtn       =   (Button)            findViewById(R.id.weight_confirm_btn);
        mHeadWheel_1      =   (WheelView)     findViewById(R.id.wheel_head_1);
        mHeadWheel_2     =   (WheelView)     findViewById(R.id.wheel_head_2);

        mHeadWheel_1.setCyclic(true);
        mHeadWheel_2.setCyclic(true);

//        mWeightLayout.setVisibility(View.VISIBLE);

//        mWeightBtn.setOnClickListener(this);
    }


    /**
     * 활동량 시간 wheel 설정
     */
    public void initActiveTime(){
        mActiveTimeLayout   =   (LinearLayout)  findViewById(R.id.active_time_layout);

        mTimeWheel_1        =   (WheelView)     findViewById(R.id.wheel_active_time_1);
        mTimeWheel_2        =   (WheelView)     findViewById(R.id.wheel_active_time_2);

        mTimeWheel_1.setCyclic(true);
        mTimeWheel_2.setCyclic(true);

        mActiveTimeLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 음식사진 업데이트 wheel 설정
     */
    public void initFoodUpdateTime(){
        mFoodUpdateTimeLayout   =   (LinearLayout)  findViewById(R.id.food_update_time_layout);

        mFoodUpdateTimeWheel_1  =   (WheelView)     findViewById(R.id.wheel_food_update_time_1);
        mFoodUpdateTimeWheel_2  =   (WheelView)     findViewById(R.id.wheel_food_update_time_2);
        mFoodUpdateTimeWheel_3  =   (WheelView)     findViewById(R.id.wheel_food_update_time_3);

//        mFoodUpdateTimeWheel_1.setCyclic(true);
        mFoodUpdateTimeWheel_2.setCyclic(true);
        mFoodUpdateTimeWheel_3.setCyclic(true);

        mFoodUpdateTimeLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 걸음 목표 wheel 설정
     */
    public void initStepGoal(){
        mStepGoalLayout     =   (LinearLayout)  findViewById(R.id.step_goal_layout);

        mStepGoalWheel_1    =   (WheelView)     findViewById(R.id.wheel_step_goal_1);
        mStepGoalWheel_2    =   (WheelView)     findViewById(R.id.wheel_step_goal_2);

        mStepGoalWheel_1.setCyclic(true);
        mStepGoalWheel_2.setCyclic(true);
    }

    /**
     * 생년월일 Wheel 설정
     * @param year  년도
     * @param month 월
     * @param day   일
     */
    public void setBirthWheel(int year, int month, int day){
        int curYear = calendar.get(Calendar.YEAR);
        NumericWheelAdapter yearAdapter = new NumericWheelAdapter(this, CommonData.BIRTHDAY_YEAR_START, curYear);
        yearAdapter.setItemResource(R.layout.wheel_text_item);
        yearAdapter.setItemTextResource(R.id.text);
        mYearWheel.setViewAdapter(yearAdapter);


        NumericWheelAdapter monthAdapter = new NumericWheelAdapter(this, CommonData.BIRTHDAY_MONTH_START, CommonData.BIRTHDAY_MONTH_END, CommonData.TWO_NUMBER_FORMAT);
        monthAdapter.setItemResource(R.layout.wheel_text_item);
        monthAdapter.setItemTextResource(R.id.text);
        mMonthWheel.setViewAdapter(monthAdapter);


        NumericWheelAdapter dayAdapter = new NumericWheelAdapter(this, CommonData.BIRTHDAY_DAY_START, CommonData.BIRTHDAY_DAY_END, CommonData.TWO_NUMBER_FORMAT);
        dayAdapter.setItemResource(R.layout.wheel_text_item);
        dayAdapter.setItemTextResource(R.id.text);
        mDayWheel.setViewAdapter(dayAdapter);

        GLog.i("year = " + year, "dd");
        int indexYear = year - CommonData.BIRTHDAY_YEAR_START;
        GLog.i("indexYear = " + indexYear, "dd");

        mYearWheel.setCurrentItem(indexYear);
        mMonthWheel.setCurrentItem(month-1);
        mDayWheel.setCurrentItem(day-1);

    }

    /**
     * 키 설정
     * @param height_1  키
     * @param height_2  소수점
     */
    public void setHeightWheel(int height_1, int height_2){

        NumericWheelAdapter heightAdapter_1 = new NumericWheelAdapter(this, CommonData.HEIGHT_START, CommonData.HEIGHT_END);
        heightAdapter_1.setItemResource(R.layout.wheel_text_item);
        heightAdapter_1.setItemTextResource(R.id.text);
        mHeightWheel_1.setViewAdapter(heightAdapter_1);

        NumericWheelAdapter heightAdapter_2 = new NumericWheelAdapter(this, 0, 9);
        heightAdapter_2.setItemResource(R.layout.wheel_text_item);
        heightAdapter_2.setItemTextResource(R.id.text);
        mHeightWheel_2.setViewAdapter(heightAdapter_2);

        GLog.i("height_1 = " + height_1, "dd");
        int indexHeight = height_1 - CommonData.HEIGHT_START;
        GLog.i("indexHeight = " + indexHeight, "dd");

        mHeightWheel_1.setCurrentItem(indexHeight);
        mHeightWheel_2.setCurrentItem(height_2);

    }

    /**
     * 몸무게 설정
     * @param weight_1  키
     * @param weight_2  소수점
     */
    public void setWeightWheel(int weight_1, int weight_2){

        NumericWheelAdapter weightAdapter_1 = new NumericWheelAdapter(this, CommonData.WEIGHT_START, CommonData.WEIGHT_END);
        weightAdapter_1.setItemResource(R.layout.wheel_text_item);
        weightAdapter_1.setItemTextResource(R.id.text);
        mWeightWheel_1.setViewAdapter(weightAdapter_1);

        NumericWheelAdapter weightAdapter_2 = new NumericWheelAdapter(this, 0, 9);
        weightAdapter_2.setItemResource(R.layout.wheel_text_item);
        weightAdapter_2.setItemTextResource(R.id.text);
        mWeightWheel_2.setViewAdapter(weightAdapter_2);

        GLog.i("weight_1 = " + weight_1, "dd");
        int indexWeight = weight_1 - CommonData.WEIGHT_START;
        GLog.i("indexHeight = " + indexWeight, "dd");

        mWeightWheel_1.setCurrentItem(indexWeight);
        mWeightWheel_2.setCurrentItem(weight_2);

    }

    /**
     * 머리둘레 설정
     * @param head_1  머리둘레
     * @param head_2  소수점
     */
    public void setHeadWheel(int head_1, int head_2){

        NumericWheelAdapter headAdapter_1 = new NumericWheelAdapter(this, CommonData.HEAD_START, CommonData.HEAD_END);
        headAdapter_1.setItemResource(R.layout.wheel_text_item);
        headAdapter_1.setItemTextResource(R.id.text);
        mHeadWheel_1.setViewAdapter(headAdapter_1);

        NumericWheelAdapter headAdapter_2 = new NumericWheelAdapter(this, 0, 9);
        headAdapter_2.setItemResource(R.layout.wheel_text_item);
        headAdapter_2.setItemTextResource(R.id.text);
        mHeadWheel_2.setViewAdapter(headAdapter_2);

        GLog.i("head_1 = " + head_1, "dd");
        int indexHead = head_1 - CommonData.HEAD_START;
        GLog.i("indexHead = " + indexHead, "dd");

        mHeadWheel_1.setCurrentItem(indexHead);
        mHeadWheel_2.setCurrentItem(head_2);

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
//            case R.id.birth_confirm_btn:    // 생년월일 완료
//                Toast.makeText(WheelPopupActivity.this, "birth_confirm_btn", Toast.LENGTH_SHORT).show();
//                int year = mYearWheel.getCurrentItem()+CommonData.BIRTHDAY_YEAR_START;
//                int month = mMonthWheel.getCurrentItem()+1;
//                int day = mDayWheel.getCurrentItem()+1;
//                mCurrentBirth = year+"-"+month+"-"+day;
//                intent.putExtra(CommonData.EXTRA_BIRTHDAY, mCurrentBirth);
//                break;
//            case R.id.height_confirm_btn:   // 키 완료
//                Toast.makeText(WheelPopupActivity.this, "height_confirm_btn", Toast.LENGTH_SHORT).show();
//                int height_1 = mHeightWheel_1.getCurrentItem()+CommonData.HEIGHT_START;
//                int height_2 = mHeightWheel_2.getCurrentItem();
//
//                String heightStr = height_1+ "." +height_2;
//                mCurrentHeight = Float.parseFloat(heightStr);
//                intent.putExtra(CommonData.EXTRA_HEIGHT, mCurrentHeight);
//
//                break;
//            case R.id.weight_confirm_btn:   // 몸무게 완료
//                Toast.makeText(WheelPopupActivity.this, "weight_confirm_btn", Toast.LENGTH_SHORT).show();
//                int weight_1 = mWeightWheel_1.getCurrentItem()+CommonData.WEIGHT_START;
//                int weight_2 = mWeightWheel_2.getCurrentItem();
//
//                String weightStr = weight_1+ "." +weight_2;
//                mCurrentWeight = Float.parseFloat(weightStr);
//                intent.putExtra(CommonData.EXTRA_WEIGHT, mCurrentWeight);
//                break;

            case R.id.click_layout: // 클릭 레이아웃
                GLog.i("click_layout", "dd");
                finish();
                break;
            case R.id.wheel_main_layout:    // wheel 메인
                GLog.i("wheel_main_layout", "dd");
                break;
            /* 주석
            case R.id.left_btn: // 왼쪽으로 넘기기
                switch(mType){
                    case CommonData.WHEEL_TYPE_BIRTHDAY:
                        GLog.i("birthday");
                        mType = CommonData.WHEEL_TYPE_BIRTHDAY;
                        mTitleTv.setText(getString(R.string.birthday));
                        mIsVisibleBirth = true;
                        break;
                    case CommonData.WHEEL_TYPE_HEIGHT:
                        mHeightLayout.setVisibility(View.GONE);
                        mBirthLayout.setVisibility(View.VISIBLE);
                        mType = CommonData.WHEEL_TYPE_BIRTHDAY;
                        mTitleTv.setText(getString(R.string.birthday));
                        mIsVisibleBirth = true;
                        break;
                    case CommonData.WHEEL_TYPE_WEIGHT:
                        mWeightLayout.setVisibility(View.GONE);
                        mHeightLayout.setVisibility(View.VISIBLE);
                        mType = CommonData.WHEEL_TYPE_HEIGHT;
                        mTitleTv.setText(getString(R.string.height));
                        mIsVIsibleHeight = true;
                        break;
                }
                break;
            case R.id.right_btn:    // 오른쪽으로 넘기기
                switch(mType){
                    case CommonData.WHEEL_TYPE_BIRTHDAY:
                        mBirthLayout.setVisibility(View.GONE);
                        mHeightLayout.setVisibility(View.VISIBLE);
                        mType = CommonData.WHEEL_TYPE_HEIGHT;
                        mTitleTv.setText(getString(R.string.height));
                        mIsVIsibleHeight = true;
                        break;
                    case CommonData.WHEEL_TYPE_HEIGHT:
                        mHeightLayout.setVisibility(View.GONE);
                        mWeightLayout.setVisibility(View.VISIBLE);
                        mType = CommonData.WHEEL_TYPE_WEIGHT;
                        mTitleTv.setText(getString(R.string.weight));
                        mIsVisibleWeight = true;
                        break;
                    case CommonData.WHEEL_TYPE_WEIGHT:
                        GLog.i("weight");
                        mType = CommonData.WHEEL_TYPE_WEIGHT;
                        mTitleTv.setText(getString(R.string.weight));
                        mIsVisibleWeight = true;
                        break;
                }
                break;
                */
            case R.id.confirm_btn:  // 저장하기

                switch (mType){
                    // Intro 에서 생년월일, 키 , 몸무게 수정
                    case CommonData.WHEEL_TYPE_BIRTHDAY:
                    case CommonData.WHEEL_TYPE_HEIGHT:
                    case CommonData.WHEEL_TYPE_WEIGHT:
                        if(mIsVisibleBirth){    // 생년월일을 봤다면
                            int year = mYearWheel.getCurrentItem()+CommonData.BIRTHDAY_YEAR_START;
                            int month = mMonthWheel.getCurrentItem()+1;
                            int day = mDayWheel.getCurrentItem()+1;
                            mCurrentBirth = year+"-"+month+"-"+day;
                            GLog.i("mCurrentBirth = " +mCurrentBirth, "dd");
                            intent.putExtra(CommonData.EXTRA_BIRTHDAY, mCurrentBirth);
                        }

                        if(mIsVIsibleHeight){   // 키를 봤다면
                            int height_1 = mHeightWheel_1.getCurrentItem()+CommonData.HEIGHT_START;
                            int height_2 = mHeightWheel_2.getCurrentItem();

                            String heightStr = height_1+ "." +height_2;
                            mCurrentHeight = Float.parseFloat(heightStr);
                            intent.putExtra(CommonData.EXTRA_HEIGHT, mCurrentHeight);
                        }

                        if(mIsVisibleWeight){   // 몸무게를 봤다면
                            int weight_1 = mWeightWheel_1.getCurrentItem()+CommonData.WEIGHT_START;
                            int weight_2 = mWeightWheel_2.getCurrentItem();

                            String weightStr = weight_1+ "." +weight_2;
                            mCurrentWeight = Float.parseFloat(weightStr);
                            intent.putExtra(CommonData.EXTRA_WEIGHT, mCurrentWeight);
                        }

                        break;

                    case CommonData.WHEEL_TYPE_MYPAGE_BIRTHDAY: // 마이페이지에서 생년월일 수정
                        int year = mYearWheel.getCurrentItem()+CommonData.BIRTHDAY_YEAR_START;
                        int month = mMonthWheel.getCurrentItem()+1;
                        int day = mDayWheel.getCurrentItem()+1;
//                        mCurrentBirth = year+"-"+month+"-"+day;
                        mCurrentBirth = ""+year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
                        GLog.i("mCurrentBirth = " +mCurrentBirth, "dd");

                        Calendar currentCal = Calendar.getInstance();

                        int diffday = Util.GetDifferenceOfDate(currentCal.get(Calendar.YEAR), (currentCal.get(Calendar.MONTH) + 1), currentCal.get(Calendar.DATE),
                                year, month, day);
                        GLog.i("diffday = " + diffday, "dd");

                        if(diffday < 0) {  // 미래라면
                            Toast.makeText(WheelPopupActivity.this, getString(R.string.toast_date_error), Toast.LENGTH_SHORT).show();
                        }else{
                            intent.putExtra(CommonData.EXTRA_BIRTHDAY, mCurrentBirth);
                        }
                        break;
                    case CommonData.WHEEL_TYPE_MYPAGE_HEIGHT:   // 마이페이지에서 키 수정
                        int height_1 = mHeightWheel_1.getCurrentItem()+CommonData.HEIGHT_START;
                        int height_2 = mHeightWheel_2.getCurrentItem();

                        String heightStr = height_1+ "." +height_2;
                        mCurrentHeight = Float.parseFloat(heightStr);
                        intent.putExtra(CommonData.EXTRA_HEIGHT, mCurrentHeight);

                        if (mHeightType.equals(CommonData.WHEEL_TYPE_BIRTHHEIGHT)){
                            intent.putExtra(CommonData.EXTRA_HEIGHTTYPE, CommonData.WHEEL_TYPE_BIRTHHEIGHT);
                        }else if (mHeightType.equals(CommonData.WHEEL_TYPE_CHECKHEIGHT)){
                            intent.putExtra(CommonData.EXTRA_HEIGHTTYPE, CommonData.WHEEL_TYPE_CHECKHEIGHT);
                        }else if (mHeightType.equals(CommonData.WHEEL_TYPE_MYHEIGHT)) {
                            intent.putExtra(CommonData.EXTRA_HEIGHTTYPE, CommonData.WHEEL_TYPE_MYHEIGHT);
                        }else if (mHeightType.equals(CommonData.WHEEL_TYPE_MOHEIGHT)) {
                            intent.putExtra(CommonData.EXTRA_HEIGHTTYPE, CommonData.WHEEL_TYPE_MOHEIGHT);
                        }else{
                            intent.putExtra(CommonData.EXTRA_HEIGHTTYPE, CommonData.WHEEL_TYPE_FAHEIGHT);
                        }
                        
                        break;
                    case CommonData.WHEEL_TYPE_MYPAGE_WEIGHT:   // 마이페이지에서 몸무게 수정
                        int wweight_1 = mWeightWheel_1.getCurrentItem()+CommonData.WEIGHT_START;
                        int wweight_2 = mWeightWheel_2.getCurrentItem();

                        String wweightStr = wweight_1+ "." +wweight_2;
                        mCurrentWeight = Float.parseFloat(wweightStr);
                        intent.putExtra(CommonData.EXTRA_WEIGHT, mCurrentWeight);
                        if (mWeightType.equals(CommonData.WHEEL_TYPE_BIRTHWEIGHT)){
                            intent.putExtra(CommonData.EXTRA_WEIGHTTYPE, CommonData.WHEEL_TYPE_BIRTHWEIGHT);
                        }else if (mWeightType.equals(CommonData.WHEEL_TYPE_CHECKWEIGHT)){
                            intent.putExtra(CommonData.EXTRA_WEIGHTTYPE, CommonData.WHEEL_TYPE_CHECKWEIGHT);
                        }else if (mWeightType.equals(CommonData.WHEEL_TYPE_MYWEIGHT)){
                            intent.putExtra(CommonData.EXTRA_WEIGHTTYPE, CommonData.WHEEL_TYPE_MYWEIGHT);
                        }else if (mWeightType.equals(CommonData.WHEEL_TYPE_MOWEIGHT)){
                            intent.putExtra(CommonData.EXTRA_WEIGHTTYPE, CommonData.WHEEL_TYPE_MOWEIGHT);
                        }else if (mWeightType.equals(CommonData.WHEEL_TYPE_FAWEIGHT)){
                            intent.putExtra(CommonData.EXTRA_WEIGHTTYPE, CommonData.WHEEL_TYPE_FAWEIGHT);
                        }

                        break;
                    case CommonData.WHEEL_TYPE_MYPAGE_HEAD:   // 마이페이지에서 머리둘레 수정
                        int hhead_1 = mHeadWheel_1.getCurrentItem()+CommonData.HEAD_START;
                        int hhead_2 = mHeadWheel_2.getCurrentItem();

                        String hheadStr = hhead_1+ "." +hhead_2;
                        mCurrentHead = Float.parseFloat(hheadStr);
                        intent.putExtra(CommonData.EXTRA_HEAD, mCurrentHead);
                        if (mHeadType.equals(CommonData.WHEEL_TYPE_BIRTHWHEAD)){
                            intent.putExtra(CommonData.EXTRA_HEADTYPE, CommonData.WHEEL_TYPE_BIRTHWHEAD);
                        }else if (mHeadType.equals(CommonData.WHEEL_TYPE_CHECKHEAD)){
                            intent.putExtra(CommonData.EXTRA_HEADTYPE, CommonData.WHEEL_TYPE_CHECKHEAD);
                        }
                        break;
                    case CommonData.WHEEL_TYPE_ACTIVE_TIME: // 활동량 수동측정
                    case CommonData.WHEEL_TYPE_TIME:    //  스마트폰 사용시간

                        mCurrentHour    =   mTimeWheel_1.getCurrentItem();
                        mCurrentMinute  =   mTimeWheel_2.getCurrentItem();

                        intent.putExtra(CommonData.EXTRA_HOUR, mCurrentHour);
                        intent.putExtra(CommonData.EXTRA_MINUTE, mCurrentMinute);
                        break;
                    case CommonData.WHEEL_TYPE_UPDATE_FOOD: // 음식사진 등록

                        mCurrentAmPm    =   mFoodUpdateTimeWheel_1.getCurrentItem();
                        mCurrentHour    =   mFoodUpdateTimeWheel_2.getCurrentItem()+1;
                        mCurrentMinute  =   mFoodUpdateTimeWheel_3.getCurrentItem();

                        intent.putExtra(CommonData.EXTRA_AMPM, mCurrentAmPm);
                        intent.putExtra(CommonData.EXTRA_HOUR, mCurrentHour);
                        intent.putExtra(CommonData.EXTRA_MINUTE, mCurrentMinute);

                        break;
                    case CommonData.WHEEL_TYPE_WEIGHT_WEIGHT:   // 체중정보기록 화면
                        int weight_1 = mWeightWheel_1.getCurrentItem()+CommonData.WEIGHT_START;
                        int weight_2 = mWeightWheel_2.getCurrentItem();

                        String weightStr = weight_1+ "." +weight_2;
                        mCurrentWeight = Float.parseFloat(weightStr);
                        intent.putExtra(CommonData.EXTRA_WEIGHT, mCurrentWeight);
                        break;
                    case CommonData.WHEEL_TYPE_STEP_GOAL:   // 걸음 목표
                        int step_1  =   mStepGoalWheel_1.getCurrentItem();
//                        int step_2  =   mStepGoalWheel_2.getCurrentItem();

                        String imsiStep_1 = mStepGoalArr_1000[step_1];
//                        String imsiStep_2 = mStepGoalArr_100[step_2];

//                        mCurrentStep = Integer.parseInt((imsiStep_1 +imsiStep_2));
                        mCurrentStep = Integer.parseInt((imsiStep_1));
                        intent.putExtra(CommonData.EXTRA_STEP_COUNT, mCurrentStep);
                        break;
                }

                setResult(RESULT_OK, intent);
                finish();

                break;

        }

    }
}
