package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.appmd.hi.gngcare.R;
// import com.tsengvn.typekit.TypekitContextWrapper;


/**
 * Created by CP on 2018. 4. 7..
 */

public class EduMainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton mCommonLeftBtn;
    private LinearLayout videoBtn01;
    private LinearLayout videoBtn02;
    private LinearLayout videoBtn03;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu_main_sec);

        intent = new Intent(EduMainActivity.this, EduVideoActivity.class);
        init();
        initEvent();
    }


    protected void init(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        videoBtn01 = (LinearLayout)findViewById(R.id.videoBtn01);
        videoBtn02 = (LinearLayout)findViewById(R.id.videoBtn02);
        videoBtn03 = (LinearLayout)findViewById(R.id.videoBtn03);

    }

    public void initEvent(){
        videoBtn01.setOnClickListener(this);
        videoBtn02.setOnClickListener(this);
        videoBtn03.setOnClickListener(this);
        mCommonLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.videoBtn01){
            intent.putExtra("TITLE", getString(R.string.edu_btn_title_01));
            intent.putExtra("ML_MCODE", "2");
        }else if(v.getId() == R.id.videoBtn02){
            intent.putExtra("TITLE", getString(R.string.edu_video_title_02));
            intent.putExtra("ML_MCODE", "1");

        }else if(v.getId() == R.id.videoBtn03){
            intent.putExtra("TITLE", getString(R.string.edu_btn_title_03));
            intent.putExtra("ML_MCODE", "3");
        }
        startActivity(intent);

    }

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}

