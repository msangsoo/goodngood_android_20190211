package com.appmd.hi.gngcare.collection;

/**
 * Created by jihoon on 2016-03-22.
 * 자녀 리스트 아이템
 * @since 0, 1
 */
public class GrowthRecordMonthGraphItem {

    private String mYYMM;
    private String mInput_Height;
    private String mInput_Weight;
    private String mInput_Head;
    /**
     * 육아 다이어리 리스트
     * @param yymm   날짜
     * @param input_height   키
     * @param input_bdwgh   몸무게
     * @param input_headcm   머리둘레
     */
    public GrowthRecordMonthGraphItem(String yymm,
                                      String input_height,
                                      String input_bdwgh,
                                      String input_headcm
    ){
        this.mYYMM      =   yymm;
        this.mInput_Height  =   input_height;
        this.mInput_Weight  =   input_bdwgh;
        this.mInput_Head    =   input_headcm;
    }


    public String getmYYMM() {
        return mYYMM;
    }

    public void setmYYMM(String mYYMM) {
        this.mYYMM = mYYMM;
    }

    public String getmInput_Height() {
        return mInput_Height;
    }

    public void setmInput_Height(String mInput_Height) {
        this.mInput_Height = mInput_Height;
    }

    public String getmInput_Weight() {
        return mInput_Weight;
    }

    public void setmInput_Weight(String mInput_Weight) {
        this.mInput_Weight = mInput_Weight;
    }

    public String getmInput_Head() {
        return mInput_Head;
    }

    public void setmInput_Head(String mInput_Head) {
        this.mInput_Head = mInput_Head;
    }

}
