package com.appmd.hi.gngcare.motherhealth;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_asstb_diet_program_req;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_asstb_diet_program_schedule_list;

import java.util.ArrayList;
import java.util.List;

/**
 * 다이어트프로그램 신청하기
 */
public class RequestDietProgramActivity extends BackBaseActivity {
    private final String TAG = getClass().getSimpleName();

    private Intent mIntent;
    private View mActionbar;
    private MakeProgress mProgress = null;

    private TextView mDescTv1;
    private TextView mDescTv2;
    private TextView mDescTv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_diet_program);

        if (mProgress == null)
            mProgress = new MakeProgress(this);

        mIntent = getIntent();

        setTitle(getString(R.string.다이어트프로그램신청_title));
        mActionbar = (View) findViewById(R.id.common_back_top);

        mDescTv1 = findViewById(R.id.diet_program_desc_tv1);
        mDescTv2 = findViewById(R.id.diet_program_desc_tv2);
        mDescTv3 = findViewById(R.id.diet_program_desc_tv3);

        mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, "0", "0"));
        mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, "0"));
        mDescTv3.setText("0%");

//        replaceFragment(fragments[mFragmentNum], true, false, null);

        // 스케쥴보기
        findViewById(R.id.diet_schedule_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupProgramInfo();
            }
        });

        // 신청
        findViewById(R.id.confirm_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDietProgram();
            }
        });

        // 취소
        findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getDietProgramList();
    }

    /**
     * 다이어트 프로그램 신청
     */
    private void requestDietProgram() {
        Tr_asstb_diet_program_req.RequestData requestData = new Tr_asstb_diet_program_req.RequestData();

        CommonData login = CommonData.getInstance();
        requestData.mber_sn = login.getMberSn();

        new ApiData().getData(RequestDietProgramActivity.this, Tr_asstb_diet_program_req.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_asstb_diet_program_req) {
                    Tr_asstb_diet_program_req tr = (Tr_asstb_diet_program_req)obj;
                    if ("Y".equals(tr.data_yn)) {
                        // 신청완료
                        popupCompleteProgram();
                    } else {
                        // 이미신청중
                        popupYetProgram();
                    }
                } else {
                    CDialog.showDlg(RequestDietProgramActivity.this, "데이터 수신에 실패 하였습니다.");
                }
            }
        });
    }

    /**
     * 다이어트 프로그램 안내 팝업
     */
    private void popupProgramInfo() {
        // 임신전 안내
        mDialog = new CustomAlertDialog(RequestDietProgramActivity.this, CustomAlertDialog.TYPE_A);
        mDialog.setTitle(RequestDietProgramActivity.this.getString(R.string.popup_dialog_a_type_title));
        View view = LayoutInflater.from(RequestDietProgramActivity.this).inflate(R.layout.popup_request_diet_program_info, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;

        ListView listView = view.findViewById(R.id.diet_program_popup_listview);
        View hearder = LayoutInflater.from(RequestDietProgramActivity.this).inflate(R.layout.popup_request_diet_program_info_header, null, false);
        listView.addHeaderView(hearder);
        LinearLayoutManager layoutManager = new LinearLayoutManager(RequestDietProgramActivity.this);

        TextView scheduleTv = view.findViewById(R.id.diet_program_popup_schedule_day_tv);

        if (mDietProgramTr != null) {
            scheduleTv.setText(getString(R.string.popup_diet_popup_content_4, mDietProgramTr.sch_day == null ? "0" : mDietProgramTr.sch_day));
            ListAdapter adapter = new ListAdapter(mDietProgramTr);
//        ListAdapter adapter = new ListAdapter(new ArrayList<>());
            listView.setAdapter(adapter);
        }

        mDialog.setContentView(view, params);
        mDialog.setPositiveButton(RequestDietProgramActivity.this.getString(R.string.popup_dialog_button_confirm), null);
//        mDialog.setTitle(getString(R.string.view_schedule));
        mDialog.show();
    }


    /**
     * 다이어트 프로그램 이미 신청중 팝업
     */
    private void popupYetProgram() {
        // 임신전 안내
        mDialog = new CustomAlertDialog(RequestDietProgramActivity.this, CustomAlertDialog.TYPE_A);
        mDialog.setContentView(R.layout.popup_request_diet_program_complete);
//        mDialog.setContent(RequestDietProgramActivity.this.getString(R.string.popup_yet_diet_program));
        mDialog.setPositiveButton(RequestDietProgramActivity.this.getString(R.string.popup_dialog_button_confirm), null);
        mDialog.show();
    }

    private Tr_asstb_diet_program_schedule_list mDietProgramTr;
    private void getDietProgramList() {
        Tr_asstb_diet_program_schedule_list.RequestData requestData = new Tr_asstb_diet_program_schedule_list.RequestData();

        CommonData login = CommonData.getInstance();
        requestData.mber_sn = login.getMberSn();

        new ApiData().getData(RequestDietProgramActivity.this, Tr_asstb_diet_program_schedule_list.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_asstb_diet_program_schedule_list) {
                    Tr_asstb_diet_program_schedule_list tr = (Tr_asstb_diet_program_schedule_list) obj;
                    mDietProgramTr = tr;

                    mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, tr.sch_day,tr.sch_day));
                    mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, tr.sch_user));
                    mDescTv3.setText(tr.sch_per+"%");

                } else {
                    mDescTv1.setText(getString(R.string.popup_diet_popup_content_1, "-", "-"));
                    mDescTv2.setText(getString(R.string.popup_diet_popup_content_2, "-"));
                    mDescTv3.setText("-%");

                    CDialog.showDlg(RequestDietProgramActivity.this, "데이터 수신에 실패 하였습니다.");
                }
            }
        });
    }

    /**
     * 다이어트 프로그램 이미 신청중 팝업
     */
    private void popupCompleteProgram() {
        // 글자 일부 색상 변경 하기
//        Spannable word = new SpannableString("Your message");
//        word.setSpan(new ForegroundColorSpan(Color.BLUE), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        TV.setText(word);

        // 임신전 안내
        mDialog = new CustomAlertDialog(RequestDietProgramActivity.this, CustomAlertDialog.TYPE_A);
        mDialog.setTitle(RequestDietProgramActivity.this.getString(R.string.popup_dialog_a_type_title));
        mDialog.setContent(RequestDietProgramActivity.this.getString(R.string.popup_complete_diet_program));
        mDialog.setPositiveButton(RequestDietProgramActivity.this.getString(R.string.do_call), null);
        mDialog.show();
    }

    @Override
    public void showProgress() {
//        super.showProgress();
        if (mProgress != null)
            mProgress.show();
    }

    @Override
    public void hideProgress() {
//        super.hideProgress();
        if (mProgress != null)
            mProgress.dismiss();
    }

    class ListAdapter extends BaseAdapter {
        Tr_asstb_diet_program_schedule_list tr;
        List<Tr_asstb_diet_program_schedule_list.Schedule_list> list = new ArrayList<>();

        public ListAdapter(Tr_asstb_diet_program_schedule_list tr) {
            this.tr = tr;
            this.list = tr.schedule_list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if(convertView == null){
                convertView =   LayoutInflater.from(RequestDietProgramActivity.this).inflate(R.layout.popup_request_diet_program_info_adapter, null);

                holder = new ViewHolder();
                holder.titleTv = (TextView) convertView.findViewById(R.id.diet_program_adapter_title_textview);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            Tr_asstb_diet_program_schedule_list.Schedule_list item = list.get(position);
            holder.titleTv.setText(item.schedule_subject);

            return convertView;
        }

        class ViewHolder {
            TextView titleTv;// = (TextView) view.findViewById(R.id.diet_program_adapter_title_textview);
        }
    }
}
