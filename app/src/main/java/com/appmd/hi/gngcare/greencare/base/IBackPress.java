package com.appmd.hi.gngcare.greencare.base;

public interface IBackPress {
    void onBackPressed();
}
