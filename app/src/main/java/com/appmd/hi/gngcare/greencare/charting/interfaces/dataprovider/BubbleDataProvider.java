package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.data.BubbleData;

public interface BubbleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    BubbleData getBubbleData();
}
