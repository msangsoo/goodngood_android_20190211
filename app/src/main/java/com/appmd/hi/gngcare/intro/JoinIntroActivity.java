package com.appmd.hi.gngcare.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;

/**
 * Created by jihoon on 2016-03-30.
 * 회원가입 화면
 *
 * @since 0, 1
 */
public class JoinIntroActivity extends IntroBaseActivity implements View.OnClickListener {
    public final String TAG = JoinIntroActivity.class.getSimpleName();



    private ImageView mMeberBtn, mJunMemberBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_intro_activity);

        setTitle(getString(R.string.join));

        init();
        setEvent();

    }

    /**
     * 초기화
     */
    public void init() {

        mMeberBtn = (ImageView) findViewById(R.id.member_btn);
        mJunMemberBtn = (ImageView) findViewById(R.id.junmember_btn);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {
        mMeberBtn.setOnClickListener(this);
        mJunMemberBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch(v.getId()){
            case R.id.member_btn :
                intent = new Intent(JoinIntroActivity.this, MemberCertifiActivity.class);
                intent.putExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);
                startActivity(intent);
                break;
            case R.id.junmember_btn :
                intent = new Intent(JoinIntroActivity.this, JunMemberCertifi1Activity.class);
                startActivity(intent);
                break;
        }
    }
}
