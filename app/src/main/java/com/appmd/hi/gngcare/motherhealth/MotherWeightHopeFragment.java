package com.appmd.hi.gngcare.motherhealth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_asstb_weight_hope_grp;

/**
 * 체중예측
 * Created by insystemscompany on 2017. 2. 28..
 */

public class MotherWeightHopeFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = MotherWeightHopeFragment.class.getSimpleName();

    public static int REQ_WEIGHT_PREDICT = 454;
    public static String INTENT_KEY_WEEK = "intent_key_week";
    public static String INTENT_KEY_WEIGHT = "intent_key_weight";

    private android.widget.EditText mInputWeek;
    private android.widget.EditText mInputKg;
    private android.widget.ImageButton mSaveButton;
    private android.widget.LinearLayout activitymain;

    public static Fragment newInstance() {
        MotherWeightHopeFragment fragment = new MotherWeightHopeFragment();
        return fragment;
    }

    private void setActionBar() {
        // CommonActionBar actionBar 는 안 씀 한화꺼
        if (getActivity() instanceof DummyActivity) {
            DummyActivity activity = (DummyActivity) getActivity();

            TextView titleTv = (TextView) activity.findViewById(R.id.common_title_tv);
            titleTv.setText(getString(R.string.mother_health_wt_prediction));

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bigdata_input, container, false);
        this.mSaveButton = (ImageButton) view.findViewById(R.id.show_result_button);
        this.mInputKg = (EditText) view.findViewById(R.id.mom_weight_kg);
        this.mInputWeek = (EditText) view.findViewById(R.id.mom_weight_week_et);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionBar();
        mSaveButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_result_button:
//                doRegistWeight();
                String weight = mInputKg.getText().toString();
                String week = mInputWeek.getText().toString();

                Intent intent = new Intent();
                intent.putExtra(INTENT_KEY_WEIGHT, weight);
                intent.putExtra(INTENT_KEY_WEEK, week);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                break;
        }
    }

    /**
     * 빅데이터 40주 그래프
     */
    private void doWeightHopeGrp() {
        Tr_asstb_weight_hope_grp.RequestData requestData = new Tr_asstb_weight_hope_grp.RequestData();

        CommonData login = CommonData.getInstance();
        requestData.mber_sn = login.getMberSn();
        requestData.input_kg = mInputKg.getText().toString();
        requestData.input_week = mInputWeek.getText().toString();

        new ApiData().getData(getContext(), Tr_asstb_weight_hope_grp.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_asstb_weight_hope_grp) {
                    Tr_asstb_weight_hope_grp tr = (Tr_asstb_weight_hope_grp)obj;
                    if ("Y".equals(tr.data_yn)) {
                        // 신청완료

                    } else {
                        // 이미신청중
                    }
                } else {
                    CDialog.showDlg(getContext(), "데이터 수신에 실패 하였습니다.");
                }
            }
        });
    }
}