package com.appmd.hi.gngcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.FrequentlyListItem;


/**
 * Created by jihoon on 2016-04-18.
 * 자주 묻는 질문 리스트 어댑터
 * @since 0, 1
 */
public class FrequentlyListAdapter extends BaseExpandableListAdapter {

    private LayoutInflater mInflater;
    private ArrayList<FrequentlyListItem> mData;
    private Context mContext;

    public FrequentlyListAdapter(Context context, ArrayList<FrequentlyListItem> items){
        super();
        this.mContext   =   context;
        this.mInflater  =   LayoutInflater.from(context);
        this.mData      =   items;

        mInflater       =   (LayoutInflater)    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mData.get(groupPosition).getmTitle();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.news_list_group_item, null);

            holder = new ViewHolder();

            holder.mGroupTv     =   (TextView)  convertView.findViewById(R.id.group_tv);
            holder.mGroupImg    =   (ImageView) convertView.findViewById(R.id.group_img);
            holder.mGroupLay    =   (RelativeLayout) convertView.findViewById(R.id.group_layout);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        if(isExpanded){
            holder.mGroupImg.setImageResource(R.drawable.btn_faq_answer_on);
        }else{
            holder.mGroupImg.setImageResource(R.drawable.btn_faq_answer);
        }

        if(getGroupCount()-1 == groupPosition){
            holder.mGroupMargin.setVisibility(View.GONE);
        }else{
            holder.mGroupMargin.setVisibility(View.GONE);
        }

//
//        if ("".equals(mData.get(groupPosition).getmTitle()) || mData.get(groupPosition).getmTitle().equals(null)) {
//            holder.mGroupLay.setVisibility(View.GONE);
//        }else {
//            holder.mGroupTv.setText(mData.get(groupPosition).getmTitle());   // 제목
//            holder.mGroupLay.setVisibility(View.VISIBLE);
//        }

        if (mData.get(groupPosition).getmTitle() == null || "".equals(mData.get(groupPosition).getmTitle())) {
            holder.mGroupLay.setVisibility(View.GONE);
            holder.mGroupMargin.setVisibility(View.GONE);
        }else {
            holder.mGroupTv.setText(mData.get(groupPosition).getmTitle());   // 제목
            holder.mGroupLay.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(groupPosition).getmContent();
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.news_list_child_item, null);

            holder = new ViewHolder();

            holder.mChildTv     =   (TextView)  convertView.findViewById(R.id.group_child_tv);
            holder.mChildGroupLay = (RelativeLayout) convertView.findViewById(R.id.group_child_layout);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        if (mData.get(groupPosition).getmContent() == null || "".equals(mData.get(groupPosition).getmContent())) {
            holder.mChildGroupLay.setVisibility(View.GONE);
        }else {
            holder.mChildTv.setText(mData.get(groupPosition).getmContent());   // 내용
            holder.mChildGroupLay.setVisibility(View.VISIBLE);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public class ViewHolder {
        public TextView mGroupTv;
        public ImageView mGroupImg;
        public TextView mChildTv;
        public View mGroupMargin;
        public RelativeLayout mGroupLay , mChildGroupLay;
    }
}