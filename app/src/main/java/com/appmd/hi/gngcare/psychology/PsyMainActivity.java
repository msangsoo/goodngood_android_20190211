package com.appmd.hi.gngcare.psychology;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.main.BabyInfoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;


/**
 * Created by MobileDoctor on 2017-02-28.
 */
public class PsyMainActivity extends PsyBaseActivity {

    public static Activity PSY_MAIN_ACTIVITY;
    public LayoutInflater mLayoutInflater;
    public Fragment mContentFragment;
    public FrameLayout mFrameContainer;
    private Toolbar toolbar;

    boolean mNoShowHelp = false;

    // 네비바
    public RelativeLayout mBgActionBar;
    private ImageButton mLefeBtn;
    private RelativeLayout mRightLayout;
    private FrameLayout mBgBabyFace;
    private ImageView mRightImg;
    private TextView mTitleTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.psy_main_activity);
        PSY_MAIN_ACTIVITY = PsyMainActivity.this;
        mLayoutInflater	=	(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        init();
        setEvent();
        mTitleTv.setText(getString(R.string.title_psy));
        mFrameContainer.postDelayed(new Runnable() { // introbaseactivity 에서 db 쿼리 소요시간을 감안하여 살짝 딜레이
            @Override
            public void run() {
                switchContent(new PsyMainFragment());
            }
        }, CommonData.ANI_DELAY_500);

        // 자녀 데이터가 있는경우 UI 세팅
        if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                mRightImg.setImageResource(R.drawable.main_fetus06b);
            } else {
                CustomImageLoader.displayImage(PsyMainActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
            }
            mRightLayout.setVisibility(View.VISIBLE);
        }else{
            mRightLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 초기화
     */
    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_no_main);

        // start custom actionbar leftmargin remove
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        // end custom actionbar leftmargin remove
        mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);

        mBgActionBar = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_action_bar);
        mBgActionBar.setBackgroundColor(getResources().getColor(R.color.h_green));
        mLefeBtn = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.left_btn);
        mRightLayout = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.right_layout);
        mBgBabyFace = (FrameLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_baby_face);
        mRightImg = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.photo_img);

        mTitleTv = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.title_tv);

    }

    /* 타이틀 변경 */
    public void setTitleTxt(String title){
        mTitleTv.setText(title);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        mRightLayout.setOnClickListener(btnListener);
        mLefeBtn.setOnClickListener(btnListener);
        mRightImg.setOnClickListener(btnListener);

    }

    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GLog.i("onStop", "dd");

    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            String str = "";
            Fragment fragment = null;

            GLog.i("v.getId() = " +v.getId(), "dd");

            switch (v.getId()){
                case R.id.left_btn:
                    onBackPressed();
                    break;
                case R.id.photo_img:
                    intent = new Intent(PsyMainActivity.this, BabyInfoActivity.class);
                    startActivityForResult(intent, CommonData.REQUEST_CHILD_MANAGE);
                    Util.BackAnimationStart(PsyMainActivity.this);
                    break;
            }

            if (!str.equals("")) {
                mTitleTv.setText(str);
            }
        }
    };


    @Override
    public void onBackPressed() {
        if(mOnKeyBackPressedListener != null){
            mOnKeyBackPressedListener.onBack();
        }else{
            finish();
        }
    }

    @Override
    public void finish(){
        super.finish();
        Util.BackAnimationEnd(PsyMainActivity.this);
    }

    public interface onKeyBackPressedListener {
        void onBack();
    }
    private onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }

    /**
     * Fragment 변경
     * @param fragment  변경할 fragment
     */
    public void switchContent(Fragment fragment){
        mContentFragment = fragment;

        if (fragment != null) {
            GLog.i("fragment != null", "dd");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
        } else {
            // error in creating fragment
            GLog.e("Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        GLog.i("onNewIntent", "dd");

        if ( CommonData.getInstance().getMemberId() == 0 ) {
            GLog.i("CommonData.getInstance().getMemberId() == 0", "dd");
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.i("requestCode = " +requestCode, "dd");
        GLog.i("resultCode = " +resultCode, "dd");
        GLog.i("data = " + data, "dd");

        if(resultCode != Activity.RESULT_OK){
            return;
        }

        switch(requestCode){
            case CommonData.REQUEST_CHILD_MANAGE:   // 자녀관리
                GLog.i("REQUEST_CHILD_MANAGE", "dd");
                mFrameContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 자녀 데이터가 있는경우 UI 세팅
                        if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {

                            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                                mRightImg.setImageResource(R.drawable.main_fetus06b);
                            } else {
                                CustomImageLoader.displayImage(PsyMainActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
                            }



                            mRightLayout.setVisibility(View.VISIBLE);
                        }else{
                            mRightLayout.setVisibility(View.GONE);
                        }

                        setResult(RESULT_OK);
                        switchContent(new PsyMainFragment());

                    }
                }, CommonData.ANI_DELAY_500);
                break;

        }

        if(mContentFragment != null) {
            GLog.i("mContentFragment != null", "dd");
            mContentFragment.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }

}