package com.appmd.hi.gngcare.motherhealth;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.util.JsonLogPrint;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public class MotherHealthRegStepOneFragment extends BaseFragment {


    private MotherHealthRegActivity activity;
    private View view;
    private ImageView motherAfterBtn;
    private ImageView motherBeforeBtn;
    private TextView motherAfterTxt;
    private TextView motherBeforeTxt;

    private Button mNextBtn;
    private int TabNum = 0;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        activity = (MotherHealthRegActivity) getActivity();

        view = inflater.inflate(R.layout.mother_health_reg_step_one_fragment, null);
        initView(view);
        initEvent();

        if(CommonData.getInstance().getHpMjYn().equals("N")){
            //Y  임신중 N: 출산후
            if(CommonData.getInstance().getMotherIsPregnancy().equals("Y")){
                motherAfterBtn.setSelected(false);
                motherAfterTxt.setSelected(false);
                motherBeforeBtn.setSelected(true);
                motherBeforeTxt.setSelected(true);
                activity.setMatherBeforeFlag(true);

            } else {
                motherAfterBtn.setSelected(true);
                motherAfterTxt.setSelected(true);
                motherBeforeBtn.setSelected(false);
                motherBeforeTxt.setSelected(false);
                activity.setMatherBeforeFlag(false);
            }


        } else {
            //Y  출산후 N: 임신중
            if("Y".equals(CommonData.getInstance().getbirth_chl_yn())){
                motherAfterBtn.setSelected(true);
                motherAfterTxt.setSelected(true);
                motherBeforeBtn.setSelected(false);
                motherBeforeTxt.setSelected(false);
                activity.setMatherBeforeFlag(false);
            }
            else{
                motherAfterBtn.setSelected(false);
                motherAfterTxt.setSelected(false);
                motherBeforeBtn.setSelected(true);
                motherBeforeTxt.setSelected(true);
                activity.setMatherBeforeFlag(true);
            }
        }

        Bundle bundle = getArguments();
//        if(bundle != null)
//            TabNum = bundle.getInt("TabNum",0);

        return view;

    }

    private void initView(View view){
        motherBeforeBtn = (ImageView) view.findViewById(R.id.motherBeforeBtn);
        motherBeforeTxt = (TextView) view.findViewById(R.id.motherBeforeTxt);
        motherAfterBtn = (ImageView) view.findViewById(R.id.motherAfterBtn);
        motherAfterTxt = (TextView) view.findViewById(R.id.motherAfterTxt);
        mNextBtn = (Button) view.findViewById(R.id.nextBtn);
        motherBeforeBtn.setSelected(true);
        motherBeforeTxt.setSelected(true);
    }

    private void initEvent(){
        motherBeforeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedBtn(view);
            }
        });

        motherAfterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedBtn(view);
            }
        });
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("TabNum",TabNum);
                activity.replaceFragment(new MotherHealthRegStepTwoFragment(),false, true, bundle);
            }
        });
    }

    private void setSelectedBtn(View v){
        if(v.getId() == R.id.motherBeforeBtn){
            motherAfterBtn.setSelected(false);
            motherAfterTxt.setSelected(false);
            motherBeforeBtn.setSelected(true);
            motherBeforeTxt.setSelected(true);
            activity.setMatherBeforeFlag(true);
        }else if(v.getId() == R.id.motherAfterBtn){
            motherAfterBtn.setSelected(true);
            motherAfterTxt.setSelected(true);
            motherBeforeBtn.setSelected(false);
            motherBeforeTxt.setSelected(false);
            activity.setMatherBeforeFlag(false);
        }

    }




}
