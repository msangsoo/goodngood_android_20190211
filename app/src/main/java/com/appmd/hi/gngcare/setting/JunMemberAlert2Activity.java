package com.appmd.hi.gngcare.setting;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.intro.LoginActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class JunMemberAlert2Activity extends BackBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{

    private LinearLayout mNameLayout, mBirthLayout;
    private RelativeLayout mGenderRv;
    private TextView mTopTv, mBirthdayTv;
    private EditText mNameEdit, mBirthTv;
    private ImageButton mNameDelBtn;

    private RadioButton mMaleRb, mFemaleRb;

    private Button mChangeBtn, mCancelBtn;

    private String mCurrentBirth =   "";
    Calendar cal = Calendar.getInstance();

    private String mBer_nm, mBer_lifyea, mBer_birthyea, mBer_sex;    // api 호출시 저장변수

    private Intent intent = null;
    private int mLoginType = 1;

    String mberNo = "";
    String mBerNm = "";
    String mBirthday = "";
    String mBirthyea ="";
    int mGender = 1;

    String mParentBerNm = "";
    String mParentBirthday = "";
    String mParentHp = "";
    String mParentJob = "";
    String mParentAfterBirth = "";
    String mParentchl_exist_yn = "";
    String mParentNation = "";
    String mParentGender = "";

    private Date mCurDate;
    GregorianCalendar mCalendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.junmember_alert2_activity);

        setTitle(getString(R.string.junmember_alert));

        init();
        setEvent();

        intent = getIntent();

        if (intent != null) {

            mParentBerNm = intent.getStringExtra(CommonData.EXTRA_MBERNM);
            mParentBirthday = intent.getStringExtra(CommonData.EXTRA_BIRTHDAY);
            mParentNation = intent.getStringExtra(CommonData.EXTRA_MBERNATION);
            mParentGender = intent.getStringExtra(CommonData.EXTRA_GENDER);
            mParentHp = intent.getStringExtra(CommonData.EXTRA_PHONENO);
            mParentJob = intent.getStringExtra(CommonData.EXTRA_JOB);
            mParentAfterBirth = intent.getStringExtra(CommonData.EXTRA_AFTERBIRTH);

        }

        if(mParentAfterBirth.equals("1")){
            mTopTv.setText("아이정보입력");
            mBirthdayTv.setText(getString(R.string.birthday));


            mNameLayout.setVisibility(View.VISIBLE);
            mGenderRv.setVisibility(View.VISIBLE);

        }else{
            mTopTv.setText("출산 예정일 입력");
            mBirthdayTv.setText(getString(R.string.mother_health_reg_week_cnt));

            mNameLayout.setVisibility(View.GONE);
            mGenderRv.setVisibility(View.GONE);
        }


        mChangeBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if(intent != null){
            mLoginType  =   intent.getIntExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);

        }else{
            GLog.i("intent = null", "dd");
        }

        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
    }

    /**
     * 초기화
     */
    public void init(){

        mNameLayout     =   (LinearLayout)      findViewById(R.id.name_layout);
        mBirthLayout    =   (LinearLayout)      findViewById(R.id.birthday_layout);

        mGenderRv           =   (RelativeLayout)    findViewById(R.id.gender_Rv);

        mBirthTv        =   (EditText)          findViewById(R.id.birthday_tv);
        mTopTv          =   (TextView)          findViewById(R.id.top_txt);
        mBirthdayTv     =   (TextView)          findViewById(R.id.birthday_Txt);

        mNameEdit       =   (EditText)          findViewById(R.id.name_edit);

        mNameDelBtn     =   (ImageButton)       findViewById(R.id.name_del_btn);

        mMaleRb         =   (RadioButton)       findViewById(R.id.male_btn);
        mFemaleRb       =   (RadioButton)       findViewById(R.id.female_btn);


        mChangeBtn     =   (Button)            findViewById(R.id.change_btn);
        mCancelBtn    =   (Button)            findViewById(R.id.cancel_btn);


        mNameEdit.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNm());

        int year;
        int month;
        int day;

        if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.YES)){
            final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
            year = mDate.getYear() + 1900;
            month = mDate.getMonth() + 1;
            day = mDate.getDate();

            String birth = String.format("%04d",year) +  String.format("%02d",month)  +  String.format("%02d",day);

            mBirthTv.setText(birth);
        }else{

            final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
            year = mDate.getYear() + 1900;
            month = mDate.getMonth() + 1;
            day = mDate.getDate();

            String birth = String.format("%04d",year) +  String.format("%02d",month)  +  String.format("%02d",day);

            mBirthTv.setText(birth);
        }

        if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals("1")){
            mMaleRb.setChecked(true);
            mFemaleRb.setChecked(false);
        }else{
            mMaleRb.setChecked(false);
            mFemaleRb.setChecked(true);
        }



    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mChangeBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        mNameDelBtn.setOnClickListener(this);


        mNameEdit.setOnFocusChangeListener(this);

        mNameEdit.addTextChangedListener(new MyTextWatcher());
        mBirthTv.addTextChangedListener(new MyTextWatcher());

    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;

        String birth = mBirthTv.getText().toString();
        if(mParentAfterBirth.equals("1")) {

            if (mNameEdit.getText().toString().trim().equals("")) {    // 이름 입력이 없다면
                mNameEdit.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent(getString(R.string.popup_dialog_name_error));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("생일은 미래일 수 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(!CDateUtil.getDateFormatBool(birth)){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("날짜형식이 아닙니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

        }else{
            if (birth.trim().length() != 8) {
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.getIntVal(birth) < StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("출산예정일은 미래 날짜로 입력해주세요.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.addDay(CDateUtil.getToday_yyyy_MM_dd(),280))){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("출산예정일은 40주를 넘길 수 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(!CDateUtil.getDateFormatBool(birth)){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("날짜형식이 아닙니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }
        }


        /*if(mCurrentBirth.equals("")){   // 생년월일
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }*/





        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(mParentAfterBirth.equals("1")) {

            if (mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2) {    // 이름 입력이 없다면
                bool = false;
                return bool;
            }
        }

            if (mBirthTv.getText().toString().trim().equals("") || mBirthTv.getText().toString().trim().length() < 8) {
                bool = false;
                return bool;
            }


        /*if(mCurrentBirth.equals("")){   // 생년월일
            bool = false;
            return bool;
        }*/


        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

        if(bool) {
            mChangeBtn.setEnabled(true);

            mChangeBtn.setBackgroundResource(R.drawable.btn_5_22396c_right);
            mChangeBtn.setTextColor(ContextCompat.getColorStateList(JunMemberAlert2Activity.this, R.color.color_ffffff_btn));
        }else{
            mChangeBtn.setEnabled(false);

            mChangeBtn.setBackgroundResource(R.drawable.background_5_e6e7e8_right);
            mChangeBtn.setTextColor(ContextCompat.getColorStateList(JunMemberAlert2Activity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 준회원 체크
     *
     */
    public void requestAuthCheck() {
//        {   "api_code": "mber_check",   "insures_code": "106", "token": "deviceToken",
//                "app_code": "android19" ,  "mber_nm": "테스트" ,"mber_lifyea": "20140101","mber_hp": "010758421333", "mber_nation": "1"  ,  "mber_sex": "2"   }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_REG_EDIT_OK);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_MBER_NM,     mParentBerNm);
            object.put(CommonData.JSON_MBER_LIFYEA,     mParentBirthday);
            object.put(CommonData.JSON_MBER_HP,     mParentHp);
            object.put(CommonData.JSON_MBER_NATION,     mParentNation);
            object.put(CommonData.JSON_MBER_SEX,     mParentGender);
            object.put(CommonData.JSON_MBER_JOB_YN,     mParentJob);

            mBer_nm = mNameEdit.getText().toString().trim();
            mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
            if(mParentAfterBirth.equals("1")) {
                mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자
            }else{
                mBer_sex = "";
            }

            mParentchl_exist_yn = mParentAfterBirth.equals("1")? "Y" : "N"; // 출산여부 Y:출산 후 N:출산 전
            setHPmoonjin(mParentchl_exist_yn);


            object.put(CommonData.JSON_CHLDRN_SN,     MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_CHLDRN_NM,     mBer_nm);
            object.put(CommonData.JSON_CHLDRN_LIFYEA,     mBer_lifyea);
            object.put(CommonData.JSON_CHLDRN_SEX,     mBer_sex);
            object.put(CommonData.JSON_CHL_EXIST_YN,     mParentchl_exist_yn);
            object.put(CommonData.JSON_MBER_GRAD,     "20");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JunMemberAlert2Activity.this, NetworkConst.NET_ASSTB_REG_EDIT_OK, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }

    /**
     * 로그인
     *
     * @param mber_id  아이디
     * @param mber_pwd 비밀번호
     */
    public void requestLogin(String mber_id, String mber_pwd) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JunMemberAlert2Activity.this, NetworkConst.NET_LOGIN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /** 사전체크 문항 초기화 여부
     *
     * @param birth
     */
    private void setHPmoonjin(String birth){
        if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(birth)){
            commonData.setHpMjYnJun(false);
        }

    }


    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.change_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                mBer_nm = mNameEdit.getText().toString().trim();
                mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
                mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자

                if(invaildCerfiti()){   // 인증 가능하다면
                    requestAuthCheck();
                }
                break;
            case R.id.name_del_btn: // 이름 삭제
                commonView.setClearEditText(mNameEdit);
                break;
            case R.id.birthday_tv:  // 생년월일
                try {
                    if(mCurDate == null){
                        mCurDate = new Date();
                    }
                    mCalendar.setTime(mCurDate);
                    int nNowYear = mCalendar.get(Calendar.YEAR);
                    int nNowMonth = mCalendar.get(Calendar.MONTH);
                    int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurDate = mCalendar.getTime();
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                        mCurrentBirth = format.format(mCurDate);
                        format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                        mBirthTv.setText( format.format(mCurDate));
                        setConfirmBtn(isConfirm());
                    }, nNowYear, nNowMonth , nNowDay);
                    datePickerDialog.setCancelable(false);
                    datePickerDialog.showYearPickerFirst(true);
                    datePickerDialog.show(getFragmentManager(),"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_btn :
                intent = new Intent(JunMemberAlert2Activity.this, SettingActivity.class);
                startActivity(intent);
                finish();
                activityClear();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.name_edit:    // 이름
//                commonView.setClearImageBt(mNameDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_ASSTB_REG_EDIT_OK:   // 회원인증 체크
                   switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                               String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                               String mbersn = resultData.getString(CommonData.JSON_MBER_SN);
                                if(data_yn.equals(CommonData.YES)){   // 준회원정보수정

                                    requestLogin(commonData.getMberId(), commonData.getMberPwd());
                                    MainActivity.mJunChangeChild = true;


                                }else {  // 현대해상 미가입자
                                    mDialog = new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_change_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;


                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;

                case NetworkConst.NET_LOGIN:    // 로그인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_LOGIN", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);


                                if(data_yn.equals(CommonData.YES)){	// 로그인 성공
                                    GLog.i("NET_LOGIN SUCCESS", "dd");

                                    commonData.setMberNm(resultData.getString(CommonData.JSON_MBER_NM));
                                    commonData.setPhoneNumber(resultData.getString(CommonData.JSON_MBER_HP));
                                    commonData.setBirthDay(resultData.getString(CommonData.JSON_MBER_BRTHDY));
                                    commonData.setGender(resultData.getString(CommonData.JSON_MBER_SEX));
                                    commonData.setMberNation(resultData.getString(CommonData.JSON_MBER_NATION));

                                    //푸시
                                    commonData.setPushAlarm(resultData.getString(CommonData.JSON_PUSH_YN).equals(CommonData.YES) ? true : false);
                                    commonData.setNewsPushAlarm(resultData.getString(CommonData.JSON_NEWS_YN).equals(CommonData.YES) ? true : false);
                                    commonData.setMapPushAlarm(resultData.getString(CommonData.JSON_HEAT_YN).equals(CommonData.YES) ? true : false);
                                    commonData.setNoticePushAlarm(resultData.getString(CommonData.JSON_NOTICE_YN).equals(CommonData.YES) ? true : false);
                                    commonData.setDietPushAlarm(resultData.getString(CommonData.JSON_DIET_YN).equals(CommonData.YES) ? true : false);
                                    commonData.setNotityPushAlarm(resultData.getString(CommonData.JSON_NOTITY_YN).equals(CommonData.YES) ? true : false);

                                    commonData.setAddressDo(resultData.getString(CommonData.JSON_AREA_DO));
                                    commonData.setAddressGu(resultData.getString(CommonData.JSON_AREA_SI));
                                    commonData.setAlarmMode(resultData.getInt(CommonData.JSON_PUSH_MTH));
//                commonData.setMberNation(data.getString(CommonData.JSON_MBER_NATION));
//            commonData.setLoginType(data.getInt(CommonData.JSON_CHIDRN_CNT) > 0 ? CommonData.LOGIN_TYPE_PARENTS : CommonData.LOGIN_TYPE_CHILD);
                                    commonData.setChildCnt(resultData.getInt(CommonData.JSON_CHIDRN_CNT));
                                    commonData.setJuminNum(resultData.getString(CommonData.JSON_JUMINNUM));
                                    commonData.setHpMjYn(resultData.getString(CommonData.JSON_HP_MJ_YN));
                                    commonData.setHiPlannerHp(resultData.getString(CommonData.JSON_HIPLANNER_HP));
                                    commonData.setTempDivices(resultData.getString(CommonData.JSON_THERMOMETERCHK));
                                    commonData.setMotherWeight(resultData.getString(CommonData.JSON_MOTHER_WEIGHT));
                                    commonData.setKg_Kind(resultData.getString(CommonData.JSON_KG_KIND)); // 엄마체중 상태
                                    commonData.setWeighingchk(resultData.getString(CommonData.JSON_WEIGHTNGCHK)); // 체중계여부
                                    commonData.setIamChild(resultData.getString(CommonData.JSON_I_AM_CHILD));
                                    commonData.setMotherWeight(resultData.getString(CommonData.JSON_MOTHER_WEIGHT));
                                    commonData.setMotherGoalWeight(resultData.getString(CommonData.JSON_MOTHER_GOAL_WEIGHT));
                                    commonData.setMotherGoalCal(resultData.getString(CommonData.JSON_MOTHER_GOAL_CAL));   //목표칼로리
                                    commonData.setMotherGoalStep(resultData.getString(CommonData.JSON_MOTHER_GOAL_STEP)); //목표체중
                                    commonData.setMberBirthDueDe(resultData.getString(CommonData.JSON_MBER_BIRTH_DUE_DE));
                                    commonData.setMberChlBirthDe(resultData.getString(CommonData.JSON_MBER_CHL_BIRTH_DE));
                                    commonData.setBefCm(resultData.getString(CommonData.JSON_BEF_CM));
                                    commonData.setBefKg(resultData.getString(CommonData.JSON_BEF_KG));
                                    commonData.setHpMjYn(resultData.getString(CommonData.JSON_HP_MJ_YN));
                                    commonData.setActqy(resultData.getString(CommonData.JSON_ACTQY));
                                    commonData.setKg_Kind(resultData.getString(CommonData.JSON_KG_KIND));
                                    commonData.setMberJob(resultData.getString(CommonData.JSON_MBER_JOB_YN));
                                    commonData.setMberAgreementYn(resultData.getString(CommonData.JSON_MBER_AGREEMENT_YN)); //동의 여부

                                    commonData.setMberGrad(resultData.getString(CommonData.JSON_MBER_GRAD)); // 정회원 여부 : 10:정회원, 20: 준회원

                                    if(!resultData.isNull(CommonData.JSON_MBER_SN)){  // 회원고유키값이 있다면 저장 ( 모든 api 호출시 사용 )
                                        commonData.setMberSn(resultData.getString(CommonData.JSON_MBER_SN));
                                    }

                                    if(resultData.getJSONArray(CommonData.JSON_CHLDRN).length() > 0) {    // 자녀정보가 있을 경우
                                        commonData.setChldrn(resultData.getJSONArray(CommonData.JSON_CHLDRN).toString()); // 자녀정보
                                    }


                                    try {
                                        PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
                                        CommonData.getInstance().setAppVersion(pi.versionName.toString());
                                        GLog.i("app_ver  = " + pi.versionName.toString(), "dd");
                                    } catch (Exception e) {
                                        GLog.e(e.toString());
                                    }



                                    // + -----------------
                                    // 임신여부를 판단.
                                    // + -----------------
                                    String isPregnancy = "N";
                                    try{
                                        if(!commonData.getChldrn().equals("")) {
                                            JSONArray childArr = new JSONArray(CommonData.getInstance().getChldrn());
                                            if (childArr.length() > 0) {
                                                for (int i = 0; i < childArr.length(); i++) {
                                                    JSONObject childData = childArr.getJSONObject(i);
                                                    String exitYN = childData.getString(CommonData.JSON_CHL_EXIST_YN);
                                                    if ("N".equals(exitYN)){
                                                        isPregnancy = "Y";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }catch(Exception e){
                                        GLog.e(e.toString());
                                    }
                                    commonData.setMotherIsPregnancy(isPregnancy);

                                    // + -----------------
                                    // 단태임신, 다태임신 여부를 판단. 1:단태, 2:다태
                                    // + -----------------
                                    if(!resultData.isNull(CommonData.JSON_MBER_CHL_TYPE)){
                                        commonData.setMberChlType(resultData.getString(CommonData.JSON_MBER_CHL_TYPE));
                                    }

                                    commonData.setbirth_chl_yn(resultData.getString(CommonData.JSON_BIRTH_CHL_YN));

                                    MainActivity mainActivity = new MainActivity();
                                    mainActivity.saveChild();

                                    mDialog =   new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_contactor_complete2));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYMMDD);
                                            Date mDate = Util.getDateFormat(mParentBirthday, CommonData.PATTERN_YYYYMMDD);


                                            Intent intent = new Intent(JunMemberAlert2Activity.this, SettingActivity.class);
                                            startActivity(intent);
                                            activityClear();
                                            finish();

                                        }
                                    });
                                    mDialog.show();


                                }else{	// 로그인 실패
                                    GLog.i("NET_LOGIN FAIL", "dd");
                                    //초기화
                                    commonData.setMberId("");
                                    commonData.setMberPwd("");
                                    mDialog	=	new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            // 로그인 실패 시 내용을 전부 지운다.
                                            dialog.dismiss();
                                            commonData.setMberPwd(null);
                                            commonData.setMain_Category("");
                                            commonData.setAutoLogin(false);
                                            commonData.setRememberId(false);
                                            commonData.setMberSn("");

                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_bef_cm", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_bef_kg", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_kg","");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_term_kg", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_chl_birth_de", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_milk_yn", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_birth_due_de", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_chl_typ", "");
                                            Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_actqy", "");


                                            commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                            mDialog.dismiss();
                                            finish();

                                            Intent intent2 = new Intent(JunMemberAlert2Activity.this, LoginActivity.class);
                                            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent2);

                                        }
                                    });
                                    mDialog.show();

                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                                //초기화
                                commonData.setMberId("");
                                commonData.setMberPwd("");
                                GLog.i("NET_LOGIN Exception", "dd");
                                mDialog	=	new CustomAlertDialog(JunMemberAlert2Activity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(CustomAlertDialog dialog, Button button) {
                                        // 로그인 실패 시 내용을 전부 지운다.
                                        dialog.dismiss();
                                        commonData.setMberPwd(null);
                                        commonData.setMain_Category("");
                                        commonData.setAutoLogin(false);
                                        commonData.setRememberId(false);
                                        commonData.setMberSn("");

                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_bef_cm", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_bef_kg", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_kg","");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_term_kg", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_chl_birth_de", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_milk_yn", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_birth_due_de", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_mber_chl_typ", "");
                                        Util.setSharedPreference(JunMemberAlert2Activity.this, "MonJin_actqy", "");


                                        commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                        mDialog.dismiss();
                                        finish();

                                        Intent intent2 = new Intent(JunMemberAlert2Activity.this, LoginActivity.class);
                                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent2);

                                    }
                                });
                                mDialog.show();
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            if (dialog != null) {
                                dialog.show();
                            }
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
