package com.appmd.hi.gngcare.main;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.Alram.AlramMainActivity;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.ChildItem;
import com.appmd.hi.gngcare.collection.EpidemicItem;
import com.appmd.hi.gngcare.common.BackPressCloseHandler;
import com.appmd.hi.gngcare.common.CircleIndicator;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.common.CustomPageTransformer;
import com.appmd.hi.gngcare.common.CustomViewPager;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.diary.GrowthMainActivity;
import com.appmd.hi.gngcare.fever.FeverMainActivity;
import com.appmd.hi.gngcare.fever.FeverMapActivity;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.component.CPeriodDialog;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperLog;
import com.appmd.hi.gngcare.greencare.util.JsonLogPrint;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.weight.WeightBigDataInputFragment;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.motherhealth.MotherHealthMainActivity;
import com.appmd.hi.gngcare.motherhealth.MotherHealthRegActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.psychology.PsyMainActivity;
import com.appmd.hi.gngcare.push.FirebaseMessagingService;
import com.appmd.hi.gngcare.setting.SettingActivity;
import com.appmd.hi.gngcare.setting.SettingAddressActivity;
import com.appmd.hi.gngcare.util.DustManager;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.GpsInfo;
import com.appmd.hi.gngcare.util.KakaoLinkUtil;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewInfoActivity;
// import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by jihoon on 2016-03-21.
 * 메인 클래스 ( 로그인 후 최상위 클래스 )
 * @since 0, 1
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private final String TAG = MainActivity.class.getSimpleName();

    public static Activity MAIN_ACTIVITY;

    public LayoutInflater mLayoutInflater;

    private DrawerLayout mDrawerLayout;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private CollapsingToolbarLayout toolbarLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationLeftView;
    private WebView webView;

    private long period = 1;
    private long count = 1;
    private int num = 1;
    private String Fetus_BubbleId = "";
    private int resStringId = 0, diffday=0;
    private String url ="";
    private String[] dzname = new String[2];
    private String[] weekago_1_per = new String[2];

    private MakeProgress mProgress			=	null;


    CustomAlertDialog mDialog;

    // 네비바
    private ImageButton mMenuBtn, mFInFoBtn;
    private TextView mTitleTv,mHompageBtn;

    private LinearLayout mlist_add_change;
    private Spinner main_spinner;
    private ImageView mCategory_img;

    public static ArrayList<ChildItem> mChildMenuItem;  // 자녀 데이터 ( 항상 동기화 )
    public static int mChildChoiceIndex = 0;    // 자녀 선택 index
    public static boolean mChangeChild = false;  // 임시 자녀변경 flag

    public static boolean mJunChangeChild = false;  // 정회원 전환 시 자녀변경 flag

    public static ArrayList<EpidemicItem> mEpidemicList;            // 유행질병 카운트

    private TextView mTxtEpidemic, mTxtFineDust, mTxtSpeech_Bubble, mSub_title, mType_title, mtype_title1, mType_content, mBottom_sub_title, text_period;
    private FrameLayout mBtnEpidemic;

    private ImageView mImgBabyPhoto, mImgNew, mImgfetusphoto, mAlramBtn, mAlramNew;
    private ImageButton mBtnLeftBaby, mBtnRightBaby;
    private TextView mTxtBabyName, mTxtBabyBirth, mTxtBabyAge, mTxtBabyBornToDay, mTxtBabyBirthSchedule, mDisease_title1, mDisease_percent1, mDisease_title2, mDisease_percent2, mBottom_sub_title1;

    private View mTxtAgeLine;

    private LinearLayout mBabyMenu, mFetusMenu;

    private LinearLayout mBabyInfoMain_1, mBabyInfoMain_2;

    private LinearLayout mSub_contents1, mSub_contents2, mSub_contents3;

    private FrameLayout mBackLayout, mActionBar_btn;

    // 왼쪽 메뉴 아이템
    private TextView mBtnSide_01, mBtnSide_02, mBtnSide_98, mBtnSide_99, mNewMessage;//mBtnSide_07
    private RelativeLayout mBtnSide_04, mBtnSide_05, mBtnSide_06,mBtnSide_03, mBtnSide_10;
    private ImageView mBtnSide_08;


    //    private Button mSettingBtn;
    private BackPressCloseHandler backPressCloseHandler;

    private Calendar currentCal = Calendar.getInstance();

    public static String mLastWeight = "";
    public static String mLastHeight = "";
    public static String mLastCmResult = "";
    public static String mLastKgResult = "";
    public static String mLastWeightDe = "";
    public static String mLastHeightDe = "";

    protected CircleIndicator indicator;

    private boolean is_temp_setting = false;


    int chl_sn = 0;

    // 새 메인 화면 UI
    private LinearLayout mBtnGrowthMain, mBtnFeverMain, mBtnMedycare, mBtnCallMain ,mBtnPsyMain;


    //hsh
    Intent inTentGo = null;
    private String sMainPopTitle ="";
    private String sMainPopTxt ="";
    private boolean bMainPop = false;
    boolean exsit_aft_baby;
    int typePush=0;

    //currentindex
    private int tempCategory = 0;

    //swiper
    private CustomViewPager viewPager; //뷰페이지
    private ViewPagerAdapter ViewPagerAdapter; //뷰페이지 어댑터
    //sub swiper
    private CustomViewPager subviewPager; // 뷰페이지
    private ViewPagerSubAdapter ViewPagerSubAdapter; // sub뷰페이지 어댑터

    //온도_data
    private String tempLocal = "";
    private String tempTempeture = "";
    private String tempBottomtext = "";

    //질병_data
    private String tempDisease1 = "";
    private String tempDisease2 = "";
    private String tempDisease_percent1 = "";
    private String tempDisease_percent2 = "";

    //아이성장_data
    private String tempWeight1 = "";
    private String tempWeight2 = "";
    private String tempBmi = "";

    //check is_first_scroll
    private boolean is_first_scroll = true;

    private View view;


    /*public Handler rotation_txt_1 = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YoYo.with(Techniques.SlideOutUp).duration(1200).playOn(mTxtEpidemic);
            YoYo.with(Techniques.SlideInUp).duration(1200).playOn(mTxtFineDust);

            rotation_txt_2.sendEmptyMessageDelayed(0, 5000);
        }
    };

    public Handler rotation_txt_2 = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YoYo.with(Techniques.SlideOutUp).duration(1200).playOn(mTxtFineDust);
            YoYo.with(Techniques.SlideInUp).duration(1200).playOn(mTxtEpidemic);

            rotation_txt_1.sendEmptyMessageDelayed(0, 5000);
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity2);
        mProgress = new MakeProgress(this);

        backPressCloseHandler = new BackPressCloseHandler(this);
        MAIN_ACTIVITY  = MainActivity.this;
        mLayoutInflater	=	(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        NotificationManager mNM	= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.cancelAll();

        init();
        setEvent();

//        try {
//            GpsInfo gps = new GpsInfo(this);
//            if(gps.isGetLocation()){
//                double latitude = gps.getLatitude();
//                double longitude = gps.getLongitude();
//
//                //GLog.d("위치", "당신의 위치 - \n위도: " + latitude + "\n경도: " + longitude);
//
//                if(latitude != 0.0d && longitude != 0.0d){
//                    String[] address = BleUtil.FindAddress(MainActivity.this, latitude, longitude).split(" ");
//
//                    if(address.length > 0){
//                        requestLocation(latitude, longitude, address[1]);
//                        DustManager.mCityName = DustManager.checkCity(address[1]);
//                        requestFineDust(DustManager.getCityNumber(DustManager.mCityName));
//                    }
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(new Date());
        mCalendar.add(Calendar.DAY_OF_MONTH, -7);
        requestEpidemicData(mCalendar.getTime(), new Date());

        //rotation_txt_1.sendEmptyMessageDelayed(0, 5000);

        GLog.i("기본 팝업"+CommonData.getInstance().getAfterBabyPopupShowCheck(), "dd");

        // 다시 안보기 체크 안하고,
        if(!CommonData.getInstance().getAfterBabyPopupShowCheck()){
            exsit_aft_baby = false;
            for (int i = 0; i < mChildMenuItem.size(); i++){
                try {
                    if(mChildMenuItem.get(i).getmChlExistYn().equals(CommonData.NO)){

                        SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
                        Date today = new Date();

                        Date AftDate = format.parse(mChildMenuItem.get(i).getmChldrnAftLifyea());

                        if(today.compareTo(AftDate) > 0){
                            exsit_aft_baby = true;
                            break;
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            GLog.i("기본 팝업2"+exsit_aft_baby, "dd");

            if(exsit_aft_baby && !CommonData.getInstance().getAfterBabyPopupShowCheck()) {
                if (CommonData.getInstance().getMberGrad().equals("10")) {
                    mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_C);
                    mDialog.setTitle(getString(R.string.app_name_kr));
                    mDialog.setContent(getString(R.string.popup_msg_aft_lifyea));
                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog, button) -> {
                        dialog.dismiss();
                        exsit_aft_baby = false;
                        showTempPop();
                    });
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        String tel = "tel:" + getString(R.string.call_center_number_2);
//                    startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(tel));
                        startActivity(intent);
                        dialog.dismiss();
                        exsit_aft_baby = false;
                        showTempPop();
                    });
                    mDialog.setCheckboxButton((dialog, button) -> {
                        mDialog.setChangeCheckboxImg();
                        CommonData.getInstance().setAfterBabyPopupShowCheck(mDialog.isChecked());
                        dialog.dismiss();
                        exsit_aft_baby = false;
                        showTempPop();
                    });
                    mDialog.setCancelable(false);
                    mDialog.show();
                } else{
                    mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.app_name_kr));
                    View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.popup_dialog_baby_passover_jun, null);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params.gravity = Gravity.CENTER;
                    mDialog.setContentView(view, params);
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_moveto), (dialog, button) -> {
                        dialog.dismiss();
                        Intent intent;
                        intent = new Intent(MainActivity.this, SettingActivity.class);
                        startActivity(intent);
                        Util.BackAnimationStart(MainActivity.this);
                    });

                    mDialog.show();
                }
            }
        }



        //hsh
        requestTempPopApi();
//        setDefaultCategory();
    }

    /**
     * 초기화
     */
    public void init(){

        resStringId = getResources().getIdentifier(Fetus_BubbleId, "string", MAIN_ACTIVITY.getPackageName());

        mChildMenuItem = new ArrayList<ChildItem>();
        mEpidemicList = new ArrayList<EpidemicItem>();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        navigationLeftView = (NavigationView) findViewById(R.id.nav_left_view);



        mlist_add_change = (LinearLayout) findViewById(R.id.list_add_change);
        mBtnLeftBaby = (ImageButton)findViewById(R.id.btn_left_baby);
        mBtnRightBaby = (ImageButton)findViewById(R.id.btn_right_baby);
        mTxtBabyName = (TextView)findViewById(R.id.txt_baby_name);
        mTxtBabyBirth = (TextView)findViewById(R.id.txt_baby_birth);
        mTxtBabyAge = (TextView)findViewById(R.id.txt_baby_age);
        mTxtBabyBornToDay = (TextView)findViewById(R.id.txt_baby_born_to_day);
        mTxtBabyBirthSchedule = (TextView)findViewById(R.id.txt_baby_birth_schedule);

        mBtnMedycare = (LinearLayout)findViewById(R.id.btn_medycare);
        mBtnGrowthMain = (LinearLayout) findViewById(R.id.btn_growth_main);
        mBtnFeverMain = (LinearLayout)findViewById(R.id.btn_fever_main);
        mBtnCallMain = (LinearLayout)findViewById(R.id.mBtnCallMain);
        mBtnPsyMain= (LinearLayout)findViewById(R.id.mBtnPsyMain);

        main_spinner = (Spinner)findViewById(R.id.main_spinner);

       // mBtnEpidemic = (FrameLayout)findViewById(R.id.btn_epidemic);
        //mTxtEpidemic = (TextView)findViewById(R.id.txt_epidemic);
        //mTxtFineDust = (TextView)findViewById(R.id.txt_fine_dust);

        mBabyMenu = (LinearLayout)findViewById(R.id.child);
        mFetusMenu = (LinearLayout)findViewById(R.id.fetus);

        mBabyInfoMain_1 = (LinearLayout)findViewById(R.id.baby_info_main_1);
        mBabyInfoMain_2 = (LinearLayout)findViewById(R.id.baby_info_main_2);



        mBtnSide_01 = (TextView)findViewById(R.id.btn_side_01);
        mBtnSide_02 = (TextView)findViewById(R.id.btn_side_02);
        mBtnSide_03 = (RelativeLayout)findViewById(R.id.btn_side_03);
        mBtnSide_04 = (RelativeLayout)findViewById(R.id.btn_side_04);
        mBtnSide_05 = (RelativeLayout)findViewById(R.id.btn_side_05);
        mBtnSide_06 = (RelativeLayout)findViewById(R.id.btn_side_06);
        mBtnSide_08 = (ImageView)findViewById(R.id.btn_side_08);
        mBtnSide_10 = (RelativeLayout)findViewById(R.id.btn_side_10);


      //  mBtnSide_07 = (Button)findViewById(R.id.btn_side_07);

        mTxtAgeLine = findViewById(R.id.baby_age_line);

        /* 아이심리 테스트 버튼*/
        mBtnSide_99 = (TextView)findViewById(R.id.btn_side_99);

        /* 엄마건강 버튼 */
        mBtnSide_98 = (TextView)findViewById(R.id.btn_side_98);

        toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        //toolbarLayout.setTitleEnabled(false);   // setTitleEnabled(false) 해야 상단에 고정됨.
        setSupportActionBar(toolbar);
//        actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_main);
        // start custom actionbar leftmargin remove
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent =(Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        // end custom actionbar leftmargin remove

        mMenuBtn    =   (ImageButton)   getSupportActionBar().getCustomView().findViewById(R.id.menu_btn);
        mAlramBtn = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.alram_btn);
        mAlramNew = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.alram_new);

        mActionBar_btn = (FrameLayout)getSupportActionBar().getCustomView().findViewById(R.id.actionbar_btn);


        mFInFoBtn     =   (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.btn_fetus_info);
        mImgNew     =   (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.img_new);

        mTitleTv    =   (TextView)      getSupportActionBar().getCustomView().findViewById(R.id.title_tv);

        //swiper
        viewPager = (CustomViewPager) findViewById(R.id.main_viewpager);

        //subswiper
        subviewPager = (CustomViewPager) findViewById(R.id.sub_viewpager);

        indicator = findViewById(R.id.indicator);
        indicator.setVisibility(View.VISIBLE);
        indicator.setItemMargin(15);
        //애니메이션 속도
        indicator.setAnimDuration(0);
        //indecator 생성
        indicator.createDotPanel(5, R.drawable.indicator_nor, R.drawable.indicator_act);

        //New 메시지 말풍선
        mNewMessage = (TextView) findViewById(R.id.new_message);

        mHompageBtn = (TextView) findViewById(R.id.hompage_btn);


        //click 저장
        view = (DrawerLayout) findViewById(R.id.drawer_layout);

        saveChild();





//        if (BuildConfig.DEBUG)
//            mTitleTv.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    new DBBackupManager().exportDB(MainActivity.this);
//                }
//            });

    }

    /**
     * 아이데이터 저장
     */

    public void saveChild(){
        try{
            if(!CommonData.getInstance().getChldrn().equals("")) {  // 자녀 데이터가 있는 경우
                JSONArray childArr = new JSONArray(CommonData.getInstance().getChldrn());
                if (childArr.length() > 0) {

                    mChildMenuItem.clear();


                    for (int i = 0; i < childArr.length(); i++) {

                        JSONObject data = childArr.getJSONObject(i);

                        ChildItem item = null;

                        if(data.getString(CommonData.JSON_CHLDRN_LIFYEA).equals("")){
                            GLog.i("자녀 생년월이이 없는 경우 없는 자식", "dd");
                            continue;
                        }

                        JsonLogPrint.printJson(data.getString(CommonData.JSON_CHLDRN_KGPER));

                        item = new ChildItem(data.getString(CommonData.JSON_CHL_SN),
                                data.getString(CommonData.JSON_CHLDRN_JOINSERIAL),
                                data.getString(CommonData.JSON_CHLDRN_NM),
                                data.getString(CommonData.JSON_CHLDRN_NCM),
                                data.getString(CommonData.JSON_CHLDRN_LIFYEA),
                                data.getString(CommonData.JSON_CHLDRN_AFT_LIFYEA),
                                data.getString(CommonData.JSON_CHL_EXIST_YN),
                                data.getString(CommonData.JSON_CHLDRN_SEX),
                                data.getString(CommonData.JSON_CHLDRN_HP),
                                data.getString(CommonData.JSON_CHLDRN_CI),
                                data.getString(CommonData.JSON_CHLDRN_HEIGHT),
                                data.getString(CommonData.JSON_CHLDRN_BDWGH),
                                data.getString(CommonData.JSON_CHLDRN_HEADCM),
                                data.getString(CommonData.JSON_CHLDRN_MAIN_IMAGE),
                                data.getString(CommonData.JSON_CHLDRN_ORG_IMAGE),
                                data.getString(CommonData.JSON_SAFE_AREA_X),
                                data.getString(CommonData.JSON_SAFE_AREA_Y),
                                data.getString(CommonData.JSON_SAFE_KM),
                                data.getString(CommonData.JSON_SAFE_ALARM_AT),
                                data.getString(CommonData.JSON_SAFE_HEDEXPLN),
                                data.getString(CommonData.JSON_SAFE_ADRES),
                                data.getString(CommonData.JSON_HP_USE_TIME),
                                data.getString(CommonData.JSON_HP_USE_ALARM_AT),
                                data.getString(CommonData.JSON_HP_USE_ESTBS_TIME),
                                data.getString(CommonData.JSON_CHLDRN_KGPER),
                                CommonData.NO

                        );


                        if (CommonData.getInstance().getSelect()) {   // 자녀 선택한 데이터가 있다면
                            if (CommonData.getInstance().getSelectChildSn().equals(item.getmChlSn())) {    // 자녀선택한 고유키값이 같다면
                                mChildChoiceIndex = i;  // 선택중인 자녀 index 저장
                                item.setmSelect(CommonData.YES);
                                item.setmChldmKgper(data.getString(CommonData.JSON_CHLDRN_KGPER));

                            } else {
                                item.setmSelect(CommonData.NO);
                                item.setmChldmKgper(data.getString(CommonData.JSON_CHLDRN_KGPER));
                            }
                        } else {
                            if (i == 0) { // 선택한 자녀가 없다면 첫번째 데이터를 선택한 자녀로 저장
                                item.setmSelect(CommonData.YES);
                                CommonData.getInstance().setSelect(true);
                                CommonData.getInstance().setSelectChildSn(data.getString(CommonData.JSON_CHL_SN));
                            }
                        }

                        try{
                            SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYMMDD);
                            item.born_to_day = Util.sumDayCount(format.parse(item.getmChldrnLifyea()), new Date());
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        mChildMenuItem.add(item);
                    }

                    // start 선택한 자녀가 없는경우 첫번째 자녀 선택
                    boolean isSelect = false;
                    for(int j=0; j<mChildMenuItem.size(); j++){
                        if(mChildMenuItem.get(j).getmSelect().equals(CommonData.YES)){
                            isSelect = true;
                        }
                    }

                    if(!isSelect){
                        mChildChoiceIndex   =   0;
                        mChildMenuItem.get(mChildChoiceIndex).setmSelect(CommonData.YES);
                    }
                    // end 선택한 자녀가 없는경우 첫번째 자녀 선택


                    Intent intent = getIntent();
                    chl_sn = intent.getIntExtra("chl_sn", 0);
                    if(chl_sn != 0){
                        for (int z = 0; z < mChildMenuItem.size(); z++){
                            if(mChildMenuItem.get(z).getmChlSn().equals(chl_sn)){
                                mChildChoiceIndex = z;
//                                setBabyData();
                                CommonData.getInstance().setSelectChildSn(mChildMenuItem.get(mChildChoiceIndex).getmChlSn());
                            }
                        }
                    }

                } else {
                    GLog.i("child length = 0", "dd");
                    mChildMenuItem.clear();
                }
            }

        }catch(Exception e){
            GLog.e(e.toString());
        }
    }


    /**
     * 이벤트 연결
     */
    public void setEvent(){

        mBtnMedycare.setOnClickListener(btnListener);
        mBtnGrowthMain.setOnClickListener(btnListener);
        mBtnFeverMain.setOnClickListener(btnListener);
        mBtnCallMain.setOnClickListener(btnListener);
        mBtnPsyMain.setOnClickListener(btnListener);


//        uploadbtn.setOnClickListener(btnListener);
        mMenuBtn.setOnClickListener(btnListener);
        mFInFoBtn.setOnClickListener(btnListener);
        mAlramBtn.setOnClickListener(btnListener);


        mlist_add_change.setOnClickListener(btnListener);


        mBtnSide_01.setOnClickListener(btnListener);
        mBtnSide_02.setOnClickListener(btnListener);
        mBtnSide_03.setOnClickListener(btnListener);
        mBtnSide_04.setOnClickListener(btnListener);
        mBtnSide_05.setOnClickListener(btnListener);
        mBtnSide_06.setOnClickListener(btnListener);
        mBtnSide_08.setOnClickListener(btnListener);
        mBtnSide_10.setOnClickListener(btnListener);
//        mBtnSide_07.setOnClickListener(btnListener);

        findViewById(R.id.btn_HNews).setOnClickListener(btnListener);
        findViewById(R.id.btn_weight_main).setOnClickListener(btnListener);
        findViewById(R.id.btn_play_main).setOnClickListener(btnListener);
        findViewById(R.id.mBtnMealMain).setOnClickListener(btnListener);
        findViewById(R.id.mBtnPressureMain).setOnClickListener(btnListener);
        findViewById(R.id.mBtnSugarMain).setOnClickListener(btnListener);
        mHompageBtn.setOnClickListener(btnListener);


        findViewById(R.id.mBtnMomHealthMain).setOnClickListener(btnListener);



        mBtnSide_99.setOnClickListener(btnListener);
        mBtnSide_98.setOnClickListener(btnListener);
//        mBtnEpidemic.setOnClickListener(btnListener);

        navigationLeftView.setNavigationItemSelectedListener(this);

        //NEW말풍선
        mNewMessage.setOnClickListener(btnListener);

        mDrawerLayout.setDrawerListener(toggle);


        //click 저장
        OnClickListener mClickListener = new OnClickListener(btnListener,view,MainActivity.this);

        //홈
        mBtnMedycare.setOnTouchListener(mClickListener);
        mBtnGrowthMain.setOnTouchListener(mClickListener);
        mBtnFeverMain.setOnTouchListener(mClickListener);
        findViewById(R.id.mBtnMomHealthMain).setOnTouchListener(mClickListener);
        mBtnCallMain.setOnTouchListener(mClickListener);
        mBtnPsyMain.setOnTouchListener(mClickListener);
        findViewById(R.id.btn_HNews).setOnTouchListener(mClickListener);

        mFInFoBtn.setOnTouchListener(mClickListener);
        findViewById(R.id.btn_weight_main).setOnTouchListener(mClickListener);
        findViewById(R.id.btn_play_main).setOnTouchListener(mClickListener);
        findViewById(R.id.mBtnMealMain).setOnTouchListener(mClickListener);
        findViewById(R.id.mBtnPressureMain).setOnTouchListener(mClickListener);
        findViewById(R.id.mBtnSugarMain).setOnTouchListener(mClickListener);


        //햄버거 메뉴
        mMenuBtn.setOnTouchListener(mClickListener);
        mBtnSide_01.setOnTouchListener(mClickListener);
        mBtnSide_02.setOnTouchListener(mClickListener);
        mBtnSide_99.setOnTouchListener(mClickListener);
        mBtnSide_98.setOnTouchListener(mClickListener);
        mBtnSide_03.setOnTouchListener(mClickListener);
        mBtnSide_04.setOnTouchListener(mClickListener);
        mBtnSide_06.setOnTouchListener(mClickListener);
        mBtnSide_05.setOnTouchListener(mClickListener);
        mHompageBtn.setOnTouchListener(mClickListener);
        mBtnSide_08.setOnTouchListener(mClickListener);

        //알림
        mAlramBtn.setOnTouchListener(mClickListener);
        mNewMessage.setOnTouchListener(mClickListener);

        //코드 부여(홈)

        mBtnGrowthMain.setContentDescription(getString(R.string.BtnGrowthMain));
        mBtnFeverMain.setContentDescription(getString(R.string.BtnFeverMain));
        mBtnPsyMain.setContentDescription(getString(R.string.BtnPsyMain));
        findViewById(R.id.mBtnMomHealthMain).setContentDescription(getString(R.string.mBtnMomHealthMain));
        mBtnMedycare.setContentDescription(getString(R.string.BtnMedycare));
        findViewById(R.id.btn_HNews).setContentDescription(getString(R.string.btn_HNews));
        mBtnCallMain.setContentDescription(getString(R.string.BtnCallMain));



        mFInFoBtn.setContentDescription(getString(R.string.FInFoBtn));
        findViewById(R.id.btn_weight_main).setContentDescription(getString(R.string.btn_weight_main));
        findViewById(R.id.btn_play_main).setContentDescription(getString(R.string.btn_play_main));
        findViewById(R.id.mBtnMealMain).setContentDescription(getString(R.string.mBtnMealMain));
        findViewById(R.id.mBtnPressureMain).setContentDescription(getString(R.string.mBtnPressureMain));
        findViewById(R.id.mBtnSugarMain).setContentDescription(getString(R.string.mBtnSugarMain));


        //코드 부여(햄버거 메뉴)
        mMenuBtn.setContentDescription(getString(R.string.MenuBtn));
        mBtnSide_01.setContentDescription(getString(R.string.BtnSide_01));
        mBtnSide_02.setContentDescription(getString(R.string.BtnSide_02));
        mBtnSide_99.setContentDescription(getString(R.string.BtnSide_99));
        mBtnSide_98.setContentDescription(getString(R.string.BtnSide_98));
        mBtnSide_03.setContentDescription(getString(R.string.BtnSide_03));
        mBtnSide_04.setContentDescription(getString(R.string.BtnSide_04));
        mBtnSide_06.setContentDescription(getString(R.string.BtnSide_06));
        mBtnSide_05.setContentDescription(getString(R.string.BtnSide_05));
        mHompageBtn.setContentDescription(getString(R.string.HompageBtn));
        mBtnSide_08.setContentDescription(getString(R.string.BtnSide_08));

        //코드 부여(알림)
        mAlramBtn.setContentDescription(getString(R.string.AlramBtn));
        mNewMessage.setContentDescription(getString(R.string.NewMessage));

    }

    public void setfetus_img(long P_days) {

        //mBabyInfoMain_2.setVisibility(View.GONE);

        mBackLayout.setBackgroundResource(R.drawable.main_fetus07);

        if (period <= 2) {
            mImgfetusphoto.setImageResource(R.drawable.main_fetus01);
        } else if (period >= 3 && period <= 6) {
            mImgfetusphoto.setImageResource(R.drawable.main_fetus02);
        } else if (period >= 7 && period <= 8) {
            mImgfetusphoto.setImageResource(R.drawable.main_fetus03);
        } else if (period >= 9 && period <= 10) {
            mImgfetusphoto.setImageResource(R.drawable.main_fetus04);
        } else if (period >= 11 && period <= 15) {
            mImgfetusphoto.setImageResource(R.drawable.main_fetus05);
        } else if (period >= 16 && period <= 40) {
            //16주차 이후 2틀마다 이미지 변경
            int flag = (((int) P_days - CommonData.AFTER_BIRTH_16) / 2) % 3;
            switch (flag) {
                case 0:
                    mImgfetusphoto.setImageResource(R.drawable.main_fetus06);
                    break;
                case 1:
                    mImgfetusphoto.setImageResource(R.drawable.main_fetus06b);
                    break;
                case 2:
                    mImgfetusphoto.setImageResource(R.drawable.main_fetus06c);
                    break;

            }

        }
    }


    /**
     * 아이 정보 화면 설정
     */
    public void setBabyData(){
        // 자녀 데이터가 있는경우 UI 세팅
        try{
            if(mChildChoiceIndex == 0){
                mBtnLeftBaby.setVisibility(View.INVISIBLE);

                if(mChildMenuItem.size()-1 == 0)
                    mBtnRightBaby.setVisibility(View.INVISIBLE);
                else
                    mBtnRightBaby.setVisibility(View.VISIBLE);

            }else{
                if(mChildMenuItem.size()-1 == mChildChoiceIndex){
                    mBtnLeftBaby.setVisibility(View.VISIBLE);
                    mBtnRightBaby.setVisibility(View.INVISIBLE);
                }else{
                    mBtnLeftBaby.setVisibility(View.VISIBLE);
                    mBtnRightBaby.setVisibility(View.VISIBLE);
                }
            }

            if(!mChildMenuItem.get(mChildChoiceIndex).getmChlSn().equals("")) {
                Logger.i(TAG,"mChildChoiceIndex : "+mChildChoiceIndex + "||"+mChildMenuItem.get(mChildChoiceIndex).getmChldrnNcm() );

                if(mChildMenuItem.get(mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)){

                    long D_days;
                    long P_days = 280;
                    final Date mDate = Util.getDateFormat(mChildMenuItem.get(mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);

                    D_days = Util.getTwoDateCompare(mChildMenuItem.get(mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
                    if (D_days > 0) {
                        mTxtBabyBirth.setText(format.format(mDate));
                        mTxtBabyBornToDay.setText(getString(R.string.fetus_txt));
                        period = 40;
                    } else {

                        mTxtBabyBornToDay.setText("출산까지" + CommonData.STRING_SPACE + Math.abs(D_days) + getString(R.string.day));

                        //임신일짜
                        P_days = CommonData.AFTER_BIRTH_PERIOD - Math.abs(D_days);
                        mTxtBabyBirth.setText("임신" + CommonData.STRING_SPACE + P_days + "일째 (" + Util.getFetusBirthNew(MainActivity.this, (int) P_days) + ")");

                        //ssshin 임시주기 수정  //계산방법 : 40 – { (출산예정일과 오늘 사이의 일 수)/7 }    중괄호 안은 소수점 첫째 자리에서 올림 처리한다
                        double tempDdays = (double) Math.abs(D_days)/7;
                        period = (long) (40 - Math.ceil(tempDdays));

                    }
                    if(period == 0 ) period =1;

                    mActionBar_btn.setVisibility(View.VISIBLE);

                    mBabyInfoMain_1.setVisibility(View.VISIBLE);
                    mBabyMenu.setVisibility(View.GONE);
                    mFetusMenu.setVisibility(View.VISIBLE);

                    mTxtAgeLine.setVisibility(View.GONE);
                    mTxtBabyAge.setVisibility(View.GONE);

                    mBtnLeftBaby.setContentDescription("HL02_002_001_!");
                    mBtnRightBaby.setContentDescription("HL02_002_001_!");




                    //기본 셋팅
                    mTxtBabyName.setText(mChildMenuItem.get(mChildChoiceIndex).getmChldrnNcm().length() > 0 ? mChildMenuItem.get(mChildChoiceIndex).getmChldrnNcm() : mChildMenuItem.get(mChildChoiceIndex).getmChldrnNm());
                    Util.setSharedPreference(MainActivity.this, "age", "0");
                }else {
                    ChildItem childItem = mChildMenuItem.get(mChildChoiceIndex);
                    // 아이 사진 세팅

                    mActionBar_btn.setVisibility(View.GONE);
                    mBabyInfoMain_1.setVisibility(View.VISIBLE);

                    mBabyMenu.setVisibility(View.VISIBLE);
                    mFetusMenu.setVisibility(View.GONE);

                    mBtnLeftBaby.setContentDescription("HL01_002_001_!");
                    mBtnRightBaby.setContentDescription("HL01_002_001_!");

                    final Date mDate = Util.getDateFormat(childItem.getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);
                    int year = mDate.getYear() + 1900;
                    int month = mDate.getMonth() + 1;
                    int day = mDate.getDate();
                    int age = 0;

                    diffday = Util.GetDifferenceOfDate(currentCal.get(Calendar.YEAR), (currentCal.get(Calendar.MONTH) + 1), currentCal.get(Calendar.DATE),
                            year, month, day);

                    GLog.i("diffDay = " + diffday, "dd");

                    age = diffday / CommonData.YEAR;

                    mTxtBabyBornToDay.setText(Util.getAfterBirth_New(MainActivity.this, currentCal.get(Calendar.YEAR), (currentCal.get(Calendar.MONTH) + 1), currentCal.get(Calendar.DATE),
                            year, month, day));

                    android.util.Log.i(TAG, "Lifyea: "+mDate + currentCal.get(Calendar.YEAR)+ (currentCal.get(Calendar.MONTH) + 1) + currentCal.get(Calendar.DATE) + year + month + day );


                    mTxtBabyName.setText(childItem.getmChldrnNcm().length() > 0 ?childItem.getmChldrnNcm() : childItem.getmChldrnNm());
                    mTxtBabyBirth.setText(format.format(mDate));

                    mTxtAgeLine.setVisibility(View.VISIBLE);
                    mTxtBabyAge.setVisibility(View.VISIBLE);
                    mTxtBabyAge.setText(getString(R.string.man) + CommonData.STRING_SPACE + age + getString(R.string.age));
                    Util.setSharedPreference(MainActivity.this, "age", String.valueOf(age));

//                    mtype_title1.setText(childItem.getmChldrnBdwgh() + "kg");

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 아이 정보 화면 설정
     */
    public void setBabyData_adapter(int position){
        // 자녀 데이터가 있는경우 UI 세팅
        try{

            if(!mChildMenuItem.get(position).getmChlSn().equals("")) {
                Logger.i(TAG,"mChildChoiceIndex_adapter : "+position + "||"+mChildMenuItem.get(position).getmChldrnOrgImage() );

                if(mChildMenuItem.get(position).getmChlExistYn().equals(CommonData.NO)){

                    long D_days;
                    long P_days = 280;
                    final Date mDate = Util.getDateFormat(mChildMenuItem.get(position).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);

                    D_days = Util.getTwoDateCompare(mChildMenuItem.get(position).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
                    if (D_days > 0) {
                        period = 40;
                    } else {


                        //임신일짜
                        P_days = CommonData.AFTER_BIRTH_PERIOD - Math.abs(D_days);

                        //ssshin 임시주기 수정  //계산방법 : 40 – { (출산예정일과 오늘 사이의 일 수)/7 }    중괄호 안은 소수점 첫째 자리에서 올림 처리한다
                        double tempDdays = (double) Math.abs(D_days)/7;
                        period = (long) (40 - Math.ceil(tempDdays));

                    }

                    if(period == 0 ) period =1;

                    //임신주기에 따른 말풍선 문구
                    Fetus_BubbleId = "fetus" + period + "_" + num;
                    resStringId = getResources().getIdentifier(Fetus_BubbleId, "string", MAIN_ACTIVITY.getPackageName());
                    mTxtSpeech_Bubble.setText(getString(resStringId));

                    mTxtSpeech_Bubble.setVisibility(View.VISIBLE);

                    mImgBabyPhoto.setVisibility(View.GONE);
                    mImgfetusphoto.setVisibility(View.VISIBLE);

                    //기본 셋팅
                    setfetus_img(P_days);

                }else {
                    ChildItem childItem = mChildMenuItem.get(position);
                    // 아이 사진 세팅
                    CustomImageLoader.clearCache();
                    CustomImageLoader.displayImageMain(this, childItem.getmChldrnOrgImage(), mImgBabyPhoto);
                    mImgfetusphoto.setVisibility(View.GONE);
                    mImgBabyPhoto.setVisibility(View.VISIBLE);

                    mTxtSpeech_Bubble.setVisibility(View.GONE);
                    mBackLayout.setBackgroundResource(R.color.color_FFF6E6);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * getCurrentView
     */
    public void getCurrentView(){
        Logger.i("getCurrentView","getCurrentView : "+ tempCategory);

        switch (tempCategory){
            case 0:
                requestDiseaseDataApi();
                break;
            case 1:
                requestDiseaseDataApi();
                break;
            case 2:

                break;
            case 3:
                requestGrowthLastDataApi(mChildMenuItem.get(mChildChoiceIndex).getmChlSn());
                break;
            case 4:
                break;

        }

    }

    public class ViewPagerAdapter extends PagerAdapter {
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View view = inflater.inflate(R.layout.main_adapter, null);
            container.addView(view);

            mBackLayout = (FrameLayout)view.findViewById(R.id.framebackgroud);
            mImgfetusphoto = (ImageView) view.findViewById(R.id.img_fetus_photo);
            mImgBabyPhoto = (ImageView) view.findViewById(R.id.img_baby_photo);
            mTxtSpeech_Bubble = (TextView)view.findViewById(R.id.speech_bubble);

            mTxtSpeech_Bubble.setTag("mTxtSpeech_Bubble"+position);

            mImgfetusphoto.setOnClickListener(btnListener);
            mImgBabyPhoto.setOnClickListener(btnListener);
            mBtnLeftBaby.setOnClickListener(btnListener);
            mBtnRightBaby.setOnClickListener(btnListener);

            //click 저장
            OnClickListener mClickListener = new OnClickListener(btnListener,view,MainActivity.this);

            //홈
            mImgfetusphoto.setOnTouchListener(mClickListener);
            mImgBabyPhoto.setOnTouchListener(mClickListener);
            mBtnLeftBaby.setOnTouchListener(mClickListener);
            mBtnRightBaby.setOnTouchListener(mClickListener);

            //코드 부여(홈)
            mImgfetusphoto.setContentDescription(getString(R.string.Imgfetusphoto));
            mImgBabyPhoto.setContentDescription(getString(R.string.ImgBabyPhoto));

            Logger.i(TAG,"instantiateItem : "+position );
            setBabyData_adapter(position);


            return view;
        }

        @Override
        public int getCount() {
            return mChildMenuItem.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

//        @Override
//        public int getItemPosition(@NonNull Object object) {
//            return POSITION_NONE;
//        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((CustomViewPager) container).removeView((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            TextView temp1 = viewPager.findViewWithTag("mTxtSpeech_Bubble"+mChildChoiceIndex);
            temp1.setText(resStringId);
        }
    }

    /**
     * ViewPager 전환시 호출되는 메서드
     */
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            mChildChoiceIndex = position;
            setBabyData();
            setDefaultCategory();
            CommonData.getInstance().setSelectChildSn(mChildMenuItem.get(mChildChoiceIndex).getmChlSn());

            Logger.i(TAG,"onPageSelected:"+position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    /**
     * sub뷰페이저
     */
    public class ViewPagerSubAdapter extends PagerAdapter {
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View view = inflater.inflate(R.layout.main_sub_adapter, null);
            container.addView(view);

            mCategory_img = (ImageView) view.findViewById(R.id.category_img);

            mSub_title = (TextView) view.findViewById(R.id.sub_title) ;
            mType_title = (TextView) view.findViewById(R.id.type_title);
            mtype_title1 = (TextView) view.findViewById(R.id.type_title1);
            mType_content = (TextView) view.findViewById(R.id.type_content);
            mBottom_sub_title = (TextView) view.findViewById(R.id.bottom_sub_title);
            mDisease_title1 = (TextView) view.findViewById(R.id.disease_title1);
            mDisease_percent1 = (TextView) view.findViewById(R.id.disease_percent1);
            mDisease_title2 = (TextView) view.findViewById(R.id.disease_title2);
            mDisease_percent2 = (TextView) view.findViewById(R.id.disease_percent2);
            mBottom_sub_title1 = (TextView) view.findViewById(R.id.bottom_sub_title1);

            mSub_title.setTag("sub_title"+position);
            mType_title.setTag("mType_title"+position);
            mtype_title1.setTag("mtype_title1"+position);
            mType_content.setTag("mType_content"+position);
            mBottom_sub_title.setTag("mBottom_sub_title"+position);
            mDisease_title1.setTag("mDisease_title1"+position);
            mDisease_percent1.setTag("mDisease_percent1"+position);
            mDisease_title2.setTag("mDisease_title2"+position);
            mDisease_percent2.setTag("mDisease_percent2"+position);
            mBottom_sub_title1.setTag("mBottom_sub_title1"+position);



            mSub_contents1 = (LinearLayout) view.findViewById(R.id.sub_contents1);
            mSub_contents2 = (LinearLayout) view.findViewById(R.id.sub_contents2);
            mSub_contents3 = (LinearLayout) view.findViewById(R.id.sub_contents3);

            view.findViewById(R.id.btn_gVideo_main).setOnClickListener(btnListener);
            view.findViewById(R.id.btn_scVideo_main).setOnClickListener(btnListener);
            view.findViewById(R.id.btn_tVideo_main).setOnClickListener(btnListener);
            mBottom_sub_title.setOnClickListener(btnListener);
            mBottom_sub_title1.setOnClickListener(btnListener);
            mCategory_img.setOnClickListener(btnListener);

            Logger.i(TAG,"subinstantiateItem : "+position );

            //click 저장
            OnClickListener mClickListener = new OnClickListener(btnListener,view,MainActivity.this);

            //홈
            view.findViewById(R.id.btn_gVideo_main).setOnTouchListener(mClickListener);
            view.findViewById(R.id.btn_scVideo_main).setOnTouchListener(mClickListener);
            view.findViewById(R.id.btn_tVideo_main).setOnTouchListener(mClickListener);
            mBottom_sub_title.setOnTouchListener(mClickListener);
            mBottom_sub_title1.setOnTouchListener(mClickListener);

            //클릭 부여(홈)
            view.findViewById(R.id.btn_gVideo_main).setContentDescription(getString(R.string.btn_gVideo_main));
            view.findViewById(R.id.btn_tVideo_main).setContentDescription(getString(R.string.btn_tVideo_main));
            view.findViewById(R.id.btn_scVideo_main).setContentDescription(getString(R.string.btn_scVideo_main));



            setCategory(position,true);

            return view;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View)object);
        }

        @Override
        public void notifyDataSetChanged() {
            if(tempCategory == 0){
                TextView temp1 = subviewPager.findViewWithTag("mType_title0");
                temp1.setText(tempLocal);

                TextView temp2 = subviewPager.findViewWithTag("mType_content0");
                temp2.setText(tempTempeture);

                TextView temp3 = subviewPager.findViewWithTag("mBottom_sub_title10");
                temp3.setText(tempBottomtext);
                if(tempLocal.equals("-")){
                    is_temp_setting = false;
                    temp3.setContentDescription("HL01_005_001_!");
                }else{
                    is_temp_setting = true;
                    temp3.setContentDescription("HL01_005_002_!");
                }
            } else if(tempCategory == 1){
                TextView temp1 = subviewPager.findViewWithTag("mDisease_title11");
                temp1.setText(tempDisease1);
                TextView temp2 = subviewPager.findViewWithTag("mDisease_title21");
                temp2.setText(tempDisease2);
                TextView temp3 = subviewPager.findViewWithTag("mDisease_percent11");
                temp3.setText(tempDisease_percent1);
                TextView temp4 = subviewPager.findViewWithTag("mDisease_percent21");
                temp4.setText(tempDisease_percent2);
            } else if(tempCategory == 3){
                TextView temp1 = subviewPager.findViewWithTag("mType_title3");
                temp1.setText(tempWeight1);
                TextView temp2 = subviewPager.findViewWithTag("mtype_title13");
                temp2.setText(tempWeight2);
                TextView temp3 = subviewPager.findViewWithTag("mType_content3");
                temp3.setText(tempBmi);
            }
        }
    }

    /**
     * SubViewPager 전환시 호출되는 메서드
     */
    private ViewPager.OnPageChangeListener mOnPageChangeSubListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {

            if(tempCategory != position) {
                tempCategory = position;
                indicator.selectDot(tempCategory);
                CommonData.getInstance().setMain_Category(Integer.toString(tempCategory));
                setDefaultCategory();
            }



            Logger.i(TAG,"subonPageSelected:"+position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if(state == subviewPager.SCROLL_STATE_DRAGGING){

            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        GLog.i("onResume", "dd");
        if(mJunChangeChild){
            saveChild();
            mJunChangeChild = false;
        }

        /**
         * 만삭체중예측 버튼 문진했고 임신중이면 보이게
         */

        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
            if(CommonData.getInstance().getbirth_chl_yn().equals("N")){
                mBtnSide_10.setVisibility(View.VISIBLE);
            } else {
                mBtnSide_10.setVisibility(View.GONE);
            }
        }else {
            mBtnSide_10.setVisibility(View.GONE);
        }


        /**
         * 만삭체중예측 버튼 문진했고 임신중이면 보이게
         */

        if(!CommonData.getInstance().getMotherBigPopupShowCheck()) {
            if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                if (CommonData.getInstance().getbirth_chl_yn().equals("N")) {
                    CustomAlertDialog mDialog2 = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_F);
                    View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.popup_dialog_noview, null);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params.gravity = Gravity.CENTER;
                    mDialog2.setContentView(view, params);
                    mDialog2.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        dialog.dismiss();
                    });

                    mDialog2.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog, button) -> {
                        dialog.dismiss();

                        Intent intent;
                        if (CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                            hideProgress();
                            CustomAlertDialog mDialog1 = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog1.setTitle(getString(R.string.popup_dialog_a_type_title));
                            mDialog1.setContent(getString(R.string.i_am_child));
                            mDialog1.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                            mDialog1.show();

                        } else {
                            if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                                DummyActivity.startActivityForResult(((Activity) MainActivity.this), WeightBigDataInputFragment.REQ_WEIGHT_PREDICT, WeightBigDataInputFragment.class, new Bundle());
                            } else {
                                intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                                startActivity(intent);
                            }

                        }

                    });

                    mDialog2.setCheckboxButtonLv((dialog, button) -> {
                        mDialog2.setChangeCheckboxImg_motherBig();
                        CommonData.getInstance().setMotherBigPopupShowCheck(mDialog2.isMotherChecked());
                    });

                    mDialog2.setCancelable(false);
                    mDialog2.show();
                }
            }
        }


        requestLastAlramDataApi();
        setBabyData();
        setDefaultCategory_adpater();
        indicator.selectDot(tempCategory);
        //swiper

        ViewPagerSubAdapter = new ViewPagerSubAdapter();
        subviewPager.setAdapter(ViewPagerSubAdapter);
        subviewPager.setOffscreenPageLimit(5);
        subviewPager.setCurrentItem(tempCategory);
        subviewPager.setPageTransformer(true, new CustomPageTransformer());

        subviewPager.addOnPageChangeListener(mOnPageChangeSubListener);

        ViewPagerAdapter = new ViewPagerAdapter();
        viewPager.setAdapter(ViewPagerAdapter);
        viewPager.setOffscreenPageLimit(mChildMenuItem.size());
        viewPager.setCurrentItem(mChildChoiceIndex);
        viewPager.addOnPageChangeListener(mOnPageChangeListener);
        requestBBSListApi();
//

        int push_type = getIntent().getIntExtra(CommonData.EXTRA_PUSH_TYPE, 0);
        GLog.i("onResume = "+push_type, "dd");
        if(push_type > 0){
            Intent intent;
            typePush = push_type;
            switch (push_type){
                case FirebaseMessagingService.NOTICE:
                    break;
                case FirebaseMessagingService.NEWS:
                    String info_sn = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);
                    intent = new Intent(MainActivity.this, BBSActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    intent.putExtra(CommonData.EXTRA_INFO_SN, info_sn);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case FirebaseMessagingService.FEVER:
                    intent = new Intent(MainActivity.this, FeverMapActivity.class);
                    inTentGo = intent;
                    /* hsh old
                    startActivity(intent);
                    BleUtil.BackAnimationStart(MainActivity.this);*/
                    break;
                //hsh start
                case FirebaseMessagingService.FEVER_MOVIE:
                    GLog.i("FirebaseMessagingService.FEVER_MOVIE", "dd");
                    String info = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);
                    intent = new Intent(MainActivity.this, YoutubeActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    intent.putExtra(CommonData.EXTRA_INFO_SN, info);
                    inTentGo = intent;
                    break;
                case FirebaseMessagingService.DIESEASE:
                    GLog.i("FirebaseMessagingService.DIESEASE", "dd");
                    intent = new Intent(MainActivity.this, FeverMapActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    inTentGo = intent;
                    break;
                case FirebaseMessagingService.DIET: //다이어트 독려
                    GLog.i("FirebaseMessagingService.DIET", "dd");
                    intent = new Intent(MainActivity.this, MotherHealthMainActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case FirebaseMessagingService.ALIMI: //알리미 공지
                    GLog.i("FirebaseMessagingService.ALIMI", "dd");
                    intent = new Intent(MainActivity.this, AlramMainActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case 8: //엄마 건강
                    GLog.i("8:엄마 건강", "dd");
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else{
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent = new Intent(MainActivity.this, MotherHealthMainActivity.class);
                        } else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                        Util.BackAnimationStart(MainActivity.this);
                        startActivity(intent);
                    }

                    break;
                case 9: //아이심리
                    GLog.i("9:아이심리", "dd");
                    intent = new Intent(MainActivity.this, PsyMainActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case 10: //아이성장
                    GLog.i("10:아이성장", "dd");
                    intent = new Intent(MainActivity.this, GrowthMainActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case 11: //심리음원
                    GLog.i("11:심리음원", "dd");
                    intent = new Intent(MainActivity.this, PsyMainActivity.class);
                    intent.putExtra(CommonData.EXTRA_PUSH_TYPE, push_type);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                //hsh end
            }
        }

        getIntent().removeExtra(CommonData.EXTRA_PUSH_TYPE);
        getIntent().removeExtra(CommonData.EXTRA_INFO_SN);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GLog.i("onStop", "dd");
    }


    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            String str = "";
            Fragment fragment = null;

            intent = new Intent(MainActivity.this, EduVideoActivity.class);

            GLog.i("v.getId() = " +v.getId(), "dd");
            showProgress();
            switch (v.getId()){
                case R.id.bottom_sub_title1:
                case R.id.bottom_sub_title:
                case R.id.category_img:
                    switch (main_spinner.getSelectedItemPosition()) {
                        case 0: //열지도 이동
                            if(is_temp_setting) {
                                GpsInfo gps = new GpsInfo(MainActivity.this);
                                if (gps.isGetLocation()) {
                                    intent = new Intent(MainActivity.this, FeverMapActivity.class);
                                    startActivity(intent);

                                } else {
                                    gps.showSettingsAlert();
                                    hideProgress();
                                }
                            }else{
                                intent = new Intent(MainActivity.this, SettingAddressActivity.class);
                                startActivity(intent);
                            }
                            break;
                        case 1: //유의질환 이동
                            intent = new Intent(MainActivity.this, FeverMapActivity.class);
                            intent.putExtra("tabNum" , 1);
                            intent.putExtra(CommonData.EXTRA_MAIN_TYPE,1);
                            startActivity(intent);
                            break;
                        case 2: //엄마건강 이동
                            if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                                mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.i_am_child));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                                mDialog.show();

                            }else{
                                if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                                    intent = new Intent(MainActivity.this, MotherHealthMainActivity.class);
                                } else {
                                    intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                                }
                                startActivity(intent);
                            }
                            break;
                        case 3: //아이성장 메인이동
                            startActivity(new Intent(MainActivity.this, GrowthMainActivity.class));
                            break;

                    }
                    hideProgress();

                    break;
                case R.id.btn_gVideo_main :
                    intent.putExtra("TITLE", getString(R.string.edu_video_title_03));
                    intent.putExtra("ML_MCODE", "2");
                    startActivity(intent);
                    break;
                case R.id.btn_scVideo_main :
                    intent.putExtra("TITLE", getString(R.string.edu_video_title_05));
                    intent.putExtra("ML_MCODE", "1");
                    startActivity(intent);
                    break;
                case R.id.btn_tVideo_main :
                    intent.putExtra("TITLE", getString(R.string.edu_video_title_04));
                    intent.putExtra("ML_MCODE", "3");
                    startActivity(intent);
                    break;
                case R.id.list_add_change :
                    hideProgress();
                    main_category();
                    break;
                case R.id.btn_fetus_info :
                    showFetusInfo();
                    break;
                case R.id.btn_weight_main:
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else {
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent = new Intent(MainActivity.this, MotherHealthMainActivity.class);
                            intent.putExtra("Num", 0);
                        } else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                    }
                    startActivity(intent);
                    break;
                case R.id.btn_play_main:
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else {
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent =  new Intent(MainActivity.this, MotherHealthMainActivity.class);
                            intent.putExtra("Num",1);
                        } else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                    }
                    startActivity(intent);
                    break;
                case R.id.mBtnMealMain:
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else {
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent =  new Intent(MainActivity.this, MotherHealthMainActivity.class);
                            intent.putExtra("Num",2);
                        }else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                    }
                    startActivity(intent);
                    break;
                case R.id.mBtnPressureMain:
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else {
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent =  new Intent(MainActivity.this, MotherHealthMainActivity.class);
                            intent.putExtra("Num",3);
                        }else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                    }
                    startActivity(intent);
                    break;
                case R.id.mBtnSugarMain:
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else {
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent =  new Intent(MainActivity.this, MotherHealthMainActivity.class);
                            intent.putExtra("Num",4);
                        }
                        else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                    }
                    startActivity(intent);
                    break;
                case R.id.btn_medycare:
                    intent = new Intent(MainActivity.this, BackWebViewInfoActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, "https://wkd.walkie.co.kr/HL_FV/INFO/m_info_and.asp");
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.title_medicare1));
                    startActivity(intent);
                    break;
                case R.id.btn_growth_main:
                case R.id.btn_side_01:
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(MainActivity.this, GrowthMainActivity.class));
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.btn_fever_main:
                case R.id.btn_side_02:
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(MainActivity.this, FeverMainActivity.class));
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.img_fetus_photo:
                    Logger.i("img_fetus_photo","img_fetus_photo :"+num);
                    hideProgress();
                    if (num == 10) {
                        num = 0;
                    }
                    num++;
                    Fetus_BubbleId = "fetus" + period + "_" + num;
                    resStringId = getResources().getIdentifier(Fetus_BubbleId, "string", MAIN_ACTIVITY.getPackageName());
//                    mTxtSpeech_Bubble.setText(getString(resStringId));
                    ViewPagerAdapter.notifyDataSetChanged();
                    break;
                case R.id.img_baby_photo:
                case R.id.btn_side_03:
                    mDrawerLayout.closeDrawers();
                    intent = new Intent(MainActivity.this, BabyInfoActivity.class);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.btn_side_05:
                    mDrawerLayout.closeDrawers();
                    intent = new Intent(MainActivity.this, SettingActivity.class);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.btn_side_06:
                case R.id.btn_HNews:
                    mDrawerLayout.closeDrawers();
                    intent = new Intent(MainActivity.this, BBSActivity.class);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.btn_side_04:
                case R.id.btn_epidemic :
                    mDrawerLayout.closeDrawers();
                    GpsInfo gps = new GpsInfo(MainActivity.this);
                    if(gps.isGetLocation()){
                        intent = new Intent(MainActivity.this, FeverMapActivity.class);
                        startActivity(intent);
                        Util.BackAnimationStart(MainActivity.this);
                    }else{
                        gps.showSettingsAlert();
                        hideProgress();
                    }
                    break;
                case R.id.mBtnCallMain :
                    hideProgress();
                    if(CommonData.getInstance().getMberGrad().equals("10")){
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_D);
                        mDialog.setCall(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_B);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.do_call_center));
                                mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                                    String tel = "tel:" + getString(R.string.call_center_number);
                                    Intent intentCall = new Intent(Intent.ACTION_DIAL);
                                    intentCall.setData(Uri.parse(tel));
                                    startActivity(intentCall);
                                    dialog.dismiss();
                                });
                                mDialog.show();
                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(CommonData.getInstance().getHiPlannerHp().length() > 0){
                                    mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_B);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.do_call_hiplanner).replace("[tel]", CommonData.getInstance().getHiPlannerHp()));
                                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                                        String tel = "tel:" + CommonData.getInstance().getHiPlannerHp();
                                        Intent intentTel = new Intent(Intent.ACTION_DIAL);
                                        intentTel.setData(Uri.parse(tel));
                                        startActivity(intentTel);
                                        dialog.dismiss();
                                    });
                                    mDialog.show();
                                }else{
                                    mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.empty_hiplaaner));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                                    mDialog.show();
                                }

                            }
                        });
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_cancel), null);
                        mDialog.show();

                    }else{
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_B);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.call_center2));
                        mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                        mDialog.setPositiveButton(getString(R.string.do_call), (dialog, button) -> {
                            String tel = "tel:" + getString(R.string.call_center_number2);
//                        startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                            Intent intentCall = new Intent(Intent.ACTION_DIAL);
                            intentCall.setData(Uri.parse(tel));
                            startActivity(intentCall);
                            dialog.dismiss();
                        });
                        mDialog.show();
                    }
                    break;
                case R.id.mBtnPsyMain :
                    intent = new Intent(MainActivity.this, PsyMainActivity.class);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);

                    break;
                case R.id.btn_side_07:
                    // 체온 영상 보기
                    mDrawerLayout.closeDrawers();
                    intent = new Intent(MainActivity.this, YoutubeActivity.class);
                    startActivity(intent);
                    Util.BackAnimationStart(MainActivity.this);
                    break;
                case R.id.menu_btn: // 우측메뉴
                    hideProgress();
                    Log.i(TAG,"onDrawerClosed mChildMenuItem.get(mChildChoiceIndex).getmChlExistYn() :" + mChildMenuItem.get(mChildChoiceIndex).getmChlExistYn());
                    if (mChildMenuItem.get(mChildChoiceIndex).getmChlExistYn().equals("N")) {
                        mBtnSide_01.setEnabled(false);
                        mBtnSide_02.setEnabled(false);
                        mBtnSide_99.setEnabled(false);
                    } else {
                        mBtnSide_01.setEnabled(true);
                        mBtnSide_02.setEnabled(true);
                        mBtnSide_99.setEnabled(true);
                    }
                    mDrawerLayout.openDrawer(GravityCompat.END);
                    break;
                case R.id.new_message :
                case R.id.alram_btn: // 알림메뉴
                    hideProgress();
                    intent = new Intent(MainActivity.this, AlramMainActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_left_baby:
                    if (mChildChoiceIndex > 0) {

                        mChildChoiceIndex--;
                        viewPager.setCurrentItem(mChildChoiceIndex);
                        setBabyData();
                        setDefaultCategory();
                        CommonData.getInstance().setSelectChildSn(mChildMenuItem.get(mChildChoiceIndex).getmChlSn());
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.first_baby), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_right_baby:
                    if (mChildChoiceIndex < mChildMenuItem.size() - 1) {

                        mChildChoiceIndex++;
                        viewPager.setCurrentItem(mChildChoiceIndex);
                        setBabyData();
                        setDefaultCategory();
                        CommonData.getInstance().setSelectChildSn(mChildMenuItem.get(mChildChoiceIndex).getmChlSn());
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.last_baby), Toast.LENGTH_SHORT).show();
                    }
                    break;

                /* 아이 심리 슬라이드버튼 */
                case R.id.btn_side_99:
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(MainActivity.this, PsyMainActivity.class));
                    Util.BackAnimationStart(MainActivity.this);
                    break;

                case R.id.mBtnMomHealthMain :
                case R.id.btn_side_98:
                    mDrawerLayout.closeDrawers();
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else{
                        Boolean temp = CommonData.getInstance().getHpMjYnJun();
                        Log.i(TAG,"getHpMjYnJun : "+temp);
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            intent = new Intent(MainActivity.this, MotherHealthMainActivity.class);
                        } else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                        }
                        startActivity(intent);
                    }
                    break;

                case R.id.btn_side_10:
                    mDrawerLayout.closeDrawers();
                    if(CommonData.getInstance().getIamChild().compareTo("Y") == 0) {
                        hideProgress();
                        mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.i_am_child));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();

                    }else{
                        if (CommonData.getInstance().getHpMjYn().compareTo("Y") == 0 && CommonData.getInstance().getHpMjYnJun()) {
                            DummyActivity.startActivityForResult(((Activity) MainActivity.this), WeightBigDataInputFragment.REQ_WEIGHT_PREDICT, WeightBigDataInputFragment.class, new Bundle());
                        } else {
                            intent = new Intent(MainActivity.this, MotherHealthRegActivity.class);
                            startActivity(intent);
                        }

                    }
                    break;

                case R.id.btn_side_08:
                    hideProgress();
                    mDrawerLayout.closeDrawers();
                    KakaoLinkUtil.kakaoAddFriends(MainActivity.this);
                    break;

                case R.id.hompage_btn:
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_homepage))));
                    break;


            }

            if (!str.equals("")) {
                mTitleTv.setText(str);
            }
        }
    };
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)){
            drawer.closeDrawer(GravityCompat.END);
        } else {
//            super.onBackPressed();
            backPressCloseHandler.onBackPressed();
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        GLog.i("onNewIntent", "dd");

        if ( CommonData.getInstance().getMemberId() == 0 ) {
            GLog.i("CommonData.getInstance().getMemberId() == 0", "dd");
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }

    /**
     * 시기별 정보 Dialog
     */
    private void showFetusInfo() {
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.fetus_period_webview, null);
        final CPeriodDialog dlg = CPeriodDialog.showDlg(MainActivity.this, view, new CPeriodDialog.DismissListener() {
            @Override
            public void onDissmiss() {

            }
        });
        text_period = (TextView) view.findViewById(R.id.period_date_textview);
        ImageButton pre_btn = (ImageButton) view.findViewById(R.id.pre_btn);
        ImageButton next_btn = (ImageButton) view.findViewById(R.id.next_btn);

        pre_btn.setOnClickListener(mDialogBtn);
        next_btn.setOnClickListener(mDialogBtn);
        url = "http://www.higngkids.co.kr/auth/HL_moms_contents_view.asp?Wkey=";

        text_period.setText(period + "주");
        webView =(WebView) view.findViewById(R.id.web_view);
        count = period;

        webView.setWebViewClient(new MainActivity.TermsWebViewClinet());
        webView.setWebChromeClient(new MainActivity.TermsWebViewChromeClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setSavePassword(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        webView.setBackgroundColor(Color.TRANSPARENT);

        if ( Build.VERSION.SDK_INT >= 11 )
            webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        webView.loadUrl(url+period);


    }

    private class TermsWebViewChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message,
                                 JsResult result) {
            // TODO Auto-generated method stub
            return super.onJsAlert(view, url, message, result);
        }
    }

    private class TermsWebViewClinet extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//            handler.proceed(); // SSL 에러가 발생해도 계속 진행!
            GLog.i("onReceivedSslError()", "dd");
            mDialog = new CustomAlertDialog(MainActivity.this, CustomAlertDialog.TYPE_B);
            mDialog.setTitle(getResources().getString(R.string.popup_dialog_a_type_title));
            mDialog.setContent(getResources().getString(R.string.popup_dialog_serucity_content));
            mDialog.setPositiveButton(getResources().getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {

                @Override
                public void onClick(CustomAlertDialog dialog, Button button) {
                    handler.proceed(); // SSL 에러가 발생해도 계속 진행!
                    dialog.dismiss();
                }
            });
            mDialog.setNegativeButton(null, new CustomAlertDialogInterface.OnClickListener() {

                @Override
                public void onClick(CustomAlertDialog dialog, Button button) {
                    handler.cancel();    // 취소
                    dialog.dismiss();
                }
            });
            mDialog.show();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgress();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideProgress();
            super.onPageFinished(view, url);
        }

    }

    View.OnClickListener mDialogBtn = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.pre_btn:
                    if(count > 1) {
                        --count;
                        text_period.setText(count + "주");
                        webView.loadUrl(url+count);
                    }

                    break;
                case R.id.next_btn:
                    if(count < 40) {
                        ++count;
                        text_period.setText(count + "주");
                        webView.loadUrl(url+count);
                    }

                    break;
            }
        }

    };


    /**
     * 컨텐츠 변경
     */
  public void main_category() {
      main_spinner.performClick();
      main_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

          @Override
          public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//              setCategory(position);
//              tempCategory = position;
              Logger.i(TAG,"main_spinner :" + view);
                  // _!: 카운트만 되는 버튼 끝에 붙임
              int j = position +1;
              view.setContentDescription("HL01_004_00"+j+"_!");
              if (view.getContentDescription().toString().contains("_!")) {
                  String temp = view.getContentDescription().toString().replace("_!", "");
                  String cod[] = temp.split("_");

                  DBHelper helper = new DBHelper(MainActivity.this);
                  DBHelperLog logdb = helper.getLogDb();

                  if (cod.length == 1) {
                      logdb.insert(cod[0], "", "", 0, 1);
                      Log.i(TAG, "view.contentDescription : " + cod[0] + "count : 1");
                  } else if (cod.length == 2) {
                      logdb.insert(cod[0], cod[1], "", 0, 1);
                      Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + "count : 1");
                  } else {
                      logdb.insert(cod[0], cod[1], cod[2], 0, 1);
                      Log.i(TAG, "view.contentDescription : " + cod[0] + cod[1] + cod[2] + "count : 1");
                  }

              }
              Log.i(TAG,"ACTION_UP");

              CommonData.getInstance().setMain_Category(Integer.toString(position));
              subviewPager.setCurrentItem(position);
          }

          @Override
          public void onNothingSelected(AdapterView<?> parent) {
          }
      });
  }

    public void setDefaultCategory(){
      android.util.Log.i("Main_category", "setDefaultCategory: "+CommonData.getInstance().getMain_Category());
      hideProgress();

        //임신여부
        CommonData common = CommonData.getInstance();
        String materPregency = common.getMotherIsPregnancy();
        String iamChild = common.getIamChild();

        //첫쨰
        String yyyyMMdd = Util.getNowYYYYDateFormat();
        String firstChild = yyyyMMdd;
        int year=0;
        int month=0;
        int day=0;
        for (int i = 0; i < mChildMenuItem.size(); i++){

            ChildItem iChildItem = mChildMenuItem.get(i);



            String chldrn_lifyea = iChildItem.getmChldrnLifyea();
            if (!chldrn_lifyea.equals("000000")
                    && chldrn_lifyea.length() > 0) {
                if (i==0) {
                    firstChild = "20"+chldrn_lifyea;
                    final Date mDate = Util.getDateFormat(iChildItem.getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);
                    year = mDate.getYear() + 1900;
                    month = mDate.getMonth() + 1;
                    day = mDate.getDate();
                }else {
                    String temp = "20"+chldrn_lifyea;
                    if ( StringUtil.getIntger(temp) < StringUtil.getIntger(firstChild)) {

                        firstChild = "20"+chldrn_lifyea;
                        final Date mDate = Util.getDateFormat(iChildItem.getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);
                        year = mDate.getYear() + 1900;
                        month = mDate.getMonth() + 1;
                        day = mDate.getDate();
                    }
                }
            }
        }

       int difday = Util.GetDifferenceOfDate(currentCal.get(Calendar.YEAR), (currentCal.get(Calendar.MONTH) + 1), currentCal.get(Calendar.DATE),
                year, month, day);





      if("".equals(CommonData.getInstance().getMain_Category())) {
          if("Y".equals(iamChild)) {
              main_spinner.setSelection(4); //교육영상
              setCategory(4,false);
          }
          else if ("Y".equals(materPregency))  { //임신중일경우
              if ("Y".equals(CommonData.getInstance().getTempDivices())) { //체온계여부
                  main_spinner.setSelection(0); //지역 체온
                  setCategory(0,false);
              } else {
                  main_spinner.setSelection(2); //엄마체중
                  setCategory(2,false);
              }
          } else {
              if ("Y".equals(CommonData.getInstance().getWeighingchk())) { //체중계 여부

                  if (difday > CommonData.AFTER_BIRTH_1095) { //3년 경과시
                      main_spinner.setSelection(3); //아이체중
                      setCategory(3,false);
                  }
                  else{
                      main_spinner.setSelection(2); //엄마체중
                      setCategory(2,false);
                  }
              }else{
                  main_spinner.setSelection(1); //유의질환
                  setCategory(1,false);
              }
          }
      }
      else{
          main_spinner.setSelection(StringUtil.getIntger(CommonData.getInstance().getMain_Category()));
          setCategory(StringUtil.getIntger(CommonData.getInstance().getMain_Category()),false);
          android.util.Log.i("Main_category", "setDefaultCategory: "+CommonData.getInstance().getMain_Category());
      }

        getCurrentView();

    }


    public void setDefaultCategory_adpater(){
        android.util.Log.i("Main_category", "setDefaultCategory: "+CommonData.getInstance().getMain_Category());
        hideProgress();

        //임신여부
        CommonData common = CommonData.getInstance();
        String materPregency = common.getMotherIsPregnancy();
        String iamChild = common.getIamChild();

        //첫쨰
        String yyyyMMdd = Util.getNowYYYYDateFormat();
        String firstChild = yyyyMMdd;
        int year=0;
        int month=0;
        int day=0;
        for (int i = 0; i < mChildMenuItem.size(); i++){

            ChildItem iChildItem = mChildMenuItem.get(i);



            String chldrn_lifyea = iChildItem.getmChldrnLifyea();
            if (!chldrn_lifyea.equals("000000")
                    && chldrn_lifyea.length() > 0) {
                if (i==0) {
                    firstChild = "20"+chldrn_lifyea;
                    final Date mDate = Util.getDateFormat(iChildItem.getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);
                    year = mDate.getYear() + 1900;
                    month = mDate.getMonth() + 1;
                    day = mDate.getDate();
                }else {
                    String temp = "20"+chldrn_lifyea;
                    if ( StringUtil.getIntger(temp) < StringUtil.getIntger(firstChild)) {

                        firstChild = "20"+chldrn_lifyea;
                        final Date mDate = Util.getDateFormat(iChildItem.getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_DOT);
                        year = mDate.getYear() + 1900;
                        month = mDate.getMonth() + 1;
                        day = mDate.getDate();
                    }
                }
            }
        }

        int difday = Util.GetDifferenceOfDate(currentCal.get(Calendar.YEAR), (currentCal.get(Calendar.MONTH) + 1), currentCal.get(Calendar.DATE),
                year, month, day);





        if("".equals(CommonData.getInstance().getMain_Category())) {
            if("Y".equals(iamChild)) {
                tempCategory = 4; //교육영상
                main_spinner.setSelection(4);
            }
            else if ("Y".equals(materPregency))  { //임신중일경우
                if ("Y".equals(CommonData.getInstance().getTempDivices())) { //체온계여부
                    tempCategory = 0; //지역 체온
                    main_spinner.setSelection(0);
                } else {
                    tempCategory = 2; //엄마체중
                    main_spinner.setSelection(2);
                }
            } else {
                if ("Y".equals(CommonData.getInstance().getWeighingchk())) { //체중계 여부

                    if (difday > CommonData.AFTER_BIRTH_1095) { //3년 경과시
                        tempCategory = 3; //아이체중
                        main_spinner.setSelection(3);
                    }
                    else{
                        tempCategory = 2; //엄마체중
                        main_spinner.setSelection(2);
                    }
                }else{
                    tempCategory = 1; //유의질환
                    main_spinner.setSelection(1);
                }
            }

        }
        else{
            tempCategory = StringUtil.getIntger(CommonData.getInstance().getMain_Category());
            main_spinner.setSelection(StringUtil.getIntger(CommonData.getInstance().getMain_Category()));
            android.util.Log.i("Main_category", "setDefaultCategory: "+CommonData.getInstance().getMain_Category());
        }

        getCurrentView();
    }

    public void onCloseMenu(View view){

        mDrawerLayout.closeDrawers();
    }


    public void setCategory(int poistion, boolean scroll){
        is_first_scroll = scroll;

        if(scroll == false)
            CommonData.getInstance().setMain_Category(Integer.toString(poistion));
        Logger.i(TAG, "setCategory["+poistion+"]["+mChildChoiceIndex+"]");

        switch (poistion){
            case 0:
                mSub_contents1.setVisibility(View.VISIBLE);
                mSub_contents2.setVisibility(View.GONE);
                mSub_contents3.setVisibility(View.GONE);
                mBottom_sub_title.setVisibility(View.GONE);
                mBottom_sub_title1.setVisibility(View.VISIBLE);
                mSub_title.setText(getString(R.string.main_category_0));
                Logger.i(TAG, "tempTempeture : "+ tempTempeture);
//                mType_title.setText(tempLocal);
//                mType_content.setText(tempTempeture);
//                mBottom_sub_title1.setText(tempBottomtext);
                mtype_title1.setText("");
                mCategory_img.setImageResource(R.drawable.icon_main_category1);
                mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_797979));


                break;
            case 1:
                mSub_contents1.setVisibility(View.GONE);
                mSub_contents2.setVisibility(View.GONE);
                mSub_contents3.setVisibility(View.VISIBLE);
                mBottom_sub_title.setVisibility(View.VISIBLE);
                mBottom_sub_title1.setVisibility(View.GONE);
//                mDisease_title1.setText(tempDisease1);
//                mDisease_title2.setText(tempDisease2);
//                mDisease_percent1.setText(tempDisease_percent1);
//                mDisease_percent2.setText(tempDisease_percent2);
                mSub_title.setText(getString(R.string.main_category_1));
                mBottom_sub_title.setText(getString(R.string.main_bottom_sub_title));
                mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_797979));
                mCategory_img.setImageResource(R.drawable.icon_main_category4);
                mBottom_sub_title.setText(getString(R.string.main_bottom_sub_title));
                mBottom_sub_title.setContentDescription("HL01_006_001_!");
                break;
            case 2:
                mSub_contents1.setVisibility(View.VISIBLE);
                mSub_contents2.setVisibility(View.GONE);
                mSub_contents3.setVisibility(View.GONE);
                mBottom_sub_title.setVisibility(View.VISIBLE);
                mBottom_sub_title1.setVisibility(View.GONE);
                mSub_title.setText(getString(R.string.main_category_2));

                String momKg = CommonData.getInstance().getMotherWeight();
                if(TextUtils.isEmpty(momKg) == false && StringUtil.getIntVal(momKg) > 0) {
                    mType_title.setText("현재 체중 :");
                    mtype_title1.setText(CommonData.getInstance().getMotherWeight() + "kg");
                    mType_content.setText(CommonData.getInstance().getKg_Kind());
                    setColorchange(CommonData.getInstance().getKg_Kind());
                }
                else{
                    mType_title.setText("체중을 입력해 주세요.");
                    mtype_title1.setText("");
                    mType_content.setText("--");
                    mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_797979));
                }
                mBottom_sub_title.setText(getString(R.string.main_bottom_sub_title));
                mCategory_img.setImageResource(R.drawable.icon_main_category2);
                mBottom_sub_title.setContentDescription("HL01_007_001_!");
                break;
            case 3:
                mSub_contents1.setVisibility(View.VISIBLE);
                mSub_contents2.setVisibility(View.GONE);
                mSub_contents3.setVisibility(View.GONE);
                mBottom_sub_title.setVisibility(View.VISIBLE);
                mBottom_sub_title1.setVisibility(View.GONE);
//                mType_title.setText(tempWeight1);
//                mtype_title1.setText(tempWeight2);
//                mType_content.setText(tempBmi);
                mSub_title.setText(getString(R.string.main_category_3));
                ChildItem item = mChildMenuItem.get(mChildChoiceIndex);

                Logger.i(TAG, "setCategory["+mChildChoiceIndex+"]"+item.getmChldrnNm());


                mBottom_sub_title.setText(getString(R.string.main_bottom_sub_title));
                mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_797979));
                mCategory_img.setImageResource(R.drawable.icon_main_category2);
                mBottom_sub_title.setContentDescription("HL01_008_001_!");
                break;
            case 4:
                mSub_contents1.setVisibility(View.GONE);
                mSub_contents2.setVisibility(View.VISIBLE);
                mSub_contents3.setVisibility(View.GONE);
                mSub_title.setText(getString(R.string.main_category_4));
                mBottom_sub_title.setVisibility(View.GONE);
                mBottom_sub_title1.setVisibility(View.GONE);
                mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_797979));
                mCategory_img.setImageResource(R.drawable.icon_main_category3);
                break;
        }
    }

    public void setColorchange(String type){
        if(type.contains("저체중군")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_F2A142));
        }else if(type.contains("정상체중군")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_73C066));
        } else if(type.contains("과체중군")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.color_ED6769));
        } else if(type.contains("비만군")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.color_CDACD2));
        } else if(type.contains("고도비만군")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.color_9ca3ac));
        }else if(type.contains("부족")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_F2A142));
        }else if(type.contains("적정")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_73C066));
        }else if(type.contains("초과")) {
            mType_content.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.color_9ca3ac));
        }
    }


    /**
     * 최근 게시판 여부
     */
    public void requestLastAlramDataApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // json={"api_code":"asstb_kbtg_alimi_view_on","insures_code":"108","mber_sn":"115232","pushk":"0"}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_KBTG_ALIMI_VIEW_ON);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN,  CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_PUSH_K, "0");               //

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_ASSTB_KBTG_ALIMI_VIEW_ON, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress(),false);
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 최근 키 몸무게 가져오기
     */
    public void requestGrowthLastDataApi(String chl_sn) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {"api_code":"chldrn_growth_last_cm_height","insures_code":"108","app_code":"android","mber_sn":"18622","chl_sn":"1312"}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CHLDRN_GROWTH_LAST_CM_HEIGHT);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);          //  os
            object.put(CommonData.JSON_MBER_SN,  CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_GROWTH_LAST_DATA, NetworkConst.getInstance().getDefDomain(), networkListener, params, null,false);
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 메인 유해질환 및 지역가져오기
     */
    public void requestDiseaseDataApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // { "API_CODE": "HX001", "LOC_NM_1": "경기도", "LOC_NM_2": "안양시", "AVG_FEVER": "37.8", "LOC_1": "37.3942527", "LOC_2": "126.9568209",
        // "DATA": [ { "DZNUM": "1", "DZNAME": "기관지염", "WEEKAGO_1": "25", "WEEKAGO_2": "31" }, { "DZNUM": "2", "DZNAME": "(모)세기관지염", "WEEKAGO_1": "6", "WEEKAGO_2": "6" } ], "DATA_LENGTH": "2" }
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE_F, CommonData.METHOD_DISEASE_HX001);    //  api 코드명
            object.put(CommonData.JSON_MBER_SN_F,  CommonData.getInstance().getMberSn());             //  회원고유값

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));

            JsonLogPrint.printJson(params.toString());

            RequestApi.requestApi(this, NetworkConst.NET_DISEASE_INFO, NetworkConst.getInstance().getFeverDomain(), networkListener, params, null,false);
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 미세먼지 데이터 받기
     * @param dust_sn  도시 번호
     */
    public void requestFineDust(String dust_sn){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.JSON_GET_DUST);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_DUST_SN, dust_sn);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_GET_DUST, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress(),true);
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 위치정보 저장
     * @param latitude  위도
     * @param longitude  경도
     * @param address  주소
     */
    public void requestLocation(double latitude, double longitude, String address){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE_F, CommonData.JSON_APINM_HL002);
            object.put(CommonData.JSON_MBER_SN_F, CommonData.getInstance().getMberSn());
            object.put(CommonData.JSON_LATITUDE_F, String.valueOf(latitude));
            object.put(CommonData.JSON_LONGITUDE_F, String.valueOf(longitude));
            object.put(CommonData.JSON_ADDRESS_F, address);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_SAND_LOCATION, NetworkConst.getInstance().getFeverDomain(), networkListener, params, getProgress(),true);
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 유의질환 가져오기
     * @param startDate  시작일
     * @param ednDate  종료일
     */
    public void requestEpidemicData(Date startDate, Date ednDate){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE);

            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE_F, CommonData.JSON_APINM_HB002);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_GET_EPIDEMIC, NetworkConst.getInstance().getFeverDomain(), networkListener, params, getProgress(),true);
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 최근 게시판 목록
     */
    public void requestBBSListApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CONTENT_SPECIAL_BBSLIST);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN,  CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_PAGENUMBER, "1");               //  페이지 번호
            object.put(CommonData.JSON_CONTENT_TYP, "0");               //  게시판 분류 번호 0: 전체  1: 질병예방 2:영양 3:운동 4: 심리

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_GET_BBS_LIST, NetworkConst.getInstance().getDefDomain(), networkListener, params,getProgress(),true);
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }
    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            hideProgress();
            switch ( type ) {
                case NetworkConst.NET_GET_EPIDEMIC:             // 유행 질병 데이터 가져오기
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {
                                JsonLogPrint.printJson(resultData.toString());
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN_F);

                                if (data_yn.equals(CommonData.YES)) {
                                    JSONArray resultArr = resultData.getJSONArray(CommonData.JSON_DATA_F);
                                    ArrayList<EpidemicItem> epidemicItems = new ArrayList<>();

                                    for(int i = 0; i < resultArr.length(); i++){
                                        JSONObject resultObject = resultArr.getJSONObject(i);

                                        EpidemicItem curItem = new EpidemicItem();
                                        curItem.setDzNum(resultObject.getInt(CommonData.JSON_DZNUM));
                                        curItem.setDzName(resultObject.getString(CommonData.JSON_DZNAME));
                                        curItem.setWeekago_1(resultObject.getInt(CommonData.JSON_WEEKAGO_1));
                                        curItem.setWeekago_2(resultObject.getInt(CommonData.JSON_WEEKAGO_2));
                                        epidemicItems.add(curItem);
                                    }

                                    epidemicItems.remove(epidemicItems.get(epidemicItems.size()-2));
                                    epidemicItems.remove(epidemicItems.get(epidemicItems.size()-2));

                                    for (int i = 0; i < epidemicItems.size(); i++){
                                        epidemicItems.get(i).setRatio(getRatio(epidemicItems.get(i).getWeekago_1(), epidemicItems.get(epidemicItems.size()-1).getWeekago_1(), 100));
                                    }

                                    epidemicItems.remove(epidemicItems.get(epidemicItems.size()-1));

                                    Collections.sort(epidemicItems, (a, b) -> a.getRatio() > b.getRatio() ? -1: a.getRatio() < b.getRatio() ? 1:0);

                                    //Collections.sort(epidemicItems, comRatio);
                                    //Collections.reverse(epidemicItems);

                                    mEpidemicList = epidemicItems;


                                    //mTxtEpidemic.setText(getString(R.string.epidemic_main) + " " + mEpidemicList.get(0).getDzName());
                                    //hsh start
                                    if(inTentGo !=null && (typePush==FirebaseMessagingService.FEVER || typePush==FirebaseMessagingService.FEVER_MOVIE||typePush==FirebaseMessagingService.DIESEASE)){
                                        startActivity(inTentGo);
                                        Util.BackAnimationStart(MainActivity.this);
                                        inTentGo = null;
                                    }
                                    //hsh end
                                }else{
                                    //mTxtEpidemic.setText(getString(R.string.epidemic_main) + " " + getString(R.string.empty_epidemic_main));
                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                                //mTxtEpidemic.setText(getString(R.string.epidemic_main) + " " + getString(R.string.empty_epidemic_main));
                            }
                    }
                    break;
                case NetworkConst.NET_GET_DUST:
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {
                                if(!resultData.getString(CommonData.JSON_DUSN_QY).equals("0")) {
                                    if(DustManager.mCityName.length() > 0){
                                        int dusn_qy = Integer.parseInt(resultData.getString(CommonData.JSON_DUSN_QY));
                                        String dusn_status = "";
                                        if( dusn_qy < 31 ) {        // 좋음
                                            dusn_status = getString(R.string.fine_dust_status_lv_1);
                                        }else if( dusn_qy < 81 ){   // 보통
                                            dusn_status = getString(R.string.fine_dust_status_lv_2);
                                        }else if( dusn_qy < 151 ){  // 나쁨
                                            dusn_status = getString(R.string.fine_dust_status_lv_3);
                                        }else{                      // 매우 나쁨
                                            dusn_status = getString(R.string.fine_dust_status_lv_4);
                                        }
                                        mTxtFineDust.setText( DustManager.mCityName + " " + getString(R.string.fine_dust_status) + " " + dusn_status + " (" + dusn_qy + ")");
                                    }else{
                                        mTxtFineDust.setText(getString(R.string.error_dust));
                                    }
                                }else{
                                    mTxtFineDust.setText(getString(R.string.error_dust));
                                }
                            }catch (Exception e){
                                mTxtFineDust.setText(getString(R.string.error_dust));
                                e.printStackTrace();
                            }
                            break;
                    }
                    break;
                case NetworkConst.NET_GROWTH_LAST_DATA:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);
                                if(data_yn.equals(CommonData.YES)){
                                    mLastHeight = resultData.getString(CommonData.JSON_LAST_HRIGTH);
                                    mLastWeight = resultData.getString(CommonData.JSON_LAST_BDWGH);
                                    mLastCmResult = resultData.getString(CommonData.JSON_CM_REUSLT);
                                    mLastKgResult = resultData.getString(CommonData.JSON_KG_REUSLT);
                                    mLastHeightDe = resultData.getString(CommonData.JSON_LAST_HRIGTH_DE);
                                    mLastWeightDe = resultData.getString(CommonData.JSON_LAST_BDWGH_DE);


                                }else{
                                    mLastHeight = "0";
                                    mLastWeight = "0";
                                    mLastCmResult = "";
                                    mLastKgResult = "";
                                    mLastHeightDe = "";
                                    mLastWeightDe = "";
                                }

                                if((mLastWeight.length() > 0 && !"0".equals(mLastWeight)) && (!"0".equals(mLastKgResult) && mLastKgResult.length() > 0)){
                                    tempWeight1 = "현재 체중 :";
                                    tempWeight2 = mLastWeight+"kg";
                                    tempBmi = mLastKgResult + "%";


                                    android.util.Log.i("MainActivity", "setCategory: "+ mChildMenuItem.get(mChildChoiceIndex).getmChldrnBdwgh());

                                }
                                else{
                                    tempWeight1 = "체중을 입력해 주세요.";
                                    tempWeight2 = "";
                                    tempBmi = "--";
                                }
                                    ViewPagerSubAdapter.notifyDataSetChanged();




                            } catch (Exception e) {
                                GLog.e(e.toString());
                                mLastHeight = "0";
                                mLastWeight = "0";
                                mLastCmResult = "";
                                mLastKgResult = "";
                                mLastHeightDe = "";
                                mLastWeightDe = "";
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            mLastHeight = "0";
                            mLastWeight = "0";
                            mLastCmResult = "";
                            mLastKgResult = "";
                            mLastHeightDe = "";
                            mLastWeightDe = "";
                            break;


                    }
                    break;
                case NetworkConst.NET_GET_BBS_LIST:
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {
                                JSONArray bbsList = resultData.getJSONArray(CommonData.JSON_BBSLIST);
                                mImgNew.setVisibility(View.GONE);
                                if(bbsList.length() > 0){
                                    for (int i= 0; i < bbsList.length(); i++){
                                        JSONObject bbsListJSONObject = bbsList.getJSONObject(i);
                                        if(!CommonData.getInstance().getBBSUrlShowCheck(bbsListJSONObject.getString(CommonData.JSON_INFO_TITLE_URL))){
                                            // new 처리
                                            //mImgNew.setVisibility(View.VISIBLE);
                                            break;
                                        }
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            break;
                    }
                    break;
                case NetworkConst.NET_GET_ALARM_POP:	// 게시판 리스트 가져오기
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_ALARM_POP API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_DATE_YN);
                                if(data_yn.equals(CommonData.YES)){
                                    GLog.i("NET_GET_ALARM_POP YYYY", "dd");
                                    JSONArray bbsList = resultData.getJSONArray(CommonData.JSON_MAIN_POP_LIST);
                                    for(int i = 0; i < bbsList.length(); i++){
                                        JSONObject resultObject = bbsList.getJSONObject(i);

                                        sMainPopTitle = resultObject.getString(CommonData.JSON_POP_TITLE);
                                        sMainPopTxt = resultObject.getString(CommonData.JSON_POP_TXT);
                                    }
                                    bMainPop = true;
                                    showTempPop();
                                }else{
                                    GLog.i("NET_GET_ALARM_POP NNNN", "dd");
                                    sMainPopTitle="";
                                    sMainPopTxt="";
                                    bMainPop = false;
                                    if(false){
                                        sMainPopTitle="체온 측정 안내";
                                        sMainPopTxt="체온 항상 똑같지 않아요.\r\n언제, 어디에 따라 달라져요.\r\n우리 아이 체온 미리 측정해요.";
                                        showTempPop();
                                    }
                                }
                            } catch (Exception e) {
                                GLog.e(e.toString());
                                sMainPopTitle="";
                                sMainPopTxt="";
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            sMainPopTitle="";
                            sMainPopTxt="";
                            break;
                    }
                    break;
                case NetworkConst.NET_DISEASE_INFO:
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {
                                JsonLogPrint.printJson(resultData.toString());

                                JSONArray Disease = resultData.getJSONArray("DATA");

                                for (int i = 0; i < Disease.length(); i++) {
                                    JSONObject resultObject = Disease.getJSONObject(i);

                                    dzname[i] = resultObject.getString("DZNAME");
                                    weekago_1_per[i] = resultObject.getString("WEEKAGO_1_PER");
                                }
                                tempDisease1 = dzname[0];
                                tempDisease2 = dzname[1];
                                tempDisease_percent1 = weekago_1_per[0] + "%";
                                tempDisease_percent2 = weekago_1_per[1] + "%";


                                if (TextUtils.isEmpty(resultData.getString("LOC_NM_2")) == false && TextUtils.isEmpty(resultData.getString("AVG_FEVER")) == false) {
                                    tempLocal = resultData.getString("LOC_NM_2");


                                    if ("0".equals(resultData.getString("AVG_FEVER"))) {
                                        tempTempeture = "체온정보없음";
                                    } else {
                                        tempTempeture = "평균 " + resultData.getString("AVG_FEVER") + "℃";

                                    }
                                    tempBottomtext = getString(R.string.main_bottom_sub_title1);


                                } else {
                                    tempLocal = "-";
                                    tempTempeture = "-";
                                    tempBottomtext = getString(R.string.main_bottom_sub_title2);

                                }

                                    ViewPagerSubAdapter.notifyDataSetChanged();


                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }


                        break;

                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            sMainPopTitle="";
                            sMainPopTxt="";
                            break;
                    }
                    break;

                case NetworkConst.NET_ASSTB_KBTG_ALIMI_VIEW_ON:
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {
                                JsonLogPrint.printJson(resultData.toString());

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                JSONArray chlmReadern = resultData.getJSONArray("chlmReadern");
                                String result_code = chlmReadern.getJSONObject(0).getString("result_code");
                                String idx = chlmReadern.getJSONObject(0).getString("kbta_idx");
                                String title = chlmReadern.getJSONObject(0).getString("kbt");
                                if(data_yn.equals(CommonData.YES)) {   // 알림 최신
                                    if(result_code.equals("8888")){
                                        Util.setSharedPreference(MainActivity.this, "kbta_idx", idx);
                                        setNewIcon(idx,title);
                                    }else{
                                        if(!Util.getSharedPreference(MainActivity.this,"kbta_idx").equals(idx)){
                                            Util.setSharedPreference(MainActivity.this, "new_check", "1"); // 1: true 0 :false
                                            Util.setSharedPreference(MainActivity.this, "kbta_idx", idx);
                                        }
                                        setNewIcon(idx,title);
                                    }

                                }
                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }
                            break;

                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }

        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();
        }
    };

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    /*public final static Comparator<EpidemicItem> comRatio = new Comparator<EpidemicItem>(){
        private final Collator collator = Collator.getInstance();
        @Override
        public int compare(EpidemicItem object1, EpidemicItem object2) {
            return object1.getRatio() > object2.getRatio() ? -1 : object1.getRatio() < object2.getRatio() ? 1:0;
        }
    };*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgress();
        clearCache();
    }

    private void clearCache() {
        final File cacheDirFile = this.getCacheDir();
        if (null != cacheDirFile && cacheDirFile.isDirectory()) {
            clearSubCacheFiles(cacheDirFile);
        }
    }

    private void clearSubCacheFiles(File cacheDirFile) {
        if (null == cacheDirFile || cacheDirFile.isFile()) {
            return;
        }
        for (File cacheFile : cacheDirFile.listFiles()) {
            if (cacheFile.isFile()) {
                if (cacheFile.exists()) {
                    cacheFile.delete();
                }
            } else {
                clearSubCacheFiles(cacheFile);
            }
        }
    }

    public double getRatio(int num_1, int num_2, int num_3){

        BigDecimal bd_1 = null;
        BigDecimal bd_2 = null;
        BigDecimal bd_3 = new BigDecimal(""+num_3);

        if(num_1 > 0){
            bd_1 = new BigDecimal(""+num_1);
            bd_2 = new BigDecimal(""+num_2);

            BigDecimal ratio = bd_1.divide(bd_2, 3, BigDecimal.ROUND_HALF_UP).multiply(bd_3) ;

            return ratio.doubleValue();
        }else{
            return 0d;
        }
    }


    @Override protected void attachBaseContext(Context newBase) {
//        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }

    /**
     * hsh 메인 체온 알람
     */
    public void requestTempPopApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ALARM_POP);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);          //  insures 코드

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(MainActivity.this, NetworkConst.NET_GET_ALARM_POP, NetworkConst.getInstance().getDefDomain(), networkListener, params,getProgress(),true);
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    private void showTempPop(){
        GLog.i("showTempPop t: "+sMainPopTitle+" : "+sMainPopTxt, "dd");
        if(mDialog!=null){
            GLog.i("showTempPop t dialog : "+mDialog.isShowing(), "dd");
        }

        if(!bMainPop || sMainPopTitle.equals("") || sMainPopTxt.equals("") || (mDialog!=null && mDialog.isShowing()))
            return;


        String sNowTime = Util.getNowDateFormat();
        GLog.i("showTempPop t now : "+sNowTime+" before : "+CommonData.getInstance().getAfterMainPopupShowCheck(), "dd");
        if(CommonData.getInstance().getAfterMainPopupShowCheck().equals(sNowTime)){

        }else{
            Intent intent = new Intent(MainActivity.this, MainPopActivity.class);
            intent.putExtra("title",sMainPopTitle);
            intent.putExtra("txt",sMainPopTxt);
            startActivity(intent);
        }

    }

    public void setNewIcon(String idx,String text){
        if(text.length() > 15){
            text = text.substring(0, 15) + "    \n" + text.substring(15);
        }
        mNewMessage.setText(text);
        if (Util.getSharedPreference(MainActivity.this,"kbta_idx").equals(idx) && !Util.getSharedPreference(MainActivity.this,"kbta_idx").equals("")) {
            if(Util.getSharedPreference(MainActivity.this,"new_check").equals("1")) {
                mAlramNew.setVisibility(View.VISIBLE);
                mNewMessage.setVisibility(View.VISIBLE);
            }else{
                mAlramNew.setVisibility(View.GONE);
                mNewMessage.setVisibility(View.GONE);
            }
        } else if(!Util.getSharedPreference(MainActivity.this,"kbta_idx").equals(idx) && !Util.getSharedPreference(MainActivity.this,"kbta_idx").equals("")){
            mAlramNew.setVisibility(View.VISIBLE);
            mNewMessage.setVisibility(View.VISIBLE);
            Util.setSharedPreference(MainActivity.this,"new_check","1");
        }else{
            Util.setSharedPreference(MainActivity.this,"new_check","0");
            mAlramNew.setVisibility(View.GONE);
            mNewMessage.setVisibility(View.GONE);
        }
    }


    public MakeProgress getProgress(){
//        /* 테스트 후 주석 해제
        if(mProgress != null) {
            return mProgress;
        }else {
            return mProgress = new MakeProgress(this);
        }
//        */
    }

    /**
     * 프로그래스 활성화
     */
    public void showProgress() {

        if ( mProgress == null )
            mProgress = new MakeProgress(this);

        mProgress.show();
    }

    /**
     * 프로그래스 비활성화
     */
    public void hideProgress() {

        if (mProgress != null) {
            mProgress.dismiss();
        }
    }
}