package com.appmd.hi.gngcare.collection;

/**
 * Created by jihoon on 2016-04-18.
 * 자주 묻는 질문 데이터
 * @since 0, 1
 */
public class FrequentlyListItem {

    private String mTitle;
    private String mContent;

    /**
     * 자주 묻는 질문
     * @param title 제목
     * @param content   내용
     */
    public FrequentlyListItem(String title,
                              String content){
        this.mTitle =   title;
        this.mContent=  content;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmContent() {
        return mContent;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }
}
