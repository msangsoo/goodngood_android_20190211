package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.BuildConfig;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class JunMemberCertifi2Activity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{

    private LinearLayout mNameLayout, mBirthLayout;
    private RelativeLayout mGenderRv;
    private TextView mPersonalTv_1, mPersonalTv_2, mPersonalTv_3, mLocationTv, mMarketingTv,mTopTv, mBirthdayTv;
    private EditText mNameEdit, mBirthTv;
    private ImageButton mNameDelBtn;

    private RadioButton mMaleRb, mFemaleRb;
    private CheckBox mPersonalCb_1, mPersonalCb_2, mPersonalCb_3, mLocationCb, mobileCb;

    private Button mCertifiBtn, mAllAgreeBtn;

    private String mCurrentBirth =   "";
    Calendar cal = Calendar.getInstance();

    private String mBer_nm, mBer_lifyea, mBer_birthyea, mBer_sex;    // api 호출시 저장변수

    private Intent intent = null;
    private int mLoginType = 1;

    String mberNo = "";
    String mBerNm = "";
    String mBirthday = "";
    String mBirthyea ="";
    int mGender = 1;

    String mParentBerNm = "";
    String mParentBirthday = "";
    String mParentHp = "";
    String mParentJob = "";
    String mParentAfterBirth = "";
    String mParentchl_exist_yn = "";
    String mParentNation = "";
    String mParentGender = "";

    private ImageButton mBackimg;


    private Date mCurDate;
    GregorianCalendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.junmember_certifi2_activity);

        setTitle(getString(R.string.junmember_certifi));

        init();
        setEvent();

        mBackimg = findViewById(R.id.common_left_btn);
        mBackimg.setVisibility(View.VISIBLE);

        mBackimg.setOnClickListener(v -> finish());

        intent = getIntent();

        if (intent != null) {

            mParentBerNm = intent.getStringExtra(CommonData.EXTRA_MBERNM);
            mParentBirthday = intent.getStringExtra(CommonData.EXTRA_BIRTHDAY);
            mParentNation = intent.getStringExtra(CommonData.EXTRA_MBERNATION);
            mParentGender = intent.getStringExtra(CommonData.EXTRA_GENDER);
            mParentHp = intent.getStringExtra(CommonData.EXTRA_PHONENO);
            mParentJob = intent.getStringExtra(CommonData.EXTRA_JOB);
            mParentAfterBirth = intent.getStringExtra(CommonData.EXTRA_AFTERBIRTH);

        }

        if(mParentAfterBirth.equals("1")){
            mTopTv.setText("아이정보입력");
            mBirthdayTv.setText(getString(R.string.birthday));


            mNameLayout.setVisibility(View.VISIBLE);
            mGenderRv.setVisibility(View.VISIBLE);

        }else{
            mTopTv.setText("출산 예정일 입력");
            mBirthdayTv.setText(getString(R.string.mother_health_reg_week_cnt));

            mNameLayout.setVisibility(View.GONE);
            mGenderRv.setVisibility(View.GONE);
        }

        Util.setTextViewCustom(JunMemberCertifi2Activity.this, mPersonalTv_1, mPersonalTv_1.getText().toString(), mPersonalTv_1.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(JunMemberCertifi2Activity.this, mPersonalTv_2, mPersonalTv_2.getText().toString(), mPersonalTv_2.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(JunMemberCertifi2Activity.this, mPersonalTv_3, mPersonalTv_3.getText().toString(), mPersonalTv_3.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(JunMemberCertifi2Activity.this, mLocationTv, mLocationTv.getText().toString(), mLocationTv.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(JunMemberCertifi2Activity.this, mMarketingTv, mMarketingTv.getText().toString(), mMarketingTv.getText().toString(), 0, 15, Typeface.BOLD, true);

        mCertifiBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if(intent != null){
            mLoginType  =   intent.getIntExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);

        }else{
            GLog.i("intent = null", "dd");
        }

        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
    }

    /**
     * 초기화
     */
    public void init(){

        mNameLayout     =   (LinearLayout)      findViewById(R.id.name_layout);
        mBirthLayout    =   (LinearLayout)      findViewById(R.id.birthday_layout);

        mGenderRv           =   (RelativeLayout)    findViewById(R.id.gender_Rv);

        mBirthTv        =   (EditText)          findViewById(R.id.birthday_tv);
        mPersonalTv_1      =   (TextView)          findViewById(R.id.personal_terms_1_tv);
        mPersonalTv_2      =   (TextView)          findViewById(R.id.personal_terms_2_tv);
        mPersonalTv_3      =   (TextView)          findViewById(R.id.personal_terms_3_tv);
        mLocationTv     =   (TextView)          findViewById(R.id.location_terms_tv);
        mMarketingTv     =   (TextView)          findViewById(R.id.marketing_tv);
        mTopTv          =   (TextView)          findViewById(R.id.top_txt);
        mBirthdayTv     =   (TextView)          findViewById(R.id.birthday_Txt);

        mNameEdit       =   (EditText)          findViewById(R.id.name_edit);

        mNameDelBtn     =   (ImageButton)       findViewById(R.id.name_del_btn);

        mMaleRb         =   (RadioButton)       findViewById(R.id.male_btn);
        mFemaleRb       =   (RadioButton)       findViewById(R.id.female_btn);

        mPersonalCb_1      =   (CheckBox)          findViewById(R.id.personal_terms_1_cb);
        mPersonalCb_2      =   (CheckBox)          findViewById(R.id.personal_terms_2_cb);
        mPersonalCb_3      =   (CheckBox)          findViewById(R.id.personal_terms_3_cb);
        mLocationCb     =   (CheckBox)          findViewById(R.id.location_terms_cb);
        mobileCb        =   (CheckBox)          findViewById(R.id.mobile_terms_cb);

        mCertifiBtn     =   (Button)            findViewById(R.id.certifi_btn);
        mAllAgreeBtn    =   (Button)            findViewById(R.id.agree_btn);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mPersonalTv_1.setOnClickListener(this);
        mPersonalTv_2.setOnClickListener(this);
        mPersonalTv_3.setOnClickListener(this);
        mLocationTv.setOnClickListener(this);
        mCertifiBtn.setOnClickListener(this);
        mAllAgreeBtn.setOnClickListener(this);
        mNameDelBtn.setOnClickListener(this);
        mMarketingTv.setOnClickListener(this);

        mPersonalCb_1.setOnCheckedChangeListener(this);
        mPersonalCb_2.setOnCheckedChangeListener(this);
        mPersonalCb_3.setOnCheckedChangeListener(this);
        mLocationCb.setOnCheckedChangeListener(this);

        mNameEdit.setOnFocusChangeListener(this);

//        mNameEdit.addTextChangedListener(new MyTextWatcher());
//        mBirthTv.addTextChangedListener(new MyTextWatcher());

    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;
        String birth = mBirthTv.getText().toString();
        if(mParentAfterBirth.equals("1")) {

            if (mNameEdit.getText().toString().trim().equals("")) {    // 이름 입력이 없다면
                mNameEdit.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent(getString(R.string.popup_dialog_name_error));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.isSpecialWordNum(mNameEdit.getText().toString().trim()) == false){
                mNameEdit.requestFocus();
                mDialog =   new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
                mDialog.setContent(getString(R.string.popup_dialog_name_error_1));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("생일은 미래일 수 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(!CDateUtil.getDateFormatBool(birth)){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("날짜형식이 아닙니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

        }else{
            if (birth.trim().length() != 8) {
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }
            if(StringUtil.getIntVal(birth) < StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("출산예정일은 미래 날짜로 입력해주세요.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.addDay(CDateUtil.getToday_yyyy_MM_dd(),280))){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("출산예정일은 40주를 넘길 수 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }

            if(!CDateUtil.getDateFormatBool(birth)){
                mBirthTv.requestFocus();
                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

                mDialog.setContent("날짜형식이 아닙니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
                bool = false;
                return bool;
            }
        }



        /*if(mCurrentBirth.equals("")){   // 생년월일
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }*/


        if(!mPersonalCb_1.isChecked() || !mPersonalCb_2.isChecked()|| !mPersonalCb_3.isChecked()|| !mLocationCb.isChecked()){    // 약관 동의가 안되어 있다면
            mDialog =   new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_terms_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }



        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(mParentAfterBirth.equals("1")) {

            if (mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2) {    // 이름 입력이 없다면
                bool = false;
                return bool;
            }
        }

            if (mBirthTv.getText().toString().trim().equals("") || mBirthTv.getText().toString().trim().length() < 8) {
                bool = false;
                return bool;
            }


        /*if(mCurrentBirth.equals("")){   // 생년월일
            bool = false;
            return bool;
        }*/


        if(!mPersonalCb_1.isChecked() || !mPersonalCb_2.isChecked()|| !mPersonalCb_3.isChecked()|| !mLocationCb.isChecked()){    // 약관 동의가 안되어 있다면
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

//        if(bool) {
            mCertifiBtn.setEnabled(true);

            mCertifiBtn.setBackgroundResource(R.drawable.btn_5_22396c);
            mCertifiBtn.setTextColor(ContextCompat.getColorStateList(JunMemberCertifi2Activity.this, R.color.color_ffffff_btn));
//        }else{
//            mCertifiBtn.setEnabled(false);
//
//            mCertifiBtn.setBackgroundResource(R.drawable.background_5_e6e7e8_sq);
//            mCertifiBtn.setTextColor(ContextCompat.getColorStateList(JunMemberCertifi2Activity.this, R.color.color_ffffff_btn));
//        }

    }

    /**
     * 준회원 체크
     *
     */
    public void requestAuthCheck() {
//        {   "api_code": "mber_check",   "insures_code": "106", "token": "deviceToken",
//                "app_code": "android19" ,  "mber_nm": "테스트" ,"mber_lifyea": "20140101","mber_hp": "010758421333", "mber_nation": "1"  ,  "mber_sex": "2"   }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_REG_CHK_OK);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_NM,     mParentBerNm);
            object.put(CommonData.JSON_MBER_LIFYEA,     mParentBirthday);
            object.put(CommonData.JSON_MBER_HP,     mParentHp);
            object.put(CommonData.JSON_MBER_NATION,     mParentNation);
            object.put(CommonData.JSON_MBER_SEX,     mParentGender);
            object.put(CommonData.JSON_MBER_JOB_YN,     mParentJob);

            mBer_nm = mNameEdit.getText().toString().trim();
            mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
            mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자
            mParentchl_exist_yn = mParentAfterBirth.equals("1")? "Y" : "N"; // 출산여부 Y:출산 후 N:출산 전



            object.put(CommonData.JSON_CHLDRN_NM,     mBer_nm);
            object.put(CommonData.JSON_CHLDRN_LIFYEA,     mBer_lifyea);
            object.put(CommonData.JSON_CHLDRN_SEX,     mBer_sex);
            object.put(CommonData.JSON_CHL_EXIST_YN,     mParentchl_exist_yn);
            object.put(CommonData.JSON_MBER_GRAD,     "20");
            object.put(CommonData.JSON_PUSH_K,     "0");
            if(BuildConfig.DEBUG){
                object.put(CommonData.JSON_APP_VER, "99");
            }else{
                //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//                object.put(CommonData.JSON_APP_VER, "99");
            }

            object.put(CommonData.JSON_MBER_SN,   CommonData.getInstance().getMberSn());

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if(RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);

            //마케팅 동의 관련
            object.put("marketing_yn", mobileCb.isChecked() ? "Y":"N");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JunMemberCertifi2Activity.this, NetworkConst.NET_ASSTB_REG_CHK_OK, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }


    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.personal_terms_1_tv:  // 개인정보 제공 동의
                intent = new Intent(JunMemberCertifi2Activity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_1_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_1));
                startActivity(intent);
                Util.BackAnimationStart(JunMemberCertifi2Activity.this);
                break;
            case R.id.personal_terms_2_tv:  // 개인민감정보 제공 동의
                intent = new Intent(JunMemberCertifi2Activity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_2_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_2));
                startActivity(intent);
                Util.BackAnimationStart(JunMemberCertifi2Activity.this);
                break;
            case R.id.personal_terms_3_tv:  // 개인정보 제3자 제공 동의
                intent = new Intent(JunMemberCertifi2Activity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_3_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_3));
                startActivity(intent);
                Util.BackAnimationStart(JunMemberCertifi2Activity.this);
                break;
            case R.id.location_terms_tv:  // 위치정보 이용 동의
                intent = new Intent(JunMemberCertifi2Activity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.LOCATION_TERMS_JUN_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.location_terms));
                startActivity(intent);
                Util.BackAnimationStart(JunMemberCertifi2Activity.this);
                break;
            case R.id.marketing_tv:  // 마케팅 이용 동의
                intent = new Intent(JunMemberCertifi2Activity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.MOBILE_TERMS_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.mobile_terms));
                startActivity(intent);
                Util.BackAnimationStart(JunMemberCertifi2Activity.this);
                break;
            case R.id.certifi_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                mBer_nm = mNameEdit.getText().toString().trim();
                mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
                mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자

                if(invaildCerfiti()){   // 인증 가능하다면
                    requestAuthCheck();
                }
                break;
            case R.id.name_del_btn: // 이름 삭제
                commonView.setClearEditText(mNameEdit);
                break;
            case R.id.birthday_tv:  // 생년월일
                try {
                    if(mCurDate == null){
                        mCurDate = new Date();
                    }
                    mCalendar.setTime(mCurDate);
                    int nNowYear = mCalendar.get(Calendar.YEAR);
                    int nNowMonth = mCalendar.get(Calendar.MONTH);
                    int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurDate = mCalendar.getTime();
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                        mCurrentBirth = format.format(mCurDate);
                        format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                        mBirthTv.setText( format.format(mCurDate));
                        setConfirmBtn(isConfirm());
                    }, nNowYear, nNowMonth , nNowDay);
                    datePickerDialog.setCancelable(false);
                    datePickerDialog.showYearPickerFirst(true);
                    datePickerDialog.show(getFragmentManager(),"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.agree_btn :
                mPersonalCb_1.setChecked(true);
                mPersonalCb_2.setChecked(true);
                mPersonalCb_3.setChecked(true);
                mLocationCb.setChecked(true);
                mobileCb.setChecked(true);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.name_edit:    // 이름
//                commonView.setClearImageBt(mNameDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_ASSTB_REG_CHK_OK:   // 회원인증 체크
                   switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                               String data_yn = resultData.getString(CommonData.JSON_DATA_YN);

                                if(data_yn.equals(CommonData.YES)){   // 준회원가입
                                    String mbersn = resultData.getString(CommonData.JSON_MBER_SN);


                                    mDialog =   new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    View view = LayoutInflater.from(JunMemberCertifi2Activity.this).inflate(R.layout.pop_join_complete, null);
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    params.gravity = Gravity.CENTER;
                                    mDialog.setContentView(view, params);
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();


                                            Intent intent = new Intent(JunMemberCertifi2Activity.this, JoinActivity.class);
                                            CommonData.getInstance().setMberSn(mbersn);
                                            intent.putExtra(CommonData.EXTRA_MBERSN, mbersn);
                                            intent.putExtra(CommonData.EXTRA_GLAD, "20");
                                            startActivity(intent);
                                            //activityClear();
                                            //finish();

                                        }
                                    });
                                    mDialog.show();

                                }else {  // 현대해상 미가입자
                                    CommonData.getInstance().setMberSn("");
                                    mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_sign_error1));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                                CommonData.getInstance().setMberSn("");

                                mDialog = new CustomAlertDialog(JunMemberCertifi2Activity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.popup_dialog_sign_error1));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(CustomAlertDialog dialog, Button button) {
                                        dialog.dismiss();
                                    }
                                });
                                mDialog.show();
                            }


                            break;


                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
