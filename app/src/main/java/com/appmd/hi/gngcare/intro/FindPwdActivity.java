package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by jihoon on 2016-03-31.
 * 비밀번호 찾기 화면
 * @since 0, 1
 */
public class FindPwdActivity extends BackBaseActivity implements View.OnClickListener , View.OnFocusChangeListener{

    public static final int PHONE_SIZE  =   3;

    private EditText[] mPhoneEdit    =    new EditText[PHONE_SIZE];
    private ImageButton[] mPhoneDelBtn  =   new ImageButton[PHONE_SIZE];

    private EditText mIdEdit;
    private ImageButton mIdDelBtn;
    private Button mConfirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_pwd_activity);

        setTitle(getString(R.string.find_pwd));

        init();
        setEvent();

        mConfirmBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());
    }

    /**
     * 초기화
     */
    public void init(){

        mIdEdit    =   (EditText)      findViewById(R.id.id_edit);
        mIdDelBtn     =   (ImageButton)   findViewById(R.id.id_del_btn);

        mPhoneEdit[0]    =   (EditText)      findViewById(R.id.phone_input_edit_1);
        mPhoneEdit[1]    =   (EditText)      findViewById(R.id.phone_input_edit_2);
        mPhoneEdit[2]    =   (EditText)      findViewById(R.id.phone_input_edit_3);

        mPhoneDelBtn[0]     =   (ImageButton)   findViewById(R.id.phone_input_del_btn_1);
        mPhoneDelBtn[1]     =   (ImageButton)   findViewById(R.id.phone_input_del_btn_2);
        mPhoneDelBtn[2]     =   (ImageButton)   findViewById(R.id.phone_input_del_btn_3);

        mConfirmBtn     =   (Button)            findViewById(R.id.confirm_btn);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mConfirmBtn.setOnClickListener(this);

        mIdDelBtn.setOnClickListener(this);

        mIdEdit.setOnFocusChangeListener(this);
        mIdEdit.addTextChangedListener(new MyTextWatcher());

        for(int i=0; i<PHONE_SIZE; i++) {
            mPhoneDelBtn[i].setOnClickListener(this);

            mPhoneEdit[i].setOnFocusChangeListener(this);
            mPhoneEdit[i].addTextChangedListener(new MyTextWatcher());
        }

    }

    /**
     * 인증 가능한지 체크
     * @param email     이메일
     * @param phone_1   휴대폰 번호 1
     * @param phone_2   휴대폰 번호 2
     * @param phone_3   휴대폰 번호 3
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(String email, String phone_1, String phone_2, String phone_3){
        boolean bool = true;

        if(email.equals("")){    // 이름 입력이 없다면
            mIdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.id_hint));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!Util.checkEmail(email)){  // 이메일 형식이 아니라면
            mIdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_mail_format_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(phone_1.equals("") || phone_2.equals("") || phone_3.equals("")){    // 이름 입력이 없다면
            mPhoneEdit[0].requestFocus();
            mDialog =   new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_phone_number_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }


        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
     * @return boolean ( true - 저장 가능, false - 저장 불가 )
     */
    public boolean isConfirm(){
        boolean bool = true;

        if(mIdEdit.getText().toString().trim().equals("") ){    // 이름 입력이 없다면
            bool = false;
            return bool;
        }

        if(mPhoneEdit[0].getText().toString().trim().equals("") || mPhoneEdit[1].getText().toString().trim().equals("") || mPhoneEdit[2].getText().toString().trim().equals("") ){  // 휴대폰 번호가 없다면
            bool = false;
            return bool;
        }


        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

        if(bool) {
            mConfirmBtn.setEnabled(true);

            mConfirmBtn.setBackgroundResource(R.drawable.btn_5_6bb0d7);
            mConfirmBtn.setTextColor(ContextCompat.getColorStateList(FindPwdActivity.this, R.color.color_ffffff_btn));
        }else{
            mConfirmBtn.setEnabled(false);

            mConfirmBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
            mConfirmBtn.setTextColor(ContextCompat.getColorStateList(FindPwdActivity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 비밀번호 찾기
     */
    public void requestLoginPwd(String mber_id, String mber_hp){
        GLog.i("requestLoginId mber_hp = " + mber_id, "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//        {   "api_code": "login_pwd",   "insures_code": "106", "token": "deviceToken", "mber_hp": "01085852255" ,  "mber_id": "tjhong@gchealthcare.com" }
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN_PWD);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            if(RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }

            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_HP, mber_hp);
            
            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(FindPwdActivity.this, NetworkConst.NET_LOGIN_PWD, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.id_del_btn: // 아이디 삭제
                commonView.setClearEditText(mIdEdit);
                break;
            case R.id.phone_input_del_btn_1: // 첫번째 전화번화 삭제
                commonView.setClearEditText(mPhoneEdit[0]);
                break;
            case R.id.phone_input_del_btn_2: // 두번째 전화번화 삭제
                commonView.setClearEditText(mPhoneEdit[1]);
                break;
            case R.id.phone_input_del_btn_3: // 세번째 전화번화 삭제
                commonView.setClearEditText(mPhoneEdit[2]);
                break;
            case R.id.confirm_btn:  // 비밀번호 찾기
                String email = mIdEdit.getText().toString().trim();
                String phone_1 = mPhoneEdit[0].getText().toString().trim();
                String phone_2 = mPhoneEdit[1].getText().toString().trim();
                String phone_3 = mPhoneEdit[2].getText().toString().trim();

                if(invaildCerfiti(email, phone_1, phone_2, phone_3)) {
                    GLog.i("아이디 찾기", "dd");
                    // 아이디 찾기 api 호출
                    requestLoginPwd(email, phone_1+phone_2+phone_3);
                }

                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.id_edit:    // 아이디
                commonView.setClearImageBt(mIdDelBtn, hasFocus);
                break;
            case R.id.phone_input_edit_1:    // 첫번째 전화번호
                commonView.setClearImageBt(mPhoneDelBtn[0], hasFocus);
                break;
            case R.id.phone_input_edit_2:	   // 두번째 전화번호
                commonView.setClearImageBt(mPhoneDelBtn[1], hasFocus);
                break;
            case R.id.phone_input_edit_3:	   // 세번째 전화번호
                commonView.setClearImageBt(mPhoneDelBtn[2], hasFocus);
                break;
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_LOGIN_PWD:									// 비밀번호 찾기

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_LOGIN_PWD", "dd");
                            try {

                                String data_yn  =   resultData.getString(CommonData.JSON_DATA_YN);

                                if(data_yn.equals(CommonData.YES)){ // api 호출에 성공
                                    final String send_mail_yn = resultData.getString(CommonData.JSON_SEND_MAIL_YN);

                                    if(send_mail_yn.equals(CommonData.YES)) {    // 비밀번호 찾기 성공

                                        mDialog = new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
                                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                        mDialog.setContent(getString(R.string.popup_dialog_find_pwd_complete));
                                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(CustomAlertDialog dialog, Button button) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        });
                                    }else{  // 비밀번호 찾기 실패
                                        mDialog = new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
                                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                        mDialog.setContent(getString(R.string.popup_dialog_pass_change_error));
                                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(CustomAlertDialog dialog, Button button) {

                                                mIdEdit.setText("");
                                                mIdEdit.requestFocus();
                                                dialog.dismiss();
                                            }
                                        });
                                    }
                                    mDialog.show();
                                }else{
                                    mDialog = new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_pass_change_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {

                                            mIdEdit.setText("");
                                            mIdEdit.requestFocus();
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }

                            } catch ( Exception e ) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:	// 시스템 오류
                            mDialog = new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                            mDialog.setContent(getString(R.string.popup_dialog_system_error_content));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {

                                @Override
                                public void onClick(CustomAlertDialog dialog, Button button) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                            mDialog.show();

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:	// 입력 데이터 오류
                            mDialog = new CustomAlertDialog(FindPwdActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                            mDialog.setContent(getString(R.string.popup_dialog_input_error_content));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {

                                @Override
                                public void onClick(CustomAlertDialog dialog, Button button) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                            mDialog.show();
                            break;

                        default:
                            mDialog.show();
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
