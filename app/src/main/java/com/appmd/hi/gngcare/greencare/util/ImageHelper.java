package com.appmd.hi.gngcare.greencare.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * 이미지 다운로드 Helper
 * <p/>
 * 작업자 : kb_card_mini_3
 * 작업일 : 2016. 8. 31.
 */
public class ImageHelper {

    public static final int IMG_TYPE_CARD = 1000;
    public static final int IMG_TYPE_COMMON = 1001;
    public static final int IMG_TYPE_COMMON_BANKBOOK = 1002;

    private static final String QR_NAME = "QR_Code.png";

    public static Bitmap rotate(Bitmap bitmap, int degrees) {

        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != converted) {
                    bitmap.recycle();

                    bitmap = converted;
                }
            } catch (OutOfMemoryError ex) {
                // 메모리가 부족하여 회전을 시키지 못할 경우 그냥 원본을 반환합니다.
                Logger.d("CommandUtil", ex.getMessage());
            }
        }
        return bitmap;
    }


    public static String saveBitmapToPng(Context context, Bitmap bitmap) {
//        File storage = context.getCacheDir();
        File storage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        File tempFile = new File(storage, QR_NAME);
        try {
            tempFile.createNewFile();
            FileOutputStream out = new FileOutputStream(tempFile);

            bitmap.compress(Bitmap.CompressFormat.PNG,100,out);

            out.close();

        } catch (FileNotFoundException e) {
            Logger.e(e);
        } catch (IOException e) {
            Logger.e(e);
        } finally {
            return tempFile.getAbsolutePath();
        }
    }


    /**
     * 데이터 형태에 맞는 이미지 세팅
     * @param imageView
     * @param object
     */
    public static void setObjectImageView(ImageView imageView, Object object){
        Logger.d("ImageHelper", "setObjectImageView() - object=" + object);
        if (object != null) {

            if (object instanceof Drawable) {
                imageView.setImageDrawable((Drawable) object);

            } else if (object instanceof Bitmap) {
                imageView.setImageBitmap((Bitmap) object);

            } else if (object instanceof Integer) {
                imageView.setImageResource((Integer) object);
            }
        } else {
        }
    }

//    private static class UrlDownloadAsyncTask extends AsyncTask<Void, Void, Object> {
//        private static LRUCache cache = new LRUCache((int) (Runtime.getRuntime().maxMemory() / 16)); // 1/16th of the maximum memory.
//        private final UrlDownloadAsyncTaskHandler handler;
//        private String url;
//
//
//        private static void download(String url, final File downloadFile, final Context context) {
//            UrlDownloadAsyncTask task = new UrlDownloadAsyncTask(url, new UrlDownloadAsyncTaskHandler() {
//                @Override
//                public void onPreExecute() {
//                    //Toast.makeText(context, "Start downloading", Toast.LENGTH_SHORT).show();
//                }
//
//                @Override
//                public Object doInBackground(File file) {
//                    if (file == null) {
//                        return null;
//                    }
//
//                    try {
//                        BufferedInputStream in = null;
//                        BufferedOutputStream out = null;
//
//                        //create output directory if it doesn't exist
//                        File dir = downloadFile.getParentFile();
//                        if (!dir.exists()) {
//                            dir.mkdirs();
//                        }
//
//                        in = new BufferedInputStream(new FileInputStream(file));
//                        out = new BufferedOutputStream(new FileOutputStream(downloadFile));
//
//                        byte[] buffer = new byte[1024 * 100];
//                        int read;
//                        while ((read = in.read(buffer)) != -1) {
//                            out.write(buffer, 0, read);
//                        }
//                        in.close();
//                        out.flush();
//                        out.close();
//
//                        return downloadFile;
//                    } catch (IOException e) {
//                        Logger.e(e);
//                    }
//
//                    return null;
//                }
//
//                @Override
//                public void onPostExecute(Object object, UrlDownloadAsyncTask task) {
//                    if (object != null && object instanceof File) {
//                        Toast.makeText(context, "Finish downloading: " + ((File) object).getAbsolutePath(), Toast.LENGTH_LONG).show();
//                    } else {
//                        Toast.makeText(context, "Error downloading", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } else {
//                task.execute();
//            }
//        }
//
//
//
//        private static void display(String url, final ImageView imageView, final boolean circle, final int type) {
//            display(url, imageView, circle, type, null);
//        }
//
//        private static void display(String url, final ImageView imageView, final boolean circle, final int type, final IHelper helper) {
////            UrlDownloadAsyncTask task = null;
////
////            if (imageView.getTag() != null && imageView.getTag() instanceof UrlDownloadAsyncTask) {
////                try {
////                    task = (UrlDownloadAsyncTask) imageView.getTag();
////                    task.cancel(true);
////                } catch (Exception e) {
////                }
////
////                imageView.setTag(null);
////            }
////
////            task = new UrlDownloadAsyncTask(url, new UrlDownloadAsyncTaskHandler() {
////                @Override
////                public void onPreExecute() {
////                }
////
////                @Override
////                public Object doInBackground(File file) {
////                    if (file == null) {
////                        return null;
////                    }
////
////                    Bitmap bm = null;
////                    try {
////                        int targetHeight = 480;
////                        int targetWidth = 320;
////
////                        BufferedInputStream bin = new BufferedInputStream(new FileInputStream(file));
////                        bin.mark(bin.available());
////
////                        BitmapFactory.Options options = new BitmapFactory.Options();
////                        options.inJustDecodeBounds = true;
////                        BitmapFactory.decodeStream(bin, null, options);
////
////                        Boolean scaleByHeight = Math.abs(options.outHeight - targetHeight) >= Math.abs(options.outWidth - targetWidth);
////
////                        if (options.outHeight * options.outWidth >= targetHeight * targetWidth) {
////                            double sampleSize = scaleByHeight
////                                    ? options.outHeight / targetHeight
////                                    : options.outWidth / targetWidth;
////                            options.inSampleSize = (int) Math.pow(2d, Math.floor(Math.log(sampleSize) / Math.log(2d)));
////                        }
////
////                        try {
////                            bin.reset();
////                        } catch (IOException e) {
////                            bin = new BufferedInputStream(new FileInputStream(file));
////                        }
////
////                        // Do the actual decoding
////                        options.inJustDecodeBounds = false;
////                        bm = BitmapFactory.decodeStream(bin, null, options);
////                    } catch (Exception e) {
////                        Logger.e(e);
////                    }
////
////                    return bm;
////                }
////
////                @Override
////                public void onPostExecute(Object object, UrlDownloadAsyncTask task) {
////                    Object obj = null;
////                    if (object != null && object instanceof Bitmap && imageView.getTag() == task) {
////                        if (circle) {
//////                            Logger.i("ImageHelper.onPostExecute Drawable");
////                            Drawable drawable = new RoundedDrawable((Bitmap) object);
////                            imageView.setImageDrawable(drawable);
////                            obj = drawable;
////                        } else {
//////                            Logger.i("ImageHelper.onPostExecute Bitmap");
////
////                            Bitmap bitmap = (Bitmap) object;
////                            imageView.setImageBitmap(bitmap);
////                            obj = bitmap;
////
////                        }
////                    } else {
////                        int resource;
////
////                        if (type == IMG_TYPE_CARD) {
////                            resource = R.drawable.bg_card;
////                        } else if(type == IMG_TYPE_COMMON_BANKBOOK){
////                            resource = R.drawable.drawable_ffffff;
////                        } else {
////                            resource = R.drawable.no_image;
////                        }
//////                        Logger.i("ImageHelper.onPostExecute "+resource);
////                        imageView.setImageResource(resource);
////                        obj = resource;
////                    }
////
////                    if (helper != null) {
////                        Logger.i("ImageHelper.onPostExecute obj="+obj);
////                        helper.saveDrawable(obj);
////                    }
////                }
////            });
////
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
////                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////            } else {
////                task.execute();
////            }
////
////            imageView.setTag(task);
////        }
////
////        public UrlDownloadAsyncTask(String url, UrlDownloadAsyncTaskHandler handler) {
////            this.handler = handler;
////            this.url = url;
////        }
////
////        public interface UrlDownloadAsyncTaskHandler {
////            void onPreExecute();
////
////            Object doInBackground(File file);
////
////            void onPostExecute(Object object, UrlDownloadAsyncTask task);
////        }
////
////        @Override
////        protected void onPreExecute() {
////            if (handler != null) {
////                handler.onPreExecute();
////            }
////        }
////
////        protected Object doInBackground(Void... args) {
////            File outFile = null;
////            try {
////                if (cache.get(url) != null && new File(cache.get(url)).exists()) { // Cache Hit
////                    outFile = new File(cache.get(url));
////                } else { // Cache Miss, Downloading a file from the url.
////                    outFile = File.createTempFile("liivmate-download", ".tmp");
////                    OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outFile));
////
////                    InputStream input = null;
////                    // FIXME 3G/LTE로 붙으면 이미지1서버 이미지가 공인인증서로 노출되고, 개발AP로 붙으면 사설인증서로 노출되어서 임시 처리함
////                    if (AppConfig.isOP() == false && url.toLowerCase().startsWith("https")) {
////                        HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(url).openConnection();
////
////                        urlConnection.setSSLSocketFactory(getSSLSocketFactory());
////                        input = new BufferedInputStream(urlConnection.getInputStream());
////                    } else {
////                        input = new BufferedInputStream(new URL(url).openStream());
////                    }
////
////                    byte[] buf = new byte[1024 * 100];
////                    int read = 0;
////                    while ((read = input.read(buf, 0, buf.length)) >= 0) {
////                        outputStream.write(buf, 0, read);
////                    }
////
////                    outputStream.flush();
////                    outputStream.close();
////                    cache.put(url, outFile.getAbsolutePath());
////                }
////            } catch (IOException e) {
////                if (outFile != null) {
////                    outFile.delete();
////                }
////
////                outFile = null;
////            }
////
////
////            if (handler != null) {
////                return handler.doInBackground(outFile);
////            }
////
////            return outFile;
////        }
////
////        protected void onPostExecute(Object result) {
////            if (handler != null) {
////                handler.onPostExecute(result, this);
////            }
////        }
////
////        private static class LRUCache {
////            private final int maxSize;
////            private int totalSize;
////            private ConcurrentLinkedQueue<String> queue;
////            private ConcurrentHashMap<String, String> map;
////
////            public LRUCache(final int maxSize) {
////                this.maxSize = maxSize;
////                this.queue = new ConcurrentLinkedQueue<String>();
////                this.map = new ConcurrentHashMap<String, String>();
////            }
////
////            public String get(final String key) {
////                if (map.containsKey(key)) {
////                    queue.remove(key);
////                    queue.add(key);
////                }
////
////                return map.get(key);
////            }
////
////            public synchronized void put(final String key, final String value) {
////                if (key == null || value == null) {
////                    throw new NullPointerException();
////                }
////
////                if (map.containsKey(key)) {
////                    queue.remove(key);
////                }
////
////                queue.add(key);
////                map.put(key, value);
////                totalSize = totalSize + getSize(value);
////
////                while (totalSize >= maxSize) {
////                    String expiredKey = queue.poll();
////                    if (expiredKey != null) {
////                        totalSize = totalSize - getSize(map.remove(expiredKey));
////                    }
////                }
////            }
////
////            private int getSize(String value) {
////                return value.length();
////            }
//        }
//    }

    public static void displayUrlImage(ImageView imageView, String url, int type, IHelper iHelper) {
        if(TextUtils.isEmpty(url) == false) {
            displayUrlImage(imageView, url, false, type, iHelper);
        } else {
            displayUrlImage(imageView, "", false, type, iHelper);
        }
    }
    public static void displayUrlImage(ImageView imageView, String url, int type) {
        displayUrlImage(imageView, url, type, null);
    }

    public static void displayUrlImage(ImageView imageView, String url, boolean circle, int type, final IHelper helper) {
//        UrlDownloadAsyncTask.display(url, imageView, circle, type, helper);
    }
    public static void displayUrlImage(ImageView imageView, String url, boolean circle, int type) {
//        UrlDownloadAsyncTask.display(url, imageView, circle, type);
    }

    public static void downloadUrl(String url, String name, Context context) throws IOException {
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File downloadFile = File.createTempFile("SendBird", name.substring(name.lastIndexOf(".")), downloadDir);
//        UrlDownloadAsyncTask.download(url, downloadFile, context);
    }

    public static class RoundedDrawable extends Drawable {
        private final Bitmap mBitmap;
        private final Paint mPaint;
        private final RectF mRectF;
        private final int mBitmapWidth;
        private final int mBitmapHeight;

        public RoundedDrawable(Bitmap bitmap) {
            mBitmap = bitmap;
            mRectF = new RectF();
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            final BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            mPaint.setShader(shader);

            mBitmapWidth = mBitmap.getWidth();
            mBitmapHeight = mBitmap.getHeight();
        }

        @Override
        public void draw(Canvas canvas) {
            canvas.drawOval(mRectF, mPaint);
        }

        @Override
        protected void onBoundsChange(Rect bounds) {
            super.onBoundsChange(bounds);

            mRectF.set(bounds);
        }

        @Override
        public void setAlpha(int alpha) {
            if (mPaint.getAlpha() != alpha) {
                mPaint.setAlpha(alpha);
                invalidateSelf();
            }
        }

        @Override
        public void setColorFilter(ColorFilter cf) {
            mPaint.setColorFilter(cf);
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public int getIntrinsicWidth() {
            return mBitmapWidth;
        }

        @Override
        public int getIntrinsicHeight() {
            return mBitmapHeight;
        }

        public void setAntiAlias(boolean aa) {
            mPaint.setAntiAlias(aa);
            invalidateSelf();
        }

        @Override
        public void setFilterBitmap(boolean filter) {
            mPaint.setFilterBitmap(filter);
            invalidateSelf();
        }

        @Override
        public void setDither(boolean dither) {
            mPaint.setDither(dither);
            invalidateSelf();
        }

        public Bitmap getBitmap() {
            return mBitmap;
        }
    }


    private static SSLSocketFactory mSSLSocketFactory;

    private static SSLSocketFactory getSSLSocketFactory() {
        if (mSSLSocketFactory != null)
            return mSSLSocketFactory;

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] chain,
                    String authType)
                    throws java.security.cert.CertificateException {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] chain,
                    String authType)
                    throws java.security.cert.CertificateException {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            mSSLSocketFactory = sc.getSocketFactory();
            return mSSLSocketFactory;
        } catch (Exception e) {
            return null;
        }
    }

    public interface IHelper {
        void saveDrawable(Object obj);
    }
}
