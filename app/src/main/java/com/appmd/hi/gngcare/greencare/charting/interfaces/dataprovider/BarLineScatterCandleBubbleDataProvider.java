package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.components.YAxis.AxisDependency;
import com.appmd.hi.gngcare.greencare.charting.data.BarLineScatterCandleBubbleData;
import com.appmd.hi.gngcare.greencare.charting.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(AxisDependency axis);
    boolean isInverted(AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
