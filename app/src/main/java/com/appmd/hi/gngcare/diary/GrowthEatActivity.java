package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CP on 2018. 4. 7..
 */

public class GrowthEatActivity extends AppCompatActivity implements View.OnClickListener {

    //    private LinearLayout mVisibleLayout;
    private View view;
    private CustomAlertDialog mDialog;

    private LinearLayout eatBtn01, eatBtn02, eatBtn03;
    private TextView childInfoTxt;
    private TextView bonTxt;

    TextView commonTitleTv;

    ImageButton mCommonLeftBtn;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.growth_eat_fragment);

        intent = new Intent(GrowthEatActivity.this, GrowthEatDtlActivity.class);
        init();
        initEvent();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");
        // String info_sn = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);

    }

    protected void init(){
//        mCommonLeftBtn = (ImageButton)view.findViewById(R.id.common_left_btn);
        eatBtn01 = (LinearLayout)findViewById(R.id.eatBtn01);
        eatBtn02 = (LinearLayout)findViewById(R.id.eatBtn02);
        eatBtn03 = (LinearLayout)findViewById(R.id.eatBtn03);
        childInfoTxt = (TextView) findViewById(R.id.childInfoTxt);
        bonTxt = (TextView) findViewById(R.id.bonTxt);

        String cName =  MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNm();
        String cHeight =  MainActivity.mLastHeight;
        String cWeight =  MainActivity.mLastWeight;
        String nYear =  MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnAftLifyea();
        childInfoTxt.setText(cName+ " / "+ cHeight + "cm / " + cWeight + "kg");
//        bonTxt.setText(nYear);


        final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR2);
        bonTxt.setText(format.format(mDate));


        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        commonTitleTv = (TextView)findViewById(R.id.titleTxt);
        commonTitleTv.setText(getString(R.string.growth_eat_title));

        view = findViewById(R.id.root_view);
    }




    public void initEvent(){
        eatBtn01.setOnClickListener(this);
        eatBtn02.setOnClickListener(this);
        eatBtn03.setOnClickListener(this);
        mCommonLeftBtn.setOnClickListener(v -> finish());


        //click 저장
        OnClickListener mClickListener = new OnClickListener(null,view, GrowthEatActivity.this);

        //아이 성장
        eatBtn01.setOnTouchListener(mClickListener);
        eatBtn02.setOnTouchListener(mClickListener);
        eatBtn03.setOnTouchListener(mClickListener);

        //코드 부여(아이 성장)
        eatBtn01.setContentDescription(getString(R.string.eatBtn01));
        eatBtn02.setContentDescription(getString(R.string.eatBtn02));
        eatBtn03.setContentDescription(getString(R.string.eatBtn03));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.eatBtn01){
            intent.putExtra("DIET_TY", "3");
            intent.putExtra("TITLE", getString(R.string.growth_eat_title_01));

        }else if(v.getId() == R.id.eatBtn02){
            intent.putExtra("DIET_TY", "1");
            intent.putExtra("TITLE", getString(R.string.growth_eat_title_02));
        }else if(v.getId() == R.id.eatBtn03){
            intent.putExtra("DIET_TY", "2");
            intent.putExtra("TITLE", getString(R.string.growth_eat_title_03));
        }
        startActivity(intent);

    }

    @Override protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}

