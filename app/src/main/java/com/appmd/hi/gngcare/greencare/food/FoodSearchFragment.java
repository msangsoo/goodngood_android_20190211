package com.appmd.hi.gngcare.greencare.food;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cursoradapter.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.CommonActionBar;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperFoodCalorie;
import com.appmd.hi.gngcare.greencare.database.DBHelperFoodCalorieSearchHis;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;
import com.dpizarro.autolabel.library.Label;

import java.util.ArrayList;


/**
 * Created by mrsohn on 2017. 3. 14..
 * 혈당관리
 */

public class FoodSearchFragment extends BaseFragment {
    private final String TAG = FoodSearchFragment.class.getSimpleName();

    public static String BUNDLE_FOOD_DETAIL_INFO = "food_detail_info";

    private ListView mListView, mListViewKeyword;
    private EditText mSearchEditText;
    private ListAdapter mAdapter;
    private ListKeywordAdapter mKeywordAdapter;
    private String mTitle;
    private AutoLabelUI mAutoLabel;
    private Cursor hiscursor;
    Intent intent ;
    private DBHelper helper;
    private TextView actionBtn;
    private ArrayList<DBHelperFoodCalorie.Data> foodListDatas = new ArrayList<>();

    public static Fragment newInstance() {
        FoodSearchFragment fragment = new FoodSearchFragment();
        return fragment;
    }

    /**
     * 액션바 세팅
     */
    @Override
    public void loadActionbar(CommonActionBar actionBar) {
    }

    private void setActionBar() {
        // CommonActionBar actionBar 는 안 씀 한화꺼
        if (getActivity() instanceof DummyActivity) {
            DummyActivity activity = (DummyActivity) getActivity();
            actionBtn = (TextView)activity.findViewById(R.id.common_right_btn1);
            TextView titleTv = (TextView) activity.findViewById(R.id.common_title_tv);
            titleTv.setText(getString(R.string.select_food));
            actionBtn.setVisibility(View.VISIBLE);

            actionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(foodListDatas.size() > 0) {
                        getActivity().setResult(Activity.RESULT_OK, intent);
                        getActivity().finish();
                    }else{
                        Toast.makeText(activity, "음식을 등록해주세요!!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

            @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_food_search, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        mTitle = bundle.getString("Title");

        mSearchEditText = (EditText) view.findViewById(R.id.food_search_edittext);
        mListView = (ListView) view.findViewById(R.id.food_search_listview);
        mListViewKeyword = (ListView) view.findViewById(R.id.food_search_keyword);
        mAutoLabel = view.findViewById(R.id.label_view);

        mListView.setVisibility(View.GONE);

        view.findViewById(R.id.food_search_button).setOnClickListener(mClickListener);
        helper = new DBHelper(getContext());


        DBHelperFoodCalorie db = helper.getFoodCalorieDb();
        Cursor cursor = db.getResult("");

        mAdapter = new ListAdapter(getContext(), cursor, true);
        mListView.setAdapter(mAdapter);
        getkeywordDB();



        mSearchEditText.addTextChangedListener(mWatcher);

        setListeners();
        setAutoLabelUISettings();

        setActionBar();
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.food_search_button) {

            }
        }
    };

    public void getkeywordDB(){
        DBHelper helper = new DBHelper(getContext());
        DBHelperFoodCalorieSearchHis hisdb = helper.getFoodCalorieSearchHisDb();
        hiscursor = hisdb.getResult("");

        mKeywordAdapter = new ListKeywordAdapter(getContext(), hiscursor, true);
        mListViewKeyword.setAdapter(mKeywordAdapter);

        mSearchEditText.setText("");

        mListView.setVisibility(View.GONE);
        mListViewKeyword.setVisibility(View.VISIBLE);
    }

    class ListAdapter extends CursorAdapter {

        public ListAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
        }

        public ListAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_food_search_item, null);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final DBHelperFoodCalorie.Data data = getDataCursor(cursor);

            TextView foodTextView = (TextView) view.findViewById(R.id.food_qty_textview);
            foodTextView.setText(data.food_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCalorieDlg(data);
                }
            });
        }
    }


    class ListKeywordAdapter extends CursorAdapter {

        public ListKeywordAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
        }

        public ListKeywordAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_food_search_keyword_item, null);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final DBHelperFoodCalorie.Data data = getDataCursor(cursor);

            TextView foodkeywordTextView = (TextView) view.findViewById(R.id.food_keyword_textview);
            foodkeywordTextView.setText(data.food_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCalorieDlg(data);
                }
            });

            view.findViewById(R.id.food_keyword_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DBHelperFoodCalorieSearchHis db = helper.getFoodCalorieSearchHisDb();
                    db.delete(data.food_name);
                    getkeywordDB();

                }
            });
        }
    }

    TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            String str = s.toString();
            Logger.i(TAG, "watcher="+str);
            if (TextUtils.isEmpty(str))
                str = "";
            else
                str = s.toString();

            if (TextUtils.isEmpty(str.trim()))
                return ;

            DBHelperFoodCalorie db = helper.getFoodCalorieDb();
            Cursor cursor = db.getResult(str);
            if (mAdapter != null) {
                mAdapter.swapCursor(cursor);
                mListView.setVisibility(View.VISIBLE);
                mListViewKeyword.setVisibility(View.GONE);
            }


//            cursor.close();
        }
    };
    /**
     * 태그 Dialog
     *
     */

    private void setAutoLabelUISettings() {
        AutoLabelUISettings autoLabelUISettings =
                new AutoLabelUISettings.Builder()
                        .withBackgroundResource(R.drawable.food_add_background)
                        .withMaxLabels(15)
                        .withShowCross(false)
                        .withLabelsClickables(true)
                        .withTextColor(R.color.color_93286C)
                        .withTextSize(R.dimen._14_dp)
                        .withLabelPadding(30)
                        .build();

        mAutoLabel.setSettings(autoLabelUISettings);
    }

    private void setListeners() {
        mAutoLabel.setOnLabelsCompletedListener(new AutoLabelUI.OnLabelsCompletedListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onLabelsCompleted() {
                Toast.makeText(getContext(), "15개 까지 등록할 수 있습니다.", Toast.LENGTH_SHORT).show();
            }
        });

        mAutoLabel.setOnRemoveLabelListener(new AutoLabelUI.OnRemoveLabelListener() {
            @Override
            public void onRemoveLabel(View view, int position) {
                Logger.i(TAG, "mAutoLabel.getLabels().size()="+mAutoLabel.getLabels().size());
                Logger.i(TAG, "foodListDatas.size()="+foodListDatas.size());
                for(int i=0; i<foodListDatas.size(); i++){
                    Logger.i(TAG, "foodListDatas.get(i).food_name="+foodListDatas.get(i).food_name);
                    String temp = foodListDatas.get(i).food_name+" "+StringUtil.getNoneZeroString2(StringUtil.getFloat(foodListDatas.get(i).forpeople))+"인분 ×";
                    if(temp.equals(view.getTag().toString())){
                        foodListDatas.remove(i);
                        Logger.i(TAG, "foodListDatas.size()_remove="+foodListDatas.size());
                    }
                }

            }
        });

        mAutoLabel.setOnLabelsEmptyListener(new AutoLabelUI.OnLabelsEmptyListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onLabelsEmpty() {
            }
        });

        mAutoLabel.setOnLabelClickListener(new AutoLabelUI.OnLabelClickListener() {
            @Override
            public void onClickLabel(View view) {
                Logger.i(TAG, "mAutoLabel.getLabels().size()="+mAutoLabel.getLabels().size());
                Logger.i(TAG, "foodListDatas.size()="+foodListDatas.size());
                for(int i=0; i<foodListDatas.size(); i++){
                    Logger.i(TAG, "foodListDatas.get(i).food_name="+foodListDatas.get(i).food_name);
                    Logger.i(TAG, "view.getTag().toString()="+view.getTag().toString());
                    String temp = foodListDatas.get(i).food_name+" "+StringUtil.getNoneZeroString2(StringUtil.getFloat(foodListDatas.get(i).forpeople))+"인분 ×";
                    if(temp.equals(view.getTag().toString())){
                        foodListDatas.remove(i);
                        mAutoLabel.removeLabel(view.getTag().toString()); //라벨 지우기

                        Logger.i(TAG, "foodListDatas.size()_remove="+foodListDatas.size());
                    }
                }
                actionBtn.setText("완료("+foodListDatas.size()+")");
            }
        });
    }


    /**
     * 음식 상세 Dialog
     * @param data
     */
    private FoodDetailDialog mFoodDlg;
    private void showCalorieDlg(final DBHelperFoodCalorie.Data data) {
        mFoodDlg = new FoodDetailDialog(getContext(), data, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelperFoodCalorie.Data foodData = mFoodDlg.getFoodData();
                DBHelperFoodCalorieSearchHis db = helper.getFoodCalorieSearchHisDb();
                db.insert(foodData);
//                CDialog.showDlg(getContext(), ""+foodData.food_name+" "+foodData.forpeople+"인분\n추가하였습니다.");
                getkeywordDB();
                FoodUpdate(foodData);


            }
        });
    }


    /**
     * array_중복체크
     *
     */
    public void FoodUpdate(DBHelperFoodCalorie.Data foodData){
        boolean is_max = false;


        DBHelperFoodCalorie.Data sameData = null;
        String updateText = null;
        int sameIdx = -1;
        exit : for(int i = 0; i < foodListDatas.size(); i++){
            if(foodListDatas.get(i).food_name.equals(foodData.food_name)){
                foodListDatas.get(i).food_gram = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_gram) + StringUtil.getFloat(foodData.food_gram));                           // 1회제공량
                foodListDatas.get(i).food_calorie = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_calorie) + StringUtil.getFloat(foodData.food_calorie));                  // 열량
                foodListDatas.get(i).food_carbohydrate = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_carbohydrate) + StringUtil.getFloat(foodData.food_carbohydrate));   // 탄수화물
                foodListDatas.get(i).food_protein = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_protein) + StringUtil.getFloat(foodData.food_protein));                  // 단백질
                foodListDatas.get(i).food_fat = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_fat) + StringUtil.getFloat(foodData.food_fat));                              // 지방
                foodListDatas.get(i).food_sugars = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_sugars) + StringUtil.getFloat(foodData.food_sugars));                     // 당류
                foodListDatas.get(i).food_salt = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_salt) + StringUtil.getFloat(foodData.food_salt));                           // 나트륨
                foodListDatas.get(i).food_cholesterol = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_cholesterol) + StringUtil.getFloat(foodData.food_cholesterol));      // 콜레스테롤
                foodListDatas.get(i).food_saturated = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_saturated) + StringUtil.getFloat(foodData.food_saturated));            // 포화지방산
                foodListDatas.get(i).food_ctransquantic = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).food_ctransquantic) + StringUtil.getFloat(foodData.food_ctransquantic));// 트랜스지방산
                foodListDatas.get(i).forpeople = String.valueOf(StringUtil.getFloat(foodListDatas.get(i).forpeople)  + StringUtil.getFloat(foodData.forpeople));                           // 인분

                mAutoLabel.getLabel(i).setText(foodData.food_name+" "+StringUtil.getNoneZeroString2(StringUtil.getFloat(foodListDatas.get(i).forpeople))+"인분 ×");
                mAutoLabel.getLabel(i).setTag(foodData.food_name+" "+StringUtil.getNoneZeroString2(StringUtil.getFloat(foodListDatas.get(i).forpeople))+"인분 ×");

                Logger.i(TAG, "forpeople="+foodListDatas.get(i).forpeople);
                Logger.i(TAG, "food_calorie="+foodListDatas.get(i).food_calorie);


                //mAutoLabel layout갱신을 위해
                ArrayList<Label> label_list = new ArrayList<>();
                label_list.addAll(mAutoLabel.getLabels());
                mAutoLabel.clear();
                for(int j=0; j<label_list.size(); j++){
                    mAutoLabel.addLabel(label_list.get(j).getText());
                }


                is_max = false;

                break exit;
            }else{
                is_max = true;
            }
        }


        if(is_max || foodListDatas.size() == 0){
            if(foodListDatas.size() < 15) {
                foodListDatas.add(foodData);
                mAutoLabel.addLabel(foodData.food_name + " " + StringUtil.getNoneZeroString2(StringUtil.getFloat(foodData.forpeople)) + "인분 ×");
                Logger.i(TAG, "data.forpeople="+StringUtil.getNoneZeroString2(StringUtil.getFloat(foodData.forpeople)));
            }else{
                //Toast 띄우기 위해
                mAutoLabel.addLabel(foodData.food_name + " " + StringUtil.getNoneZeroString2(StringUtil.getFloat(foodData.forpeople)) + "인분 ×");
            }

        }



        actionBtn.setText("완료("+foodListDatas.size()+")");

        Logger.i(TAG, "foodListDatas="+is_max);


        intent = new Intent();
        intent.putExtra(BUNDLE_FOOD_DETAIL_INFO, foodListDatas);



    }

    private DBHelperFoodCalorie.Data getDataCursor(Cursor cursor) {
        DBHelperFoodCalorie.Field tb = new DBHelperFoodCalorie.Field();
        DBHelperFoodCalorie.Data data = new DBHelperFoodCalorie.Data();
        data.food_code = cursor.getString(cursor.getColumnIndex(tb.FOOD_CODE));
        data.food_kind = cursor.getString(cursor.getColumnIndex(tb.FOOD_KIND));
        data.food_name = cursor.getString(cursor.getColumnIndex(tb.FOOD_NAME));
        data.food_gram = cursor.getString(cursor.getColumnIndex(tb.FOOD_GRAM));
        data.food_unit = cursor.getString(cursor.getColumnIndex(tb.FOOD_UNIT));
        data.food_calorie = cursor.getString(cursor.getColumnIndex(tb.FOOD_CALORIE));
        data.food_carbohydrate = cursor.getString(cursor.getColumnIndex(tb.FOOD_CARBOHYDRATE));
        data.food_fat = cursor.getString(cursor.getColumnIndex(tb.FOOD_FAT));
        data.food_protein = cursor.getString(cursor.getColumnIndex(tb.FOOD_PROTEIN));
        data.food_sugars = cursor.getString(cursor.getColumnIndex(tb.FOOD_SUGARS));
        data.food_salt = cursor.getString(cursor.getColumnIndex(tb.FOOD_SALT));
        data.food_cholesterol = cursor.getString(cursor.getColumnIndex(tb.FOOD_CHOLESTEROL));
        data.food_saturated = cursor.getString(cursor.getColumnIndex(tb.FOOD_SATURATED));
        data.food_transquanti = cursor.getString(cursor.getColumnIndex(tb.FOOD_TRANSQUANTI));

        return data;
    }
}
