package com.appmd.hi.gngcare.greencare.food;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.base.value.Define;
import com.appmd.hi.gngcare.greencare.base.value.TypeDataSet;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperFoodMain;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_meal_input_data;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_meal_input_food_data;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.ChartTimeUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.ViewUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * 등록하기 화면
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodManageWriteView {
    private static final String TAG = FoodManageWriteView.class.getSimpleName();

    private final int FOOD_INPUT_REQ = 8888;

    public ChartTimeUtil mTimeClass;

    private RelativeLayout mBreafastInputLayout;
    private RelativeLayout mBreakfastSnackInputLayout;
    private RelativeLayout mLunchInputLayout;
    private RelativeLayout mLunchSnackInputLayout;
    private RelativeLayout mDinnerInputLayout;
    private RelativeLayout mDinnerSnackInputLayout;

    private RelativeLayout mHeight_Layout1;
    private RelativeLayout mHeight_Layout2;
    private RelativeLayout mHeight_Layout3;
    private RelativeLayout mHeight_Layout4;
    private RelativeLayout mHeight_Layout5;
    private RelativeLayout mHeight_Layout6;


    private ImageView mFoodIv01;
    private ImageView mFoodIv02;
    private ImageView mFoodIv03;
    private ImageView mFoodIv04;
    private ImageView mFoodIv05;
    private ImageView mFoodIv06;

    private TextView mKcalTv01;
    private TextView mKcalTv02;
    private TextView mKcalTv03;
    private TextView mKcalTv04;
    private TextView mKcalTv05;
    private TextView mKcalTv06;

    private TextView mTimeTv01;
    private TextView mTimeTv02;
    private TextView mTimeTv03;


    private TextView mDateTv;
    private BaseFragment mBaseFragment;
    private View mView;

    private Tr_get_meal_input_data.ReceiveDatas mMealDataA;     // 아침
    private Tr_get_meal_input_data.ReceiveDatas mMealDataB;     // 점심
    private Tr_get_meal_input_data.ReceiveDatas mMealDataC;     // 저녁
    private Tr_get_meal_input_data.ReceiveDatas mMealDataD;     // 아침간식
    private Tr_get_meal_input_data.ReceiveDatas mMealDataE;     // 점심간식
    private Tr_get_meal_input_data.ReceiveDatas mMealDataF;     // 저녁간식

    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListA = new ArrayList<>();  // 아침
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListB = new ArrayList<>();  // 점심
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListC = new ArrayList<>();  // 저녁
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListD = new ArrayList<>();  // 아침간식
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListE = new ArrayList<>();  // 점심간식
    private List<Tr_get_meal_input_food_data.ReceiveDatas> mFoodListF = new ArrayList<>();  // 저녁간식

    private ImageView foodbreakfestimage;
    private ImageView foodbreakfestsnackimage;
    private ImageView foodlunchimage;
    private ImageView foodlunchsnackimage;
    private ImageView fooddinnerimage;
    private ImageView fooddinnersnackimage;

    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private FoodHistoryProgress mProgressView;
    private List<DBHelperFoodMain.Data> mFoodNutriList = new ArrayList<>();
    private TextView mCarbohydrateValue, mProteinValue, mFatValue;



    public FoodManageWriteView(BaseFragment baseFragment, View view) {
        mBaseFragment = baseFragment;
        mView = view;

        mDateTv = (TextView) view.findViewById(R.id.txtCurrDate);

        mFoodIv01 = (ImageView) view.findViewById(R.id.ivImage01);
        mFoodIv02 = (ImageView) view.findViewById(R.id.ivImage02);
        mFoodIv03 = (ImageView) view.findViewById(R.id.ivImage03);
        mFoodIv04 = (ImageView) view.findViewById(R.id.ivImage04);
        mFoodIv05 = (ImageView) view.findViewById(R.id.ivImage05);
        mFoodIv06 = (ImageView) view.findViewById(R.id.ivImage06);

        mKcalTv01 = (TextView) view.findViewById(R.id.tvKcal1);
        mKcalTv02 = (TextView) view.findViewById(R.id.tvKcal2);
        mKcalTv03 = (TextView) view.findViewById(R.id.tvKcal3);
        mKcalTv04 = (TextView) view.findViewById(R.id.tvKcal4);
        mKcalTv05 = (TextView) view.findViewById(R.id.tvKcal5);
        mKcalTv06 = (TextView) view.findViewById(R.id.tvKcal6);

        foodbreakfestimage = (ImageView) view.findViewById(R.id.food_breakfest_image);             // 아침
        foodbreakfestsnackimage = (ImageView) view.findViewById(R.id.food_breakfest_snack_image);  // 아침간식
        foodlunchimage = (ImageView) view.findViewById(R.id.food_lunch_image);                     // 점심
        foodlunchsnackimage = (ImageView) view.findViewById(R.id.food_lunch_snack_image);          // 점심간식
        fooddinnerimage = (ImageView) view.findViewById(R.id.food_dinner_image);                   // 저녁
        fooddinnersnackimage = (ImageView) view.findViewById(R.id.food_dinner_snack_image);        // 저녁간식

        mTimeTv01 = (TextView) view.findViewById(R.id.tvTime01);
        mTimeTv02 = (TextView) view.findViewById(R.id.tvTime02);
        mTimeTv03 = (TextView) view.findViewById(R.id.tvTime03);
        imgPre_btn                  = (ImageButton) view.findViewById(R.id.btn_calLeft);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.btn_calRight);

        mBreafastInputLayout = (RelativeLayout) view.findViewById(R.id.food_breakfest_input_layout);
        mBreakfastSnackInputLayout = (RelativeLayout) view.findViewById(R.id.food_breakfast_snack_input_layout);
        mLunchInputLayout = (RelativeLayout) view.findViewById(R.id.food_lunch_input_layout);
        mLunchSnackInputLayout = (RelativeLayout) view.findViewById(R.id.food_lunch_snack_input_layout);
        mDinnerInputLayout = (RelativeLayout) view.findViewById(R.id.food_dinner_input_layout);
        mDinnerSnackInputLayout = (RelativeLayout) view.findViewById(R.id.food_dinner_snack_input_layout);

        mProgressView = view.findViewById(R.id.food_history_progress);

        mCarbohydrateValue =  view.findViewById(R.id.carbohydrate_tv);
        mProteinValue =  view.findViewById(R.id.protein_tv);
        mFatValue =  view.findViewById(R.id.fat_tv);

        mBreafastInputLayout.setOnClickListener(mClickListener);
        mBreakfastSnackInputLayout.setOnClickListener(mClickListener);
        mLunchInputLayout.setOnClickListener(mClickListener);
        mLunchSnackInputLayout.setOnClickListener(mClickListener);
        mDinnerInputLayout.setOnClickListener(mClickListener);
        mDinnerSnackInputLayout.setOnClickListener(mClickListener);

        view.findViewById(R.id.btn_calLeft).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_calRight).setOnClickListener(mClickListener);

        mTimeClass = new ChartTimeUtil(TypeDataSet.Period.PERIOD_DAY);

        // 음식 검색후 재 진입시 날자 데이터
        Bundle backBundle = mBaseFragment.getBackData();
        String date = backBundle.getString(FoodInputFragment.BUNDLE_FOOD_INPUT_DATE);
        Logger.i(TAG, "backBundle.date=" + date);
        if (backBundle != null && TextUtils.isEmpty(date) == false) {
            Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(date);
            mTimeClass.setStartTime(cal.getTimeInMillis());
        }

        getData();
        setNextButtonVisible();


        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, mBaseFragment.getContext());

        //엄마 건강
        mBreafastInputLayout.setOnTouchListener(ClickListener);
        mLunchInputLayout.setOnTouchListener(ClickListener);
        mDinnerInputLayout.setOnTouchListener(ClickListener);
        mBreakfastSnackInputLayout.setOnTouchListener(ClickListener);
        mLunchSnackInputLayout.setOnTouchListener(ClickListener);
        mDinnerSnackInputLayout.setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        mBreafastInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.BreafastInputLayout));
        mLunchInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.LunchInputLayout));
        mDinnerInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.DinnerInputLayout));
        mBreakfastSnackInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.BreakfastSnackInputLayout));
        mLunchSnackInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.LunchSnackInputLayout));
        mDinnerSnackInputLayout.setContentDescription(mBaseFragment.getContext().getString(R.string.DinnerSnackInputLayout));



    }
    public void setVisibility(int visibility) {
        mView.setVisibility(visibility);
    }

    public int getVisibility() {
        if (mView == null)
            return View.GONE;
        else
            return mView.getVisibility();
    }

    /*ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @SuppressLint("NewApi")
        @Override
        public void onGlobalLayout() {

            RelativeLayout array_layout [] = {mHeight_Layout1,mHeight_Layout2,mHeight_Layout3,mHeight_Layout4,mHeight_Layout5,mHeight_Layout6};

            for(int i = 0; i<array_layout.length; i++) {
                ViewGroup.LayoutParams params = array_layout[i].getLayoutParams();
                params.height = array_layout[i].getMeasuredWidth();
                array_layout[i].setLayoutParams(params);
                array_layout[i].getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }
    };*/

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.btn_calLeft) {
                mTimeClass.calTime(-1);
                getData();
            } else if (vId == R.id.btn_calRight) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
                getData();
            } else if (vId == R.id.food_breakfest_input_layout
                    || vId == R.id.food_breakfast_snack_input_layout
                    || vId == R.id.food_lunch_input_layout
                    || vId == R.id.food_lunch_snack_input_layout
                    || vId == R.id.food_dinner_input_layout
                    || vId == R.id.food_dinner_snack_input_layout) {

                FoodInputFragment fragment = (FoodInputFragment) FoodInputFragment.newInstance();
                String mealType = v.getTag().toString();
                Bundle bundle = getBundleData(mealType);
                fragment.setArguments(bundle);
                DummyActivity.startActivityForResult(mBaseFragment, FOOD_INPUT_REQ, fragment.getClass(), bundle);
            }
            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }
    /**
     * 날자 계산 후 조회
     */
    public void getData() {
        mBaseFragment.showProgress();
        long startTime = mTimeClass.getStartTime();

        mMealDataA = null;     // 아침
        mMealDataB = null;     // 점심
        mMealDataC = null;     // 저녁
        mMealDataD = null;     // 아침간식
        mMealDataE = null;     // 점심간식
        mMealDataF = null;     // 저녁간식

        mFoodListA.clear();  // 아침
        mFoodListB.clear();  // 점심
        mFoodListC.clear();  // 저녁
        mFoodListD.clear();  // 아침간식
        mFoodListE.clear();  // 점심간식
        mFoodListF.clear();  // 저녁간식

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        String startDate = sdf.format(startTime);
        mDateTv.setText(startDate);

        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);

        getMealData(startDate);

        mProgressView.setProgress(setCalory(startDate));





        mBaseFragment.hideProgress();
    }

    /**
     * 섭취 칼로리 가져오기
     */
    private int setCalory(String startDate){

        DBHelper helper = new DBHelper(mBaseFragment.getContext());

        DBHelperFoodMain db = helper.getFoodMainDb();
        int[] datas = db.getMealSum(startDate, startDate);
        // 섭취칼로리
        int totTakeCal = datas[0] + datas[1] + datas[2];

        Log.d(TAG,"calory:"+totTakeCal);

        mFoodNutriList.clear();
        mFoodNutriList.addAll(getNutri(startDate,(float)totTakeCal));


        String carbohydrate = String.format("%.1f",mFoodNutriList.get(0).carbohydrate);
        String protein = String.format("%.1f",mFoodNutriList.get(0).protein);
        String fat = String.format("%.1f",mFoodNutriList.get(0).fat);

        //영양소 셋팅
        mCarbohydrateValue.setText(carbohydrate+"g");
        mProteinValue.setText(protein+"g");
        mFatValue.setText(fat+"g");

        if(mFoodNutriList.get(0).Recomand_carbohydrate==1){
            mCarbohydrateValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_FFBD39));
        } else if(mFoodNutriList.get(0).Recomand_carbohydrate == 2){
            mCarbohydrateValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_00B15A));
        } else{
            mCarbohydrateValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_E7000C));
        }

        if(mFoodNutriList.get(0).Recomand_protein==1){
            mProteinValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_FFBD39));
        } else if(mFoodNutriList.get(0).Recomand_protein == 2){
            mProteinValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_00B15A));
        } else{
            mProteinValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_E7000C));
        }

        if(mFoodNutriList.get(0).Recomand_fat==1){
            mFatValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_FFBD39));
        } else if(mFoodNutriList.get(0).Recomand_fat == 2){
            mFatValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_00B15A));
        } else{
            mFatValue.setTextColor(mBaseFragment.getContext().getResources().getColor(R.color.color_E7000C));
        }

        return totTakeCal;
    }

    /**
     * 영양소 정보 가져오기
     */

    private List<DBHelperFoodMain.Data> getNutri(String startDate, float totTakeCal){
        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperFoodMain db = helper.getFoodMainDb();
        return db.getNutrient(startDate, startDate, totTakeCal, (float)new DeviceDataUtil().getPregnancyRecommendCal(), 1);
    }



    /**
     * 데이터 가져오기(식사)
     */
    private void getMealData(final String reqDate) {
        DBHelper helper = new DBHelper(mBaseFragment.getContext());
        DBHelperFoodMain db = helper.getFoodMainDb();
        Tr_get_meal_input_data foodData = db.getResultDay(reqDate);
        setMealData(foodData);
    }

    /**
     * 식사데이터
     *
     * @return
     */
    private void setMealData(Tr_get_meal_input_data data) {
        mFoodIv01.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food_after));
        mFoodIv02.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food2_after));
        mFoodIv03.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food_after));
        mFoodIv04.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food2_after));
        mFoodIv05.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food_after));
        mFoodIv06.setImageDrawable(ContextCompat.getDrawable(mBaseFragment.getContext(), R.drawable.icon_food2_after));
        mFoodIv01.setVisibility(View.VISIBLE);
        mFoodIv02.setVisibility(View.VISIBLE);
        mFoodIv03.setVisibility(View.VISIBLE);
        mFoodIv04.setVisibility(View.VISIBLE);
        mFoodIv05.setVisibility(View.VISIBLE);
        mFoodIv06.setVisibility(View.VISIBLE);

        foodbreakfestimage.setImageBitmap(null);
        foodbreakfestsnackimage.setImageBitmap(null);
        foodlunchimage.setImageBitmap(null);
        foodlunchsnackimage.setImageBitmap(null);
        fooddinnerimage.setImageBitmap(null);
        fooddinnersnackimage.setImageBitmap(null);


        mKcalTv01.setText("0 kcal");
        mKcalTv01.setTypeface(null,Typeface.NORMAL);
        mKcalTv01.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
        mTimeTv01.setText("   ");
        mTimeTv01.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_time, 0, 0, 0);
        mKcalTv02.setText("0 kcal");
        mKcalTv02.setTypeface(null,Typeface.NORMAL);
        mKcalTv02.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
        mTimeTv02.setText("   ");
        mTimeTv02.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_time, 0, 0, 0);
        mKcalTv03.setText("0 kcal");
        mKcalTv03.setTypeface(null,Typeface.NORMAL);
        mKcalTv03.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
        mTimeTv03.setText("   ");
        mTimeTv03.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_time, 0, 0, 0);
        mKcalTv04.setText("0 kcal");
        mKcalTv04.setTypeface(null,Typeface.NORMAL);
        mKcalTv04.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
        mKcalTv05.setText("0 kcal");
        mKcalTv05.setTypeface(null,Typeface.NORMAL);
        mKcalTv05.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
        mKcalTv06.setText("0 kcal");
        mKcalTv06.setTypeface(null,Typeface.NORMAL);
        mKcalTv06.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));

        for (Tr_get_meal_input_data.ReceiveDatas recv : data.data_list) {

            String mealType = recv.mealtype;
            String idx = recv.idx;
            String definePhotoPath = Define.getFoodPhotoPath(idx);
            Logger.i(TAG, "mealType=" + mealType + ", idx=" + idx + ", photoPath=" + definePhotoPath + ", amounttime=" + recv.amounttime);
            if (mBaseFragment.getContext().getString(R.string.text_breakfast_code).equals(mealType)) {
                mMealDataA = recv;
                mKcalTv01.setText(recv.calorie + " kcal");
                mKcalTv01.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
                if (TextUtils.isEmpty(recv.calorie) == false) {
                    if (TextUtils.isEmpty(recv.amounttime)==true)
                        mTimeTv01.setText("");
                    else
                        mTimeTv01.setText(recv.amounttime + "분");

                    mTimeTv01.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

//                mKcalTv01.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                if (TextUtils.isEmpty(mMealDataA.picture) == false){
//                    mBaseFragment.getImageData(mMealDataA.picture, mFoodIv01);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataA.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(foodbreakfestimage);
                    mKcalTv01.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv01.setVisibility(View.GONE);  // 이미지 세팅
                    mKcalTv01.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, foodbreakfestimage, mFoodIv01, R.drawable.icon_food_after,mKcalTv01,mBaseFragment.getContext());   // 이미지 세팅
                }
            } else if (mBaseFragment.getContext().getString(R.string.text_lunch_code).equals(mealType)) {
                mMealDataB = recv;
                mKcalTv03.setText(recv.calorie + " kcal");
                mKcalTv03.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
                if (TextUtils.isEmpty(recv.calorie) == false) {
                    if (TextUtils.isEmpty(recv.amounttime)==true)
                        mTimeTv02.setText("");
                    else
                        mTimeTv02.setText(recv.amounttime + "분");
                    mTimeTv02.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                //foodlunchimage.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mMealDataB.picture) == false){
//                    mBaseFragment.getImageData(mMealDataB.picture, mFoodIv03);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataB.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(foodlunchimage);
                    mKcalTv03.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv03.setVisibility(View.GONE);
                    mKcalTv03.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, foodlunchimage,mFoodIv03,R.drawable.icon_food_after,mKcalTv03,mBaseFragment.getContext());   // 이미지 세팅
                }

            } else if (mBaseFragment.getContext().getString(R.string.text_dinner_code).equals(mealType)) {
                mMealDataC = recv;
                mKcalTv05.setText(recv.calorie + " kcal");
                mKcalTv05.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
                if (TextUtils.isEmpty(recv.calorie) == false) {
                    if (TextUtils.isEmpty(recv.amounttime)==true)
                        mTimeTv03.setText("");
                    else
                        mTimeTv03.setText(recv.amounttime + "분");
                    mTimeTv03.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
               // fooddinnerimage.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mMealDataC.picture) == false){
//                    mBaseFragment.getImageData(mMealDataB.picture, mFoodIv05);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataC.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(fooddinnerimage);
                    mKcalTv05.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv05.setVisibility(View.GONE);
                    mKcalTv05.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, fooddinnerimage,mFoodIv05,R.drawable.icon_food_after,mKcalTv05,mBaseFragment.getContext());   // 이미지 세팅
                }
            } else if (mBaseFragment.getContext().getString(R.string.text_breakfast_snack_code).equals(mealType)) {
                mMealDataD = recv;
                mKcalTv02.setText(recv.calorie + " kcal");
                mKcalTv02.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
                //foodbreakfestsnackimage.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mMealDataD.picture) == false){
//                    mBaseFragment.getImageData(mMealDataD.picture, mFoodIv02);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataD.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(foodbreakfestsnackimage);
                    mKcalTv02.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv02.setVisibility(View.GONE);
                    mKcalTv02.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, foodbreakfestsnackimage,mFoodIv02,R.drawable.icon_food2_after,mKcalTv02,mBaseFragment.getContext());   // 이미지 세팅
                }
            } else if (mBaseFragment.getContext().getString(R.string.text_lunch_snack_code).equals(mealType)) {
                mMealDataE = recv;
                mKcalTv04.setText(recv.calorie + " kcal");
                mKcalTv04.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
               // foodlunchsnackimage.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mMealDataE.picture) == false){
//                    mBaseFragment.getImageData(mMealDataE.picture, mFoodIv04);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataE.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(foodlunchsnackimage);
                    mKcalTv04.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv04.setVisibility(View.GONE);
                    mKcalTv04.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, foodlunchsnackimage,mFoodIv04,R.drawable.icon_food2_after,mKcalTv04,mBaseFragment.getContext());   // 이미지 세팅
                }
            } else if (mBaseFragment.getContext().getString(R.string.text_dinner_snack_code).equals(mealType)) {
                mMealDataF = recv;
                mKcalTv06.setText(recv.calorie + " kcal");
                mKcalTv06.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.color_FB8AD3));
                //fooddinnersnackimage.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mMealDataF.picture) == false){
//                    mBaseFragment.getImageData(mMealDataF.picture, mFoodIv06);  // 서버에 이미지가 있다면 서버이미지 우선.
                    Glide.with(mBaseFragment).load(mMealDataF.picture)
                            .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(fooddinnersnackimage);
                    mKcalTv06.setTextColor(ContextCompat.getColor(mBaseFragment.getContext(), R.color.colorWhite));
                    mFoodIv06.setVisibility(View.GONE);
                    mKcalTv06.setTypeface(null, Typeface.NORMAL);
                }else{
                    ViewUtil.getIndexToImageData(idx, fooddinnersnackimage,mFoodIv06,R.drawable.icon_food2_after,mKcalTv06,mBaseFragment.getContext());   // 이미지 세팅
                }
            }
        }
    }

    /**
     * 음식 데이터
     *
     * @return
     */
    private void setFoodListData(Tr_get_meal_input_food_data data) {
        mFoodListA.clear();
        mFoodListB.clear();
        mFoodListC.clear();
        mFoodListD.clear();
        mFoodListE.clear();
        mFoodListF.clear();
        for (Tr_get_meal_input_food_data.ReceiveDatas recv : data.data_list) {
            String mealType = recv.forpeople;
            if (mBaseFragment.getContext().getString(R.string.text_breakfast_code).equals(mealType)) {
                mFoodListA.add(recv);
            } else if (mBaseFragment.getContext().getString(R.string.text_lunch_code).equals(mealType)) {
                mFoodListB.add(recv);
            } else if (mBaseFragment.getContext().getString(R.string.text_dinner_code).equals(mealType)) {
                mFoodListC.add(recv);
            } else if (mBaseFragment.getContext().getString(R.string.text_breakfast_snack_code).equals(mealType)) {
                mFoodListD.add(recv);
            } else if (mBaseFragment.getContext().getString(R.string.text_lunch_snack_code).equals(mealType)) {
                mFoodListE.add(recv);
            } else if (mBaseFragment.getContext().getString(R.string.text_dinner_snack_code).equals(mealType)) {
                mFoodListF.add(recv);
            }
        }
    }

    /**
     * 음식검색에서 사용될 BundleData
     *
     * @param mealType
     * @return
     */
    private Bundle getBundleData(String mealType) {
        Tr_get_meal_input_data.ReceiveDatas mealData = null;
        List<Tr_get_meal_input_food_data.ReceiveDatas> foodList = new ArrayList<>();
        String title = "";
        if (mBaseFragment.getContext().getString(R.string.text_breakfast_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_breakfast);
            mealData = mMealDataA;
            foodList.addAll(mFoodListA);
        } else if (mBaseFragment.getContext().getString(R.string.text_lunch_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_lunch);
            mealData = mMealDataB;
            foodList.addAll(mFoodListB);
        } else if (mBaseFragment.getContext().getString(R.string.text_dinner_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_dinner);
            mealData = mMealDataC;
            foodList.addAll(mFoodListC);
        } else if (mBaseFragment.getContext().getString(R.string.text_breakfast_snack_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_breakfast_snack);
            mealData = mMealDataD;
            foodList.addAll(mFoodListD);
        } else if (mBaseFragment.getContext().getString(R.string.text_lunch_snack_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_lunch_snack);
            mealData = mMealDataE;
            foodList.addAll(mFoodListE);
        } else if (mBaseFragment.getContext().getString(R.string.text_dinner_snack_code).equals(mealType)) {
            title = mBaseFragment.getContext().getString(R.string.text_dinner_snack);
            mealData = mMealDataF;
            foodList.addAll(mFoodListF);
        }

        Bundle bundle = new Bundle();
        String date = mDateTv.getText().toString();
        bundle.putString(FoodInputFragment.BUNDLE_FOOD_INPUT_DATE, date);
        bundle.putString(FoodInputFragment.BUNDLE_FOOD_MEAL_TYPE, mealType);

        bundle.putParcelable(FoodInputFragment.BUNDLE_MEAL_DATA, mealData);
        bundle.putParcelableArrayList(FoodInputFragment.BUNDLE_FOOD_DATA, (ArrayList<? extends Parcelable>) foodList);
        Logger.i(TAG, "Bundle.foodList=" + foodList.size());

        bundle.putString(FoodInputFragment.ACTION_TITLE, title);// 액션바 타이틀

        return bundle;
    }

    public void onResume() {
        getData();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}