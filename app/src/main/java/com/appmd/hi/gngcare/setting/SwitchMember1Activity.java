package com.appmd.hi.gngcare.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.intro.LoginActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class SwitchMember1Activity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener{

    private RadioButton mYesRb, mNoRb;
    private Button mConfirmBtn;
    private Intent intent = null;

    private String mberno ="";
    private String memname ="";
    private String memId ="";
    private String memId_site_bool ="";
    private String memId_app_bool ="";

    private ImageButton mBackimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.switch_member_activity1);

        setTitle(getString(R.string.member_switch1));

        init();
        setEvent();

        intent = getIntent();

        if(intent != null){
            mberno  =   intent.getStringExtra(CommonData.EXTRA_MBERNO);
            memname = intent.getStringExtra(CommonData.EXTRA_MEMNAME);
            memId = intent.getStringExtra(CommonData.EXTRA_MEMID);
            memId_site_bool = intent.getStringExtra(CommonData.EXTRA_MEMID_SITE_BOOL);
            memId_app_bool = intent.getStringExtra(CommonData.EXTRA_MEMID_APP_BOOL);

        }else{
            GLog.i("intent = null", "dd");
        }


        mConfirmBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

    }

    /**
     * 초기화
     */
    public void init(){
        mYesRb        =   (RadioButton)       findViewById(R.id.memberswitch_yes_btn);
        mNoRb    =   (RadioButton)       findViewById(R.id.memberswitch_no_btn);
        mConfirmBtn     =   (Button)            findViewById(R.id.confirm_btn);

        mYesRb.setOnCheckedChangeListener(this);
        mNoRb.setOnCheckedChangeListener(this);

        mBackimg = findViewById(R.id.common_left_btn);
        mBackimg.setOnClickListener(v -> finish());
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mConfirmBtn.setOnClickListener(this);
    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;

        if(!mYesRb.isChecked() && !mNoRb.isChecked()){    // 이름 입력이 없다면
            mDialog =   new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent("데이터 유지여부를 선택해주세요.");//getString(R.string.popup_dialog_name_error)
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }




        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(!mYesRb.isChecked() && !mNoRb.isChecked()){    // 선택을 안했다면
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

        if(bool) {
        mConfirmBtn.setEnabled(true);

        mConfirmBtn.setBackgroundResource(R.drawable.btn_5_22396c);
        mConfirmBtn.setTextColor(ContextCompat.getColorStateList(SwitchMember1Activity.this, R.color.color_ffffff_btn));
        } else{
        mConfirmBtn.setEnabled(false);

        mConfirmBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
        mConfirmBtn.setTextColor(ContextCompat.getColorStateList(SwitchMember1Activity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 정회원 바로 전환
     *
     */
    public void requestSwitchMember() {
//        json={"api_code":"asstb_mber_keep_member"
//                                    ,"insures_code":"108"
//                                    ,"mber_sn":"115232"
//                                    ,"mber_no":"9999999999970"
//                                    ,"memname":"김명균"
//                                    }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_KEEP_MEMBER);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_MBER_NO,     mberno);
            object.put(CommonData.JSON_MEMNAME,     memname);

            if( memId != null && !memId.equals("")){
                object.put(CommonData.JSON_SEL_SITE_YN,     memId_site_bool);
                object.put(CommonData.JSON_APP_SITE_YN,     memId_app_bool);
                object.put(CommonData.JSON_SEL_MEMID,     memId);
            }

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember1Activity.this, NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }


    /**
     * 아이디 선택 후 정회원 바로 전환
     *
     */
    public void requestSelectSwitchMember() {
//        json={
//          "api_code":"asstb_mber_keep_member_id_change"
//          ,"insures_code":"108"
//          ,"mber_sn":"115232"
//          ,"mber_no":"9999999999970"
//          ,"memname":"김명균"
//          ,"sel_site_yn":"N"
//          ,"app_site_yn":"Y"
//          ,"sel_memid":"tjhong@gchealthcare.com"
//          }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_MBER_NO,     mberno);
            object.put(CommonData.JSON_MEMNAME,     memname);


            object.put(CommonData.JSON_SEL_SITE_YN,     memId_site_bool);
            object.put(CommonData.JSON_APP_SITE_YN,     memId_app_bool);
            object.put(CommonData.JSON_SEL_MEMID,     memId);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember1Activity.this, NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }


    /**
     * 로그인
     *
     * @param mber_id  아이디
     * @param mber_pwd 비밀번호
     */
    public void requestLogin(String mber_id, String mber_pwd) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember1Activity.this, NetworkConst.NET_LOGIN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER:   // 정회원 전환
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 정회원 전환
                                    dialog.dismiss();
                                    requestLogin(commonData.getMberId(), commonData.getMberPwd());
                                    MainActivity.mJunChangeChild = true;

                                }else {  // 회원을 가입시킬수 없다
                                    mDialog = new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_not_found));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;
                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;

                case NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE:   // 아이디 선택 후 정회원 전환
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 정회원 전환
                                    dialog.dismiss();
                                    requestLogin(commonData.getMberId(), commonData.getMberPwd());
                                    MainActivity.mJunChangeChild = true;

                                }else {  // 회원을 가입시킬수 없다
                                    mDialog = new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_not_found));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;
                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;

                case NetworkConst.NET_LOGIN:    // 로그인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_LOGIN", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);


                                if(data_yn.equals(CommonData.YES)){	// 로그인 성공
                                    GLog.i("NET_LOGIN SUCCESS", "dd");

                                    mDialog =   new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setHtmlContent(getString(R.string.popup_dialog_contactor_complete4));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            commonData.setMberId(commonData.getMberId());
                                            commonData.setMberPwd(commonData.getMberPwd());

                                            if (!commonData.getMberId().equals("")) {    // 아이디가 있다면 부모
                                                commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                            } else {    // 없다면 자녀
                                                commonData.setLoginType(CommonData.LOGIN_TYPE_CHILD);
                                            }

                                            loginSuccess(SwitchMember1Activity.this, resultData,true);

                                        }
                                    });
                                    mDialog.show();

                                }else{	// 로그인 실패
                                    GLog.i("NET_LOGIN FAIL", "dd");
                                    //초기화
                                    commonData.setMberId("");
                                    commonData.setMberPwd("");
                                    mDialog	=	new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            // 로그인 실패 시 내용을 전부 지운다.
                                            dialog.dismiss();
                                            commonData.setMberPwd(null);
                                            commonData.setMain_Category("");
                                            commonData.setAutoLogin(false);
                                            commonData.setRememberId(false);
                                            commonData.setMberSn("");

                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_bef_cm", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_bef_kg", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_kg","");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_term_kg", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_chl_birth_de", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_milk_yn", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_birth_due_de", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_chl_typ", "");
                                            Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_actqy", "");


                                            commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                            mDialog.dismiss();
                                            finish();

                                            Intent intent2 = new Intent(SwitchMember1Activity.this, LoginActivity.class);
                                            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent2);

                                        }
                                    });
                                    mDialog.show();

                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                                //초기화
                                commonData.setMberId("");
                                commonData.setMberPwd("");
                                GLog.i("NET_LOGIN Exception", "dd");
                                mDialog	=	new CustomAlertDialog(SwitchMember1Activity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(CustomAlertDialog dialog, Button button) {
                                        // 로그인 실패 시 내용을 전부 지운다.
                                        dialog.dismiss();
                                        commonData.setMberPwd(null);
                                        commonData.setMain_Category("");
                                        commonData.setAutoLogin(false);
                                        commonData.setRememberId(false);
                                        commonData.setMberSn("");

                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_bef_cm", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_bef_kg", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_kg","");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_term_kg", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_chl_birth_de", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_milk_yn", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_birth_due_de", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_mber_chl_typ", "");
                                        Util.setSharedPreference(SwitchMember1Activity.this, "MonJin_actqy", "");


                                        commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                        mDialog.dismiss();
                                        finish();

                                        Intent intent2 = new Intent(SwitchMember1Activity.this, LoginActivity.class);
                                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent2);

                                    }
                                });
                                mDialog.show();
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            if (dialog != null) {
                                dialog.show();
                            }
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };


    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()) {
            case R.id.confirm_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                if (invaildCerfiti()) {   // 인증 가능하다면
                    if(mYesRb.isChecked()){
                        if( memId != null && !memId.equals("")){
                            intent = new Intent(SwitchMember1Activity.this, SwitchMember2Activity.class);
                            intent.putExtra(CommonData.EXTRA_MBERNO, mberno);
                            intent.putExtra(CommonData.EXTRA_MEMID, memId);
                            intent.putExtra(CommonData.EXTRA_MEMID_APP_BOOL, memId_app_bool);
                            intent.putExtra(CommonData.EXTRA_MEMID_SITE_BOOL, memId_site_bool);
                        }else {
                            intent = new Intent(SwitchMember1Activity.this, SwitchMember2Activity.class);
                            intent.putExtra(CommonData.EXTRA_MBERNO, mberno);

                        }
                        startActivity(intent);
                    }else{
                        if( memId != null && !memId.equals("")){
                            requestSelectSwitchMember();
                        }else{
                            requestSwitchMember();
                        }

                    }

                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }
    }
}
