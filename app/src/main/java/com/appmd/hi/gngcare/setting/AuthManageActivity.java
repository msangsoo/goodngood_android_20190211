package com.appmd.hi.gngcare.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.kyleduo.switchbutton.SwitchButton;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class AuthManageActivity extends BackBaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener {

    // 부모
    private LinearLayout mParentLayout;
    private TextView mIdTv;
    private EditText mOldPwdEdit, mNewPwdEdit, mNewConfirmPwdEdit;
    private ImageButton mOldPwdDelBtn, mNewPwdDelBtn, mNewConfirmPwdDelBtn;
    private SwitchButton mAutoLoginSb;
    private Button mChangeBtn, mBtnLeaveService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_manage_activity);

        init();
        setEvent();

        mChangeBtn.setEnabled(false);

        mIdTv.setText(commonData.getMberId());
        mAutoLoginSb.setChecked(commonData.getAutoLogin());

        setTitle(getString(R.string.id_info));

        Util.setAlphaAni(mParentLayout, CommonData.ANI_DELAY_500, false, 0);

    }

    /**
     * 초기화
     */
    public void init(){
        mParentLayout   =   (LinearLayout)  findViewById(R.id.parent_layout);
        mIdTv          =   (TextView)      findViewById(R.id.id_tv);
        mOldPwdEdit     =   (EditText)      findViewById(R.id.pwd_edit);
        mNewPwdEdit     =   (EditText)      findViewById(R.id.pwd_new_edit);
        mNewConfirmPwdEdit= (EditText)      findViewById(R.id.pwd_confirm_edit);
        mOldPwdDelBtn   =   (ImageButton)   findViewById(R.id.pwd_del_btn);
        mNewPwdDelBtn   =   (ImageButton)   findViewById(R.id.pwd_new_del_btn);
        mNewConfirmPwdDelBtn=   (ImageButton)findViewById(R.id.pwd_confirm_del_btn);
        mAutoLoginSb=  (SwitchButton)findViewById(R.id.auto_login_cb);
        mChangeBtn   =   (Button)    findViewById(R.id.pwd_change_btn);
        mBtnLeaveService = (Button) findViewById(R.id.btn_leave_service);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mChangeBtn.setOnClickListener(this);
        mAutoLoginSb.setOnCheckedChangeListener(this);

        mOldPwdDelBtn.setOnClickListener(this);
        mNewPwdDelBtn.setOnClickListener(this);
        mNewConfirmPwdDelBtn.setOnClickListener(this);
        mBtnLeaveService.setOnClickListener(this);

        mOldPwdEdit.setOnFocusChangeListener(this);
        mNewPwdEdit.setOnFocusChangeListener(this);
        mNewConfirmPwdEdit.setOnFocusChangeListener(this);


        mOldPwdEdit.addTextChangedListener(new MyTextWatcher());
        mNewPwdEdit.addTextChangedListener(new MyTextWatcher());
        mNewConfirmPwdEdit.addTextChangedListener(new MyTextWatcher());
    }

    /**
     * 비밀번호 변경 체크
     * @return boolean ( true - 저장 가능, false - 저장 불가 )
     */
    public boolean isConfirm(){
        boolean bool = true;

        if(mOldPwdEdit.getText().toString().trim().equals("")){    // 기존 비밀번호
            bool = false;
            return bool;
        }

        if(mNewPwdEdit.getText().toString().trim().equals("")){   // 새로운 비밀번호
            bool = false;
            return bool;
        }

        if(mNewConfirmPwdEdit.getText().toString().trim().equals("")){   // 새로운 비밀번호 확인
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

        if(bool) {
            mChangeBtn.setEnabled(true);

            mChangeBtn.setBackgroundResource(R.drawable.btn_5_6bb0d7);
            mChangeBtn.setTextColor(ContextCompat.getColorStateList(AuthManageActivity.this, R.color.color_ffffff_btn));
        }else{
            mChangeBtn.setEnabled(false);

            mChangeBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
            mChangeBtn.setTextColor(ContextCompat.getColorStateList(AuthManageActivity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 비밀번호 변경 가능 체크
     * @param old_pwd           기존 비밀번호
     * @param new_pwd           변경할 비밀번호
     * @param new_confirm_pwd   변경할 비밀번호 확인
     * @return bool ( true - 가능, false - 불가 )
     */
    public boolean invaildPwd(String old_pwd, String new_pwd, String new_confirm_pwd){
        boolean bool = true;

        if(old_pwd.equals("") ){    // 기존 비밀번호 오류
            mOldPwdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_login_input_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(new_pwd.equals("") || !Util.checkPasswd(new_pwd)){   // 새로운 비밀번호 오류
            mNewPwdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_invalid_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(new_confirm_pwd.equals("") || !Util.checkPasswd(new_confirm_pwd)){  // 새로운 비밀번호 확인 오류
            mNewConfirmPwdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_invalid_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!new_pwd.equals(new_confirm_pwd) || !old_pwd.equals(commonData.getMberPwd())){
            mNewConfirmPwdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_not_match_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
        }


        return bool;

    }

    /**
     * 비밀번호 변경하기 api
     * @param old_pwd           기존 비밀번호
     * @param new_pwd           새로운 비밀번호
     * @param new_confirm_pwd   새로운 비밀번호 확인
     */
    public void requestChangePwd(String old_pwd, String new_pwd, String new_confirm_pwd){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

//        {   "api_code": "mber_user_pwd_edit",   "insures_code": "106", "mber_sn": "10035" , "mber_before_pwd": "pwd12345" , "mber_pwd": "pwd1234"   }

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_USER_PWD_EDIT);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());
//            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_BEFORE_PWD, old_pwd);
            object.put(CommonData.JSON_MBER_PWD, new_pwd);


            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(AuthManageActivity.this, NetworkConst.NET_MBER_USER_PWD_EDIT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 회원 탈퇴 api
     */
    public void requestUesrOut(){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_USER_OUT);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(AuthManageActivity.this, NetworkConst.NET_MBER_USER_OUT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    public void logOut(){
        Intent intent = new Intent(AuthManageActivity.this, IntroActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        commonData.setAutoLogin(false);
        commonData.setRememberId(false);
        commonData.setMberId("");
        commonData.setMberPwd("");
        commonData.setMberNm(null);
        commonData.setPhoneNumber(null);
        commonData.setBirthDay(null);
        commonData.setGender(null);
        commonData.setPushAlarm(true);
        commonData.setAlarmMode(0);
        commonData.setChildCnt(0);
        commonData.setJuminNum(null);
        commonData.setHiPlannerHp(null);
        commonData.setMberSn(null);
        commonData.setChldrn(null); // 자녀정보
        CommonData.getInstance().setAppVersion(null);

        startActivity(intent);
        finish();

        SettingActivity settingActivity = (SettingActivity)SettingActivity.SettingActivity;
        MainActivity mainActivity = (MainActivity)MainActivity.MAIN_ACTIVITY;
        settingActivity.finish();
        mainActivity.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pwd_change_btn:   // 비밀번호 변경

                String old_pwd, new_pwd, new_confirm_pwd;

                old_pwd =   mOldPwdEdit.getText().toString().trim();
                new_pwd =   mNewPwdEdit.getText().toString().trim();
                new_confirm_pwd =   mNewConfirmPwdEdit.getText().toString().trim();

                if (invaildPwd(old_pwd, new_pwd, new_confirm_pwd)) {    // 비밀번호 변경 가능하다면
                    requestChangePwd(old_pwd, new_pwd, new_confirm_pwd);
                    GLog.i("비밀번호 변경 api 호출", "dd");
                }

                break;
            case R.id.pwd_del_btn:  // 기존 비밀번호 삭제
                commonView.setClearEditText(mOldPwdEdit);
                break;
            case R.id.pwd_new_del_btn:  // 새로운 비밀번호 삭제
                commonView.setClearEditText(mNewPwdEdit);
                break;
            case R.id.pwd_confirm_del_btn:  // 새로운 비밀번호 확인 삭제
                commonView.setClearEditText(mNewConfirmPwdEdit);
                break;
            case R.id.btn_leave_service:    // 서비스 탈퇴
                mDialog = new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_B);
                mDialog.setTitle(getString(R.string.app_name_kr));
                mDialog.setContent(getString(R.string.service_secession_confirm));
                mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                    requestUesrOut();
                    dialog.dismiss();
                });
                mDialog.show();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        commonData.setAutoLogin(isChecked); // 자동로그인 변경
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.pwd_edit:    // 기존 비밀번호
                commonView.setClearImageBt(mOldPwdDelBtn, hasFocus);
                break;
            case R.id.pwd_new_edit:	   // 새로운 비밀번호
                commonView.setClearImageBt(mNewPwdDelBtn, hasFocus);
                break;
            case R.id.pwd_confirm_edit:	   // 새로운 비밀번호 확인
                commonView.setClearImageBt(mNewConfirmPwdDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_MBER_USER_PWD_EDIT:	// 비밀번호 변경 완료
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("API_SUCCESS", "dd");
                            commonData.setMberPwd(mNewPwdEdit.getText().toString().trim()); // 비밀번호 저장

                            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle("");
                            mDialog.setContent(getString(R.string.popup_dialog_change_complete));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog12, button) -> {
                                dialog12.dismiss();
                                finish();
                            });
                            mDialog.show();

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.app_name_kr));
                            mDialog.setContent(getString(R.string.popup_dialog_change_fail));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog1, button) -> {
                                dialog1.dismiss();
                            });
                            mDialog.show();
                            break;
                    }
                    break;
                case NetworkConst.NET_MBER_USER_OUT:    // 회원 탈퇴
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("API_SUCCESS", "dd");
                            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.app_name_kr));
                            mDialog.setContent(getString(R.string.service_secession_complete));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog1, button) -> {
                                dialog1.dismiss();
                                logOut();
                            });
                            mDialog.setCancelable(false);
                            mDialog.show();
                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            mDialog =   new CustomAlertDialog(AuthManageActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.app_name_kr));
                            mDialog.setContent(getString(R.string.service_secession_fail));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog1, button) -> {
                                dialog1.dismiss();
                            });
                            mDialog.show();
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
