package com.appmd.hi.gngcare.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.common.FileManager;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.PermissionUtils;
import com.appmd.hi.gngcare.util.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.roundedimageview.RoundedImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.htmlparser.jericho.FormField;
import net.htmlparser.jericho.FormFields;
import net.htmlparser.jericho.Source;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class BabyInfoActivity extends BackBaseActivity implements View.OnClickListener {

    private RoundedImageView mImgBabyPhoto;
    private ImageButton mBtnLeftBaby, mBtnRightBaby;
    private TextView mTxtBabyName, mTxtBabyBirth, mTxtBabySex, mTxtBabyNick, mTxtBirth;
    private LinearLayout mBabySexLay;

    // 업로드할 사진 관련
    private ArrayList<String> mUploadPhotoList; // api 에 넘겨줄 사진 데이터
    private FileManager mFileManager;
    private Uri mImageCaptureUri;

    private String mDumpNick;
    private String mChangeNick;

    private String VIEWSTATE;
    private String EVENTVALIDATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.babyinfo_activity);

        setTitle(getString(R.string.baby_info));

        init();
        setEvent();
        setBabyData();

        mUploadPhotoList = new ArrayList<String>();
        mFileManager	=	new FileManager(getApplicationContext(), CommonData.FILE_TYPE_IMAGE);
    }

    /**
     * 초기화
     */
    public void init(){
        mImgBabyPhoto = (RoundedImageView)findViewById(R.id.img_baby_photo);
        mBtnLeftBaby = (ImageButton)findViewById(R.id.btn_left_baby);
        mBtnRightBaby = (ImageButton) findViewById(R.id.btn_right_baby);
        mTxtBabyNick = (TextView) findViewById(R.id.txt_baby_nick);
        mTxtBabyName = (TextView)findViewById(R.id.txt_baby_name);
        mTxtBabyBirth = (TextView)findViewById(R.id.txt_baby_birth);
        mTxtBabySex = (TextView)findViewById(R.id.txt_baby_sex);

        mTxtBirth = (TextView)findViewById(R.id.txt_birth);
        mBabySexLay = (LinearLayout)findViewById(R.id.baby_sex_lay);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mImgBabyPhoto.setOnClickListener(this);
        mBtnLeftBaby.setOnClickListener(this);
        mBtnRightBaby.setOnClickListener(this);
        mTxtBabyNick.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch(v.getId()){
            case R.id.img_baby_photo: // 아이 사진 변경
                if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (PermissionUtils.canAccessStorage(BabyInfoActivity.this)) {
                            cropImage();
                        } else {
                            ActivityCompat.requestPermissions(BabyInfoActivity.this, PermissionUtils.STORAGE_PERMS, CommonData.PERMISSION_REQUEST_STORAGE);
                        }
                    } else {
                        cropImage();
                    }
                }
                break;
            case R.id.btn_left_baby:  // 왼쪽으로
                if(MainActivity.mChildChoiceIndex > 0){
                    MainActivity.mChildChoiceIndex--;
                    setBabyData();
                    CommonData.getInstance().setSelectChildSn(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
                    MainActivity.mChangeChild = true;
                    setResult(RESULT_OK);
                }else{
                    Toast.makeText(BabyInfoActivity.this, getString(R.string.first_baby), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_right_baby: // 오른쪽으로
                if(MainActivity.mChildChoiceIndex < MainActivity.mChildMenuItem.size()-1){
                    MainActivity.mChildChoiceIndex++;
                    setBabyData();
                    CommonData.getInstance().setSelectChildSn(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
                    MainActivity.mChangeChild = true;
                    setResult(RESULT_OK);
                }else{
                    Toast.makeText(BabyInfoActivity.this, getString(R.string.last_baby), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_baby_nick:
                GLog.i("nick_layout", "dd");
                intent = new Intent(BabyInfoActivity.this, BabyNickActivity.class);
                intent.putExtra(CommonData.EXTRA_NICK, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNcm());
                intent.putExtra(CommonData.EXTRA_CHL_SN, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
                intent.putExtra(CommonData.EXTRA_CHILD_INDEX, MainActivity.mChildChoiceIndex);
                startActivityForResult(intent, CommonData.REQUEST_CODE_BABY_INFO);
                Util.BackAnimationStart(BabyInfoActivity.this);
                break;
        }
    }


    /**
     * 아이 정보 화면 설정
     */
    public void setBabyData(){
        // 자녀 데이터가 있는경우 UI 세팅
        try{
            if(MainActivity.mChildChoiceIndex == 0){
                mBtnLeftBaby.setVisibility(View.INVISIBLE);
                if(MainActivity.mChildMenuItem.size()-1 == 0)
                    mBtnRightBaby.setVisibility(View.INVISIBLE);
                else
                    mBtnRightBaby.setVisibility(View.VISIBLE);
            }else{
                if(MainActivity.mChildMenuItem.size()-1 == MainActivity.mChildChoiceIndex){
                    mBtnLeftBaby.setVisibility(View.VISIBLE);
                    mBtnRightBaby.setVisibility(View.INVISIBLE);
                }else{
                    mBtnLeftBaby.setVisibility(View.VISIBLE);
                    mBtnRightBaby.setVisibility(View.VISIBLE);
                }
            }

            if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
                // 아이 사진 세팅
                CustomImageLoader.clearCache();
                if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                    mImgBabyPhoto.setImageResource(R.drawable.main_fetus06b);
                } else {
                    CustomImageLoader.displayImageMain(this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mImgBabyPhoto);
                }

                if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)){
                    mBabySexLay.setVisibility(View.INVISIBLE);
                    mTxtBirth.setText(getString(R.string.baby_birth_after));

                    final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnAftLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                    mTxtBabyBirth.setText(format.format(mDate));
                }else{
                    mBabySexLay.setVisibility(View.VISIBLE);
                    mTxtBirth.setText(getString(R.string.baby_birth));

                    final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                    SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                    mTxtBabyBirth.setText(format.format(mDate));

                    mTxtBabySex.setText( (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(commonData.MALE) ? getString(R.string.male) : getString(R.string.female)) );
                }

                mTxtBabyNick.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNcm());
                mDumpNick = MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNcm();
                mTxtBabyName.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNm());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void requestPhotoUpdate(final String upfile){

        mImgBabyPhoto.postDelayed(() -> {

            try{
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();

                client.setTimeout(CommonData.TIME_OUT_DELAY);
                client.addHeader(CommonData.ACCEPT_CHARSET, CommonData.ENCODING_EUC_KR);
                client.addHeader(CommonData.USER_AGENT, new WebView(getBaseContext()).getSettings().getUserAgentString());

                File file = new File(upfile);

                if(file.exists()){
                    params.setContentEncoding(CommonData.ENCODING_EUC_KR);
                    // data form
                    params.put(CommonData.VIEWSTATE, VIEWSTATE);
                    params.put(CommonData.EVENTVALIDATION, EVENTVALIDATION);
                    params.put(CommonData.JSON_MBER_SN, commonData.getMberSn());
                    params.put(CommonData.JSON_CHL_SN, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());    // 자녀 고유번호
                    params.put(CommonData.IMAGEBUTTON_1_X, "27");
                    params.put(CommonData.IMAGEBUTTON_1_Y, "22");
                    params.put(CommonData.JSON_UPFILE, file, MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"));          //  이미지 파일

                    GLog.i("1. " + VIEWSTATE +"="+ VIEWSTATE, "dd");
                    GLog.i("2. " + EVENTVALIDATION +"="+ EVENTVALIDATION, "dd");
                    GLog.i("3. " + CommonData.JSON_MBER_SN +"=" + "="+commonData.getMberSn(), "dd");
                    GLog.i("4. " + CommonData.JSON_CHL_SN +"=" + MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), "dd");
                    GLog.i("5. " + CommonData.IMAGEBUTTON_1_X +"="+ "27", "dd");
                    GLog.i("6. " + CommonData.IMAGEBUTTON_1_Y +"="+ "22", "dd");
                    GLog.i("7. " + CommonData.JSON_UPFILE +":::"+file+":::"+ MimeTypeMap.getSingleton().getMimeTypeFromExtension("jpg"), "dd");

//                    mAsyncHttpHandler.setCharset(CommonData.ENCODING_EUC_KR);
                    client.post(CommonData.CHLDRN_INPUT_UPLOAD_URL, params, mAsyncHttpHandler);
                    showProgress();
                }else{
                    Toast.makeText(this,"이미지 파일이 없습니다.", Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){
                GLog.e(e.toString());
            }
        }, CommonData.INTRO_POST_DELAYED);
    }


    private AsyncHttpResponseHandler mAsyncHttpHandler = new AsyncHttpResponseHandler() {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            GLog.i("onSuccess", "dd");

            try {

                int count = headers.length;

                for (int i = 0; i < count; i++) {
                    GLog.v(headers[i].getName() + ": " + headers[i].getValue());
                }

                String response = new String(responseBody);
                GLog.i("response = " +response, "dd");
                JSONObject resultData = null;

                if(response.startsWith("<")){   // api result 문자열 시작값이 "<" 로 시작하는 경우는 xml 폼
                    try {
                        response =  Util.parseXml(response);
                        GLog.i("response = " +response, "dd");
                    }catch(Exception e){
                        GLog.e(e.toString());
                    }

                    resultData = new JSONObject(response);

                }

                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);

                GLog.i("resultData =" + resultData.toString(), "dd");
                if (data_yn.equals(CommonData.YES)) {
                    GLog.i("photo update ok", "dd");

                    MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).setmChldrnOrgImage("file://" + mUploadPhotoList.get(mUploadPhotoList.size()-1));
                    MainActivity.mChangeChild = true;

                    if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                        mImgBabyPhoto.setImageResource(R.drawable.main_fetus06b);
                    } else {
                        CustomImageLoader.displayImage(BabyInfoActivity.this, "file://" + mUploadPhotoList.get(mUploadPhotoList.size()-1), mImgBabyPhoto);
                    }



                    setResult(RESULT_OK);

                    mDialog = new CustomAlertDialog(BabyInfoActivity.this, CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.popup_dialog_update_complete));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        setResult(RESULT_OK);
                        dialog.dismiss();
                        finish();
                    });
                    //mDialog.show();
                } else {
                    mDialog = new CustomAlertDialog(BabyInfoActivity.this, CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.popup_dialog_note_update_error));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> dialog.dismiss());
                    mDialog.show();
                }

            } catch (Exception e) {
                GLog.e(e.toString());
            }

            hideProgress();
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            GLog.i("onFailure", "dd");
            String response = new String(responseBody);
            GLog.i("statusCode = " +statusCode, "dd");
            GLog.i("response = " +response, "dd");
            hideProgress();
        }

    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        GLog.i("onRequestPermissionsResult", "dd");
        if (requestCode == CommonData.PERMISSION_REQUEST_STORAGE) {
            if(PermissionUtils.canAccessStorage(this)){
                cropImage();
            }else{
                Toast.makeText(BabyInfoActivity.this, getString(R.string.toast_permission_storage), Toast.LENGTH_LONG).show();
            }
        } else {
            GLog.i("onRequestPermissionsResult else", "dd");
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        //GLog.i("resultCode : " + resultCode);
        //GLog.i("requestCode :" + requestCode);

        if(resultCode != RESULT_OK){
            GLog.i("resultCode != RESULT_OK", "dd");
            return;
        }

        try {
            switch (requestCode) {
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:       // 사진 자르기 return
                    GLog.i("NATIVE_CROP", "dd");
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);

                    mImageCaptureUri = result.getUri();

                    Bitmap myBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);
                    // 모든 이미지를 강재로 640x640으로 변경
                    // 보여주기 편하게 하기 위함
                    //GLog.i("Crop file : " + file.getAbsolutePath());
                    mUploadPhotoList.clear();
                    GLog.i("mUploadPhotoList.size() = " + mUploadPhotoList.size(), "dd");

                    GLog.i("System.currentTimeMillis() = " + System.currentTimeMillis(), "dd");
                    long mTime = System.currentTimeMillis();

                    mFileManager.SaveBitmapToFileCache(myBitmap, mUploadPhotoList.size() + "_" + mTime + ".jpg", 70);


                    GLog.i("filepath() = " + mFileManager.getFilePath() + mUploadPhotoList.size() + "_" + mTime + ".jpg", "dd");
//                mUploadPhotoList.put(mUploadPhotoList.size() + "", mFileManager.getFilePath() + mUploadPhotoList.size() + ".jpg");
                    mUploadPhotoList.add(mFileManager.getFilePath() + mUploadPhotoList.size() + "_" + mTime + ".jpg");


                    GLog.i("mFileManager file = " + mFileManager.getFilePath(), "dd");
                    GLog.i("uploadPhotolist = " + mUploadPhotoList.get(mUploadPhotoList.size() - 1), "dd");

                    GLog.i("mUploadPhotoList.size() = " +mUploadPhotoList.size(), "dd");

                    new Thread(){
                        public void run(){
                            HTMLParseHidden();
                        }
                    }.start();

                    break;
                case CommonData.REQUEST_CODE_BABY_INFO:
                    mTxtBabyNick.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNcm());
                    break;
            }
        }catch(Exception e){
            GLog.e(e.toString());
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void cropImage() {

        CropImage.activity(null)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setAutoZoomEnabled(true)
                .setAspectRatio(256, 256)
                .start(this);

    }

    private void HTMLParseHidden(){
        try {
            Source source = new Source(new URL(CommonData.CHLDRN_INPUT_UPLOAD_URL));
            FormFields formFields = source.getFormFields();

            for (FormField formField : formFields) {

                if(formField.getName().equals(CommonData.VIEWSTATE.toLowerCase()))
                    VIEWSTATE = formField.getValues().get(0);

                if(formField.getName().equals(CommonData.EVENTVALIDATION.toLowerCase()))
                    EVENTVALIDATION = formField.getValues().get(0);
            }

            requestPhotoUpdate(mUploadPhotoList.get(mUploadPhotoList.size()-1));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
