package com.appmd.hi.gngcare.psychology;

import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CP on 2018. 4. 14..
 */

public class PsyHandlerParcel implements Parcelable {

    private Handler handler;

    private String mQueNum;
    private String mQueTitle;
    private String mQueImgYn;
    private String mQueImg;

    private String mAnsTitle1;
    private String mAnsScr1;
    private String mAnsImg1;
    private String mAnsImgYn1;

    private String mAnsTitle2;
    private String mAnsScr2;
    private String mAnsImg2;
    private String mAnsImgYn2;

    private String mAnsTitle3;
    private String mAnsScr3;
    private String mAnsImg3;
    private String mAnsImgYn3;

    private String mAnsTitle4;
    private String mAnsScr4;
    private String mAnsImg4;
    private String mAnsImgYn4;

    private String mAnsTitle5;
    private String mAnsScr5;
    private String mAnsImg5;
    private String mAnsImgYn5;

    private String mAnsCnt;


    protected PsyHandlerParcel(Parcel in) {
    }


    public PsyHandlerParcel(Handler handler, JSONObject obj) {
        this.handler = handler;


        try {
            setmQueTitle(obj.getString("QUE_TITLE"));

            setmQueImg(obj.getString("QUE_IMG"));
            setmQueImgYn(obj.getString("QUE_IMGYN"));
            setmQueNum(obj.getString("QUE_NUM"));

            setmAnsCnt(obj.getString("ANS_CNT"));

            setmAnsTitle1(obj.getString("ANS_TITLE1"));
            setmAnsImg1(obj.getString("ANS_IMG1"));
            setmAnsImgYn1(obj.getString("ANS_IMGYN1"));
            setmAnsScr1(obj.getString("ANS_SCR1"));

            setmAnsTitle2(obj.getString("ANS_TITLE2"));
            setmAnsImg2(obj.getString("ANS_IMG2"));
            setmAnsImgYn2(obj.getString("ANS_IMGYN2"));
            setmAnsScr2(obj.getString("ANS_SCR2"));

            setmAnsTitle3(obj.getString("ANS_TITLE3"));
            setmAnsImg3(obj.getString("ANS_IMG3"));
            setmAnsImgYn3(obj.getString("ANS_IMGYN3"));
            setmAnsScr3(obj.getString("ANS_SCR3"));

            setmAnsTitle4(obj.getString("ANS_TITLE4"));
            setmAnsImg4(obj.getString("ANS_IMG4"));
            setmAnsImgYn4(obj.getString("ANS_IMGYN4"));
            setmAnsScr4(obj.getString("ANS_SCR4"));

//            setmAnsTitle5(obj.getString("ANS_TITLE5"));
//            setmAnsImg5(obj.getString("ANS_IMG5"));
//            setmAnsImgYn5(obj.getString("ANS_IMGYN5"));
//            setmAnsScr5(obj.getString("ANS_SCR5"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Handler getHandler() {

        return handler;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PsyHandlerParcel> CREATOR = new Creator<PsyHandlerParcel>() {
        @Override
        public PsyHandlerParcel createFromParcel(Parcel in) {
            return new PsyHandlerParcel(in);
        }

        @Override
        public PsyHandlerParcel[] newArray(int size) {
            return new PsyHandlerParcel[size];
        }
    };


    public void setmQueNum(String mQueNum) {
        this.mQueNum = mQueNum;
    }

    public String getmQueNum() {
        return mQueNum;
    }

    public void setmQueTitle(String mQueTitle) {
        this.mQueTitle = mQueTitle;
    }

    public String getmQueTitle() {
        return mQueTitle;
    }

    public void setmQueImgYn(String mQueImgYn) {
        this.mQueImgYn = mQueImgYn;
    }

    public String getmQueImgYn() {
        return mQueImgYn;
    }

    public void setmQueImg(String mQueImg) {
        this.mQueImg = mQueImg;
    }

    public String getmQueImg() {
        return mQueImg;
    }


    public void setmAnsTitle1(String mAnsTitle1) {
        this.mAnsTitle1 = mAnsTitle1;
    }

    public String getmAnsTitle1() {
        return mAnsTitle1;
    }

    public void setmAnsScr1(String mAnsScr1) {
        this.mAnsScr1 = mAnsScr1;
    }

    public String getmAnsScr1() {
        return mAnsScr1;
    }

    public void setmAnsImg1(String mAnsImg1) {
        this.mAnsImg1 = mAnsImg1;
    }

    public String getmAnsImg1() {
        return mAnsImg1;
    }

    public void setmAnsImgYn1(String mAnsImgYn1) {
        this.mAnsImgYn1 = mAnsImgYn1;
    }

    public String getmAnsImgYn1() {
        return mAnsImgYn1;
    }


    public void setmAnsTitle2(String mAnsTitle2) {
        this.mAnsTitle2 = mAnsTitle2;
    }

    public String getmAnsTitle2() {
        return mAnsTitle2;
    }

    public void setmAnsScr2(String mAnsScr2) {
        this.mAnsScr2 = mAnsScr2;
    }

    public String getmAnsScr2() {
        return mAnsScr2;
    }

    public void setmAnsImg2(String mAnsImg2) {
        this.mAnsImg2 = mAnsImg2;
    }

    public String getmAnsImg2() {
        return mAnsImg2;
    }

    public void setmAnsImgYn2(String mAnsImgYn2) {
        this.mAnsImgYn2 = mAnsImgYn2;
    }

    public String getmAnsImgYn2() {
        return mAnsImgYn2;
    }


    public void setmAnsTitle3(String mAnsTitle3) {
        this.mAnsTitle3 = mAnsTitle3;
    }

    public String getmAnsTitle3() {
        return mAnsTitle3;
    }

    public void setmAnsScr3(String mAnsScr3) {
        this.mAnsScr3 = mAnsScr3;
    }

    public String getmAnsScr3() {
        return mAnsScr3;
    }

    public void setmAnsImg3(String mAnsImg3) {
        this.mAnsImg3 = mAnsImg3;
    }

    public String getmAnsImg3() {
        return mAnsImg3;
    }


    public void setmAnsImgYn3(String mAnsImgYn3) {
        this.mAnsImgYn3 = mAnsImgYn3;
    }

    public String getmAnsImgYn3() {
        return mAnsImgYn3;
    }


    public void setmAnsTitle4(String mAnsTitle4) {
        this.mAnsTitle4 = mAnsTitle4;
    }

    public String getmAnsTitle4() {
        return mAnsTitle4;
    }

    public void setmAnsScr4(String mAnsScr4) {
        this.mAnsScr4 = mAnsScr4;
    }

    public String getmAnsScr4() {
        return mAnsScr4;
    }

    public void setmAnsImg4(String mAnsImg4) {
        this.mAnsImg4 = mAnsImg4;
    }

    public String getmAnsImg4() {
        return mAnsImg4;
    }

    public void setmAnsImgYn4(String mAnsImgYn4) {
        this.mAnsImgYn4 = mAnsImgYn4;
    }

    public String getmAnsImgYn4() {
        return mAnsImgYn4;
    }


    public void setmAnsTitle5(String mAnsTitle5) {
        this.mAnsTitle5 = mAnsTitle5;
    }

    public String getmAnsTitle5() {
        return mAnsTitle5;
    }

    public void setmAnsScr5(String mAnsScr5) {
        this.mAnsScr5 = mAnsScr5;
    }

    public String getmAnsScr5() {
        return mAnsScr5;
    }

    public void setmAnsImg5(String mAnsImg5) {
        this.mAnsImg5 = mAnsImg5;
    }

    public String getmAnsImg5() {
        return mAnsImg5;
    }

    public void setmAnsImgYn5(String mAnsImgYn5) {
        this.mAnsImgYn5 = mAnsImgYn5;
    }

    public String getmAnsImgYn5() {
        return mAnsImgYn5;
    }


    public void setmAnsCnt(String mAnsCnt) {
        this.mAnsCnt = mAnsCnt;
    }

    public String getmAnsCnt() {
        return mAnsCnt;
    }


}
