package com.appmd.hi.gngcare.psychology;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.ApplinkDialog;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.main.BabyInfoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.KakaoLinkUtil;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by CP on 2018. 4. 6..
 */

public class PsyCheckResultActivity extends PsyBaseActivity {

    public static Activity PSY_CHECK_RESULT_ACTIVITY;
    public LayoutInflater mLayoutInflater;
    public Fragment mContentFragment;
    public FrameLayout mFrameContainer;
    private Toolbar toolbar;


    boolean mNoShowHelp = false;

    // 네비바
    public RelativeLayout mBgActionBar;
    private ImageButton mLefeBtn;
    private RelativeLayout mRightLayout;
    private ImageButton callBtn;
    private FrameLayout mBgBabyFace;
    private ImageView mRightImg;
    private TextView mTitleTv;

    private TextView psyResultScoreTxt;
    private TextView psyResultDateTxt;
    private TextView psyResultTitleTxt;
    private TextView psyResultStatusTxt;
    private TextView psyResultDescTxt;

    private Button psyDtlBtn;
    private TextView scoreTxt;
    private Button selTwoBtn;
    private String rememberBack;


    private Intent intent;

    private String gTitle;
    private String gComment;
    private String objSeq;

    private PsyProgress mLevelProgress;

    private View view;

    private ImageView mPsy_check_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.psy_check_result_activity);
        PSY_CHECK_RESULT_ACTIVITY = PsyCheckResultActivity.this;
        init();
        setEvent();

        intent = getIntent();

        objSeq = intent.getStringExtra("objSeq");

        setTitleTxt(getString(R.string.psy_req_result1));

        // 자녀 데이터가 있는경우 UI 세팅
        /*if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                mRightImg.setImageResource(R.drawable.main_fetus06b);
            } else {
                CustomImageLoader.displayImage(PsyCheckResultActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
            }

            mRightLayout.setVisibility(View.VISIBLE);
        }else{*/
            mRightLayout.setVisibility(View.GONE);
        //}

        requestPsyCheckResult(objSeq);
    }

    /**
     * 초기화
     */
    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_no_main);

        // start custom actionbar leftmargin remove
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        // end custom actionbar leftmargin remove
        mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);

        mLevelProgress = findViewById(R.id.child_progress_bar);

        mBgActionBar = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_action_bar);
        mBgActionBar.setBackgroundColor(getResources().getColor(R.color.h_green));
        mLefeBtn = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.left_btn);
        mRightLayout = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.right_layout);
        mBgBabyFace = (FrameLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_baby_face);
        mRightImg = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.photo_img);

        mTitleTv = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.title_tv);

        psyResultScoreTxt = (TextView) findViewById(R.id.psyResultScoreTxt);
        psyResultDateTxt = (TextView) findViewById(R.id.psyResultDateTxt);
        psyResultTitleTxt = (TextView) findViewById(R.id.psyResultTitleTxt);
        psyResultStatusTxt = (TextView) findViewById(R.id.psyResultStatusTxt);
        psyResultDescTxt = (TextView) findViewById(R.id.psyResultDescTxt);

        callBtn = (ImageButton) findViewById(R.id.Hcall_btn);

        psyDtlBtn = (Button) findViewById(R.id.psyDtlBtn);
        scoreTxt =  (TextView) findViewById(R.id.scoreTxt);
        psyDtlBtn.setVisibility(View.GONE);
        scoreTxt.setVisibility(View.INVISIBLE);

        view = findViewById(R.id.root_view);

        mPsy_check_img = view.findViewById(R.id.psy_check_img);


    }


    public void requestPsyCheckResult(String objSeq) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP004);
            object.put(CommonData.JSON_CHL_SN_F, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_MJ_SEQ_F, objSeq);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(this, NetworkConst.NET_PSY_CHECK_RESULT_CMT, NetworkConst.getInstance().getPsyDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 심리 공유
     */
    public void requestSharePsy(){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();

            object.put(CommonData.JSON_API_CODE, "asstb_mber_cntr_trl_result_two");
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_CNTR_TYP, "14");
            object.put(CommonData.JSON_MBER_SN, CommonData.getInstance().getMberSn());             //  회원고유값
            object.put("trl_nm", "");
            object.put("trl_chk_de", "");
            object.put("trl_check_level", "");
            object.put("chl_sn", MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put("mj_seq", objSeq);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_ASSTB_MBER_CNTR_TRL_RESULT_TWO, NetworkConst.getInstance().getDefDomain(), networkListener, params, new MakeProgress(this));
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {

            hideProgress();

            switch (type) {
                case NetworkConst.NET_PSY_CHECK_RESULT_CMT:  //심리 결과
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                if (resultData.getString("RESULT_CODE").compareTo("0000") == 0) {

                                    psyResultScoreTxt.setText(resultData.getString("TOT_SCR"));
                                    psyResultDateTxt.setText(resultData.getString("REGDATE"));
                                    psyResultTitleTxt.setText(resultData.getString("M_NAME"));
                                    psyResultStatusTxt.setText(resultData.getString("R_STAGE"));
                                    psyResultDescTxt.setText(resultData.getString("R_COMMENT"));

                                    //psyDtlBtn.setText(resultData.getString("G_TITLE"));
                                    psyDtlBtn.setText(getString(R.string.psy_tip));
                                    gTitle = resultData.getString("G_TITLE");
                                    gComment = resultData.getString("G_COMMENT");



                                    psyDtlBtn.setVisibility(View.VISIBLE);
                                    scoreTxt.setVisibility(View.VISIBLE);

                                    // XXXMrsohn 20190216 신규 Progress
                                    mLevelProgress.setProgress(resultData.getString("M_NAME"), StringUtil.getIntVal(resultData.getString("TOT_SCR")));  // 프로그래스 표시, 하단라벨설정

                                    setImg(resultData.getString("M_NAME").trim().replace(" ",""));

                                } else {
                                    //코드 에러
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;

                case NetworkConst.NET_ASSTB_MBER_CNTR_TRL_RESULT_TWO:  //심리 공유
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);
                                if (data_yn.equals(CommonData.YES)) {
                                    String imgUrl = "https://wkd.walkie.co.kr/HL_FV/info/image/01_1314.png";
                                    String cntr_url = resultData.getString("cntr_url");

                                    if(cntr_url.contains("https://wkd.walkie.co.kr"));
                                    String param = cntr_url.replace("https://wkd.walkie.co.kr","");

                                    View view = LayoutInflater.from(PsyCheckResultActivity.this).inflate(R.layout.applink_dialog_layout, null);
                                    ApplinkDialog dlg = ApplinkDialog.showDlg(PsyCheckResultActivity.this, view);
                                    dlg.setSharing(imgUrl, "img", "", "","[현대해상 "+ KakaoLinkUtil.getAppname(getPackageName(),PsyCheckResultActivity.this)+"]","우리 아이 심리 체크 결과","자세히보기","",false,"chl_psy.png",param,cntr_url);

                                } else {
                                    //코드 에러
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type,
                                   int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog
                dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };

    /* 타이틀 변경 */
    public void setTitleTxt(String title){
        mTitleTv.setText(title);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        mRightLayout.setOnClickListener(btnListener);
        mLefeBtn.setOnClickListener(btnListener);
        mRightImg.setOnClickListener(btnListener);
        callBtn.setOnClickListener(btnListener);
        psyDtlBtn.setOnClickListener(btnListener);

        // XXXMrSohn GC밸런스심리케어센터​
        findViewById(R.id.gc_balance_care_center).setOnClickListener(btnListener);
        // 공유하기 버튼 클릭시
        findViewById(R.id.share_btn).setOnClickListener(btnListener);

        //click 저장
        OnClickListener mClickListener = new OnClickListener(btnListener,view, PsyCheckResultActivity.this);

        //아이 심리
        psyDtlBtn.setOnTouchListener(mClickListener);
        findViewById(R.id.gc_balance_care_center).setOnTouchListener(mClickListener);
        findViewById(R.id.share_btn).setOnTouchListener(mClickListener);

        //코드 부여(아이 심리)
        psyDtlBtn.setContentDescription(getString(R.string.psyDtlBtn));
        findViewById(R.id.gc_balance_care_center).setContentDescription(getString(R.string.gc_center));
        findViewById(R.id.share_btn).setContentDescription(getString(R.string.share_btn6));


    }

    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GLog.i("onStop", "dd");

    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            String str = "";
            Fragment fragment = null;

            GLog.i("v.getId() = " +v.getId(), "dd");

            switch (v.getId()){
                case R.id.left_btn:
                    onBackPressed();
                    break;
                case R.id.photo_img:
                    intent = new Intent(PsyCheckResultActivity.this, BabyInfoActivity.class);
                    startActivityForResult(intent, CommonData.REQUEST_CHILD_MANAGE);
                    Util.BackAnimationStart(PsyCheckResultActivity.this);
                    break;
                case R.id.Hcall_btn:
                    if(CommonData.getInstance().getMberGrad().equals("10")) {
                        CustomAlertDialog mDialog = new CustomAlertDialog(PsyCheckResultActivity.this, CustomAlertDialog.TYPE_B);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.do_call_center));
                        mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                            String tel = "tel:" + getString(R.string.call_center_number);
                            Intent intent1 = new Intent(Intent.ACTION_DIAL);
                            intent1.setData(Uri.parse(tel));
                            startActivity(intent1);
                            dialog.dismiss();
                        });

                        mDialog.show();
                    }else{
                        CustomAlertDialog mDialog = new CustomAlertDialog(PsyCheckResultActivity.this, CustomAlertDialog.TYPE_B);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.call_center2));
                        mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                        mDialog.setPositiveButton(getString(R.string.do_call), (dialog, button) -> {
                            String tel = "tel:" + getString(R.string.call_center_number2);
                            Intent intentCall = new Intent(Intent.ACTION_DIAL);
                            intentCall.setData(Uri.parse(tel));
                            startActivity(intentCall);
                            dialog.dismiss();
                        });
                        mDialog.show();
                    }
                    break;

                case R.id.psyDtlBtn:
                    intent = new Intent(PsyCheckResultActivity.this, PsyCheckResultDtlActivity.class);
                    intent.putExtra("G_TITLE", gTitle);
                    intent.putExtra("G_COMMENT", gComment);
                    startActivity(intent);

                    break;

                case R.id.gc_balance_care_center:
                    intent = new Intent(PsyCheckResultActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.URL_GCBALANCECENTER);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.gc_balance_care_center_title));
                    startActivity(intent);
                    Util.BackAnimationStart(PsyCheckResultActivity.this);
                    break;
                case R.id.share_btn:
                    requestSharePsy();
                    break;
            }

            if (!str.equals("")) {
                mTitleTv.setText(str);
            }
        }
    };


    @Override
    public void onBackPressed() {
        if(mOnKeyBackPressedListener != null){
            mOnKeyBackPressedListener.onBack();
        }else{
            finish();
        }
    }

    @Override
    public void finish(){
        super.finish();
        Util.BackAnimationEnd(PsyCheckResultActivity.this);
    }

    public interface onKeyBackPressedListener {
        void onBack();
    }
    private PsyMainActivity.onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(PsyMainActivity.onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }

    /**
     * Fragment 변경
     * @param fragment  변경할 fragment
     */
    public void switchContent(Fragment fragment){
        mContentFragment = fragment;

        if (fragment != null) {
            GLog.i("fragment != null", "dd");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
        } else {
            // error in creating fragment
            GLog.e("Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        GLog.i("onNewIntent", "dd");

        if ( CommonData.getInstance().getMemberId() == 0 ) {
            GLog.i("CommonData.getInstance().getMemberId() == 0", "dd");
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.i("requestCode = " +requestCode, "dd");
        GLog.i("resultCode = " +resultCode, "dd");
        GLog.i("data = " + data, "dd");

        if(resultCode != Activity.RESULT_OK){
            return;
        }

        switch(requestCode){
            case CommonData.REQUEST_CHILD_MANAGE:   // 자녀관리
                GLog.i("REQUEST_CHILD_MANAGE", "dd");
                mFrameContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 자녀 데이터가 있는경우 UI 세팅
                        /*if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {

                            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                                mRightImg.setImageResource(R.drawable.main_fetus06b);
                            } else {
                                CustomImageLoader.displayImage(PsyCheckResultActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
                            }



                            mRightLayout.setVisibility(View.VISIBLE);
                        }else{*/
                            mRightLayout.setVisibility(View.GONE);
                       // }

                        setResult(RESULT_OK);
                        switchContent(new PsyMainFragment());

                    }
                }, CommonData.ANI_DELAY_500);
                break;

        }

        if(mContentFragment != null) {
            GLog.i("mContentFragment != null", "dd");
            mContentFragment.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }

    public void setImg(String name){
        switch (name){
            case "대인관계":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_01);
                break;
            case "자아정체감":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_03);
                break;
            case "소아우울증":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_04);
                break;
            case "집중도":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_01);
                break;
            case "학습욕구":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_02);
                break;
            case "두뇌건강":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_03);
                break;
            case "스마트폰중독":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_04);
                break;
            case "교육강박검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_01);
                break;
            case "엄마우울증검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_02);
                break;
            case "상황판단검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_03);
                break;




        }
    }

}