package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.EduVideoListAdapter;
import com.appmd.hi.gngcare.collection.EduVideoItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
// import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class EduVideoActivity extends AppCompatActivity {

    EduVideoListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mEduListLay;
    TextView mTxtNullData;
    TextView commonTitleTv;
    RelativeLayout mCommon_bg_layout;

    ImageButton mCommonLeftBtn;

    ArrayList<EduVideoItem> mEduList;


    int mPageNum;
    int mMaxPageNum;
    int mContentsType;

    private String title;
    private String mCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu_video);

        Intent intent = getIntent();

        title = intent.getStringExtra("TITLE");
        mCode = intent.getStringExtra("ML_MCODE");

        initView();
        init();
        initEvent();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");
       // String info_sn = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);

    }

    protected void initView(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        mCommon_bg_layout = (RelativeLayout)findViewById(R.id.common_bg_layout);
        mEduListLay = (RecyclerView)findViewById(R.id.edu_list_lay);
        mEduListLay.setHasFixedSize(true);
        mTxtNullData = (TextView)findViewById(R.id.txt_null_data);
        commonTitleTv = (TextView)findViewById(R.id.common_title_tv);
        commonTitleTv.setText(title);
    }

    protected void initEvent(){
        mCommonLeftBtn.setOnClickListener(v -> finish());


    }

    protected void init(){
        mPageNum = 1;
  //      mContentsType = 0;
        mEduList = new ArrayList<EduVideoItem>();

        if(mCode.equals("2")){
            mCommon_bg_layout.setBackgroundColor(getResources().getColor(R.color.bg_blue_dark));
        }else if(mCode.equals("1")){
            mCommon_bg_layout.setBackgroundColor(getResources().getColor(R.color.h_green));
        }else{
            mCommon_bg_layout.setBackgroundColor(getResources().getColor(R.color.bg_yellow_light));
        }

        mLayoutManager = new GridLayoutManager(EduVideoActivity.this,2);
        mEduListLay.setLayoutManager(mLayoutManager);
        mAdapter = new EduVideoListAdapter(EduVideoActivity.this, mEduList, mEduListLay, mCode);
        mEduListLay.setAdapter(mAdapter);
        requestEduViewApi(mPageNum);
        mAdapter.setOnLoadMoreListener(() -> {

            if(mEduList.size() < mMaxPageNum){
                mPageNum++;
                requestEduViewApi(mPageNum);
            }
        });
    }

    /**
     * 최근 게시판 목록
     */
    public void requestEduViewApi(int mPageNum) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put("DOCNO", "HM001");
            object.put("ML_MCODE", mCode);
            object.put("PGSIZE", "15");
            object.put("NOWPG",  String.valueOf(mPageNum));//);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(EduVideoActivity.this, NetworkConst.NET_EDU_VIDEO_LIST, NetworkConst.getInstance().getPsyDomain(), networkListener, params,  new MakeProgress(this));
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }

    }

    
    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_EDU_VIDEO_LIST:	// 게시판 리스트 가져오기
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_EDU_VIDEO_LIST", "dd");

                            try {


                                    JSONArray eduList = resultData.getJSONArray("DATA");

                                    if(eduList.length() > 0){

                                        if(mPageNum == 1)
                                            mEduList.clear();

                                        // eduListJSONObject.getInt(CommonData.JSON_CONTENT_TYP)

                                        for (int i= 0; i < eduList.length(); i++){
                                            JSONObject eduListJSONObject = eduList.getJSONObject(i);
                                            EduVideoItem newItem;
                                            try {
                                                newItem = new EduVideoItem(
                                                        eduListJSONObject.getString("ML_SN"),
                                                        eduListJSONObject.getString("IMG"),
                                                        eduListJSONObject.getString("IURL"),
                                                        eduListJSONObject.getString("TIT"),
                                                        eduListJSONObject.getString("TOTPG"));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                                newItem = new EduVideoItem(
                                                        eduListJSONObject.getString("ML_SN"),
                                                        eduListJSONObject.getString("IMG"),
                                                        eduListJSONObject.getString("IURL"),
                                                        eduListJSONObject.getString("TIT"),
                                                        eduListJSONObject.getString("TOTPG"));
                                            }

                                            mEduList.add(newItem);
                                            mAdapter.notifyItemInserted(mEduList.size());
                                        }
                                        mAdapter.setLoaded();

                                        mAdapter.notifyDataSetChanged();
                                        mTxtNullData.setVisibility(View.GONE);
                                        mEduListLay.setVisibility(View.VISIBLE);
                                    }

                            }catch(Exception e){
                                GLog.e(e.toString());
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");

                            break;
                    }
                    break;

            }
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();

        }
    };

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}
