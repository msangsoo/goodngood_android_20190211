package com.appmd.hi.gngcare.greencare.googleFitness;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.appmd.hi.gngcare.BuildConfig;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by MrsWin on 2017-05-02.
 */

public class GoogleFitInstance {
    private final String TAG = GoogleFitInstance.class.getSimpleName();
    public static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;
    public static final int GOOGLE_PERMISSION_REQ_CODE = 0x567;

    private OnDataPointListener mListener;
    private static GoogleFitInstance instance;

    public static GoogleFitInstance getInstance() {
        if (instance == null)
            instance = new GoogleFitInstance();
        return instance;
    }

    /**
     * 구글 피트니스 인증 여부
     * @param context
     * @return
     */
    public static boolean isFitnessAuth(Context context) {
        FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_WRITE)
                        .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_WRITE)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_ACTIVITY_SAMPLES)
                        .addDataType(DataType.TYPE_LOCATION_SAMPLE)
                        .build();

        return GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), fitnessOptions);
    }

    public static void requestGoogleFitnessAuth(Activity activity) {
        FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_WRITE)
                        .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_WRITE)
                        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                        .addDataType(DataType.TYPE_ACTIVITY_SAMPLES)
                        .addDataType(DataType.TYPE_LOCATION_SAMPLE)
                        .build();
        GoogleSignIn.requestPermissions(
                activity,
                GOOGLE_PERMISSION_REQ_CODE,
                GoogleSignIn.getLastSignedInAccount(activity),
                fitnessOptions);
    }

    /**
     * 구글 인증 SignOut
     * @param context
     * @param handler
     */
    public static void signOutGoogleClient(Context context, final ApiData.IStep handler) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
//                .requestIdToken("<web client id>")
                .build();

        GoogleSignIn.getClient(context, gso).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (handler != null)
                    handler.next(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                if (handler != null)
                    handler.next(false);
            }
        });
    }

    /**
     * 구글 피트니스 권한 창 띄우기
     * @param callBack
     */
    public void subscribe(final Activity activity, final ApiData.IStep callBack) {
        // To create a subscription, invoke the Recording API. As soon as the subscription is
        // active, fitness data will start recording.
        Fitness.getRecordingClient(activity, GoogleSignIn.getLastSignedInAccount(activity))
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i(TAG, "subscribe.onSuccess");
                        SharedPref.getInstance(activity).savePreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, true);
                        callBack.next(null);
                    }
                })
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Log.i(TAG, "subscribe.onComplete task.isSuccessful()="+task.isSuccessful());
                                if (task.isSuccessful()) {
                                    Log.i(TAG, "Successfully subscribed!");
                                    SharedPref.getInstance(activity).savePreferences(SharedPref.IS_REGIST_GOOGLE_FITNESS, true);
                                } else {
                                    Log.w(TAG, "There was a problem subscribing.", task.getException());
                                }
                                callBack.next(null);
                            }
                        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "subscribe.onFailure");
//                        callBack.next(null);
                        activity.finish();
                    }
                });
    }

//    ResultCallback mSubcribeResultCallback = new ResultCallback<Status>() {
//        @Override
//        public void onResult(Status status) {
//            if (status.isSuccess()) {
//                if (status.getStatusCode() == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
//                    GLog.i(TAG, "Existing subscription for activity detected.");
//                } else {
//                    GLog.i(TAG, "Successfully subscribed!");
//                }
//            } else {
//                GLog.w(TAG, "There was a problem subscribing.");
//            }
//        }
//    };

//    /**
//     * 총걸음수, 칼로리 구하기
//     */
//    public void totalValDataTask(final GoogleApiClient client, final DataType dataType, final ApiData.IStep callBack) {
//        new TotalValDataTask(client, dataType, callBack).execute();
//    }
//
//    private class TotalValDataTask extends AsyncTask<Object, Object, Long> {
//        private GoogleApiClient client = null;
//        private DataType dataType;
//        private ApiData.IStep callBack;
//
//        public TotalValDataTask(final GoogleApiClient client, DataType dataType, ApiData.IStep callBack) {
//            this.client = client;
//            this.dataType = dataType;
//            this.callBack = callBack;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        protected Long doInBackground(Object... params) {
//            long total = 0;
//            PendingResult<DailyTotalResult> result = Fitness.HistoryApi.readDailyTotal(client, dataType);
//            DailyTotalResult totalResult = result.await(30, TimeUnit.SECONDS);
//            if (totalResult.getStatus().isSuccess()) {
//                DataSet totalSet = totalResult.getTotal();
//                if (dataType == DataType.TYPE_STEP_COUNT_DELTA
//                        || dataType == DataType.TYPE_STEP_COUNT_CADENCE
//                        || dataType == DataType.AGGREGATE_STEP_COUNT_DELTA
//                        ) {
//                    total = totalSet.isEmpty()
//                            ? 0
//                            : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
//                } else if (dataType == DataType.TYPE_CALORIES_EXPENDED
//                        || dataType == DataType.AGGREGATE_CALORIES_EXPENDED) {
//                    float calorie = totalSet.isEmpty()
//                            ? 0
//                            : totalSet.getDataPoints().get(0).getValue(Field.FIELD_CALORIES).asFloat();
//                    total = (long) calorie;
//                } else if (dataType == DataType.AGGREGATE_ACTIVITY_SUMMARY) {
//
//                }
//            } else {
//                Logger.w(TAG, "There was a problem getting the step count.");
//            }
//            Logger.i(TAG, "TotalValDataTask.total=" + total + ", " + dataType.getName());
//
//            return total;
//        }
//
//        @Override
//        protected void onPostExecute(Long aLong) {
//            super.onPostExecute(aLong);
//            if (callBack != null)
//                callBack.next(aLong);
//        }
//    }

    public void stopClient(BaseFragment fragment) {
//        if (mClient != null && mClient.isConnected()) {
        if (mClient != null) {
            Logger.i(TAG, "GoogleApiClient stopClient");
            mClient.stopAutoManage(fragment.getActivity());
            mClient.disconnect();
            mClient = null;
        }
        fragment.hideProgress();
    }

//    public void buildApiClient(final FragmentActivity activity, final IConnectResult iConnectResult) {
//        if (mClient != null) {
//            Logger.i(TAG, "buildFitnessClientApis.mClient isCreated=" + mClient + ", success");
//            iConnectResult.success(mClient);
//            return;
//        }
//
//        if (mClient != null) {
//            mClient.connect();
//            iConnectResult.success(mClient);
//            return;
//        }
//
//        mClient = new GoogleApiClient.Builder(activity)
//                .addApi(Fitness.SENSORS_API)
//                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
//                .addApi(Fitness.HISTORY_API)
//                .addApi(Fitness.RECORDING_API)
//                .addApi(Fitness.SESSIONS_API)
//                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
//                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
//                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
//                    @Override
//                    public void onConnected(Bundle bundle) {
//                        Logger.i(TAG, "buildFitnessClientApis Connected!!! ");
//                        iConnectResult.success(mClient);
//                    }
//
//                    @Override
//                    public void onConnectionSuspended(int i) {
//                        if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
//                            Logger.i(TAG, "buildFitnessClientApis Connection lost.  Cause: Network Lost.");
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(NETWORK_LOST)");
//
//                        } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
//                            Logger.i(TAG, "buildFitnessClientApis Connection lost.  Reason: Service Disconnected");
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(Disconnected)");
//                        }
//
//                        iConnectResult.fail();
//                    }
//                })
//                .enableAutoManage(activity, 0, new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(ConnectionResult result) {
//
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.toString());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorCode: " + result.getErrorCode());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorMessage: " + result.getErrorMessage());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. getResolution: " + result.getResolution());
//                        Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.isSuccess());
//
//                        if (ConnectionResult.CANCELED == result.getErrorCode()) {
//                            String message = "계정인증 후 이용 가능합니다.";
//                            if (BuildConfig.DEBUG) {
//                                message = message + "1 ConnectionResult.CANCELED" + TAG;
//                            }
//                            CDialog.showDlg(activity, message, new CDialog.DismissListener() {
//                                @Override
//                                public void onDissmiss() {
//                                    SharedPref.getInstance(activity).savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, -1);
////                                    activity.finish();
////                                    Intent intent = new Intent(activity, activity.getClass());
////                                    activity.startActivity(intent);
//
//                                }
//                            });
//                        } else {
//                            CDialog.showDlg(activity, "Google 서비스 연결에 실패 했습니다.(" + result.toString() + ")");
//                        }
//                        iConnectResult.fail();
//                    }
//                })
//                .build();
//    }


    private GoogleApiClient mClient;

    public void buildApiClient(final BaseFragment baseFragment, final IConnectResult iConnectResult) {
        if (mClient != null && mClient.isConnected()) {
            Logger.i(TAG, "buildFitnessClientApis.mClient isCreated=" + mClient + ", success");
            iConnectResult.success(mClient);
            return;
        }

        try {

            if (mClient != null) {
                mClient.connect();
                iConnectResult.success(mClient);
                return;
            }

            mClient = new GoogleApiClient.Builder(baseFragment.getContext())
                    .addApi(Fitness.SENSORS_API)
                    .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                    .addApi(Fitness.HISTORY_API)
                    .addApi(Fitness.RECORDING_API)
                    .addApi(Fitness.SESSIONS_API)
                    .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                    .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            baseFragment.hideProgress();
                            Logger.i(TAG, "buildFitnessClientApis Connected!!! ");
                            iConnectResult.success(mClient);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            baseFragment.hideProgress();
                            stopClient(baseFragment);
                            if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                                Logger.i(TAG, "buildFitnessClientApis Connection lost.  Cause: Network Lost.");
                                CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(NETWORK_LOST)");

                            } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                                Logger.i(TAG, "buildFitnessClientApis Connection lost.  Reason: Service Disconnected");
                                CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(Disconnected)");
                            }

                            iConnectResult.fail();
                        }
                    })
                    .enableAutoManage(baseFragment.getActivity(), 0, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult result) {
                            baseFragment.hideProgress();


                            Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.toString());
                            Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorCode: " + result.getErrorCode());
                            Logger.e(TAG, "buildFitnessClientApis connection failed. getErrorMessage: " + result.getErrorMessage());
                            Logger.e(TAG, "buildFitnessClientApis connection failed. getResolution: " + result.getResolution());
                            Logger.e(TAG, "buildFitnessClientApis connection failed. Cause: " + result.isSuccess());

                            if (ConnectionResult.CANCELED == result.getErrorCode()) {
                                stopClient(baseFragment);
                                String message = "계정인증 후 이용 가능합니다.";
                                if (BuildConfig.DEBUG) {
                                    message = message + "2 ConnectionResult.CANCELED";
                                }
                                CDialog.showDlg(baseFragment.getContext(), message, new CDialog.DismissListener() {
                                    @Override
                                    public void onDissmiss() {
                                        SharedPref.getInstance(baseFragment.getContext()).savePreferences(SharedPref.STEP_DATA_SOURCE_TYPE, -1);
//                                    restartMainActivity();
                                        baseFragment.getActivity().finish();
                                    }
                                });
                            } else {
                                CDialog.showDlg(baseFragment.getContext(), "Google 서비스 연결에 실패 했습니다.(" + result.toString() + ")");
                            }
                            iConnectResult.fail();
                        }
                    })
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO : XXX 구글 피트니스 에러 체크
            if (BuildConfig.DEBUG && baseFragment != null)
                Toast.makeText(baseFragment.getContext(), "디버깅메시지 GoogleFitness:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 총걸음수, 칼로리 구하기
     */
    public void totalReqDataTask(final GoogleApiClient client, DataReadRequest readRequest, final ApiData.IStep callBack) {
        new TotalValReqDataTask(client, readRequest, callBack).execute();
    }

    private class TotalValReqDataTask extends AsyncTask<Object, Object, Long> {
        private GoogleApiClient client = null;
        private DataReadRequest readRequest;
        private ApiData.IStep callBack;

        public TotalValReqDataTask(final GoogleApiClient client, DataReadRequest readRequest, ApiData.IStep callBack) {
            this.client = client;
            this.readRequest = readRequest;
            this.callBack = callBack;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Long doInBackground(Object... params) {
            long total = 0;

            PendingResult<DataReadResult> result = Fitness.HistoryApi.readData(client, readRequest);
            DataReadResult totalResult = result.await(30, TimeUnit.SECONDS);
            if (totalResult.getStatus().isSuccess()) {
                List<DataSet> totalSet = totalResult.getDataSets();
                for (DataSet dataSet : totalSet) {
                    Logger.i(TAG, "dataSet=" + dataSet);
                }

            } else {
                Logger.w(TAG, "There was a problem getting the step count.");
            }

            return total;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            if (callBack != null)
                callBack.next(aLong);
        }
    }

    public void onStop(FragmentActivity activity) {
        if (mClient != null) {// && mClient.isConnected()) {
            Logger.i(TAG, "GoogleFitness onStop()");
            mClient.stopAutoManage(activity);
            mClient.disconnect();
        }
    }

    private void signOut(FragmentActivity activity, GoogleApiClient client) {
        client.clearDefaultAccountAndReconnect();
        client.disconnect();

//        new GoogleFitInstance().buildApiClient(activity, new IConnectResult() {
//            @Override
//            public void success(GoogleApiClient client) {
//                client.clearDefaultAccountAndReconnect();
//                client.disconnect();
//            }
//
//            @Override
//            public void fail() {
//
//            }
//        });
    }
}
