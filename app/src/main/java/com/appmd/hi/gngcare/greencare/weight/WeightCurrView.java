package com.appmd.hi.gngcare.greencare.weight;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.ProgressItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.motherhealth.MotherWeightInputFragment;
import com.appmd.hi.gngcare.motherhealth.RequestDietProgramActivity;
import com.appmd.hi.gngcare.util.CustomSeekBar;

import java.util.ArrayList;


/**
 * 현재체중
 */

public class WeightCurrView extends LinearLayout {
    private final String TAG = WeightCurrView.class.getSimpleName();

    private Context context;

    private CustomSeekBar seekbar;
    private float totalSpan = 100;
    private float span01 = 15;
    private float span02 = 30;
    private float span03 = 15;
    private float span04 = 15;
    private float span05;

    private ArrayList<ProgressItem> progressItemList;
    private ProgressItem mProgressItem;

    public WeightCurrView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public WeightCurrView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public WeightCurrView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.weight_curr_view, null);
        LayoutParams contentParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        seekbar = ((CustomSeekBar) view.findViewById(R.id.seekBar));
        addView(view, contentParams);

//        json={"api_code":"mber_mother_bdwgh_view","insures_code":"108","mber_sn": "21182"}



        // 임신중 체중 안내
        view.findViewById(R.id.pregnant_weight_noti_btn).setOnClickListener(mClickListener);
        // 엄마체중측정
        view.findViewById(R.id.mom_weight_measurement_btn).setOnClickListener(mClickListener);
        // 엄마체중예측
        view.findViewById(R.id.mom_weight_prediction_btn).setOnClickListener(mClickListener);
        // 상담연결하기
        view.findViewById(R.id.Hcall_btn).setOnClickListener(mClickListener);
        // 다이어트 프로그램 신청하기
        view.findViewById(R.id.req_diet_btn).setOnClickListener(mClickListener);

        //click 저장
        com.appmd.hi.gngcare.greencare.component.OnClickListener ClickListener = new com.appmd.hi.gngcare.greencare.component.OnClickListener(mClickListener, view, context);

        //엄마 건강
        view.findViewById(R.id.mom_weight_measurement_btn).setOnTouchListener(ClickListener);
        view.findViewById(R.id.pregnant_weight_noti_btn).setOnTouchListener(ClickListener);
        view.findViewById(R.id.mom_weight_prediction_btn).setOnTouchListener(ClickListener);
        view.findViewById(R.id.Hcall_btn).setOnTouchListener(ClickListener);
        view.findViewById(R.id.req_diet_btn).setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        view.findViewById(R.id.mom_weight_measurement_btn).setContentDescription(context.getString(R.string.mom_weight_measurement_btn));
        view.findViewById(R.id.pregnant_weight_noti_btn).setContentDescription(context.getString(R.string.pregnant_weight_noti_btn));
        view.findViewById(R.id.mom_weight_prediction_btn).setContentDescription(context.getString(R.string.mom_weight_prediction_btn));
        view.findViewById(R.id.Hcall_btn).setContentDescription(context.getString(R.string.HCallBtn7));
        view.findViewById(R.id.req_diet_btn).setContentDescription(context.getString(R.string.req_diet_btn));

    }


    OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.pregnant_weight_noti_btn) {
                bmiInfoPopup();
            }else if(vId == R.id.mom_weight_measurement_btn){
                DummyActivity.startActivity(((Activity)context), MotherWeightInputFragment.class, new Bundle());
            }else if(vId == R.id.mom_weight_prediction_btn){
                DummyActivity.startActivityForResult(((Activity) getContext()), WeightBigDataInputFragment.REQ_WEIGHT_PREDICT, WeightBigDataInputFragment.class, new Bundle());
            }else if(vId == R.id.req_diet_btn) {
                reqDietProgram();
            }else if(vId == R.id.Hcall_btn){
                if("10".equals(CommonData.getInstance().getMberGrad())) {
                    CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(getContext().getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getContext().getString(R.string.do_call_center));
                    mDialog.setNegativeButton(getContext().getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(getContext().getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        String tel = "tel:" + getContext().getString(R.string.call_center_number);
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(tel));
                        getContext().startActivity(intent);
                        dialog.dismiss();
                    });

                    mDialog.show();
                }else{
                    CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(getContext().getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getContext().getString(R.string.call_center2));
                    mDialog.setNegativeButton(getContext().getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(getContext().getString(R.string.do_call), (dialog, button) -> {
                        String tel = "tel:" + getContext().getString(R.string.call_center_number2);
//                        startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                        Intent intentCall = new Intent(Intent.ACTION_DIAL);
                        intentCall.setData(Uri.parse(tel));
                        getContext().startActivity(intentCall);
                        dialog.dismiss();
                    });
                    mDialog.show();
                }
            }
        }
    };

    /**
     * 임신중 체중안내
     * mber_mother_bdwgh_view 전문에서 문구
     */
    public void bmiInfoPopup() {
        CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
        mDialog.setTitle("");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_bmi, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        mDialog.setContentView(view, params);

        TextView title = view.findViewById(R.id.bmi_title);

        TextView rateTv1  = view.findViewById(R.id.pregnant_rate_1);
        TextView rateTv2  = view.findViewById(R.id.pregnant_rate_2);
        TextView rateTv3  = view.findViewById(R.id.pregnant_rate_3);
        TextView rateTv4  = view.findViewById(R.id.pregnant_rate_4);

        rateTv1.setText(WeightManageFragment.bmi1);
        rateTv2.setText(WeightManageFragment.bmi2);
        rateTv3.setText(WeightManageFragment.bmi3);
        rateTv4.setText(WeightManageFragment.bmi4);

        title.setText(getContext().getString(R.string.임신기간체중안내팝업타이틀1,WeightManageFragment.bmi5));
//        mDialog.setContent("임신중 체중 안내 문구 넣어야 함");
        mDialog.setPositiveButton(getContext().getString(R.string.popup_dialog_button_confirm), null);
        mDialog.show();
    }


    /**
     * 다이어트프로그램신청하기
     */
    public void reqDietProgram() {
        CommonData commonData = CommonData.getInstance();
//        String materPregency = common.getMotherIsPregnancy(); //임신여부
        if ("10".equals(commonData.getMberGrad())) {
            // 정회원인 경우
            Intent intent = new Intent(getContext(), RequestDietProgramActivity.class);
            getContext().startActivity(intent);
        } else {
            // 임신전 안내
            CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
            mDialog.setTitle(getContext().getString(R.string.popup_dialog_a_type_title));
            mDialog.setContent(getContext().getString(R.string.다이어트프로그램준회원안내));
            mDialog.setNegativeButton(getContext().getString(R.string.popup_dialog_button_cancel), null);
            mDialog.setPositiveButton(getContext().getString(R.string.do_call), new CustomAlertDialogInterface.OnClickListener() {
                @Override
                public void onClick(CustomAlertDialog dialog, Button button) {
                    String tel = "tel:" + getContext().getString(R.string.call_center_number_2);
                    Intent intentCall = new Intent(Intent.ACTION_DIAL);
                    intentCall.setData(Uri.parse(tel));
                    getContext().startActivity(intentCall);
                    dialog.dismiss();
                }
            });
            mDialog.show();
        }

    }
    /* 임신 중 */
    private void initDataToSeekbarCase01() {

    }

    public void initData(ArrayList<ProgressItem> progressItemList, float curBmi, float goalBmi) {
        curBmi = ((curBmi / 18) * 100);
        goalBmi = ((goalBmi / 18) * 100);

        seekbar.setMax(100);

        seekbar.initData(progressItemList, curBmi, goalBmi);
        seekbar.setEnabled(false);

        seekbar.invalidate();
    }

    public void initData(ArrayList<ProgressItem> progressItemList, int curWtInt, int goalWtInt, boolean flag) {
        seekbar.initData(progressItemList, curWtInt, goalWtInt, false);
        seekbar.setEnabled(false);

        seekbar.invalidate();
    }
}