package com.appmd.hi.gngcare.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.googleFitness.GoogleFitInstance;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.intro.LoginActivity;
import com.appmd.hi.gngcare.motherhealth.MotherHealthRegActivity;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;

/**
 * Created by jihoon on 2016-03-28.
 * 설정 클래스
 * @since 0, 1
 */
public class SettingActivity extends BackBaseActivity implements View.OnClickListener {
    private final String TAG = getClass().getSimpleName();

    public static Activity SettingActivity;

    private RelativeLayout mMonJinLayout, mAlarmLayout, mAuthLayout, mAboutServiceLayout,
                           mPersonalTerms_1_Layout, mPersonalTerms_2_Layout, mLocationTermsLayout,
                           mPersonalTerms_3_Layout, mVersionLayout, mLogoutLayout, mJunMemberLayout, mJungSwitchLayout;
    private TextView mVersionTv, mAuthTitleTv, mJunMemberTv, mJungSwitchTv;
    private ImageView mVersionImg;
    private View view;
    private View junline, memline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        view = findViewById(R.id.root_view);


        SettingActivity = this;

        setTitle(getString(R.string.setting));

        init();
        setEvent();

        mVersionTv.setText(commonData.getAppVersion()); // 버전 정보

        switch(commonData.getLoginType()){
            case CommonData.LOGIN_TYPE_PARENTS: // 로그인 타입 부모
                mAuthTitleTv.setText(getString(R.string.id_info));
                break;
            case CommonData.LOGIN_TYPE_CHILD:   // 로그인 타입 자녀
                mAuthTitleTv.setText(getString(R.string.auth_management));
                break;
        }

    }

    /**
     * 초기화
     */
    public void init(){

        mMonJinLayout = (RelativeLayout) findViewById(R.id.MonJin_layout);
        mAlarmLayout    =   (RelativeLayout)    findViewById(R.id.alarm_layout);
        mAuthLayout    =   (RelativeLayout)    findViewById(R.id.auth_layout);
        mAboutServiceLayout=    (RelativeLayout)    findViewById(R.id.about_service_layout);
        mPersonalTerms_1_Layout = (RelativeLayout)findViewById(R.id.personal_terms_1_layout);
        mPersonalTerms_2_Layout = (RelativeLayout)findViewById(R.id.personal_terms_2_layout);
        mPersonalTerms_3_Layout = (RelativeLayout)findViewById(R.id.personal_terms_3_layout);
        mLocationTermsLayout = (RelativeLayout)findViewById(R.id.location_terms_layout);
        mVersionLayout    =   (RelativeLayout)    findViewById(R.id.version_info_layout);
        mLogoutLayout    =   (RelativeLayout)    findViewById(R.id.logout_layout);
        mJunMemberLayout = (RelativeLayout) findViewById(R.id.junmember_layout);
        mJungSwitchLayout = (RelativeLayout) findViewById(R.id.memberswitch_layout);

        mVersionTv      =   (TextView)          findViewById(R.id.version_info_tv);
        mAuthTitleTv    =   (TextView)          findViewById(R.id.auth_tv);
        mJunMemberTv    =   (TextView)          findViewById(R.id.junmember_tv);
        mJungSwitchTv    =   (TextView)          findViewById(R.id.memberswitch_tv);

        mVersionImg     =   (ImageView)         findViewById(R.id.version_info_img);
        junline  = findViewById(R.id.jun_line);
        memline = findViewById(R.id.mem_line);

        if(CommonData.getInstance().getMberGrad().equals("10")){
            mJungSwitchTv.setTextColor(ContextCompat.getColor(SettingActivity.this,R.color.color_BFBFBF));
            mJunMemberTv.setTextColor(ContextCompat.getColor(SettingActivity.this,R.color.color_BFBFBF));

            mJunMemberLayout.setVisibility(View.GONE);
            mJungSwitchLayout.setVisibility(View.GONE);
            junline.setVisibility(View.GONE);
            memline.setVisibility(View.GONE);

            mJungSwitchLayout.setEnabled(false);
            mJunMemberLayout.setEnabled(false);
        }else{
            mJungSwitchTv.setTextColor(ContextCompat.getColor(SettingActivity.this,R.color.txt_dark_bold));
            mJunMemberTv.setTextColor(ContextCompat.getColor(SettingActivity.this,R.color.txt_dark_bold));

            mJunMemberLayout.setVisibility(View.VISIBLE);
            mJungSwitchLayout.setVisibility(View.VISIBLE);

            junline.setVisibility(View.VISIBLE);
            memline.setVisibility(View.VISIBLE);

            mJungSwitchLayout.setEnabled(true);
            mJunMemberLayout.setEnabled(true);
        }

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //사전 체크 문항 설정 추가 2018.5.30
        mMonJinLayout.setOnClickListener(this);
        mAlarmLayout.setOnClickListener(this);
        mAuthLayout.setOnClickListener(this);
        mAboutServiceLayout.setOnClickListener(this);
        mPersonalTerms_1_Layout.setOnClickListener(this);
        mPersonalTerms_2_Layout.setOnClickListener(this);
        mPersonalTerms_3_Layout.setOnClickListener(this);
        mLocationTermsLayout.setOnClickListener(this);
        mVersionLayout.setOnClickListener(this);
        mLogoutLayout.setOnClickListener(this);
        mJunMemberLayout.setOnClickListener(this);
        mJungSwitchLayout.setOnClickListener(this);

        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view,SettingActivity.this);

        //설정
        mMonJinLayout.setOnTouchListener(mClickListener);
        mJunMemberLayout.setOnTouchListener(mClickListener);
        mJungSwitchLayout.setOnTouchListener(mClickListener);
        mAlarmLayout.setOnTouchListener(mClickListener);
        mAuthLayout.setOnTouchListener(mClickListener);
        mAboutServiceLayout.setOnTouchListener(mClickListener);

        //코드 부여(설정)
        mMonJinLayout.setContentDescription(getString(R.string.MonJin));
        mJunMemberLayout.setContentDescription(getString(R.string.JunMember));
        mJungSwitchLayout.setContentDescription(getString(R.string.JungSwitch));
        mAlarmLayout.setContentDescription(getString(R.string.Alarm));
        mAuthLayout.setContentDescription(getString(R.string.Auth));
        mAboutServiceLayout.setContentDescription(getString(R.string.AboutService));
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch(v.getId()){
            case R.id.MonJin_layout: // 사전체크문항
                intent = new Intent(SettingActivity.this, MotherHealthRegActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.alarm_layout: // 알림설정
                intent = new Intent(SettingActivity.this, AlarmSettingActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.auth_layout:  // 인증관리
                intent = new Intent(SettingActivity.this, AuthManageActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.about_service_layout: // 서비스 소개
                intent = new Intent(SettingActivity.this, AboutServiceActivity.class);
                startActivity(intent);
                Util.PopupAnimationStart(SettingActivity.this);
                break;
            case R.id.personal_terms_1_layout:  // 개인정보 제공 동의
                if(commonData.getMberGrad().equals("10")) {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_1_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_1));
                }else{
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_1_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_1));
                }
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.personal_terms_2_layout:  // 개인민감정보 제공 동의
                if(commonData.getMberGrad().equals("10")) {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_2_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_2));
                }else {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_2_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_2));
                }
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.personal_terms_3_layout:  // 개인정보 제3자 제공 동의
                if(commonData.getMberGrad().equals("10")) {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_3_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_3));
                }else {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_JUN_3_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_3));
                }
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.location_terms_layout:  // 위치정보 이용 동의
                if(commonData.getMberGrad().equals("10")) {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.LOCATION_TERMS_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.location_terms));
                }else {
                    intent = new Intent(SettingActivity.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, commonData.LOCATION_TERMS_JUN_URL);
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.location_terms));
                }
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.version_info_layout:  // 버전정보
                intent = new Intent(SettingActivity.this, VersionActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;

            case R.id.logout_layout:  // 로그아웃

                mDialog = new CustomAlertDialog(SettingActivity.this, CustomAlertDialog.TYPE_B);
                mDialog.setTitle(getString(R.string.title_logout));
                mDialog.setContent(getString(R.string.title_logout_text));
                mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {

                    commonData.setMberPwd(null);
                    commonData.setMain_Category("");
                    commonData.setAutoLogin(false);
                    commonData.setRememberId(false);
                    commonData.setMberSn("");
                    commonData.setHpMjYnJun(true);

                    Util.setSharedPreference(SettingActivity.this, "MonJin_bef_cm", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_bef_kg", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_kg","");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_term_kg", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_chl_birth_de", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_milk_yn", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_birth_due_de", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_mber_chl_typ", "");
                    Util.setSharedPreference(SettingActivity.this, "MonJin_actqy", "");


                    commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                    mDialog.dismiss();
                    finish();

                    Intent intent2 = new Intent(SettingActivity.this, LoginActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent2);

                    GoogleFitInstance.signOutGoogleClient(SettingActivity.this, new ApiData.IStep() {
                        @Override
                        public void next(Object obj) {
                            Log.i(TAG, "구글피트니스 로그아웃 됨.");
                        }
                    });

//                    BleUtil.BackAnimationStart(SettingActivity.this);
                });
                mDialog.show();
                break;
            case R.id.junmember_layout :
                intent = new Intent(SettingActivity.this, JunMemberAlert1Activity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
            case R.id.memberswitch_layout :
                intent = new Intent(SettingActivity.this, SwitchMemberActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(SettingActivity.this);
                break;
        }
    }
}
