package com.appmd.hi.gngcare.greencare.googleFitness;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.collection.StepItem;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.BluetoothManager;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.bluetooth.model.BandModel;
import com.appmd.hi.gngcare.greencare.charting.data.BarEntry;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;



public class IntroStepUpload {
    private final String TAG = IntroBaseActivity.class.getSimpleName();
    private Context mContext;
    private BluetoothManager.IBluetoothResult mIResult;
    private SimpleDateFormat sdf;

    public IntroStepUpload(Context context, BluetoothManager.IBluetoothResult iResult) {
        mContext = context;
        mIResult = iResult;
    }

    public void start() {
        if (GoogleFitInstance.isFitnessAuth(mContext) == false) {
            Log.e(TAG,"구글 피트니스 인증 안됨 ");
            mIResult.onResult(true);
            return;
        }

        String lastSaveDate = SharedPref.getInstance(mContext).getPreferences(SharedPref.APP_CONNECT_TIME2);
        Log.i(TAG,"lastDate="+lastSaveDate);

        sdf = new SimpleDateFormat("yyyyMMdd");
        if (TextUtils.isEmpty(lastSaveDate)) {
            Log.e(TAG,"마지막 접속 기록 없음, GoogleFitness 데이터 전송 안함."+lastSaveDate);
            SharedPref.getInstance(mContext).savePreferences(SharedPref.APP_CONNECT_TIME2, sdf.format(new Date()));
            mIResult.onResult(true);
            return;
        }

//        lastSaveDate = "20190403";

        // 시작 기준일 오전 12시 00분 00000
        Calendar startCal = CDateUtil.getCalendar_yyyyMMdd(lastSaveDate);
        startCal.set(Calendar.HOUR_OF_DAY, startCal.getMinimum(Calendar.HOUR_OF_DAY));
        startCal.set(Calendar.MINUTE, startCal.getMinimum(Calendar.MINUTE));
        startCal.set(Calendar.SECOND, startCal.getMinimum(Calendar.SECOND));
        startCal.set(Calendar.MILLISECOND, startCal.getMinimum(Calendar.MILLISECOND));

        // 어제 날자
        Calendar yestDay = Calendar.getInstance();
        yestDay.add(Calendar.DATE, -1);
        yestDay.set(Calendar.HOUR_OF_DAY, yestDay.getMaximum(Calendar.HOUR_OF_DAY));
        yestDay.set(Calendar.MINUTE, yestDay.getMaximum(Calendar.MINUTE));
        yestDay.set(Calendar.SECOND, yestDay.getMaximum(Calendar.SECOND));
        yestDay.set(Calendar.MILLISECOND, yestDay.getMaximum(Calendar.MILLISECOND));


        // 어제 기준 1개월 날자
        Calendar Month3Cal = Calendar.getInstance();
        Month3Cal.add(Calendar.MONTH, -1);
        Month3Cal.add(Calendar.DATE, -1);
        Month3Cal.set(Calendar.HOUR_OF_DAY, Month3Cal.getMinimum(Calendar.HOUR_OF_DAY));
        Month3Cal.set(Calendar.MINUTE, Month3Cal.getMinimum(Calendar.MINUTE));
        Month3Cal.set(Calendar.SECOND, Month3Cal.getMinimum(Calendar.SECOND));
        Month3Cal.set(Calendar.MILLISECOND, Month3Cal.getMinimum(Calendar.MILLISECOND));


        String startTime = sdf.format(startCal.getTimeInMillis());
        String yestTime = sdf.format(yestDay.getTimeInMillis());
        String Month3Time = sdf.format(Month3Cal.getTimeInMillis());

        Log.i(TAG, "strTime="+startTime);
        Log.i(TAG, "yestTime="+yestTime );
        Log.i(TAG, "Month3Time="+Month3Time );
        Log.i(TAG, "compareTo="+startTime.compareTo(yestTime) );
        Log.i(TAG, "compare3To="+startTime.compareTo(Month3Time) );

        int comp3To = startTime.compareTo(Month3Time);

        if(comp3To < 0){
            Log.i(TAG, "1개월보다 큼:: strTime="+startTime+", yestTime="+Month3Time+", compareTo="+startTime.compareTo(Month3Time) );
            startCal.set(Calendar.YEAR, Month3Cal.get(Calendar.YEAR));
            startCal.set(Calendar.MONTH, Month3Cal.get(Calendar.MONTH));
            startCal.set(Calendar.DATE, Month3Cal.get(Calendar.DATE));
            startTime = sdf.format(startCal.getTimeInMillis());
        }

        int compTo = startTime.compareTo(yestTime);
        if (compTo > 0) {
            Log.i(TAG, "시작일자가 더 큼:: strTime="+startTime+", yestTime="+yestTime+", compareTo="+startTime.compareTo(yestTime) );
            mIResult.onResult(true);
            return;
        }

            startTime = sdf.format(startCal.getTimeInMillis());

            System.out.println("IntroStepUpload strTime="+startTime+", yestTime="+yestTime+", compareTo="+startTime.compareTo(yestTime) );

            Fitness.getHistoryClient(mContext,
                    GoogleSignIn.getLastSignedInAccount(mContext))
                    .readData(queryFitnessData(startCal.getTimeInMillis(),yestDay.getTimeInMillis()))
                    .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                        @Override
                        public void onSuccess(DataReadResponse response) {
                            readData(response.getBuckets());
                            uploadGoogleFitStepData();
                        }
                    });

    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessData(long start, long end) {
        DataType dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
        DataType dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;


        DateFormat dateFormat = getDateTimeInstance();
        Log.i(TAG, "Range Start: " + dateFormat.format(start));
        Log.i(TAG, "Range End: " + dateFormat.format(end));
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, TimeUnit.HOURS)
                .setTimeRange(start, end, MILLISECONDS)
                .build();

        return readRequest;
    }


    /**
     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
     * @param buckets
     */
    public void readData(List<Bucket> buckets) {
        mArrIdx = 0;
        mGoogleFitStepMap.clear();
        if (buckets.size() > 0) {
            Log.i(TAG, "BucketSize=" + buckets.size());
            for (Bucket bucket : buckets) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
//                    mTotalVal +=
                    dumpDataSet(dataSet);
                }
            }
        }
    }




    /**
     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
     * @param dataSet
     * @return
     */
    private int mArrIdx = 0;
    protected List<BarEntry> mYVals = new ArrayList<>();
    private long dumpDataSet(DataSet dataSet) {
        Log.i(TAG, "====================");
        DateFormat dateFormat = getDateTimeInstance();

        long dumpTotalVal = 0;   // 총 칼로리, 총 걸음수
        mYVals.add(new BarEntry(mArrIdx, 0f));

        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
            Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\t Value:" + field.getName() + "=" + dp.getValue(field) + "[" + mArrIdx + "]");
                // sqlite에 저장 여부를 판단할 용도로 사용할 Map
                StepItem item = new StepItem();
                item.startTime = dp.getStartTime(MILLISECONDS);
                item.step = (int) StringUtil.getFloat(dp.getValue(field).toString());
                mGoogleFitStepMap.put(mArrIdx, item);
                int steps = 0;
                try {
                    steps += StringUtil.getFloat(dp.getValue(field).toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                mYVals.set(mArrIdx, new BarEntry(mArrIdx, (float) steps));
                dumpTotalVal += steps;
            }
        }
        if (dumpTotalVal > 0)
            Log.i(TAG, "mDumpValue=" + dumpTotalVal);
        mArrIdx++;

        return dumpTotalVal;
    }

    /**
     * 구글 걸음 데이터를 서버에 전송 및 Sqlite에 저장하기
     * 일별 조회 일때만 저장하기
     *
     * 20190214 인트로에서 업로드 하도록 수정
     */
    protected Map<Integer, StepItem> mGoogleFitStepMap = new HashMap<>();           // 구글 피트니스 조회 데이터 저장
    private void uploadGoogleFitStepData() {
        if (mGoogleFitStepMap == null || mGoogleFitStepMap.size() <= 0){
            mIResult.onResult(true);
            // 접속 일자 저장
            SharedPref.getInstance(mContext).savePreferences(SharedPref.APP_CONNECT_TIME2, sdf.format(new Date()));
            return;
        }


//        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
        List<BandModel> dataModelArr = new ArrayList<>();

        Iterator<Integer> iter = mGoogleFitStepMap.keySet().iterator();

        String dDate = "";

        Calendar cal = Calendar.getInstance();
        long temp_idx = StringUtil.getLongVal(CDateUtil.getForamtyyMMddHHmmss(new Date(cal.getTimeInMillis())));
        try {
            while(iter.hasNext()){
                int key = iter.next();
//                System.out.println("Map Value:"+mGoogleFitStepMap.get(key));

                StepItem item = mGoogleFitStepMap.get(key);
                long startTime = item.startTime;
                int step = item.step;

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(startTime);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                ++temp_idx;


                BandModel model = new BandModel();
                model.setStep(step);
                model.setRegtype("D");  // 구글 피트니스
                model.setIdx(String.valueOf(temp_idx));
                model.setRegDate(CDateUtil.getForamtyyyyMMddHHmm(new Date(calendar.getTimeInMillis())));

                dDate = model.getIdx();

                DateFormat dateFormat = getDateTimeInstance();
                Log.i(TAG, "StepGoogle.regDate=" + model.getRegDate() + ", idx=" + model.getIdx() + ", step=" + model.getStep());

                dataModelArr.add(model);

            }
        } catch (Exception e) {
            e.printStackTrace();
            mIResult.onResult(false);
        }



//        DBHelper helper = new DBHelper(mContext);
//        DBHelperStep db = helper.getStepDb();
//        List<BandModel> newModelArr = db.getResultRegistData(dataModelArr);
//
        if (dataModelArr.size() > 0) {
            Log.e(TAG, "==================="+dDate+" saved ===================="+dataModelArr.size());
            String finalDDate = dDate;
            new DeviceDataUtil().uploadStepData(mContext, dataModelArr, new BluetoothManager.IBluetoothResult() {
                @Override
                public void onResult(boolean isSuccess) {
//                        mBaseFragment.hideProgress();
                    mIResult.onResult(true);
                    // 접속 일자 저장
                    SharedPref.getInstance(mContext).savePreferences(SharedPref.APP_CONNECT_TIME2, sdf.format(new Date()));
                    Log.e(TAG, "==================="+ finalDDate +" saved ===================="+dataModelArr.size());
                }
            });
        }
    }
}
