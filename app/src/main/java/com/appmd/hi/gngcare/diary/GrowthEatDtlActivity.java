package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.EduVideoListAdapter;
import com.appmd.hi.gngcare.collection.EduVideoItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class GrowthEatDtlActivity extends AppCompatActivity {

    EduVideoListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mEduListLay;
    TextView mTxtNullData;
    TextView commonTitleTv;
    TextView eatTitleTxt;

    TextView cmtTxt;

    ImageButton mCommonLeftBtn;
    ArrayList<EduVideoItem> mEduList;


    TextView dayTxt;



    TextView  row02_2, row02_3;
    TextView  row03_2, row03_3;
    TextView  row04_2, row04_3;
    TextView  row05_2, row05_3;
    TextView  row06_2, row06_3;

    ImageView rightBtn;
    ImageView leftBtn;

    int mPageNum = 0;
    int mMaxPageNum;

    private String mDietTy;

    private ArrayList<ArrayList<JSONObject>> obj = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.growth_eat_dtl_activity);

        Intent intent = getIntent();

        mDietTy = intent.getStringExtra("DIET_TY");


        initView();
        init();
        initEvent();
        requestEduViewApi();

        commonTitleTv.setText(intent.getStringExtra("TITLE"));


        dayTxt.setText(week[0]);

    }



    private String[] sumStrTyp = new String[5];
    private String[] sumStrNm = new String[5];
    private String[] sumStrWt = new String[5];

    int cnt = 0;
    public void setChartData(int index){

        try {
            ArrayList<JSONObject> data = obj.get(index);

            sumStrTyp = new String[5];
            sumStrNm = new String[5];
            sumStrWt = new String[5];

            int cnt = 0;

            GLog.i(data.toString(), "dd");

            for(int i=0; i<data.size(); i++){
                JSONObject tmp = data.get(i);


                String typ = tmp.getString("FOOD_TYP");
                String nm = tmp.getString("FOOD_NM");
                String wt = tmp.getString("FOOD_WT");

                if(typ.compareTo("아침") == 0 ){
                    sumStrNm[0] =  sumStrNm[0]==null?nm:sumStrNm[0] + "\n" + nm;
                    sumStrWt[0] =  sumStrWt[0]==null?wt:sumStrWt[0] + "\n" + wt;
                    sumStrTyp[0] = typ;
                }else if(cnt == 0 &&typ.compareTo("간식") == 0 ){

                    sumStrNm[1] =  sumStrNm[1]==null?nm:sumStrNm[1]  + "\n" + nm;
                    sumStrWt[1] =  sumStrWt[1]==null?wt:sumStrWt[1]  + "\n" + wt;
                    sumStrTyp[1] = typ;
                }else if(typ.compareTo("점심") == 0 ){
                    cnt = 1;
                    sumStrNm[2] =  sumStrNm[2]==null?nm:sumStrNm[2]  + "\n" + nm;
                    sumStrWt[2] =  sumStrWt[2]==null?wt:sumStrWt[2]  + "\n" + wt;
                    sumStrTyp[2] = typ;
                }else if(cnt == 1 &&typ.compareTo("간식") == 0 ){
                    sumStrNm[3] =  sumStrNm[3]==null?nm:sumStrNm[3]  + "\n" + nm;
                    sumStrWt[3] =  sumStrWt[3]==null?wt:sumStrWt[3]  + "\n" + wt;
                    sumStrTyp[3] = typ;
                }else if(typ.compareTo("저녁") == 0 ){
                    sumStrNm[4] =  sumStrNm[4]==null?nm:sumStrNm[4]  + "\n" + nm;
                    sumStrWt[4] =  sumStrWt[4]==null?wt:sumStrWt[4]  + "\n" + wt;
                    sumStrTyp[4] = typ;
                }
            }
            

                row02_2.setText(sumStrNm[0]);
                row02_3.setText(sumStrWt[0]);

                row03_2.setText(sumStrNm[1]);
                row03_3.setText(sumStrWt[1]);

                row04_2.setText(sumStrNm[2]);
                row04_3.setText(sumStrWt[2]);

                row05_2.setText(sumStrNm[3]);
                row05_3.setText(sumStrWt[3]);

                row06_2.setText(sumStrNm[4]);
                row06_3.setText(sumStrWt[4]);



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");
        // String info_sn = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);

    }

    protected void initView() {
        mCommonLeftBtn = (ImageButton) findViewById(R.id.common_left_btn);
        commonTitleTv = (TextView) findViewById(R.id.titleTxt);
        eatTitleTxt = (TextView) findViewById(R.id.eatTitleTxt);
        cmtTxt = (TextView) findViewById(R.id.cmtTxt);
        dayTxt  = (TextView) findViewById(R.id.dayTxt);
        rightBtn = (ImageView) findViewById(R.id.rightBtn);
        leftBtn = (ImageView) findViewById(R.id.leftBtn);

        //   commonTitleTv.setText(title);

        row02_2 = (TextView)findViewById(R.id.row_2_2);
        row02_3 = (TextView)findViewById(R.id.row_2_3);


        row03_2 = (TextView)findViewById(R.id.row_3_2);
        row03_3 = (TextView)findViewById(R.id.row_3_3);


        row04_2 = (TextView)findViewById(R.id.row_4_2);
        row04_3 = (TextView)findViewById(R.id.row_4_3);


        row05_2 = (TextView)findViewById(R.id.row_5_2);
        row05_3 = (TextView)findViewById(R.id.row_5_3);


        row06_2 = (TextView)findViewById(R.id.row_6_2);
        row06_3 = (TextView)findViewById(R.id.row_6_3);

    }

    private String[] week = {"월요일","화요일", "수요일","목요일", "금요일","토요일", "일요일"};

    protected void initEvent() {
        mCommonLeftBtn.setOnClickListener(v -> finish());
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPageNum--;
                if(mPageNum == -1 ){
                    mPageNum = 6;
                }
                dayTxt.setText(week[mPageNum ]);
                setChartData(mPageNum);
            }
        });

        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPageNum++;
                if(mPageNum == 7 ){
                    mPageNum = 0;
                }

                dayTxt.setText(week[mPageNum]);
                setChartData(mPageNum);
            }
        });


    }

    protected void init() {

    }

    /**
     * 최근 게시판 목록
     */
    public void requestEduViewApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put("DOCNO", "HF001");
            object.put("CHL_SN", MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
//            object.put("CHL_SN", 1310);
            object.put("DIET_TY", mDietTy);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(GrowthEatDtlActivity.this, NetworkConst.NET_EAT_PLAN_ROWS, NetworkConst.getInstance().getPsyDomain(), networkListener, params, new MakeProgress(this));
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }

    }




    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch (type) {
                case NetworkConst.NET_EAT_PLAN_ROWS:    // 게시판 리스트 가져오기
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_EDU_VIDEO_LIST", "dd");

                            try {

                                JSONArray eatData = resultData.getJSONArray("DATA");

                                ArrayList<JSONObject> oneArr = new ArrayList<>();
                                ArrayList<JSONObject> twoArr = new ArrayList<>();
                                ArrayList<JSONObject> threeArr = new ArrayList<>();
                                ArrayList<JSONObject> fourArr = new ArrayList<>();
                                ArrayList<JSONObject> fiveArr = new ArrayList<>();
                                ArrayList<JSONObject> sixArr = new ArrayList<>();
                                ArrayList<JSONObject> sevenArr = new ArrayList<>();

                                if (eatData.length() > 0) {
                                    cmtTxt.setText(resultData.getString("CMT"));
                                    eatTitleTxt.setText(resultData.getString("DIET_NM") + " " + resultData.getString("GK_KCAL") + "Kcal");
                                    for (int i = 0; i < eatData.length(); i++) {
                                        JSONObject tmpObj = eatData.getJSONObject(i);
                                        String dfk = tmpObj.getString("DFK");
                                        if (dfk.compareTo("1") == 0) {
                                            oneArr.add(tmpObj);
                                        } else if (dfk.compareTo("2") == 0) {
                                            twoArr.add(tmpObj);
                                        } else if (dfk.compareTo("3") == 0) {
                                            threeArr.add(tmpObj);
                                        } else if (dfk.compareTo("4") == 0) {
                                            fourArr.add(tmpObj);
                                        } else if (dfk.compareTo("5") == 0) {
                                            fiveArr.add(tmpObj);
                                        } else if (dfk.compareTo("6") == 0) {
                                            sixArr.add(tmpObj);
                                        } else if (dfk.compareTo("7") == 0) {
                                            sevenArr.add(tmpObj);
                                        }
                                    }

                                    obj.add(oneArr);
                                    obj.add(twoArr);
                                    obj.add(threeArr);
                                    obj.add(fourArr);
                                    obj.add(fiveArr);
                                    obj.add(sixArr);
                                    obj.add(sevenArr);

                           //         android.util.GLog.i();

                                    setChartData(0);
                                }

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");

                            break;
                    }
                    break;

            }
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();

        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}

