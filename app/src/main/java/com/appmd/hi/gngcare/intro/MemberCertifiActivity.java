package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class MemberCertifiActivity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{

    private LinearLayout mNameLayout, mBirthLayout, mPhoneLayout;
    private TextView mPhoneTitleTv, mPersonalTv_1, mPersonalTv_2, mPersonalTv_3, mLocationTv;
    private EditText mNameEdit, mPhoneEdit, mBirthTv;
    private ImageButton mNameDelBtn, mPhoneDelBtn;

    private RadioButton mLocalRb, mForeignerRb, mMaleRb, mFemaleRb, mJobRb, mNoJobRb;
    private CheckBox mPersonalCb_1, mPersonalCb_2, mPersonalCb_3, mLocationCb;

    private Button mCertifiBtn, mAllAgreeBtn;

    private String mCurrentBirth =   "";
    Calendar cal = Calendar.getInstance();

    private String mBer_nm, mBer_lifyea, mBer_hp, mBer_nation, mBer_sex, mBer_job;    // api 호출시 저장변수

    private Intent intent = null;
    private int mLoginType = 1;

    String mberNo = "";
    String mBerNm = "";
    String mBerPhone = "";
    String mBirthday = "";
    String mBerjob ="Y";
    int mBerNation = 1;
    int mGender = 1;


    private Date mCurDate;
    GregorianCalendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_certifi_activity);

        setTitle(getString(R.string.member_certifi));

        init();
        setEvent();

        Util.setTextViewCustom(MemberCertifiActivity.this, mPersonalTv_1, mPersonalTv_1.getText().toString(), mPersonalTv_1.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(MemberCertifiActivity.this, mPersonalTv_2, mPersonalTv_2.getText().toString(), mPersonalTv_2.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(MemberCertifiActivity.this, mPersonalTv_3, mPersonalTv_3.getText().toString(), mPersonalTv_3.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(MemberCertifiActivity.this, mLocationTv, mLocationTv.getText().toString(), mLocationTv.getText().toString(), 0, 15, Typeface.BOLD, true);

        mCertifiBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if(intent != null){
            mLoginType  =   intent.getIntExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);

        }else{
            GLog.i("intent = null", "dd");
        }

        GLog.i("mLoginType = " +mLoginType, "dd");
        switch (mLoginType){
            case CommonData.LOGIN_TYPE_PARENTS: // 계약자 ( 부모 )
                mPhoneTitleTv.setText(getString(R.string.mobile_phone_number));
                mPhoneEdit.setHint(getString(R.string.phone_number_hint));
                break;
            case CommonData.LOGIN_TYPE_CHILD:   // 피보험자 ( 자녀 )
                mPhoneTitleTv.setText(getString(R.string.unique_number));
                mPhoneEdit.setHint(getString(R.string.uniq_hint));
                break;
        }

        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
    }

    /**
     * 초기화
     */
    public void init(){

        mNameLayout     =   (LinearLayout)      findViewById(R.id.name_layout);
        mBirthLayout    =   (LinearLayout)      findViewById(R.id.birthday_layout);
        mPhoneLayout    =   (LinearLayout)      findViewById(R.id.phone_number_layout);

        mBirthTv        =   (EditText)          findViewById(R.id.birthday_tv);
        mPersonalTv_1      =   (TextView)          findViewById(R.id.personal_terms_1_tv);
        mPersonalTv_2      =   (TextView)          findViewById(R.id.personal_terms_2_tv);
        mPersonalTv_3      =   (TextView)          findViewById(R.id.personal_terms_3_tv);
        mLocationTv     =   (TextView)          findViewById(R.id.location_terms_tv);
        mPhoneTitleTv   =   (TextView)          findViewById(R.id.phone_number_title_tv);

        mNameEdit       =   (EditText)          findViewById(R.id.name_edit);
        mPhoneEdit      =   (EditText)          findViewById(R.id.phone_number_edit);

        mNameDelBtn     =   (ImageButton)       findViewById(R.id.name_del_btn);
        mPhoneDelBtn    =   (ImageButton)       findViewById(R.id.phone_number_del_btn);

        mLocalRb        =   (RadioButton)       findViewById(R.id.local_btn);
        mForeignerRb    =   (RadioButton)       findViewById(R.id.foreigner_btn);
        mMaleRb         =   (RadioButton)       findViewById(R.id.male_btn);
        mFemaleRb       =   (RadioButton)       findViewById(R.id.female_btn);
        mJobRb         =   (RadioButton)       findViewById(R.id.job_btn);
        mNoJobRb       =   (RadioButton)       findViewById(R.id.no_job_btn);

        mPersonalCb_1      =   (CheckBox)          findViewById(R.id.personal_terms_1_cb);
        mPersonalCb_2      =   (CheckBox)          findViewById(R.id.personal_terms_2_cb);
        mPersonalCb_3      =   (CheckBox)          findViewById(R.id.personal_terms_3_cb);
        mLocationCb     =   (CheckBox)          findViewById(R.id.location_terms_cb);

        mCertifiBtn     =   (Button)            findViewById(R.id.certifi_btn);
        mAllAgreeBtn    =   (Button)            findViewById(R.id.agree_btn);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mPersonalTv_1.setOnClickListener(this);
        mPersonalTv_2.setOnClickListener(this);
        mPersonalTv_3.setOnClickListener(this);
        mLocationTv.setOnClickListener(this);
        mCertifiBtn.setOnClickListener(this);
        mAllAgreeBtn.setOnClickListener(this);
        mNameDelBtn.setOnClickListener(this);
        mPhoneDelBtn.setOnClickListener(this);

        mPersonalCb_1.setOnCheckedChangeListener(this);
        mPersonalCb_2.setOnCheckedChangeListener(this);
        mPersonalCb_3.setOnCheckedChangeListener(this);
        mLocationCb.setOnCheckedChangeListener(this);

        mNameEdit.setOnFocusChangeListener(this);
        mPhoneEdit.setOnFocusChangeListener(this);

//        mNameEdit.addTextChangedListener(new MyTextWatcher());
//        mPhoneEdit.addTextChangedListener(new MyTextWatcher());
//        mBirthTv.addTextChangedListener(new MyTextWatcher());

    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("")){    // 이름 입력이 없다면
            mNameEdit.requestFocus();
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_name_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

//        if(StringUtil.isSpecialWordNum(mNameEdit.getText().toString().trim()) == false){
//            mNameEdit.requestFocus();
//            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
//            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
//            mDialog.setContent(getString(R.string.popup_dialog_name_error_1));
//            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
//            mDialog.show();
//            bool = false;
//            return bool;
//        }

        String birth = mBirthTv.getText().toString();

        if(birth.trim().length() != 8){
            mBirthTv.requestFocus();
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
            mBirthTv.requestFocus();
            mDialog = new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent("생일은 미래일 수 없습니다.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!CDateUtil.getDateFormatBool(birth)){
            mBirthTv.requestFocus();
            mDialog = new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent("날짜형식이 아닙니다.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        /*if(mCurrentBirth.equals("")){   // 생년월일
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }*/

        if(mPhoneEdit.getText().toString().trim().equals("")){   // 휴대폰 번호
            mPhoneEdit.requestFocus();
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_phone_number_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(mPhoneEdit.getText().toString().trim().length() < 10){
            mPhoneEdit.requestFocus();
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_phone_number_error_1));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!mPersonalCb_1.isChecked() || !mPersonalCb_2.isChecked()|| !mPersonalCb_3.isChecked()|| !mLocationCb.isChecked()){    // 약관 동의가 안되어 있다면
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_terms_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }



        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2){    // 이름 입력이 없다면
            bool = false;
            return bool;
        }

        if(mBirthTv.getText().toString().trim().equals("") || mBirthTv.getText().toString().trim().length() < 8){
            bool = false;
            return bool;
        }

        /*if(mCurrentBirth.equals("")){   // 생년월일
            bool = false;
            return bool;
        }*/

        if(mPhoneEdit.getText().toString().trim().equals("") || mPhoneEdit.getText().toString().trim().length() < 10){   // 휴대폰 번호
            bool = false;
            return bool;
        }

        if(!mPersonalCb_1.isChecked() || !mPersonalCb_2.isChecked()|| !mPersonalCb_3.isChecked()|| !mLocationCb.isChecked()){    // 약관 동의가 안되어 있다면
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

//        if(bool) {
            mCertifiBtn.setEnabled(true);

            mCertifiBtn.setBackgroundResource(R.drawable.btn_5_22396c);
            mCertifiBtn.setTextColor(ContextCompat.getColorStateList(MemberCertifiActivity.this, R.color.color_ffffff_btn));
//        }
//        }else{
//            mCertifiBtn.setEnabled(false);
//
//            mCertifiBtn.setBackgroundResource(R.drawable.background_5_e6e7e8_sq);
//            mCertifiBtn.setTextColor(ContextCompat.getColorStateList(MemberCertifiActivity.this, R.color.color_ffffff_btn));
//        }

    }

    /**
     * 회원 체크 ( 부모 )
     * @param mber_nm
     * @param mber_lifyea
     * @param mber_hp
     * @param mber_nation
     * @param mber_sex
     */
    public void requestAuthCheck(String mber_nm, String mber_lifyea, String mber_hp, String mber_nation, String mber_sex, String mber_job) {
//        {   "api_code": "mber_check",   "insures_code": "106", "token": "deviceToken",
//                "app_code": "android19" ,  "mber_nm": "테스트" ,"mber_lifyea": "20140101","mber_hp": "010758421333", "mber_nation": "1"  ,  "mber_sex": "2"   }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_CHECK);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_NM,     mber_nm);
            object.put(CommonData.JSON_MBER_LIFYEA,     mber_lifyea);
            object.put(CommonData.JSON_MBER_HP,     mber_hp);
            object.put(CommonData.JSON_MBER_NATION,     mber_nation);
            object.put(CommonData.JSON_MBER_SEX,     mber_sex);
            object.put(CommonData.JSON_MBER_JOB_YN,     mber_job);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if(RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(MemberCertifiActivity.this, NetworkConst.NET_MBER_CHECK, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }

    /**
     * 회원 체크 ( 자녀 )
     * @param mber_nm       자녀 이름
     * @param mber_lifyea   자녀 생년월일
     * @param mber_chldrn_sn    자녀 고유번호 ( 부모 휴대폰의 자녀정보에 있음 )
     * @param mber_nation   자녀 내국인 & 외국인 ( 1 - 내국인, 2 - 외국인 )
     * @param mber_sex      자녀 성별 ( 1 - 남자 , 2 - 여자 )
     */
    public void requestChldrnInput(String mber_nm, String mber_lifyea, String mber_chldrn_sn, String mber_nation, String mber_sex) {

//        {   "api_code": "mber_chldrn_input",   "insures_code": "106", "token": "APA91bHanHsaue_chJqab7",  "app_code": "android" ,
//                "mber_nm": "홍길동" ,"mber_lifyea": "20140101","mber_hp": "","mber_chldrn_sn": "1000", "mber_nation": "1"  ,
//                "mber_sex": "1"   , "phone_model": "SM-N910S" ,  "pushk": "ET", "app_ver": "0.28"  }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_CHLDRN_INPUT);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_NM,     mber_nm);
            object.put(CommonData.JSON_MBER_LIFYEA,     mber_lifyea);
            object.put(CommonData.JSON_MBER_CHLDRN_SN,  mber_chldrn_sn);

            TelephonyManager telephony = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            String  telPhoneNo = telephony.getLine1Number();    // 전화번호 가져오기

            if(telPhoneNo == null || telPhoneNo.equals("")) {   // usim 정보를 가져올 수 없다면
                object.put(CommonData.JSON_MBER_HP, "");
            }else{
                object.put(CommonData.JSON_MBER_HP, telPhoneNo);
            }
            object.put(CommonData.JSON_MBER_NATION,     mber_nation);
            object.put(CommonData.JSON_MBER_SEX, mber_sex);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if(RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            object.put(CommonData.JSON_PUSH_K,  "0");
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(MemberCertifiActivity.this, NetworkConst.NET_MBER_CHLDRN_INPUT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }



    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.personal_terms_1_tv:  // 개인정보 제공 동의
                intent = new Intent(MemberCertifiActivity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_1_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_1));
                startActivity(intent);
                Util.BackAnimationStart(MemberCertifiActivity.this);
                break;
            case R.id.personal_terms_2_tv:  // 개인민감정보 제공 동의
                intent = new Intent(MemberCertifiActivity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_2_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_2));
                startActivity(intent);
                Util.BackAnimationStart(MemberCertifiActivity.this);
                break;
            case R.id.personal_terms_3_tv:  // 개인정보 제3자 제공 동의
                intent = new Intent(MemberCertifiActivity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.PERSONAL_TERMS_3_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.personal_terms_3));
                startActivity(intent);
                Util.BackAnimationStart(MemberCertifiActivity.this);
                break;
            case R.id.location_terms_tv:  // 위치정보 이용 동의
                intent = new Intent(MemberCertifiActivity.this, BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.LOCATION_TERMS_URL);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.location_terms));
                startActivity(intent);
                Util.BackAnimationStart(MemberCertifiActivity.this);
                break;
            case R.id.certifi_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                mBer_nm = mNameEdit.getText().toString().trim();
                mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
                mBer_hp = mPhoneEdit.getText().toString().trim();
                mBer_nation = mLocalRb.isChecked() ? "1" : "2"; // 1 - 내국인, 2 - 외국인
                if(mBer_nation.equals("1")){
                    mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자
                }else{
                    mBer_sex = mMaleRb.isChecked() ? "5" : "6"; // 5 - 남자, 6 - 여자
                }

                mBer_job = mJobRb.isChecked() ? "Y" : "N"; // Y: 직업있음, N : 직업없음

                if(invaildCerfiti()){   // 인증 가능하다면
                    switch(mLoginType){
                        case CommonData.LOGIN_TYPE_PARENTS: // 부모
                            requestAuthCheck(mBer_nm, mBer_lifyea, mBer_hp, mBer_nation, mBer_sex, mBer_job);
                            break;
                        case CommonData.LOGIN_TYPE_CHILD:   // 자녀
                            requestChldrnInput(mBer_nm, mBer_lifyea, mBer_hp, mBer_nation, mBer_sex);
                            break;
                    }
                }
                break;
            case R.id.name_del_btn: // 이름 삭제
                commonView.setClearEditText(mNameEdit);
                break;
            case R.id.phone_number_del_btn: // 전화번호 삭제
                commonView.setClearEditText(mPhoneEdit);
                break;
            case R.id.birthday_tv:  // 생년월일
                try {
                    if(mCurDate == null){
                        mCurDate = new Date();
                    }
                    mCalendar.setTime(mCurDate);
                    int nNowYear = mCalendar.get(Calendar.YEAR);
                    int nNowMonth = mCalendar.get(Calendar.MONTH);
                    int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurDate = mCalendar.getTime();
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                        mCurrentBirth = format.format(mCurDate);
                        format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                        mBirthTv.setText( format.format(mCurDate));
                        setConfirmBtn(isConfirm());
                    }, nNowYear, nNowMonth , nNowDay);
                    datePickerDialog.setCancelable(false);
                    datePickerDialog.showYearPickerFirst(true);
                    datePickerDialog.show(getFragmentManager(),"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.agree_btn :
                mPersonalCb_1.setChecked(true);
                mPersonalCb_2.setChecked(true);
                mPersonalCb_3.setChecked(true);
                mLocationCb.setChecked(true);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.name_edit:    // 이름
//                commonView.setClearImageBt(mNameDelBtn, hasFocus);
                break;
            case R.id.phone_number_edit:	   // 전화번호
//                commonView.setClearImageBt(mPhoneDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_MBER_CHECK:   // 회원인증 체크
                   switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                               String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 현대해상 상품 가입자
//                                    commonData.setMemberCertifi(true);  // 회원인증 완료 스텝 저장

                                    mBerNm  =   mBer_nm;
                                    mBirthday=  mBer_lifyea;
                                    mBerNation=     Integer.valueOf(mBer_nation);
                                    mGender =   Integer.valueOf(mBer_sex);
                                    mberNo  =   resultData.getString(CommonData.JSON_MBER_NO);
                                    mBerjob = mBer_job;
                                    mBerPhone = mBer_hp;

                                    // 인증 api 호출

                                    String mber_id = resultData.getString(CommonData.JSON_MBER_ID);

                                    if(!mber_id.equals("")){    // 이미 가입한 회원이라면

                                        commonData.setMberId(mber_id);
                                        commonData.setMemberCertifi(false);

                                        mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
                                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                        mDialog.setContent(getString(R.string.popup_dialog_already_join_content));
                                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(CustomAlertDialog dialog, Button button) {
                                                dialog.dismiss();

                                                Intent intent = new Intent(MemberCertifiActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                activityClear();
                                                finish();

                                            }
                                        });
                                        mDialog.show();
                                    }else{                      // 가입한 회원이 아니라면
                                        mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
                                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                        mDialog.setContent(getString(R.string.popup_dialog_contactor_complete));
                                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(CustomAlertDialog dialog, Button button) {
                                                dialog.dismiss();

                                                commonData.setMberNm(mBer_nm);
                                                commonData.setBirthDay(mBer_lifyea);
                                                commonData.setMberNation(mBer_nation);
                                                commonData.setGender(mBer_sex);
                                                commonData.setMberJob(mBer_job);


                                                Intent intent = new Intent(MemberCertifiActivity.this, JoinActivity.class);
                                                intent.putExtra(CommonData.EXTRA_MBERNM, mBerNm);
                                                intent.putExtra(CommonData.EXTRA_BIRTHDAY, mBirthday);
                                                intent.putExtra(CommonData.EXTRA_MBERNATION, mBerNation);
                                                intent.putExtra(CommonData.EXTRA_GENDER, mGender);
                                                intent.putExtra(CommonData.EXTRA_PHONENO, mBerPhone);
                                                intent.putExtra(CommonData.EXTRA_MBERNO, mberNo);
                                                intent.putExtra(CommonData.EXTRA_JOB, mBerjob);
                                                intent.putExtra(CommonData.EXTRA_GLAD, "10");
                                                startActivity(intent);
                                                //activityClear();
                                                //finish();

                                            }
                                        });
                                        mDialog.show();
                                    }

                                }else{  // 현대해상 미가입자
                                    mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_certifi_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();

                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;


                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
