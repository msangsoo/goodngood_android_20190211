package com.appmd.hi.gngcare.diary;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BleManagerCallbacks;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BleUtil;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BluetoothLeService;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.util.JsonLogPrint;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.util.TextWatcherUtil;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 * 성장기록 입력 클래스
 * @since 0, 1
 */
public class GrowthRecordInPut extends BackBaseActivity implements BleManagerCallbacks{
    private final String TAG = GrowthRecordInPut.class.getSimpleName();

    // 성장 정보 입력 쪽 변수
    private LinearLayout mInputLay;
    private TextView mCheckDateEdt;

    private EditText mCheckHeightEd , mCheckWeightEd, mCheckHeadEd;     // 성장 정보 입력 측정 정보 작성시
    private EditText mBirthHeightEd , mBirthWeightEd , mBirthHeadEd;    // 성장 정보 입력 출생 작성시

    // 성장 평가 쪽 변수
    private ScrollView mResult_Lay; // 성장 평가 결과 레이아웃
    private LinearLayout mMove_Lay1 , mMove_Lay2 , mMove_Lay3; // 퍼센티지 레이아웃 부모
    private LinearLayout mPer_Lay1 , mPer_Lay2 , mPer_Lay3; // 그래프 퍼센티지 표시 레이아웃
    private LinearLayout mHeightLay, mWeightLay, mHeadLay; // 결과값 레이아웃
    private TextView mResult_Tv , mHeight_Tv , mWeight_Tv , mHead_Tv; // 출생정보 키 몸무게 머리 둘레 표시 textview
    private TextView mPer_Tv1 , mPer_Tv2 , mPer_Tv3; // 그래프 퍼센티지 표시 textview
    private TextView mResultHeight_Tv , mResultWeight_Tv , mResultHead_Tv; // 키 몸무게 머리둘레 결과물 표시 textview

    private ProgressBar mHeight_Pbar , mWeight_Pbar , mHead_Pbar;

    private String mCheckDate;
    private Date mCurDate;
    private GregorianCalendar mCalendar;
    private CDialog mWeightDialog;

    private boolean mIsBirthInfoData = false;   // 출생정보가 있는지 유무

    private boolean mBeforeHeight = false , mBeforeWeight = false , mBeforeHead = false;

    private boolean mHeadFlag = true;
    private boolean mHeight=false , mWeight=false , mHead = false;

    //  공통
    private ImageButton mConfirmBtn , mSaveBtn; // 확인&저장 버튼
    // 성장 정보 입력 후 성장 평가 결과 화면으로 들어와서 프로그래스바 위에 표시하는 % -> 2초후 사라짐

    //private WeightDeviceScan mWeightDevice;

    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_BLUETOOTH_GET_DATA = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 30 seconds.
    private static final long SCAN_PERIOD = 20000;
    private BluetoothLeService mBluetoothLeService;
    private String mDeviceAddress;
    //private ProgressDialog mProgressDialog;

    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    int mSelectedIndex = 0;

    //ble callback interface
    private BleManagerCallbacks bleManagerCallbacks;


    //ssshin add
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                updateConnectionState(R.string.connected);
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                updateConnectionState(R.string.disconnected);
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                //updateConnectionState(R.string.ble_discovered);
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

            }
        }
    };

    //ssshin add
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                android.util.Log.e("dckim", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.setBLECallback(bleManagerCallbacks);
            mBluetoothLeService.connect(mDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.growth_record_input);

        bleManagerCallbacks = this;

//        String childebirth = BleUtil.getDateSpecialCharacter(GrowthRecordInPut.this, CommonData.STRING_TWOTEN + GrowthMainActivity.mChildMenuItem.get(GrowthMainActivity.mChildChoiceIndex).getmChldrnLifyea(), 4);
        Date imsiDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
        int year = imsiDate.getYear() + 1900;
        int month = imsiDate.getMonth()+1;
        int day = imsiDate.getDate();

        String imsiBirth = "" + year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
        GLog.i("imsiBirth = " +imsiBirth, "dd");

        String childebirth = Util.getDateSpecialCharacter(GrowthRecordInPut.this, imsiBirth, 4);
        long twoDateCompare = Util.getTwoDateCompare(childebirth, CommonData.PATTERN_DATE);
        GLog.i("twoDateCompare  -->   " + twoDateCompare, "dd");
        if (twoDateCompare <= 2400){
            mHeadFlag = false;
        }else {
            mHeadFlag = true;
        }
        GLog.i("111 --> " + MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex), "dd");

        init();
        setEvent();

        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
        mCheckDateEdt.setText(format.format(mCurDate));
        format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
        mCheckDate = format.format(mCurDate);



        mCheckWeightEd.addTextChangedListener(watcher);
        mCheckWeightEd.setTag("U");
        android.util.Log.i(TAG, "onCreate weightTag: " + mCheckWeightEd.getTag().toString());
        /*new TextWatcherUtil().setTextWatcher(mCheckHeightEd, 250, 1);
        new TextWatcherUtil().setTextWatcher(mCheckWeightEd, 250, 2);
        new TextWatcherUtil().setTextWatcher(mCheckHeadEd, 80, 1);

        new TextWatcherUtil().setTextWatcher(mBirthHeightEd, 250, 1);
        new TextWatcherUtil().setTextWatcher(mBirthWeightEd, 250, 2);
        new TextWatcherUtil().setTextWatcher(mBirthHeadEd, 80, 1);*/

        findViewById(R.id.use_weight_device_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deviceAlert().show();
                initBLE();

            }
        });

        findViewById(R.id.use_weight_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = new CustomAlertDialog(GrowthRecordInPut.this, CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent(getString(R.string.howtouseweight));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
            }
        });

        bleConnectionState();

    }

    /**
     * 초기화
     */
    public void init() {


        // 성장 정보 레이아웃
        mInputLay               =       (LinearLayout)  findViewById(R.id.input_lay);

        // 성장 정보 입력 Edit (순서대로 출생 -> 키 , 몸무게 , 머리둘레 / 측정정보 -> 측정일 , 키 ,몸무게 , 머리둘레
        mCheckHeightEd          =       (EditText)  findViewById(R.id.checkheight_ed);
        mCheckWeightEd          =       (EditText)  findViewById(R.id.checkweight_ed);
        mCheckHeadEd            =       (EditText)  findViewById(R.id.checkhead_ed);
        mBirthHeightEd          =       (EditText)  findViewById(R.id.birthheight_ed);
        mBirthWeightEd          =       (EditText)  findViewById(R.id.birthweight_ed);
        mBirthHeadEd            =       (EditText)  findViewById(R.id.birthhead_ed);

        mCheckDateEdt           =       (TextView)      findViewById(R.id.checkdate_edt);
        // 성장 결과 레이아웃
        mResult_Lay             =       (ScrollView)  findViewById(R.id.result_lay);
        // 그래프 퍼센티지 부모 레이아웃
        mMove_Lay1              =       (LinearLayout)  findViewById(R.id.move_lay1);
        mMove_Lay2              =       (LinearLayout)  findViewById(R.id.move_lay2);
        mMove_Lay3              =       (LinearLayout)  findViewById(R.id.move_lay3);
        // 그래프 퍼센티지 표시 레이아웃
        mPer_Lay1               =       (LinearLayout)  findViewById(R.id.per_lay1);
        mPer_Lay2               =       (LinearLayout)  findViewById(R.id.per_lay2);
        mPer_Lay3               =       (LinearLayout)  findViewById(R.id.per_lay3);
        // 결과값 표시 레이아웃
        mHeightLay               =       (LinearLayout)  findViewById(R.id.height_lay);
        mWeightLay               =       (LinearLayout)  findViewById(R.id.weight_lay);
        mHeadLay                 =       (LinearLayout)  findViewById(R.id.head_lay);



        // 성장 결과 Text (순서대로 출생정보 -> 출생정보 키 몸무게 머리둘레 // 키 몸무게 머리둘레 결과물 표시
        mResult_Tv              =       (TextView)      findViewById(R.id.result_tv);
        mHeight_Tv              =       (TextView)      findViewById(R.id.height_tv);
        mWeight_Tv              =       (TextView)      findViewById(R.id.weight_tv);
        mHead_Tv                =       (TextView)      findViewById(R.id.head_tv);
        mResultHeight_Tv        =       (TextView)      findViewById(R.id.resultheight_tv);
        mResultWeight_Tv        =       (TextView)      findViewById(R.id.resultweight_tv);
        mResultHead_Tv          =       (TextView)      findViewById(R.id.resulthead_tv);
        // 그래프 퍼센트 표시 Text
        mPer_Tv1                =       (TextView)      findViewById(R.id.per_tv1);
        mPer_Tv2                =       (TextView)      findViewById(R.id.per_tv2);
        mPer_Tv3                =       (TextView)      findViewById(R.id.per_tv3);
        // 프로그래스 바
        mHeight_Pbar            =       (ProgressBar)   findViewById(R.id.height_pbar);
        mWeight_Pbar            =       (ProgressBar)   findViewById(R.id.weight_pbar);
        mHead_Pbar              =       (ProgressBar)   findViewById(R.id.head_pbar);

        // 공통 확인&저장버튼
        mConfirmBtn             =       (ImageButton)    findViewById(R.id.confirm_btn);
        mSaveBtn                =       (ImageButton)    findViewById(R.id.save_btn);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        setTitle(R.string.title_growth_record);
        findViewById(R.id.confirm_btn).setOnClickListener(btnListener);
        findViewById(R.id.save_btn).setOnClickListener(btnListener);
        findViewById(R.id.checkdate_edt).setOnClickListener(btnListener);

//        mCheckHeightEd.addTextChangedListener(new CustomTextWatcher(this, mCheckHeightEd, null));
//        mCheckWeightEd.addTextChangedListener(new CustomTextWatcher(this, mCheckWeightEd, null));
//        mCheckHeadEd.addTextChangedListener(new CustomTextWatcher(this, mCheckHeadEd, null));

//        mBirthHeightEd.addTextChangedListener(new CustomTextWatcher(this, mBirthHeightEd, null));
//        mBirthWeightEd.addTextChangedListener(new CustomTextWatcher(this, mBirthWeightEd, null));
//        mBirthHeadEd.addTextChangedListener(new CustomTextWatcher(this, mBirthHeadEd, null));

        GLog.i("222 --> " + mHeadFlag, "dd");
        if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO))        // 태아
            mHeadFlag = false;

        //ssshin add 2018.10.30 범위 적용(이상 입력 금지)
        new TextWatcherUtil().setTextWatcher(mCheckHeightEd, 210, 2);
        new TextWatcherUtil().setTextWatcher(mCheckWeightEd, 130, 2);
        new TextWatcherUtil().setTextWatcher(mCheckHeadEd, 80, 2);
        new TextWatcherUtil().setTextWatcher(mBirthHeightEd, 210, 2);
        new TextWatcherUtil().setTextWatcher(mBirthWeightEd, 130, 2);
        new TextWatcherUtil().setTextWatcher(mBirthHeadEd, 80, 2);


        if (!mHeadFlag) {
//            mCheckHeadEd.addTextChangedListener(new CustomTextWatcher(this, mCheckHeadEd, null));
            mCheckHeadEd.setText("");
            mCheckHeadEd.setEnabled(true);
            mCheckHeadEd.setHint(R.string.growthhint);
            mCheckHeadEd.setBackgroundResource(R.drawable.bg_txtbox);
            //mCheckWeightEd.setImeOptions(EditorInfo.IME_ACTION_GO);
        }else {
            mCheckWeightEd.setImeOptions(EditorInfo.IME_ACTION_DONE);
            mCheckHeadEd.setHint(R.string.dont_input_message);
            mCheckHeadEd.setBackgroundResource(R.drawable.bg_txtbox_block);
            //mCheckHeadEd.setText(getString(R.string.head_over));
            mCheckHeadEd.setFocusable(false);
            mCheckHeadEd.setOnClickListener(btnListener);

        }

        GLog.i("MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeadCm() --> ", "dd");

        // 출생정보 입력값이 있는지 유무 체크
        if(!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeight().equals("") &&
                !MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnBdwgh().equals("") &&
                !MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeadCm().equals("")){

            mIsBirthInfoData =   true;   // 출생정보 데이터가 있다

            if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeight().equals(CommonData.STRING_ZERO)) {
                mBirthHeightEd.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeight());
            }else {
                mBirthHeightEd.setText("");
            }
            if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnBdwgh().equals(CommonData.STRING_ZERO)) {
                mBirthWeightEd.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnBdwgh());
            }else {
                mBirthWeightEd.setText("");
            }
            if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeadCm().equals(CommonData.STRING_ZERO)) {
                mBirthHeadEd.setText(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeadCm());
            } else {
                mBirthHeadEd.setText("");
            }



            GLog.i("!!! -- > " + MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeight(), "dd");
            GLog.i("@@@ -- > " + MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnBdwgh(), "dd");
            GLog.i("### -- > " + MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnHeadCm(), "dd");

        }
    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            switch (v.getId()) {
                case R.id.checkhead_ed:
                    Toast.makeText(GrowthRecordInPut.this, getString(R.string.head_over), Toast.LENGTH_LONG).show();
                    break;
                case R.id.confirm_btn:   // 확인
                    GLog.i("--confirm_btn--", "dd");
                    String msg = isConfirm();
                    if(msg.length() > 1){
                        Toast.makeText(GrowthRecordInPut.this, msg, Toast.LENGTH_SHORT).show();
                    }else{
                        requestInPutApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
                    }
                    break;
                case R.id.save_btn:
                    requestInPutSaveApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
                    break;
                case R.id.checkdate_edt:  // 측정날짜
                    try {
                        if(mCurDate == null){
                            mCurDate = new Date();
                        }
                        mCalendar.setTime(mCurDate);
                        int nNowYear = mCalendar.get(Calendar.YEAR);
                        int nNowMonth = mCalendar.get(Calendar.MONTH);
                        int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog alert = new DatePickerDialog(GrowthRecordInPut.this, new DatePickerDialog.OnDateSetListener()
                        {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth)
                            {
                                mCalendar.set(year, monthOfYear, dayOfMonth);
                                Date checkDate = new Date();
                                if(mCalendar.getTime().compareTo(checkDate) >= 0) {    // 오늘 지남
                                    Toast.makeText(GrowthRecordInPut.this, getString(R.string.over_time), Toast.LENGTH_LONG).show();
                                    return;
                                }
                                mCurDate = mCalendar.getTime();
                                SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                                mCheckDate = format.format(mCurDate);
                                format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                                GLog.i("mCheckDate---> " + mCheckDate, "dd");
                                mCheckDateEdt.setText( format.format(mCurDate));
                            }
                        }, nNowYear, nNowMonth , nNowDay);

                        alert.setCancelable(false);

                        alert.show();
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };




    /**
     *  성장정보 입력하기
     */
    public void requestInPutApi(String chl_sn){
        GLog.i("requestAppInfo", "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            // {   "api_code": "chldrn_growth",   "insures_code": "106", "mber_sn": "10035"  ,"chl_sn": "1000",  "bsis_height": "160","bsis_bdwgh": "21","bsis_headcm": "13",
            // "input_height": "110","input_bdwgh": "24","input_headcm": "25","input_de": "20160321" }

            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_SET_GROWTH);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값



            if (mBirthHeightEd.getText().toString().equals("") || mBirthHeightEd.getText().toString().length() ==0){
                object.put(CommonData.JSON_BASIS_HEIGHT, CommonData.STRING_ZERO);           //  기초 키
            }else {
                object.put(CommonData.JSON_BASIS_HEIGHT, Util.checkLastDot(mBirthHeightEd.getText().toString(),"."));           //  기초 키
            }

            if (mBirthWeightEd.getText().toString().equals("") || mBirthWeightEd.getText().toString().length()==0){
                object.put(CommonData.JSON_BASIS_BDWGH, CommonData.STRING_ZERO);           //  기초 몸무게
            }else {
                object.put(CommonData.JSON_BASIS_BDWGH, Util.checkLastDot(mBirthWeightEd.getText().toString(),"."));             //  기초 몸무게
            }

            if (mBirthHeadEd.getText().toString().equals("") || mBirthHeadEd.getText().toString().length()==0  ){
                object.put(CommonData.JSON_BASIS_HEADCM, CommonData.STRING_ZERO);           //  기초 머리둘레
            }else {

                String headround = mBirthHeadEd.getText().toString();
                object.put(CommonData.JSON_BASIS_HEADCM, Util.checkLastDot(headround,"."));            //  기초 머리둘레
            }


            if (mCheckHeightEd.getText().toString().equals("") || mCheckHeightEd.getText().toString().length() ==0){
                object.put(CommonData.JSON_INPUT_HEIGHT, CommonData.STRING_ZERO);          //  측정 키
                mHeight = false;
            }else {
                object.put(CommonData.JSON_INPUT_HEIGHT, Util.checkLastDot(mCheckHeightEd.getText().toString(),"."));          //  측정 키
                mHeight = true;
            }

            if (mCheckWeightEd.getText().toString().equals("") || mCheckWeightEd.getText().toString().length() ==0){
                object.put(CommonData.JSON_INPUT_BDWGH, CommonData.STRING_ZERO);          //  측정 몸무게
                mWeight = false;
            }else {
                object.put(CommonData.JSON_INPUT_BDWGH, Util.checkLastDot(mCheckWeightEd.getText().toString(),"."));            //  측정 몸무게
                mWeight = true;
            }

            if (!mHeadFlag) {
                if (mCheckHeadEd.getText().toString().equals("") || mCheckHeadEd.getText().toString().length()==0){
                    object.put(CommonData.JSON_INPUT_HEADCM, CommonData.STRING_ZERO);          //  측정 머리둘레
                    mHead = false;
                }else {
                    object.put(CommonData.JSON_INPUT_HEADCM, Util.checkLastDot(mCheckHeadEd.getText().toString(),"."));            //  측정 머리둘레
                    mHead = true;
                }
            }else {
                object.put(CommonData.JSON_INPUT_HEADCM, CommonData.STRING_ZERO);           //  측정 머리둘레
            }
            object.put(CommonData.JSON_INPUT_DE, mCheckDate);         //  측정 일 (ex: 20160329)

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));
            JsonLogPrint.printJson(params.toString());


            RequestApi.requestApi(GrowthRecordInPut.this, NetworkConst.NET_RECORD_INPUT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());

        }catch(Exception e){
            GLog.i(e.toString(), "dd");
            e.printStackTrace();
        }
    }

    /**
     *  성장정보 DB 저장하기
     */
    public void requestInPutSaveApi(String chl_sn){
        Logger.i(TAG,"requestAppInfo");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            // {   "api_code": "chldrn_growth_input_ok",   "insures_code": "106", "mber_sn": "10035"  ,"chl_sn": "1001",  "bsis_height": "160.1","bsis_bdwgh": "21.5",
            // "bsis_headcm": "13.3","input_height": "110.1","input_bdwgh": "24.1","input_headcm": "45","input_de": "20160401",, "regtype":"U" }

            JSONObject object = new JSONObject();



            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_SET_GROWTH_INPUT_OK);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값
            object.put(CommonData.JSON_REGTYPE, mCheckWeightEd.getTag().toString()); // 디바이스 정보

            if (mBirthHeightEd.getText().toString().equals("") || mBirthHeightEd.getText().toString().length()==0){
                object.put(CommonData.JSON_BASIS_HEIGHT, CommonData.STRING_ZERO);           //  기초 키
            }else {
                object.put(CommonData.JSON_BASIS_HEIGHT, Util.checkLastDot(mBirthHeightEd.getText().toString(),"."));           //  기초 키
            }

            if (mBirthWeightEd.getText().toString().equals("") || mBirthWeightEd.getText().toString().length()==0){
                object.put(CommonData.JSON_BASIS_BDWGH, CommonData.STRING_ZERO);           //  기초 몸무게
            }else {
                object.put(CommonData.JSON_BASIS_BDWGH, Util.checkLastDot(mBirthWeightEd.getText().toString(),"."));             //  기초 몸무게
            }

            if (mBirthHeadEd.getText().toString().equals("") || mBirthHeadEd.getText().toString().length()==0){
                object.put(CommonData.JSON_BASIS_HEADCM, CommonData.STRING_ZERO);           //  기초 머리둘레
            }else {
                object.put(CommonData.JSON_BASIS_HEADCM, Util.checkLastDot(mBirthHeadEd.getText().toString(),"."));            //  기초 머리둘레
            }

            if (mCheckHeightEd.getText().toString().equals("") || mCheckHeightEd.getText().toString().length()==0){
                object.put(CommonData.JSON_INPUT_HEIGHT, CommonData.STRING_ZERO);          //  측정 키
            }else {
                object.put(CommonData.JSON_INPUT_HEIGHT, Util.checkLastDot(mCheckHeightEd.getText().toString(),"."));          //  측정 키
                mBeforeHeight = true;
            }

            if (mCheckWeightEd.getText().toString().equals("") || mCheckWeightEd.getText().toString().length()==0){
                object.put(CommonData.JSON_INPUT_BDWGH, CommonData.STRING_ZERO);          //  측정 키
            }else {
                object.put(CommonData.JSON_INPUT_BDWGH, Util.checkLastDot(mCheckWeightEd.getText().toString(),"."));            //  측정 몸무게
                mBeforeWeight = true;
            }

            if (!mHeadFlag) {
                if (mCheckHeadEd.getText().toString().equals("") || mCheckHeadEd.getText().toString().length()==0){
                    object.put(CommonData.JSON_INPUT_HEADCM, CommonData.STRING_ZERO);          //  측정 머리둘레
                }else {
                    object.put(CommonData.JSON_INPUT_HEADCM, Util.checkLastDot(mCheckHeadEd.getText().toString(), "."));            //  측정 머리둘레
                    mBeforeHead = true;
                }
            }else {
                object.put(CommonData.JSON_INPUT_HEADCM, CommonData.STRING_ZERO);           //  측정 머리둘레
            }

            object.put(CommonData.JSON_INPUT_DE, mCheckDate);         //  측정 일 (ex: 20160329)

            android.util.Log.i(TAG, "regtype " + mCheckWeightEd.getTag().toString());

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));
            JsonLogPrint.printJson(params.toString());

            RequestApi.requestApi(GrowthRecordInPut.this, NetworkConst.NET_RECORD_INPUT_OK, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());

        }catch(Exception e){
            GLog.i(e.toString(), "dd");
            e.printStackTrace();
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_RECORD_INPUT:

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");

                            try {
                                String result = resultData.getString(CommonData.JSON_DATA_YN);
                                if ("Y".equals(result) == false) {
                                    CustomAlertDialog dlg = new CustomAlertDialog(GrowthRecordInPut.this, CustomAlertDialog.TYPE_A);
                                    dlg.setTitle("알림");
                                    dlg.setContent("조회 결과가 없습니다.");
                                    dlg.setPositiveButton("확  인", null);
                                    dlg.show();
                                    return;
                                }

                                String height="" , weight="", head="", heightper="" , weightper="" , headper="" , heighttxt="", weighttxt="", headtxt="";
                                CommonData commonData = CommonData.getInstance();

                                float fHeight  = 0.0f;
                                float fWeight  = 0.0f;
                                float fHead    = 0.0f;

                                if (mHeight){
                                    height = resultData.getString(CommonData.JSON_HEIGHTCM);
                                    heightper = resultData.getString(CommonData.JSON_HEIGHTPER);
                                    heighttxt = resultData.getString(CommonData.JSON_HEIGHTTXT);
                                    mHeight_Tv.setText(height + getString(R.string.cm));
                                    mResultHeight_Tv.setText(heighttxt);
                                    mPer_Tv1.setText(heightper + commonData.STRING_PERCENT);
                                }
                                if (mWeight){
                                    weight = resultData.getString(CommonData.JSON_MBER_KG);
                                    weightper = resultData.getString(CommonData.JSON_WEIGHTPER);
                                    weighttxt = resultData.getString(CommonData.JSON_WEIGHTTXT);
                                    mWeight_Tv.setText(weight + getString(R.string.kg));
                                    mResultWeight_Tv.setText(weighttxt);
                                    mPer_Tv2.setText(weightper + commonData.STRING_PERCENT);
                                }
                                if (mHead){
                                    head = resultData.getString(CommonData.JSON_HEADCM);
                                    headper = resultData.getString(CommonData.JSON_HEADPER);
                                    headtxt = resultData.getString(CommonData.JSON_HEADTXT);
                                    mHead_Tv.setText(head + getString(R.string.cm));
                                    mResultHead_Tv.setText(headtxt);
                                    mPer_Tv3.setText(headper + commonData.STRING_PERCENT);
                                }

                                if (TextUtils.isEmpty(heightper))   heightper = "0.0f";
                                if (TextUtils.isEmpty(weightper))   weightper = "0.0f";
                                if (TextUtils.isEmpty(headper))     headper = "0.0f";
                                float fHeightper  = Float.parseFloat(heightper);
                                float fWeightper  = Float.parseFloat(weightper);
                                float fHeadper    = Float.parseFloat(headper);

                                if (TextUtils.isEmpty(height))   height = "0.0f";
                                if (TextUtils.isEmpty(weight))   weight = "0.0f";
                                if (TextUtils.isEmpty(head))     head = "0.0f";
                                fHeight  = Float.parseFloat(height);
                                fWeight  = Float.parseFloat(weight);
                                fHead    = Float.parseFloat(head);

                                Growth(fHeightper, fWeightper, fHeadper, fHeight, fWeight, fHead); // 등록 후 리턴 값으로 키, 몸무게, 머리둘레 % 받은 후 다음 화면에서 표시

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:	// 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
                case NetworkConst.NET_RECORD_INPUT_OK:

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");

                            try {

                                if(mBirthHeightEd.getText().length() > 0)
                                    MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).setmChldrnHeight(Util.checkLastDot(mBirthHeightEd.getText().toString(),"."));

                                if(mBirthWeightEd.getText().length() > 0)
                                    MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).setmChldrnBdwgh(Util.checkLastDot(mBirthWeightEd.getText().toString(),"."));

                                if(mBirthHeadEd.getText().length() > 0)
                                    MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).setmChldrnHeadCm(Util.checkLastDot(mBirthHeadEd.getText().toString(),"."));

                                if (mHeight){
                                    MainActivity.mLastHeight = resultData.getString(CommonData.JSON_HEIGHTCM);
                                    MainActivity.mLastCmResult = resultData.getString(CommonData.JSON_HEIGHTPER);
                                }
                                if (mWeight){
                                    MainActivity.mLastHeight = resultData.getString(CommonData.JSON_MBER_KG);
                                    MainActivity.mLastKgResult = resultData.getString(CommonData.JSON_WEIGHTPER);
                                }
                                Intent intent = new Intent();
                                setResult(RESULT_OK, intent);
                                finish();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:	// 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();
            hideProgress();

        }
    };

    public void Growth(float heightper , float weightper , float headper, float height , float weight , float head) {
        GLog.i("heightper-->" + heightper, "dd");
        GLog.i("weightper-->" + weightper, "dd");
        GLog.i("headper-->" + headper, "dd");

        setTitle(R.string.titleresult);
        mInputLay.setVisibility(View.GONE);
        mConfirmBtn.setVisibility(View.GONE);
        mResult_Lay.setVisibility(View.VISIBLE);
        mSaveBtn.setVisibility(View.VISIBLE);

        // 타이틀 문구
        // 생년월일
        try {
            final Date mDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);

            int year = mDate.getYear() + 1900;
            int month = mDate.getMonth() + 1;
            int day = mDate.getDate();
            int age = 0;

            int diffday = Util.GetDifferenceOfDate(StringUtil.getIntVal(mCheckDate.substring(0, 4)), StringUtil.getIntVal(mCheckDate.substring(4,6)), StringUtil.getIntVal(mCheckDate.substring(6, 8)),
                    year, month, day);

            GLog.i("diffDay = " + diffday, "dd");

            mResult_Tv.setText(Util.getAfterBirth(GrowthRecordInPut.this, diffday) + CommonData.STRING_SPACE + getString(R.string.result_title_desc));

        }catch (Exception e){
            GLog.e(e.toString());
            e.printStackTrace();
        }

        if (heightper > 0 || height > 0){
            mHeight_Pbar.setProgress((int)heightper);

            GLog.i("mHeight_Pbar.getWidth()----" + mHeight_Pbar.getWidth(), "dd");
            GLog.i("StringUtil.getIntVal(heightper)----" + heightper, "dd");

            float interval1 = mHeight_Pbar.getWidth() * heightper / 100;
            LinearLayout.LayoutParams layparam1 = (LinearLayout.LayoutParams) mMove_Lay1.getLayoutParams();
            layparam1.leftMargin = (int)interval1;
            mMove_Lay1.setLayoutParams(layparam1);
            mHeightLay.setVisibility(View.VISIBLE);
        }else {
            mHeightLay.setVisibility(View.GONE);
        }

        if (weightper > 0 || weight > 0){
            mWeight_Pbar.setProgress((int)weightper);
            float interval2 = mWeight_Pbar.getWidth() * weightper / 100;
            LinearLayout.LayoutParams layparam2 = (LinearLayout.LayoutParams) mMove_Lay2.getLayoutParams();
            layparam2.leftMargin = (int)interval2;
            mMove_Lay2.setLayoutParams(layparam2);
            mWeightLay.setVisibility(View.VISIBLE);
        }else {
            mWeightLay.setVisibility(View.GONE);
        }

        if (headper > 0 || head > 0){
            mHead_Pbar.setProgress((int)headper);
            float interval3 = mHead_Pbar.getWidth() * headper / 100;
            LinearLayout.LayoutParams layparam3 = (LinearLayout.LayoutParams) mMove_Lay3.getLayoutParams();
            layparam3.leftMargin = (int)interval3;
            mMove_Lay3.setLayoutParams(layparam3);
            mHeadLay.setVisibility(View.VISIBLE);
        }else {
            mHeadLay.setVisibility(View.GONE);
        }

//        mHandler.postDelayed(mTask, 2000);
        GLog.i("mMove_Lay1----" + mMove_Lay1.getWidth(), "dd");

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        mInputLay.setVisibility(View.VISIBLE);
        mConfirmBtn.setVisibility(View.VISIBLE);
        mResult_Lay.setVisibility(View.INVISIBLE);
        mSaveBtn.setVisibility(View.GONE);
        GLog.i("mMove_Lay1----" + mMove_Lay1.getWidth(), "dd");
        GLog.i("mMove_Lay2----" + mMove_Lay2.getWidth(), "dd");
        GLog.i("mMove_Lay3----" + mMove_Lay3.getWidth(), "dd");
    }


    /**
     * * 모든 데이터 기입 했는지 체크
     * @return boolean ( true - 가능, false - 불가 )
     */
    public String isConfirm(){
        int nCnt = 0;
        String message = "";
        try{
            if(mCheckHeightEd.getText().toString().length() <= 0){
                    nCnt++;

            }

            if(mCheckWeightEd.getText().toString().length() <= 0){
                    nCnt++;
            }

            if(mCheckHeadEd.getText().toString().length() <= 0){
                    nCnt++;
            }

        }catch (Exception e){
            e.printStackTrace();

        }

        if(nCnt>=3 ) {
            message = getString(R.string.non_growthdata_new);
        }

        if (mCheckHeightEd.getText().length() > 0) {
            if (!(StringUtil.getFloat(mCheckHeightEd.getText().toString()) >= 20 && StringUtil.getFloat(mCheckHeightEd.getText().toString()) <= 250)) {
                message = "키의 입력가능 범위는 20~250cm 입니다.";
            }
        }
        if (mCheckWeightEd.getText().length() > 0) {
            if (!(StringUtil.getFloat(mCheckWeightEd.getText().toString()) >= 1 && StringUtil.getFloat(mCheckWeightEd.getText().toString()) <= 250)) {
                message = "몸무게 입력가능 범위는 1~250kg 입니다.";
            }
        }
        if (mCheckHeadEd.getText().length() > 0) {
            if (!(StringUtil.getFloat(mCheckHeadEd.getText().toString()) >= 20 && StringUtil.getFloat(mCheckHeadEd.getText().toString()) <= 80)) {
                message = "머리둘레의 입력가능 범위는 20~80cm 입니다.";

            }
        }
        return message;
    }


    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mWeightDialog != null)
                mWeightDialog.dismiss();


        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(mCheckWeightEd.isFocusable()){
                mCheckWeightEd.setTag("U");
            }
        }
    };

    public CDialog deviceAlert() {
        if (mWeightDialog == null) {
            //mCheckWeightEd.setTag("D");
            mWeightDialog = CDialog.showDlg(this, R.layout.alert_weight_device_wait_progress);
            mWeightDialog.setOkButton("취  소", null);
            mWeightDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    scanLeDevice(false);
                }
            });
        }
        return mWeightDialog;
    }

    //ssshin
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }


    @Override
    protected void onResume() {
        super.onResume();


        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        scanLeDevice(false);
        //unregisterReceiver(mGattUpdateReceiver);
    }


    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //ssshin
                //btnBluetoothStart.setText(resourceId);
            }
        });
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);

                    if(deviceAlert() != null){
                        deviceAlert().dismiss();
                    }

                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            //mProgressDialog = ProgressDialog.show(mContext, "", "Wait...", true);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);

            if(deviceAlert() != null){
                deviceAlert().dismiss();
            }
        }
    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (device.getName() != null && !device.getName().equals("") && device.getName().equals("Chipsea-BLE")) {
                                android.util.Log.e("dckim", "device::" + device.getName());
                                mDeviceAddress = device.getAddress();
                                if (mScanning) {

                                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                                    mScanning = false;

                                }

                                if (mBluetoothLeService != null) {
                                    final boolean result = mBluetoothLeService.connect(mDeviceAddress);
                                }
                                //finish();
                            }
                        }
                    });
                }
            };

    private void bleConnectionState(){
        /*if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.location_access_title));
                builder.setMessage(getResources().getString(R.string.location_access_message));
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(23)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });

                builder.show();
            }
        }*/

        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final android.bluetooth.BluetoothManager bluetoothManager =
                (android.bluetooth.BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        /*if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }*/
    }

    private void initBLE(){

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (Build.VERSION.SDK_INT >= 23) {
            // Android M Permission check
            if (checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.location_access_title));
                builder.setMessage(getResources().getString(R.string.location_access_message));
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(23)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });

                builder.show();
            } else {
                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    scanLeDevice(true);
                } else {
                    Toast.makeText(this, R.string.bluetooth_disable, Toast.LENGTH_SHORT);
                }
            }
        } else {
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                scanLeDevice(true);
            } else {
                Toast.makeText(this, R.string.bluetooth_disable, Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(getResources().getString(R.string.location_access_cancel_title));
                    builder.setMessage(getResources().getString(R.string.location_access_cancel_message));
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public void onDeviceConnected() {
        android.util.Log.e("dckim", "onDeviceConnected::");
    }

    @Override
    public void onDeviceDisconnect() {
        android.util.Log.e("dckim", "onDeviceDisconnect::");
    }

    @Override
    public void onDeviceClose() {

        android.util.Log.e("dckim", "onDeviceClose::");
    }

    @Override
    public void onServicesDiscovered(boolean optionalServicesFound) {

        android.util.Log.e("dckim", "onServicesDiscovered::");
    }

    @Override
    public void onDeviceReady() {

        android.util.Log.e("dckim", "onDeviceReady::");
        if (mBluetoothLeService != null) {
            mBluetoothLeService.getWeightValue();
        }
    }

    @Override
    public void onBondingRequired() {

        android.util.Log.e("dckim", "onBondingRequired::");
    }

    @Override
    public void onBonded() {
        android.util.Log.e("dckim", "onBonded::");
    }

    @Override
    public void onError(String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported() {

    }

    @Override
    public void onWeightChangeSuccess() {
        android.util.Log.e("dckim", "onWeightChangeSuccess::");

    }

    @Override
    public void onWeightValueSuccess() {
        android.util.Log.e("dckim", "onWeightValueSuccess::");

    }

    @Override
    public void onBluetoothClose() {

    }

    private void displayData(String data) {
        if (data != null) {
            NumberFormat numberformat = new DecimalFormat("###,###.##");
            float mValue1;
            if (mSelectedIndex == 0) {
                mValue1 = BleUtil.getHexToDec(data);

            } else {
                int mValue = BleUtil.getHexToInt(data);
                mValue *= 11023;
                mValue += 5000;
                mValue /= 10000;
                mValue <<= 1;
                mValue1 = BleUtil.getHexToDec(String.valueOf(mValue));

            }
            String strVal = numberformat.format(mValue1 / 100.0D);
            //tvWeightValue.setText(strVal);
            mCheckWeightEd.setText(strVal);
            mCheckWeightEd.setTag("D"); // 디바이스

            if (deviceAlert() != null) {
                deviceAlert().dismiss();
            }


        }

    }
}
