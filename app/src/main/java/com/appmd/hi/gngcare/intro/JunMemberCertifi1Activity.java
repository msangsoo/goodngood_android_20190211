package com.appmd.hi.gngcare.intro;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.util.GLog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class JunMemberCertifi1Activity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{

    private LinearLayout mNameLayout, mBirthLayout, mPhoneLayout;
    private TextView mPhoneTitleTv;
    private EditText mNameEdit, mPhoneEdit, mBirthTv;
    private ImageButton mNameDelBtn, mPhoneDelBtn;

    private RadioButton mLocalRb, mForeignerRb, mMaleRb, mFemaleRb, mJobRb, mNoJobRb;
    private CheckBox mAfterBirthRb;

    private Button mNextBtn;

    private String mCurrentBirth =   "";
    Calendar cal = Calendar.getInstance();

    private String mBer_nm, mBer_lifyea, mBer_hp, mBer_nation, mBer_sex, mBer_job, mBer_afterbirth;    // api 호출시 저장변수

    private Intent intent = null;
    private int mLoginType = 1;

    String mberNo = "";
    String mBerNm = "";
    String mBirthday = "";
    String mBerjob ="Y";
    int mBerNation = 1;
    int mGender = 1;
    int mAfterBirth = 1;

    private ImageButton mBackimg;


    private Date mCurDate;
    GregorianCalendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.junmember_certifi1_activity);

        setTitle(getString(R.string.junmember_certifi));

        init();
        setEvent();

        mBackimg = findViewById(R.id.common_left_btn);
        mBackimg.setVisibility(View.VISIBLE);

        mBackimg.setOnClickListener(v -> finish());


        mNextBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if(intent != null){
            mLoginType  =   intent.getIntExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);

        }else{
            GLog.i("intent = null", "dd");
        }

        mPhoneTitleTv.setText(getString(R.string.mobile_phone_number));
        mPhoneEdit.setHint(getString(R.string.phone_number_hint));


        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
    }

    /**
     * 초기화
     */
    public void init(){

        mNameLayout     =   (LinearLayout)      findViewById(R.id.name_layout);
        mBirthLayout    =   (LinearLayout)      findViewById(R.id.birthday_layout);
        mPhoneLayout    =   (LinearLayout)      findViewById(R.id.phone_number_layout);

        mBirthTv        =   (EditText)          findViewById(R.id.birthday_tv);
        mPhoneTitleTv   =   (TextView)          findViewById(R.id.phone_number_title_tv);

        mNameEdit       =   (EditText)          findViewById(R.id.name_edit);
        mPhoneEdit      =   (EditText)          findViewById(R.id.phone_number_edit);

        mNameDelBtn     =   (ImageButton)       findViewById(R.id.name_del_btn);
        mPhoneDelBtn    =   (ImageButton)       findViewById(R.id.phone_number_del_btn);

        mLocalRb        =   (RadioButton)       findViewById(R.id.local_btn);
        mForeignerRb    =   (RadioButton)       findViewById(R.id.foreigner_btn);
        mMaleRb         =   (RadioButton)       findViewById(R.id.male_btn);
        mFemaleRb       =   (RadioButton)       findViewById(R.id.female_btn);
        mJobRb         =   (RadioButton)       findViewById(R.id.job_btn);
        mNoJobRb       =   (RadioButton)       findViewById(R.id.no_job_btn);
        mAfterBirthRb  =   (CheckBox)       findViewById(R.id.after_birth);


        mNextBtn     =   (Button)            findViewById(R.id.next_btn);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
        mNameDelBtn.setOnClickListener(this);
        mPhoneDelBtn.setOnClickListener(this);

        mNameEdit.setOnFocusChangeListener(this);
        mPhoneEdit.setOnFocusChangeListener(this);

//        mNameEdit.addTextChangedListener(new MyTextWatcher());
//        mPhoneEdit.addTextChangedListener(new MyTextWatcher());
//        mBirthTv.addTextChangedListener(new MyTextWatcher());

    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("")){    // 이름 입력이 없다면
            mNameEdit.requestFocus();
            mDialog =   new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_name_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(StringUtil.isSpecialWordNum(mNameEdit.getText().toString().trim()) == false){
            mNameEdit.requestFocus();
            mDialog =   new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_name_error_1));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        String birth = mBirthTv.getText().toString();

        if(birth.trim().length() != 8){
            mBirthTv.requestFocus();
            mDialog =   new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(StringUtil.getIntVal(birth) > StringUtil.getIntVal(CDateUtil.getToday_yyyy_MM_dd())){
            mBirthTv.requestFocus();
            mDialog = new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent("생일은 미래일 수 없습니다.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!CDateUtil.getDateFormatBool(birth)){
            mBirthTv.requestFocus();
            mDialog = new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent("날짜형식이 아닙니다.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }



        /*if(mCurrentBirth.equals("")){   // 생년월일
            mDialog =   new CustomAlertDialog(MemberCertifiActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }*/

        if(mPhoneEdit.getText().toString().trim().equals("")){   // 휴대폰 번호
            mPhoneEdit.requestFocus();
            mDialog =   new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_phone_number_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(mPhoneEdit.getText().toString().trim().length() < 10){
            mPhoneEdit.requestFocus();
            mDialog =   new CustomAlertDialog(JunMemberCertifi1Activity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_phone_number_error_1));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }



        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2){    // 이름 입력이 없다면
            bool = false;
            return bool;
        }

        if(mBirthTv.getText().toString().trim().equals("") || mBirthTv.getText().toString().trim().length() < 8){
            bool = false;
            return bool;
        }

        /*if(mCurrentBirth.equals("")){   // 생년월일
            bool = false;
            return bool;
        }*/

        if(mPhoneEdit.getText().toString().trim().equals("") || mPhoneEdit.getText().toString().trim().length() < 10){   // 휴대폰 번호
            bool = false;
            return bool;
        }


        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

//        if(bool) {
            mNextBtn.setEnabled(true);

            mNextBtn.setBackgroundResource(R.drawable.btn_5_22396c);
            mNextBtn.setTextColor(ContextCompat.getColorStateList(JunMemberCertifi1Activity.this, R.color.color_ffffff_btn));
//        }else{
//            mNextBtn.setEnabled(false);
//
//            mNextBtn.setBackgroundResource(R.drawable.background_5_e6e7e8_sq);
//            mNextBtn.setTextColor(ContextCompat.getColorStateList(JunMemberCertifi1Activity.this, R.color.color_ffffff_btn));
//        }

    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.next_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                mBer_nm = mNameEdit.getText().toString().trim();
                mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
                mBer_hp = mPhoneEdit.getText().toString().trim();
                mBer_nation = mLocalRb.isChecked() ? "1" : "2"; // 1 - 내국인, 2 - 외국인
                if(mBer_nation.equals("1")){
                    mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자
                }else{
                    mBer_sex = mMaleRb.isChecked() ? "5" : "6"; // 5 - 남자, 6 - 여자
                }

                mBer_job = mJobRb.isChecked() ? "Y" : "N"; // Y: 직업있음, N : 직업없음
                mBer_afterbirth = mAfterBirthRb.isChecked() ? "2" : "1"; // 1:출산 후, 2: 출산 전


                if(invaildCerfiti()){   // 인증 가능하다면
                    intent = new Intent(JunMemberCertifi1Activity.this, JunMemberCertifi2Activity.class);
                    intent.putExtra(CommonData.EXTRA_MBERNM, mBer_nm);
                    intent.putExtra(CommonData.EXTRA_BIRTHDAY, mBer_lifyea);
                    intent.putExtra(CommonData.EXTRA_MBERNATION, mBer_nation);
                    intent.putExtra(CommonData.EXTRA_GENDER, mBer_sex);
                    intent.putExtra(CommonData.EXTRA_PHONENO, mBer_hp);
                    intent.putExtra(CommonData.EXTRA_JOB, mBer_job);
                    intent.putExtra(CommonData.EXTRA_AFTERBIRTH, mBer_afterbirth);
                    startActivity(intent);
                }
                break;
            case R.id.name_del_btn: // 이름 삭제
                commonView.setClearEditText(mNameEdit);
                break;
            case R.id.phone_number_del_btn: // 전화번호 삭제
                commonView.setClearEditText(mPhoneEdit);
                break;
            case R.id.birthday_tv:  // 생년월일
                try {
                    if(mCurDate == null){
                        mCurDate = new Date();
                    }
                    mCalendar.setTime(mCurDate);
                    int nNowYear = mCalendar.get(Calendar.YEAR);
                    int nNowMonth = mCalendar.get(Calendar.MONTH);
                    int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurDate = mCalendar.getTime();
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                        mCurrentBirth = format.format(mCurDate);
                        format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                        mBirthTv.setText( format.format(mCurDate));
                        setConfirmBtn(isConfirm());
                    }, nNowYear, nNowMonth , nNowDay);
                    datePickerDialog.setCancelable(false);
                    datePickerDialog.showYearPickerFirst(true);
                    datePickerDialog.show(getFragmentManager(),"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.name_edit:    // 이름
//                commonView.setClearImageBt(mNameDelBtn, hasFocus);
                break;
            case R.id.phone_number_edit:	   // 전화번호
//                commonView.setClearImageBt(mPhoneDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
