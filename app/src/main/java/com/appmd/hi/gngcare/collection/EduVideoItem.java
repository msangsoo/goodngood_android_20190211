package com.appmd.hi.gngcare.collection;

/**
 * Created by MobileDoctor on 2017-06-07.
 */

public class EduVideoItem {

    private String mMlSn;
    private String mImg;
    private String mIUrl;
    private String mTit;
    private String mTotpg;


    public EduVideoItem(){}

    public EduVideoItem(String mMlSn, String mImg, String mIUrl, String mTit, String mTotpg){
        this.mMlSn = mMlSn;
        this.mImg = mImg;
        this.mIUrl = mIUrl;
        this.mTit = mTit;
        this.mTotpg = mTotpg;

    }


    public String getmMlSn(){
        return mMlSn;
    }
    public void setmMlSn(String mMlSn){
        this.mMlSn = mMlSn;
    }

    public String getmImg(){
        return mImg;
    }
    public void setmImg(String mImg){
        this.mImg = mImg;
    }

    public String getmIUrl( ){
        return mIUrl;
    }
    public void set(String mIUrl){
        this.mIUrl = mIUrl;
    }

    public String getmTit(){
        return mTit;
    }
    public void setmTit(String mTit){
        this.mTit = mTit;
    }

    public String getmTotpg(){
        return mTotpg;
    }
    public void setmTotpg(String mTotpg){
        this.mTotpg = mTotpg;
    }

}
