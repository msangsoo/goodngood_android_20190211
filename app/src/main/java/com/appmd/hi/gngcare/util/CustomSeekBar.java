package com.appmd.hi.gngcare.util;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.ProgressItem;

@SuppressLint("AppCompatCustomView")
public class CustomSeekBar extends SeekBar {

    private ArrayList<ProgressItem> mProgressItemsList;
    private float goalBmi;
    private int goalWt;
    private float curBmi;
    private int curInt;
    private int type = 0;
    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initData(ArrayList<ProgressItem> progressItemsList, float curBmi, float goalBmi) {
        this.goalBmi = goalBmi;
        this.mProgressItemsList = progressItemsList;
        this.curBmi = curBmi;
        this.type = 0;
    }

    public void initData(ArrayList<ProgressItem> progressItemsList, int curInt, int goalWt, boolean flag) {
        this.mProgressItemsList = progressItemsList;
        this.goalWt = goalWt;
        this.curInt = curInt;
        this.type = 1;
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec,
                                          int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onDraw(Canvas canvas) {
        if(mProgressItemsList == null)
            return;

        if (mProgressItemsList.size() > 0) {

            if(type == 0) {

                int progressBarWidth = getWidth();
                int progressBarHeight = getHeight();
                int thumboffset = getThumbOffset();
                int lastProgressX = 0;
                int progressItemWidth, progressItemRight;

                int prevWidth = 0;

                int titleAreaWidth = progressBarWidth / (mProgressItemsList.size() - 1);

                int titleAreaWidthCnt = 0;
                for (int i = 0; i < mProgressItemsList.size(); i++) {
                    ProgressItem progressItem = mProgressItemsList.get(i);

                    Paint progressPaint = new Paint();
                    progressPaint.setColor(getResources().getColor(
                            progressItem.color));

                    Paint textPaint = new Paint();
                    //   textPaint.setARGB(255, 255, 255, 255);
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(dpToPx(12));
                    textPaint.setColor(getResources().getColor(R.color.color_646464));
                    textPaint.setTypeface(Typeface.DEFAULT_BOLD);

                    progressItemWidth = (int) (progressItem.progressItemPercentage
                            * progressBarWidth / 100);

                    progressItemRight = lastProgressX + progressItemWidth;

                    // for last item give right to progress item to the width
                    if (i == mProgressItemsList.size() - 1
                            && progressItemRight != progressBarWidth) {
                        progressItemRight = progressBarWidth;
                    }
                    Rect progressRect = new Rect();

                    progressRect.set(lastProgressX, thumboffset / 2,
                            progressItemRight, progressBarHeight - thumboffset / 2);
                    canvas.drawRect(progressRect, progressPaint);
                    lastProgressX = progressItemRight;

                    if (lastProgressX < 0)
                        lastProgressX = dpToPx(1);

                    if (lastProgressX >= progressBarWidth)
                        lastProgressX = progressBarWidth - dpToPx(1);
                    canvas.drawText(progressItem.pointStr, lastProgressX, dpToPx(22), textPaint);

                    Paint bottomPaint = new Paint();
                    bottomPaint.setTextAlign(Paint.Align.CENTER);
                    bottomPaint.setTextSize(dpToPx(12));
                    bottomPaint.setColor(getResources().getColor(R.color.color_646464));
                    bottomPaint.setTypeface(Typeface.DEFAULT_BOLD);

                    if (lastProgressX != progressBarWidth)
                        canvas.drawText(progressItem.bottomStr, lastProgressX, dpToPx(70), bottomPaint);

                    Paint paint = new Paint();
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(getResources().getColor(progressItem.color));

                    if (i != 1) {
                        int titleAreaX = titleAreaWidth * titleAreaWidthCnt;
                        canvas.drawRect(titleAreaX, dpToPx(77), titleAreaX + dpToPx(13), dpToPx(90), paint);
                        Paint titlePaint = new Paint();
                        titlePaint.setTextAlign(Paint.Align.LEFT);
                        titlePaint.setTextSize(dpToPx(11));
                        titlePaint.setColor(getResources().getColor(R.color.color_646464));
                        titlePaint.setTypeface(Typeface.DEFAULT_BOLD);
                        canvas.drawText(progressItem.descStr, titleAreaX + dpToPx(20), dpToPx(88), titlePaint);
                        titleAreaWidthCnt++;
                    } else {
                        Paint descPaint = new Paint();
                        //   textPaint.setARGB(255, 255, 255, 255);
                        descPaint.setTextAlign(Paint.Align.CENTER);
                        descPaint.setTextSize(dpToPx(11));
                        descPaint.setColor(getResources().getColor(R.color.mdtp_white));
                        descPaint.setTypeface(Typeface.DEFAULT_BOLD);
                        int x = ((lastProgressX - prevWidth) / 2) + prevWidth;
                        if (lastProgressX != progressBarWidth)
                            canvas.drawText(progressItem.descStr, x, dpToPx(49), descPaint);
                    }
                    prevWidth = lastProgressX;
                }

//                Paint descPaint = new Paint();
//                descPaint.setTextAlign(Paint.Align.CENTER);
//                descPaint.setTextSize(dpToPx(11));
//                descPaint.setColor(getResources().getColor(R.color.mdtp_white));
//                descPaint.setTypeface(Typeface.DEFAULT_BOLD);

            /* kg */
                Paint kgPaint = new Paint();
                kgPaint.setTextAlign(Paint.Align.LEFT);
                kgPaint.setTextSize(dpToPx(12));
                kgPaint.setColor(getResources().getColor(R.color.color_646464));
                kgPaint.setTypeface(Typeface.DEFAULT_BOLD);
                canvas.drawText("kg", 0, dpToPx(70), kgPaint);


             /* bmi */
                Paint bmiPaint = new Paint();
                bmiPaint.setTextAlign(Paint.Align.LEFT);
                bmiPaint.setTextSize(35);
                bmiPaint.setColor(getResources().getColor(R.color.color_646464));
                bmiPaint.setTypeface(Typeface.DEFAULT_BOLD);
                canvas.drawText("BMI", 0, dpToPx(22), bmiPaint);


                Bitmap bitmap1 = BitmapFactory.decodeResource(
                        getResources(),
                        R.drawable.goal_thumb
                );

                if(goalBmi < 0)
                    goalBmi = dpToPx(1);

                if((goalBmi * progressBarWidth / 100) >= progressBarWidth){
                    canvas.drawBitmap(
                            bitmap1,
                            progressBarWidth - dpToPx(1),
                            dpToPx(25),
                            null
                    );

                }else{
                    canvas.drawBitmap(
                            bitmap1,
                            (int) (goalBmi
                                    * progressBarWidth / 100),
                            dpToPx(25),
                            null
                    );
                }


                Bitmap bitmap2 = BitmapFactory.decodeResource(
                        getResources(),
                        R.drawable.cur_thumb
                );

                if(curBmi < 0)
                    curBmi = dpToPx(1);

                if((curBmi * progressBarWidth / 100) >= progressBarWidth){
                    canvas.drawBitmap(
                            bitmap2,
                            progressBarWidth - dpToPx(1),
                            dpToPx(25),
                            null
                    );

                }else{
                    canvas.drawBitmap(
                            bitmap2,
                            (int) (curBmi
                                    * progressBarWidth / 100),
                            dpToPx(25),
                            null
                    );
                }





                super.onDraw(canvas);


            }else {

                int progressBarWidth = getWidth();
                int progressBarHeight = getHeight();
                int thumboffset = getThumbOffset();
                int lastProgressX = 0;
                int progressItemWidth, progressItemRight;

                int prevWidth = 0;

                int titleAreaWidth = progressBarWidth / (mProgressItemsList.size() - 1);

                int titleAreaWidthCnt = 0;
                for (int i = 0; i < mProgressItemsList.size(); i++) {
                    ProgressItem progressItem = mProgressItemsList.get(i);

                    Paint progressPaint = new Paint();
                    progressPaint.setColor(getResources().getColor(
                            progressItem.color));

                    Paint textPaint = new Paint();
                    //   textPaint.setARGB(255, 255, 255, 255);
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(dpToPx(12));
                    textPaint.setColor(getResources().getColor(R.color.color_646464));
                    textPaint.setTypeface(Typeface.DEFAULT_BOLD);

                    progressItemWidth = (int) (progressItem.progressItemPercentage
                            * progressBarWidth / 100);

                    progressItemRight = lastProgressX + progressItemWidth;

                    // for last item give right to progress item to the width
                    if (i == mProgressItemsList.size() - 1
                            && progressItemRight != progressBarWidth) {
                        progressItemRight = progressBarWidth;
                    }
                    Rect progressRect = new Rect();

                    progressRect.set(lastProgressX, thumboffset / 2,
                            progressItemRight, progressBarHeight - thumboffset / 2);
                    canvas.drawRect(progressRect, progressPaint);
                    lastProgressX = progressItemRight;

                    if (lastProgressX < 0)
                        lastProgressX = dpToPx(1);
                    if (lastProgressX >= progressBarWidth)
                        lastProgressX = progressBarWidth - dpToPx(1);
                    canvas.drawText(progressItem.pointStr, lastProgressX, dpToPx(22), textPaint);

                    Paint bottomPaint = new Paint();
                    //   textPaint.setARGB(255, 255, 255, 255);
                    bottomPaint.setTextAlign(Paint.Align.CENTER);
                    bottomPaint.setTextSize(dpToPx(12));
                    bottomPaint.setColor(getResources().getColor(R.color.color_646464));
                    bottomPaint.setTypeface(Typeface.DEFAULT_BOLD);

                    if (lastProgressX != progressBarWidth)
                        canvas.drawText(progressItem.bottomStr, lastProgressX, dpToPx(70), bottomPaint);

                    Paint paint = new Paint();
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(getResources().getColor(progressItem.color));

                    Paint descPaint = new Paint();

                    descPaint.setTextAlign(Paint.Align.CENTER);
                    descPaint.setTextSize(dpToPx(11));
                    descPaint.setColor(getResources().getColor(R.color.mdtp_white));
                    descPaint.setTypeface(Typeface.DEFAULT_BOLD);
                    int x = ((lastProgressX - prevWidth) / 2) + prevWidth;
                    canvas.drawText(progressItem.descStr, x, dpToPx(49), descPaint);

                    prevWidth = lastProgressX;
                }

//                Paint descPaint = new Paint();
//                descPaint.setTextAlign(Paint.Align.CENTER);
//                descPaint.setTextSize(dpToPx(11));
//                descPaint.setColor(getResources().getColor(R.color.mdtp_white));
//                descPaint.setTypeface(Typeface.DEFAULT_BOLD);

            /* kg */
                Paint kgPaint = new Paint();
                kgPaint.setTextAlign(Paint.Align.LEFT);
                kgPaint.setTextSize(dpToPx(12));
                kgPaint.setColor(getResources().getColor(R.color.color_646464));
                kgPaint.setTypeface(Typeface.DEFAULT_BOLD);
                canvas.drawText("kg", 0, dpToPx(70), kgPaint);



//                Bitmap bitmap1 = BitmapFactory.decodeResource(
//                        getResources(),
//                        R.drawable.goal_thumb
//                );
//
//
//                if(goalWt < 0)
//                    goalWt = dpToPx(1);
//
//                if(goalWt >= progressBarWidth)
//                    goalWt = progressBarWidth - dpToPx(1);
//
//                canvas.drawBitmap(
//                        bitmap1,
//                        (int) (goalWt
//                                * progressBarWidth / 100),
//                        dpToPx(25),
//                        null
//                );


                Bitmap bitmap2 = BitmapFactory.decodeResource(
                        getResources(),
                        R.drawable.cur_thumb
                );

                if(curInt < 0)
                    curInt = dpToPx(1);

                if(curInt < 0)
                    curInt = dpToPx(1);

                if((curInt * progressBarWidth / 100) >= progressBarWidth){
                    canvas.drawBitmap(
                            bitmap2,
                            progressBarWidth - dpToPx(1),
                            dpToPx(25),
                            null
                    );

                }else{
                    canvas.drawBitmap(
                            bitmap2,
                            (int) (curInt
                                    * progressBarWidth / 100),
                            dpToPx(25),
                            null
                    );
                }



                super.onDraw(canvas);

            }

        }

    }

    public int dpToPx(float dp){
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
        return px;
    }


}
