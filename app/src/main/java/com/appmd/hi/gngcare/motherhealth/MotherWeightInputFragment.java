package com.appmd.hi.gngcare.motherhealth;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BleManagerCallbacks;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BleUtil;
import com.appmd.hi.gngcare.greencare.bluetooth.device.BluetoothLeService;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.BluetoothManager;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.component.CDatePicker;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_hedctdata;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.util.TextWatcherUtil;
import com.appmd.hi.gngcare.util.Util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class MotherWeightInputFragment extends BaseFragment implements BleManagerCallbacks{
    private static final String TAG = MotherWeightInputFragment.class.getSimpleName();
    //private WeightDeviceScan mWeightDevice;
    private EditText weightEt;
    private ImageView useweightbtn;
    private CustomAlertDialog mDialog;
    private CDialog mWeightDialog;
    private int cal_year;
    private int cal_month;
    private int cal_day;
    private int cal_hour;
    private int cal_min;
    private TextView mDateTv, mTimeTv;

    //ssshin add
    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_BLUETOOTH_GET_DATA = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 20000;
    private BluetoothLeService mBluetoothLeService;
    private String mDeviceAddress;
    //private ProgressDialog mProgressDialog;

    private Context mContext;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    int mSelectedIndex = 0;

    //ble callback interface
    private BleManagerCallbacks bleManagerCallbacks;

    //ssshin add
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                updateConnectionState(R.string.connected);
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                updateConnectionState(R.string.disconnected);
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

            }
        }
    };

    //ssshin add
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e("dckim", "Unable to initialize Bluetooth");
                getActivity().finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.setBLECallback(bleManagerCallbacks);
            mBluetoothLeService.connect(mDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private ImageButton saveBtn;

    public static Fragment newInstance() {
        MotherWeightInputFragment fragment = new MotherWeightInputFragment();
        return fragment;
    }

    private void setActionBar() {
        // CommonActionBar actionBar 는 안 씀 한화꺼
        if (getActivity() instanceof DummyActivity) {
            DummyActivity activity = (DummyActivity) getActivity();

            TextView titleTv = (TextView)activity.findViewById(R.id.common_title_tv);
            titleTv.setText(getString(R.string.mother_health_wt_mesure1));

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_mother_weight_input, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bleManagerCallbacks = this;

        //ssshin
        mContext = getActivity();

        setActionBar();



        mDateTv = (TextView) view.findViewById(R.id.weight_input_date_textview);
        mTimeTv = (TextView) view.findViewById(R.id.weight_input_time_textview);

        String now_time = CDateUtil.getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
        java.util.Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(now_time);
        cal_year = cal.get(Calendar.YEAR);
        cal_month = cal.get(Calendar.MONTH);
        cal_day = cal.get(Calendar.DAY_OF_MONTH);
        cal_hour = cal.get(Calendar.HOUR_OF_DAY);
        cal_min = cal.get(Calendar.MINUTE);

        mDateTvSet(cal_year, cal_month, cal_day);
        mTimeTvSet(cal_hour, cal_min);

        mDateTv.setOnTouchListener(mTouchListener);
        mTimeTv.setOnTouchListener(mTouchListener);

        mDateTv.addTextChangedListener(watcher);
        mTimeTv.addTextChangedListener(watcher);



        useweightbtn = (ImageView) view.findViewById(R.id.use_weight_btn);

        weightEt = (EditText) view.findViewById(R.id.mom_weight_et);
        weightEt.setTag("U");
        weightEt.setText(CommonData.getInstance().getMotherWeight());
        new TextWatcherUtil().setTextWatcher(weightEt, 130, 2);
        weightEt.addTextChangedListener(watcher);
        view.findViewById(R.id.use_weight_device_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deviceAlert().show();
                initBLE();
            }
        });

        saveBtn = view.findViewById(R.id.show_result_button);

        view.findViewById(R.id.show_result_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                beforeRegistWeight();
            }
        });

        useweightbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent(getString(R.string.howtouseweight));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
            }
        });


        bleConnectionState();

        saveBtn.setEnabled(true);

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mWeightDialog != null)
                mWeightDialog.dismiss();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(weightEt.isFocusable()){
                weightEt.setTag("U");
            }
        }
    };

    public CDialog deviceAlert() {
        if (mWeightDialog == null) {
            mWeightDialog = CDialog.showDlg(getContext(), R.layout.alert_weight_device_wait_progress);
            mWeightDialog.setOkButton("취  소", null);
            mWeightDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    scanLeDevice(false);
                }
            });
        }
        return mWeightDialog;
    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int vId = v.getId();
                if (vId == R.id.weight_input_date_textview) {
                    showDatePicker(v);
                } else if (vId == R.id.weight_input_time_textview) {
                    showTimePicker();
                }
            }
            return false;
        }
    };

    private boolean DateTimeCheck(String type, int pram1, int pram2, int pram3){
        Calendar cal = Calendar.getInstance();

        if(type.equals("D")){
            cal.set(Calendar.YEAR, pram1);
            cal.set(Calendar.MONTH, pram2);
            cal.set(Calendar.DAY_OF_MONTH, pram3);
            cal.set(Calendar.HOUR_OF_DAY, cal_hour);
            cal.set(Calendar.MINUTE, cal_min);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });
                return false;
            }else{
                return true;
            }
        }else{
            cal.set(Calendar.YEAR, cal_year);
            cal.set(Calendar.MONTH, cal_month);
            cal.set(Calendar.DAY_OF_MONTH, cal_day);
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });

                return false;
            }else{
                return true;
            }
        }
    }

    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(date) == false) {
            year = StringUtil.getIntVal(date.substring(0 , 4));
            month = StringUtil.getIntVal(date.substring(4 , 6))-1;
            day = StringUtil.getIntVal(date.substring(6 , 8));
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day, false).show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(DateTimeCheck("D",year, monthOfYear, dayOfMonth)) {
                cal_year = year;
                cal_month = monthOfYear;
                cal_day = dayOfMonth;
                mDateTvSet(year, monthOfYear, dayOfMonth);
            }
        }

    };

    private void mDateTvSet(int year, int monthOfYear, int dayOfMonth){
        String msg = String.format("%d.%d.%d", year, monthOfYear + 1, dayOfMonth);
        String tagMsg = String.format("%d%02d%02d", year, monthOfYear + 1, dayOfMonth);
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear + 1);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        mDateTv.setText(msg+" "+ CDateUtil.getDateToWeek(tagMsg)+"요일");
        mDateTv.setTag(tagMsg);
    }

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String time = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2 , 4));

            Logger.i(TAG, "hour="+hour+", minute="+minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();
    }


    /**
     * 시간 피커 완료
     */
    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(DateTimeCheck("S",hourOfDay, minute, 0)) {
                cal_hour = hourOfDay;
                cal_min = minute;
                mTimeTvSet(hourOfDay, minute);
            }
        }
    };

    private void mTimeTvSet(int hourOfDay, int minute){
        // 설정버튼 눌렀을 때
        String amPm = getString(R.string.text_morning);
        int hour = hourOfDay;
        if (hourOfDay > 11) {
            amPm = getString(R.string.text_afternoon);
            if (hourOfDay >= 13)
                hour -= 12;
        } else {
            hour = hour == 0 ? 12 : hour;
        }
        String tagMsg = String.format("%02d%02d", hourOfDay, minute);
        String timeStr = String.format("%02d:%02d", hour, minute);
        mTimeTv.setText(amPm + " " + timeStr);
        mTimeTv.setTag(tagMsg);
    }

    private void beforeRegistWeight() {

        String weight = weightEt.getText().toString();

        if (TextUtils.isEmpty(weight)) {
            mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.noti));
            mDialog.setContent("몸무게를 입력해 주세요.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            //CDialog.showDlg(getContext(), "몸무게를 입력해 주세요.");
            return;
        }

        if (TextUtils.isEmpty(weight) == false) {
//            String bmitext = String.valueOf(Math.round((Float.parseFloat(weight) / (((Float.parseFloat(tall)) * (Float.parseFloat(tall))) / 10000)) * 10d) / 10d);
//            SparseArray<WeightModel> model = makeWeightModel(weight);

            if (StringUtil.getIntVal(weight) > 0) {
                String date = mDateTv.getTag().toString();
                String time = mTimeTv.getTag().toString();
                saveBtn.setEnabled(false);
                uploadWeight(Util.checkLastDot(weight, "."), date, time);
            } else{
                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent("체중 정보가 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
            }
                //Toast.makeText(getContext(), "체중 정보가 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 체중 데이터 입력
     *
     */
    public void uploadWeight(String weight, String regDate , String timeStr) {
        showProgress();

        regDate += timeStr;
        CommonData commonData = CommonData.getInstance();

        Tr_get_hedctdata.DataList data = new Tr_get_hedctdata.DataList();
        data.bmr = "0";
        data.bodywater = "0"     ;
        data.bone = "0"          ;
        data.fat = "0"           ;
        data.heartrate = "0"     ;
        data.muscle = "0"        ;
        data.obesity = "0"       ;
        data.weight = "" + weight;
        data.bdwgh_goal = "" + commonData.getMotherGoalWeight();//weightModel.getBdwgh_goal();


        data.idx = CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis()));
        data.regtype = weightEt.getTag().toString();
        data.reg_de = regDate;

        List<Tr_get_hedctdata.DataList> datas = new ArrayList<>();
        datas.add(data);

        new DeviceDataUtil().uploadWeight(this, datas, new BluetoothManager.IBluetoothResult() {
            @Override
            public void onResult(boolean isSuccess) {
                Logger.i(TAG, "DeviceDataUtil().uploadWeight.weight="+weight);
                hideProgress();

                commonData.setMotherWeight(weight);

                String msg = isSuccess ? "저장되었습니다." : "저장에 실패 했습니다.";

                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent(msg);
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                    @Override
                    public void onClick(CustomAlertDialog dialog, Button button) {
                        saveBtn.setEnabled(true);
                        dialog.dismiss();
                        onBackPressed();
                    }
                });
                mDialog.show();
            }
        });

    }

    //ssshin
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.

        /*if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }*/
        getActivity().registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());


    }

    @Override
    public void onPause() {
        super.onPause();
        scanLeDevice(false);
        getActivity().unregisterReceiver(mGattUpdateReceiver);
    }

    private void updateConnectionState(final int resourceId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //ssshin
                //btnBluetoothStart.setText(resourceId);
            }
        });
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);

                    if(deviceAlert() != null){
                        deviceAlert().dismiss();
                    }

                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            //mProgressDialog = ProgressDialog.show(mContext, "", "Wait...", true);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);

            if(deviceAlert() != null){
                deviceAlert().dismiss();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*scanLeDevice(false);
        getActivity().unregisterReceiver(mGattUpdateReceiver);*/
    }

    // Device scan callback.

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (device.getName() != null && !device.getName().equals("") && device.getName().equals("Chipsea-BLE")) {
                                mDeviceAddress = device.getAddress();
                                android.util.Log.e("dckim", "device::" + device.getName()+">>"+mDeviceAddress);

                                if (mScanning) {

                                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                                    mScanning = false;

                                }

                                if (mBluetoothLeService != null) {
                                    final boolean result = mBluetoothLeService.connect(mDeviceAddress);
                                    android.util.Log.e("dckim ssshin", "device::" + device.getName()+">>"+mDeviceAddress+">>"+result);
                                }
                                //finish();
                            }
                        }
                    });
                }
            };

    private void bleConnectionState(){
        /*if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getResources().getString(R.string.location_access_title));
                builder.setMessage(getResources().getString(R.string.location_access_message));
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(23)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });

                builder.show();
            }
        }*/



        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(getContext(), R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return;
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final android.bluetooth.BluetoothManager bluetoothManager =
                (android.bluetooth.BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(getContext(), R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            getActivity().finish();
            return;
        }

        Intent gattServiceIntent = new Intent(mContext, BluetoothLeService.class);
        getActivity().bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);


    }

    private void initBLE(){
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (Build.VERSION.SDK_INT >= 23) {
            // Android M Permission check
            if (getActivity().checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getResources().getString(R.string.location_access_title));
                builder.setMessage(getResources().getString(R.string.location_access_message));
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(23)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });

                builder.show();
            } else {
                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    scanLeDevice(true);
                } else {
                    Toast.makeText(getContext(), R.string.bluetooth_disable, Toast.LENGTH_SHORT);
                }
            }
        } else {
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                scanLeDevice(true);
            } else {
                Toast.makeText(getContext(), R.string.bluetooth_disable, Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(getResources().getString(R.string.location_access_cancel_title));
                    builder.setMessage(getResources().getString(R.string.location_access_cancel_message));
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public void onDeviceConnected() {
        android.util.Log.e("dckim", "onDeviceConnected::");
    }

    @Override
    public void onDeviceDisconnect() {
        android.util.Log.e("dckim", "onDeviceDisconnect::");
    }

    @Override
    public void onDeviceClose() {

        android.util.Log.e("dckim", "onDeviceClose::");
    }

    @Override
    public void onServicesDiscovered(boolean optionalServicesFound) {

        android.util.Log.e("dckim", "onServicesDiscovered::");
    }

    @Override
    public void onDeviceReady() {

        android.util.Log.e("dckim", "onDeviceReady::");
        if (mBluetoothLeService != null) {
            mBluetoothLeService.getWeightValue();
        }
    }

    @Override
    public void onBondingRequired() {

        android.util.Log.e("dckim", "onBondingRequired::");
    }

    @Override
    public void onBonded() {
        android.util.Log.e("dckim", "onBonded::");
    }

    @Override
    public void onError(String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported() {

    }

    @Override
    public void onWeightChangeSuccess() {
        android.util.Log.e("dckim", "onWeightChangeSuccess::");

    }

    @Override
    public void onWeightValueSuccess() {
        android.util.Log.e("dckim", "onWeightValueSuccess::");

    }

    @Override
    public void onBluetoothClose() {

    }

    private void displayData(String data) {
        if (data != null) {
            NumberFormat numberformat = new DecimalFormat("###,###.##");
            float mValue1;
            if (mSelectedIndex == 0) {
                mValue1 = BleUtil.getHexToDec(data);

            } else {
                int mValue = BleUtil.getHexToInt(data);
                mValue *= 11023;
                mValue += 5000;
                mValue /= 10000;
                mValue <<= 1;
                mValue1 = BleUtil.getHexToDec(String.valueOf(mValue));

            }
            String strVal = numberformat.format(mValue1 / 100.0D);
            //tvWeightValue.setText(strVal);
            weightEt.setText(strVal);
            weightEt.setTag("D"); // 디바이스


            if (deviceAlert() != null) {
                deviceAlert().dismiss();
            }
        }

    }
}