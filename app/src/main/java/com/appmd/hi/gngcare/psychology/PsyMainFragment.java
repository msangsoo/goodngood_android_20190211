package com.appmd.hi.gngcare.psychology;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.main.EduVideoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by MobileDoctor on 2017-02-28.
 */

public class PsyMainFragment extends BaseFragment implements View.OnClickListener, PsyMainActivity.onKeyBackPressedListener {

    private PsyMainActivity activity;
    private View view;

    private LinearLayout mPsyExstLy;
    private LinearLayout mPsyImgExstLy;
    private LinearLayout mPsyNotExstLy;

    private ImageButton mPsyCheckBtn;
    private ImageButton mPsyCheckHistoryBtn;
    private ImageButton mPsyMediaBtn;
    private ImageButton mPsyVideoBtn;
    
    private TextView mPsyTitleTxt;
    private TextView mPsyScoreTxt;
    private TextView mPsyResultTxt;
    private TextView mPsyDateTxt;

    private ImageView mPsyThumbImg;
    private TextView mPsyImgDescTxt;
    private TextView mPsyImgDateTxt;
    private ImageView mPsy_check_img;

    private AQuery aq;

    private PsyProgress mLevelProgress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (PsyMainActivity)getActivity();
        view = inflater.inflate(R.layout.psy_main_fragment, null);
        aq = new AQuery(getContext());
        init(view);
        setEvent();
        return view;
    }

    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {

        mPsyExstLy = (LinearLayout) view.findViewById(R.id.psyExstLy);
        mPsyImgExstLy = (LinearLayout) view.findViewById(R.id.psyImgExstLy);
        mPsyNotExstLy = (LinearLayout) view.findViewById(R.id.psyNotExstLy);

        mPsyMediaBtn = (ImageButton) view.findViewById(R.id.psy_media_btn);
        mPsyCheckBtn = (ImageButton) view.findViewById(R.id.psy_check_btn);
        mPsyCheckHistoryBtn = (ImageButton) view.findViewById(R.id.psy_check_history_btn);
        mPsyVideoBtn = (ImageButton) view.findViewById(R.id.psy_video_btn);

        mPsyTitleTxt = (TextView) view.findViewById(R.id.psy_title_txt);
        mPsyScoreTxt = (TextView) view.findViewById(R.id.psy_score_txt);
        mPsyResultTxt = (TextView) view.findViewById(R.id.psy_result_txt);
        mPsyDateTxt = (TextView) view.findViewById(R.id.psy_date_txt);


        mPsyThumbImg = (ImageView) view.findViewById(R.id.psyThumbImg);
        mPsyImgDescTxt  = (TextView) view.findViewById(R.id.psyImgDescTxt);
        mPsyImgDateTxt    = (TextView) view.findViewById(R.id.psy_img_date_txt);

        mLevelProgress = view.findViewById(R.id.child_progress_bar);

        mPsy_check_img = view.findViewById(R.id.psy_check_img);
    }


    public void requestPsyMainData() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP002);
            object.put(CommonData.JSON_CHL_SN_F, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(getActivity(), NetworkConst.NET_PSY_MAIN, NetworkConst.getInstance().getPsyDomain(), networkListener, params,  activity.getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    public void setEvent() {
        mPsyMediaBtn.setOnClickListener(this);
        mPsyCheckBtn.setOnClickListener(this);
        mPsyCheckHistoryBtn.setOnClickListener(this);
        mPsyVideoBtn.setOnClickListener(this);


        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view, getContext());

        //아이 심리
        mPsyCheckHistoryBtn.setOnTouchListener(mClickListener);
        mPsyMediaBtn.setOnTouchListener(mClickListener);
        mPsyVideoBtn.setOnTouchListener(mClickListener);

        //코드 부여(아이 심리)
        mPsyCheckHistoryBtn.setContentDescription(getString(R.string.PsyCheckHistoryBtn));
        mPsyMediaBtn.setContentDescription(getString(R.string.PsyMediaBtn));
        mPsyVideoBtn.setContentDescription(getString(R.string.PsyVideoBtn));


    }

    @Override
    public void onClick(View v) {
        PsyMainActivity activity = (PsyMainActivity) getActivity();
        switch (v.getId()) {

            case R.id.psy_check_btn:

                if(Util.getSharedPreference(getActivity(), "psyCheckDataCode").compareTo("") == 0 ){
                    Util.setSharedPreference(getActivity(), "psyCheckDataCode", "");
                    switchFragment(new PsyCheckMainFragment());
                }else{
                    mDialog = new CustomAlertDialog(getActivity(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle("심리 체크");
                    mDialog.setContent(getString(R.string.psy_dialog_desc_text));
                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog, button) -> {
                        Util.setSharedPreference(getActivity(), "psyCheckDataCode", "");
                        Util.setSharedPreference(getActivity(), "psyCheckData", "[]");
                        dialog.dismiss();
                    });
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                    //    BleUtil.setSharedPreference(getActivity(), "psyCheckData", "");

                        Intent intent = null;

                        if(Util.getSharedPreference(getActivity(),"psyCheckDataCode").compareTo("LK01002") == 0){
                            intent = new Intent(getActivity(), PsyCheckStartImgActivity.class);
                        }else{
                            intent = new Intent(getActivity(), PsyCheckStartActivity.class);
                        }
                        intent.putExtra(CommonData.JSON_M_CODE_F, Util.getSharedPreference(getActivity(), "psyCheckDataCode"));
                        startActivity(intent);
                        dialog.dismiss();
                    });
                    mDialog.show();
                }


                break;
            case R.id.psy_check_history_btn:
                switchFragment(new PsyHistoryFragment());
                break;
            case R.id.psy_media_btn:
                //        if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)){
//                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
//                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
//                    mDialog.setContent(getString(R.string.call_center));
//                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
//                    mDialog.show();
//                }else{
                switchFragment(new PsyMediaFragment());

//                }
                break;
            case R.id.psy_video_btn:
                Intent intent = new Intent(getActivity(), EduVideoActivity.class);
                intent.putExtra("TITLE", getString(R.string.edu_video_title_05));
                intent.putExtra("ML_MCODE", "1");
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitleTxt(getString(R.string.title_psy));
        mPsyNotExstLy.setVisibility(View.GONE);
        mPsyExstLy.setVisibility(View.GONE);
        mPsyImgExstLy.setVisibility(View.GONE);
        requestPsyMainData();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((PsyMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        PsyMainActivity activity = (PsyMainActivity) getActivity();
        activity.finish();
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            activity.hideProgress();
            switch (type) {

                case NetworkConst.NET_PSY_MAIN:             // 음원 메인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            //     try {

                            mPsyNotExstLy.setVisibility(View.GONE);
                            mPsyExstLy.setVisibility(View.GONE);
                            mPsyImgExstLy.setVisibility(View.GONE);

                            try {
                                if (resultData.getString("RESULT_CODE").compareTo("0000") == 0) {

                                        mPsyNotExstLy.setVisibility(View.GONE);

                                    if (resultData.getString("ANS_IMGYN1").compareTo("Y") == 0) {

                                        mPsyImgExstLy.setVisibility(View.VISIBLE);
//                                        aq.id( mPsyThumbImg ).image(resultData.getString("ANS_IMG1"));
                                        Glide.with(getContext()).load(resultData.getString("ANS_IMG1"))
                                                .apply(new RequestOptions()
                                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                        .skipMemoryCache(true)).into(mPsyThumbImg);

                                        mPsyImgDescTxt.setText(resultData.getString("RC"));
                                        mPsyImgDateTxt.setText(resultData.getString("REGDATE"));
                                    }else{
                                        mPsyExstLy.setVisibility(View.VISIBLE);
                                        mPsyTitleTxt.setText(getString(R.string.psy_result_title));
                                        mPsyScoreTxt.setText(resultData.getString("TOT_SCR"));
//                                        mPsyResultTxt.setText(resultData.getString("M_NAME")+"\n"+resultData.getString("R_KIND"));
                                        mPsyResultTxt.setText(resultData.getString("M_NAME"));
                                        mPsyDateTxt.setText(resultData.getString("REGDATE"));

                                        setImg(resultData.getString("M_NAME").trim().replace(" ",""));



                                        // XXXMrsohn 20190216 신규 Progress
                                        mLevelProgress.setProgress(resultData.getString("M_NAME"), StringUtil.getIntVal(resultData.getString("TOT_SCR")));  // 프로그래스 표시, 하단라벨설정
                                    }


                                } else {

                                    mPsyNotExstLy.setVisibility(View.VISIBLE);
                                    mPsyExstLy.setVisibility(View.GONE);
                                    mPsyImgExstLy.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;
            }

        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };

    public void setImg(String name){
        switch (name){
            case "대인관계":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_01);
                break;
            case "자아정체감":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_03);
                break;
            case "소아우울증":
                mPsy_check_img.setImageResource(R.drawable.psy_kids_start_icon_04);
                break;
            case "집중도":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_01);
                break;
            case "학습욕구":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_02);
                break;
            case "두뇌건강":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_03);
                break;
            case "스마트폰중독":
                mPsy_check_img.setImageResource(R.drawable.psy_edu_start_icon_04);
                break;
            case "교육강박검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_01);
                break;
            case "엄마우울증검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_02);
                break;
            case "상황판단검사":
                mPsy_check_img.setImageResource(R.drawable.psy_parents_start_icon_03);
                break;




        }
    }

}
