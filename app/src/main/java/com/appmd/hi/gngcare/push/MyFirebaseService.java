package com.appmd.hi.gngcare.push;

import com.appmd.hi.gngcare.RCApplication;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by MobileDoctor on 2017-02-08.
 */

public class MyFirebaseService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh(){
        RCApplication.deviceToken = FirebaseInstanceId.getInstance().getToken();
    }
}
