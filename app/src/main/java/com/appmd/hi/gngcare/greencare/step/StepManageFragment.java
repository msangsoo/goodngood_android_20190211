package com.appmd.hi.gngcare.greencare.step;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.CommonActionBar;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.base.value.Define;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.googleFitness.GoogleFitInstance;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.greencare.util.DisplayUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.appmd.hi.gngcare.util.GLog;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class StepManageFragment extends BaseFragment {
    private static final String TAG = StepManageFragment.class.getSimpleName();

    private GoogleFitChartView mGoogleFitView;
    private View mView;

    private View mVisibleView1;
    private View mVisibleView2;
    private View mVisibleView3;
    private View mVisibleView4;
    private View mVisibleView5;
    private View mChartView;
    private ImageView mChartCloseBtn, mChartZoomBtn;
    private ScrollView mContentScrollView;
    private LinearLayout mHCallBtn;
    private RelativeLayout mDateLayout;


    private android.widget.TextView mTab2;
    private android.widget.TextView mTab1;


    public static Fragment newInstance() {
        StepManageFragment fragment = new StepManageFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_walk_manage, container, false);
        return view;
    }

    @Override
    public void loadActionbar(CommonActionBar actionBar) {
        super.loadActionbar(actionBar);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;

        mGoogleFitView = new GoogleFitChartView(StepManageFragment.this, view);   // 차트뷰 세팅

        mVisibleView1 = view.findViewById(R.id.visible_layout1);
        mVisibleView2 = view.findViewById(R.id.visible_layout_3);
        mVisibleView3 = view.findViewById(R.id.visible_layout_4);
        mVisibleView4 = view.findViewById(R.id.period_radio_group);
        mVisibleView5 = view.findViewById(R.id.result_tip_layout);
        mChartView = view.findViewById(R.id.chart1);
        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mContentScrollView = view.findViewById(R.id.view_scrollview);
        mDateLayout = view.findViewById(R.id.date_Layout);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);
        mHCallBtn = view.findViewById(R.id.Hcall_btn);

        view.findViewById(R.id.target_value_btn).setOnClickListener(mClickListener);
        mChartZoomBtn.setOnClickListener(mClickListener);
        mChartCloseBtn.setOnClickListener(mClickListener);
        mHCallBtn.setOnClickListener(mClickListener);

        setVisibleOrientationLayout();

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());

        //엄마 건강
        mHCallBtn.setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        mHCallBtn.setContentDescription(getString(R.string.HCallBtn6));
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.target_value_btn) {
                DummyActivity.startActivityForResult(getActivity(), 1111, StepInputFragment.class, new Bundle());
            }else if(vId == R.id.landscape_btn){
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }else if(vId == R.id.chart_close_btn){
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else if(vId == R.id.Hcall_btn){
                if(CommonData.getInstance().getMberGrad().equals("10")) {
                    CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(getContext().getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getContext().getString(R.string.do_call_center));
                    mDialog.setNegativeButton(getContext().getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(getContext().getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        String tel = "tel:" + getContext().getString(R.string.call_center_number);
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(tel));
                        getContext().startActivity(intent);
                        dialog.dismiss();
                    });

                    mDialog.show();
                }else{
                    CustomAlertDialog mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.call_center2));
                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(getString(R.string.do_call), (dialog, button) -> {
                        String tel = "tel:" + getString(R.string.call_center_number2);
//                        startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                        Intent intentCall = new Intent(Intent.ACTION_DIAL);
                        intentCall.setData(Uri.parse(tel));
                        startActivity(intentCall);
                        dialog.dismiss();
                    });
                    mDialog.show();
                }
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int dataSource = SharedPref.getInstance(getContext()).getPreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_GOOGLE_FIT);
        Logger.i(TAG, "dataSource="+dataSource);
//        if (Define.STEP_DATA_SOURCE_GOOGLE_FIT == dataSource) {
            mGoogleFitView = new GoogleFitChartView(StepManageFragment.this, mView);   // 차트뷰 세팅
//        } else if (Define.STEP_DATA_SOURCE_BAND == dataSource) {
//            new BandChartView(StepManageFragment.this, mView);   // 차트뷰 세팅
//        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GoogleFitInstance.REQUEST_OAUTH_REQUEST_CODE) {
                GoogleFitInstance fitness = new GoogleFitInstance();
                fitness.subscribe(getActivity(), new ApiData.IStep() {
                    @Override
                    public void next(Object obj) {
                        mGoogleFitView.getData();
                    }
                });
            }
        } else {
            if (requestCode == GoogleFitInstance.REQUEST_OAUTH_REQUEST_CODE) {
                String message = "계정인증 후 이용 가능합니다.";
                CDialog.showDlg(getActivity(), message, new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {
                        getActivity().finish();
                    }
                });
            }
        }
    }

    public void call(int idx) {
        Toast.makeText(getContext(), "걸음 탭 선택 기능 구현 해야함="+idx, Toast.LENGTH_SHORT).show();

    }

    public void onChartViewResume(){
        mGoogleFitView = new GoogleFitChartView(StepManageFragment.this, mView);   // 차트뷰 세팅
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleFitView != null) {
            mGoogleFitView.onResume();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mGoogleFitView != null) {
            mGoogleFitView.onStop();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Logger.i(TAG, "onDetach");
        if (mGoogleFitView != null) {
            mGoogleFitView.onDetach();
        }
    }

    private boolean isLandScape = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        GLog.i("onConfigurationChanged="+newConfig.orientation, "");
        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE: //가로 모드
                isLandScape = true;
                break;
            case Configuration.ORIENTATION_PORTRAIT: //세로 모드
                isLandScape = false;
                break;
        }

        setVisibleOrientationLayout();
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    private void setVisibleOrientationLayout() {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView2.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView3.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView4.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView5.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mHCallBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mChartView.getLayoutParams();
        android.util.Log.i(TAG, "isLandScape="+isLandScape+", dm.widthPixels="+dm.widthPixels+", dm.heightPixels="+dm.heightPixels );

//        int height = (int) (dm.heightPixels - mDateLayout.getLayoutParams().height);//(dm.heightPixels *0.20)); // 15% 작게
        int height = (int) (dm.heightPixels - dm.heightPixels *0.20); // 20% 작게
        params.height = isLandScape ? height : DisplayUtil.getDpToPix(getContext(),250);
//        params.weight = isLandScape ? 0 : 1;
        mChartView.setLayoutParams(params);
        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        mContentScrollView.smoothScrollTo(0,0);
    }
}