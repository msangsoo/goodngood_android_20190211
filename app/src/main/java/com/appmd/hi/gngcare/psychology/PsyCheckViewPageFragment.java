package com.appmd.hi.gngcare.psychology;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;


public class PsyCheckViewPageFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.psy_check_viewpage, container, false);

        Bundle extra = getArguments();
        int position = extra.getInt("position");
        String title = extra.getString("title");

        TextView mTxt = (TextView) linearLayout.findViewById(R.id.mTxt);
        mTxt.setText(title);


        return linearLayout;

    }
}