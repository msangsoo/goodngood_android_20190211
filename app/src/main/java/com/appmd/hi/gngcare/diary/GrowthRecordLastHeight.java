package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.TextWatcherUtil;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-29.
 * 최종 키 예측 클래스
 * @since 0, 1
 */
public class GrowthRecordLastHeight extends BaseFragment implements GrowthMainActivity.onKeyBackPressedListener{


    private View view;
    private TextView mMotherTv , mFatherTv;
    private ImageButton mResultBtn;
    static Boolean resume_flag = false;

    private EditText mMyHeightEd, mMyWeightEd;  // 자녀 키 몸무게
    private EditText mMotherHeightEd , mMotherWeightEd;  // 어머니 키 몸무게
    private EditText mFatherHeightEd , mFatherWeightEd;  // 아버지 키 몸무게

    private boolean mAgeFlag;

    private LinearLayout mMeLay , mMoWeightLay , mFaWeightLay ;

    @Override
    public void onResume() {
        GLog.i("onResume--", "dd");
        if (resume_flag){
            mMyHeightEd.setText("");
            mMyWeightEd.setText("");

            mMotherHeightEd.setText("");
            mMotherWeightEd.setText("");

            mFatherHeightEd.setText("");
            mFatherWeightEd.setText("");

            resume_flag = false;
        }
        super.onResume();

        GrowthMainActivity.GROWTH_MAIN_ACTIVITY.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.growth_record_lastheight, null);
        init(view);
        setEvent(view);

        return view;
    }

    /**
     * 객체 초기화
     * @param view view
     */
    public void init(View view){
        GLog.i("init", "dd");

        mMyHeightEd =         (EditText)  view.findViewById(R.id.myheight_ed);
        mMyWeightEd        =         (EditText)  view.findViewById(R.id.myweight_ed);

        mMotherHeightEd       =         (EditText)  view.findViewById(R.id.motherheight_ed);
        mMotherWeightEd        =         (EditText)  view.findViewById(R.id.motherweight_ed);

        mFatherHeightEd       =         (EditText)  view.findViewById(R.id.fatherheight_ed);
        mFatherWeightEd        =         (EditText)  view.findViewById(R.id.fatherweight_ed);

        mMotherTv           =       (TextView)  view.findViewById(R.id.mother_tv);
        mFatherTv           =       (TextView)  view.findViewById(R.id.father_tv);
        mResultBtn          =       (ImageButton)    view.findViewById(R.id.result_btn);

        mMeLay              =       (LinearLayout)  view.findViewById(R.id.me_lay);
        mMoWeightLay        =       (LinearLayout)  view.findViewById(R.id.moweight_lay);
        mFaWeightLay        =       (LinearLayout)  view.findViewById(R.id.faweight_lay);

       }

    /**
     * 이벤트 연결
     */
    public void setEvent(View view){

        view. findViewById(R.id.result_btn).setOnClickListener(btnListener);

        mMyHeightEd.addTextChangedListener(watcher);

        /*mMyHeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mMyHeightEd, null));
        mMyWeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mMyWeightEd, null));
        mMotherHeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mMotherHeightEd, null));
        mMotherWeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mMotherWeightEd, null));
        mFatherHeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mFatherHeightEd, null));
        mFatherWeightEd.addTextChangedListener(new CustomTextWatcher(getContext(), mFatherWeightEd, null));*/

        new TextWatcherUtil().setTextWatcher(mMyHeightEd, 210, 1);
        new TextWatcherUtil().setTextWatcher(mMyWeightEd, 130, 2);
        new TextWatcherUtil().setTextWatcher(mMotherHeightEd, 210, 1);
        new TextWatcherUtil().setTextWatcher(mMotherWeightEd, 130, 2);
        new TextWatcherUtil().setTextWatcher(mFatherHeightEd, 210, 1);
        new TextWatcherUtil().setTextWatcher(mFatherWeightEd, 130, 2);

        if (MainActivity.mChildMenuItem.size() < 1) {
            mDialog = new CustomAlertDialog(getActivity(), CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
            mDialog.setContent(getString(R.string.popup_dialog_no_child_register));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();


        } else {

            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)){
                mAgeFlag = false;
            }else{
                Date imsiDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                int year = imsiDate.getYear() + 1900;
                int month = imsiDate.getMonth()+1;
                int day = imsiDate.getDate();

                String imsiBirth = "" + year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
                GLog.i("imsiBirth = " +imsiBirth, "dd");


                String childebirth = Util.getDateSpecialCharacter(getActivity(), imsiBirth, 4);
                double mTwoDateCompare = Util.getTwoDateCompare(childebirth , CommonData.PATTERN_DATE);
                GLog.i("mTwoDateCompare  -->   " + mTwoDateCompare, "dd");
                if (mTwoDateCompare <= 745){
                    mAgeFlag = false;
                } else {
                    mAgeFlag = true;
                }
            }

            if (!mAgeFlag) {
                mMotherTv.setText(getString(R.string.lastheight_mothertitle2));
                mFatherTv.setText(getString(R.string.lastheight_fathertitle2));
            }
        }
//        mResultBtn.setEnabled(false);


        if (!mAgeFlag) {
            mMeLay.setVisibility(View.GONE);
            mMoWeightLay.setVisibility(View.GONE);
            mFaWeightLay.setVisibility(View.GONE);

        }

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            Calendar cal = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.result_btn:   // 결과보기
                    GLog.i("--result_btn--", "dd");
                    if(isConfirm()){
                        if (mAgeFlag) {
                            requestResultApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex()
                                    , MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea());
                        }else {
                            GLog.i("24.5개월미만일 시", "dd");
                            intent = new Intent(getActivity(), GrowthRecordLastHeightResult.class);
                            intent.putExtra(CommonData.JSON_MBER_FLAG , mAgeFlag);
                            intent.putExtra(CommonData.JSON_MBER_MOTHER , Util.checkLastDot(mMotherHeightEd.getText().toString(),"."));
                            intent.putExtra(CommonData.JSON_MBER_FATHER , Util.checkLastDot(mFatherHeightEd.getText().toString(),"."));
                            startActivity(intent);
                            Util.BackAnimationStart(getActivity());
                        }
                    }else{
                        Toast.makeText(getContext(), getString(R.string.non_growthdata), Toast.LENGTH_SHORT).show();
                    }

                    break;
            }
        }
    };


    /**
     * 결과보기
     */
    public void requestResultApi(String chl_sn , String chl_sex , String chl_lifyea){
        GLog.i("requestAppInfo", "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        //{   "api_code": "chldrn_hra_next_cm", "insures_code": "106", "mber_sn": "10035", "chl_sn": "1000" , "bsis_yymm": "120815","bsis_sex": "1" , "bsis_height": "120","bsis_bdwgh":
        // "25","input_dad_height": "180","input_dad_bdwgh": "82","input_mom_height": "160","input_mon_bdwgh": "50" }";
        try {
            GLog.i("mMyHeight --> " + mMyHeightEd.getText().toString(), "dd");
            GLog.i("mMyWeight --> " + mMyWeightEd.getText().toString(), "dd");
            GLog.i("mMotherHeight --> " + mMotherHeightEd.getText().toString(), "dd");
            GLog.i("mMotherWeight --> " + mMotherWeightEd.getText().toString(), "dd");
            GLog.i("mFatherHeight --> " + mFatherHeightEd.getText().toString(), "dd");
            GLog.i("mFatherWeight --> " + mFatherWeightEd.getText().toString(), "dd");

            JSONObject object = new JSONObject();

            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_GET_HRA_NEXT);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값
            object.put(CommonData.JSON_BSIS_YYMM, chl_lifyea);           //  자녀 생일
            object.put(CommonData.JSON_BSIS_SEX, chl_sex);           //  성별 1.남 2.여
            object.put(CommonData.JSON_BSIS_HEIGHT, Util.checkLastDot(mMyHeightEd.getText().toString(),"."));             //  자녀 키
            object.put(CommonData.JSON_BSIS_BDWGH, Util.checkLastDot(mMyWeightEd.getText().toString(),"."));            // 자녀 몸무게
            object.put(CommonData.JSON_INPUT_MOM_HEIGHT, Util.checkLastDot(mMotherHeightEd.getText().toString(),"."));          //  아빠 키
            object.put(CommonData.JSON_INPUT_MOM_BDWGH, Util.checkLastDot(mMotherWeightEd.getText().toString(),"."));            //  아빠 몸무게
            object.put(CommonData.JSON_INPUT_DAD_HEIGHT, Util.checkLastDot(mFatherHeightEd.getText().toString(),"."));           //  엄마 키
            object.put(CommonData.JSON_INPUT_DAD_BDWGH, Util.checkLastDot(mFatherWeightEd.getText().toString(),"."));         //  엄마 몸무게

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_HEIGHT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_HEIGHT:									// 앱 정보 가져오기

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String startcm , mycm, endcm;
                                startcm = resultData.getString(CommonData.JSON_MBER_LAST_CM);
                                mycm = resultData.getString(CommonData.JSON_MBER_START_PER);
                                endcm = resultData.getString(CommonData.JSON_MBER_END_PER);
                                Intent intent = new Intent(getActivity(), GrowthRecordLastHeightResult.class);
                                intent.putExtra(CommonData.JSON_MBER_FLAG , mAgeFlag);
                                intent.putExtra(CommonData.JSON_MBER_LAST_CM , startcm);
                                intent.putExtra(CommonData.JSON_MBER_START_PER , mycm);
                                intent.putExtra(CommonData.JSON_MBER_END_PER , endcm);
                                startActivity(intent);
                                Util.BackAnimationStart(getActivity());
                            }catch (Exception e){

                            }
                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:	// 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:	// 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();

        }
    };

    /**
     * * 모든 데이터 기입 했는지 체크
     * @return boolean ( true - 가능, false - 불가 )
     */
    public boolean isConfirm(){
        boolean bool = false;
        try{
            if (mAgeFlag) {
                if ( Double.parseDouble(mMyHeightEd.getText().toString()) > 0 && Double.parseDouble(mMyWeightEd.getText().toString()) > 0
                        && Double.parseDouble(mFatherHeightEd.getText().toString()) > 0 && Double.parseDouble(mFatherWeightEd.getText().toString()) > 0
                        && Double.parseDouble(mMotherWeightEd.getText().toString()) > 0 && Double.parseDouble(mMotherHeightEd.getText().toString()) > 0)
                {
                    bool = true;
                    return bool;
                }
            }else {
                if (Double.parseDouble(mMotherHeightEd.getText().toString()) > 0 && Double.parseDouble(mFatherHeightEd.getText().toString()) > 0) {
                    bool = true;
                    return bool;
                }
            }
            return bool;
        }catch (Exception e){
            e.printStackTrace();
            return bool;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((GrowthMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        GrowthMainActivity activity = (GrowthMainActivity)getActivity();
        activity.switchActionBarTheme(GrowthMainActivity.THEME_DARK_BLUE);
        switchFragment(new GrowthMainFragment());
    }
}
