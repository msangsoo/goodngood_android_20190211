package com.appmd.hi.gngcare.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class SwitchMemberActivity extends BackBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{

    private LinearLayout mNameLayout, mBirthLayout, mPhoneLayout;
    private TextView mPhoneTitleTv, mNotFoundTv;
    private EditText mNameEdit, mPhoneEdit, mBirthTv;
    private ImageButton mNameDelBtn, mPhoneDelBtn;

    private RadioButton mLocalRb, mForeignerRb, mMaleRb, mFemaleRb;

    private Button mNextBtn;

    private String mCurrentBirth =   "";
    Calendar cal = Calendar.getInstance();

    private String mBer_nm, mBer_lifyea, mBer_hp, mBer_nation, mBer_sex, mBer_job, mBer_afterbirth;    // api 호출시 저장변수

    private Intent intent = null;
    private int mLoginType = 1;

    String mberNo = "";
    String mBerNm = "";
    String mBirthday = "";
    String mBerjob ="Y";
    int mBerNation = 1;
    int mGender = 1;
    int mAfterBirth = 1;


    private Date mCurDate;
    GregorianCalendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.switch_member_activity);

        setTitle(getString(R.string.member_switch));

        init();
        setEvent();


//        mNextBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if(intent != null){
            mLoginType  =   intent.getIntExtra(CommonData.EXTRA_TYPE, CommonData.LOGIN_TYPE_PARENTS);

        }else{
            GLog.i("intent = null", "dd");
        }

        mPhoneTitleTv.setText(getString(R.string.mobile_phone_number));
        mPhoneEdit.setHint(getString(R.string.phone_number_hint));
        mNotFoundTv.setVisibility(View.GONE);


        mCalendar = new GregorianCalendar();
        mCurDate = new Date();
    }

    /**
     * 초기화
     */
    public void init(){

        mNameLayout     =   (LinearLayout)      findViewById(R.id.name_layout);
        mBirthLayout    =   (LinearLayout)      findViewById(R.id.birthday_layout);
        mPhoneLayout    =   (LinearLayout)      findViewById(R.id.phone_number_layout);

        mBirthTv        =   (EditText)          findViewById(R.id.birthday_tv);
        mNotFoundTv     =   (TextView)          findViewById(R.id.notfound);
        mPhoneTitleTv   =   (TextView)          findViewById(R.id.phone_number_title_tv);

        mNameEdit       =   (EditText)          findViewById(R.id.name_edit);
        mPhoneEdit      =   (EditText)          findViewById(R.id.phone_number_edit);

        mNameDelBtn     =   (ImageButton)       findViewById(R.id.name_del_btn);
        mPhoneDelBtn    =   (ImageButton)       findViewById(R.id.phone_number_del_btn);

        mLocalRb        =   (RadioButton)       findViewById(R.id.local_btn);
        mForeignerRb    =   (RadioButton)       findViewById(R.id.foreigner_btn);
        mMaleRb         =   (RadioButton)       findViewById(R.id.male_btn);
        mFemaleRb       =   (RadioButton)       findViewById(R.id.female_btn);


        mNextBtn     =   (Button)            findViewById(R.id.next_btn);


    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
        mNameDelBtn.setOnClickListener(this);
        mPhoneDelBtn.setOnClickListener(this);

        mNameEdit.setOnFocusChangeListener(this);
        mPhoneEdit.setOnFocusChangeListener(this);

        mNameEdit.addTextChangedListener(new MyTextWatcher());
        mPhoneEdit.addTextChangedListener(new MyTextWatcher());
        mBirthTv.addTextChangedListener(new MyTextWatcher());

    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2){    // 이름 입력이 없다면
            mNameEdit.requestFocus();
            mDialog =   new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent("이름 2자리 이상 입력해주세요");//getString(R.string.popup_dialog_name_error)
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        String birth = mBirthTv.getText().toString();

        if(birth.trim().length() != 8){
            mBirthTv.requestFocus();
            mDialog =   new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_birthday_error));//getString(R.string.popup_dialog_birthday_error)
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(mPhoneEdit.getText().toString().trim().equals("") || mPhoneEdit.getText().toString().trim().length() < 10){   // 휴대폰 번호
            mPhoneEdit.requestFocus();
            mDialog =   new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent("보험 가입 정보를 입력해 주세요.");//getString(R.string.popup_dialog_phone_number_error)
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!CDateUtil.getDateFormatBool(birth)){
            mBirthTv.requestFocus();
            mDialog = new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent("날짜형식이 아닙니다.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }



        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

        if(mNameEdit.getText().toString().trim().equals("") || mNameEdit.getText().toString().trim().length() < 2){    // 이름 입력이 없다면
            bool = false;
            return bool;
        }

        if(mBirthTv.getText().toString().trim().equals("") || mBirthTv.getText().toString().trim().length() < 8){
            bool = false;
            return bool;
        }


        if(mPhoneEdit.getText().toString().trim().equals("") || mPhoneEdit.getText().toString().trim().length() < 10){   // 휴대폰 번호
            bool = false;
            return bool;
        }


        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

//        if(bool) {
            mNextBtn.setEnabled(true);

            mNextBtn.setBackgroundResource(R.drawable.btn_5_22396c);
            mNextBtn.setTextColor(ContextCompat.getColorStateList(SwitchMemberActivity.this, R.color.color_ffffff_btn));
//        }else{
//            mNextBtn.setEnabled(false);
//
//            mNextBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
//            mNextBtn.setTextColor(ContextCompat.getColorStateList(SwitchMemberActivity.this, R.color.color_ffffff_btn));
//        }

    }

    /**
     * 정회원 인
     *
     */
    public void requestAuthCheck() {
//        {"api_code":"asstb_mber_chl_find_yn","insures_code":"108","mber_nm":"김명균","mber_lifyea":"19911017","mber_hp":"0101234789","mber_nation":"1","mber_sex":"1"}

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_CHL_FIND_YN);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);

            mBer_nm = mNameEdit.getText().toString().trim();
            mBer_lifyea = mBirthTv.getText().toString().trim(); //mCurrentBirth;
            mBer_hp = mPhoneEdit.getText().toString().trim();
            mBer_nation = mLocalRb.isChecked() ? "1" : "2"; // 1 - 내국인, 2 - 외국인
            mBer_sex = mMaleRb.isChecked() ? "1" : "2"; // 1 - 남자, 2 - 여자



            object.put(CommonData.JSON_MBER_NM,     mBer_nm);
            object.put(CommonData.JSON_MBER_LIFYEA,     mBer_lifyea);
            object.put(CommonData.JSON_MBER_HP,     mBer_hp);
            object.put(CommonData.JSON_MBER_NATION,     mBer_nation);
            object.put(CommonData.JSON_MBER_SEX,     mBer_sex);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());



            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMemberActivity.this, NetworkConst.NET_ASSTB_MBER_CHL_FIND_YN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_ASSTB_MBER_CHL_FIND_YN:   // 정회원 체크
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 정회원 체크
                                    String mberno = resultData.getString(CommonData.JSON_MBER_NO);
                                    String memname = resultData.getString(CommonData.JSON_MEMNAME);

                                    mDialog =   new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setHtmlContent(getString(R.string.popup_dialog_contactor_complete3));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();

                                            Intent intent = new Intent(SwitchMemberActivity.this, SwitchMember1Activity.class);
                                            intent.putExtra(CommonData.EXTRA_MBERNO, mberno);
                                            intent.putExtra(CommonData.EXTRA_MEMNAME, memname);
                                            startActivity(intent);
                                            finish();

                                        }
                                    });
                                    mDialog.show();

                                }else if(data_yn.equals(CommonData.NO)) {  // 현대해상 미가입자
//                                    mDialog = new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
//                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
//                                    mDialog.setContent(getString(R.string.popup_dialog_not_found));
//                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(CustomAlertDialog dialog, Button button) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                                    mDialog.show();
                                    mNotFoundTv.setVisibility(View.VISIBLE);
                                }else if(data_yn.equals("YN")){
                                    mDialog = new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_already_member));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                } else {
                                    String site_memid = resultData.getString(CommonData.JSON_SITE_MEMID);
                                    String app_memid = resultData.getString(CommonData.JSON_APP_MEMID);
                                    String mberno = resultData.getString(CommonData.JSON_MBER_NO);
                                    String memname = resultData.getString(CommonData.JSON_MEMNAME);
                                    mDialog = new CustomAlertDialog(SwitchMemberActivity.this, CustomAlertDialog.TYPE_E);
                                    mDialog.setSelectId(site_memid,app_memid,getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            Log.i("SwitchMember","홈페이지 아이디 :"+mDialog.getHomepageId());
                                            Log.i("SwitchMember","app 아이디 :"+mDialog.getAppId());

                                            Intent intent = new Intent(SwitchMemberActivity.this, SwitchMember1Activity.class);
                                            intent.putExtra(CommonData.EXTRA_MBERNO, mberno);
                                            intent.putExtra(CommonData.EXTRA_MEMNAME, memname);

                                            if(mDialog.getAppId().equals("Y")){
                                                intent.putExtra(CommonData.EXTRA_MEMID, app_memid);
                                                intent.putExtra(CommonData.EXTRA_MEMID_APP_BOOL, mDialog.getAppId());
                                                intent.putExtra(CommonData.EXTRA_MEMID_SITE_BOOL, mDialog.getHomepageId());
                                            }else{
                                                intent.putExtra(CommonData.EXTRA_MEMID, site_memid);
                                                intent.putExtra(CommonData.EXTRA_MEMID_SITE_BOOL, mDialog.getHomepageId());
                                                intent.putExtra(CommonData.EXTRA_MEMID_APP_BOOL, mDialog.getAppId());
                                            }

                                            startActivity(intent);

                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                                    mDialog.show();

                                }

                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;


                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };










    @Override
    public void onClick(View v) {

        Intent intent = null;
        mNotFoundTv.setVisibility(View.GONE);
        switch (v.getId()){
            case R.id.next_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");

                if(invaildCerfiti()){   // 인증 가능하다면
                    requestAuthCheck();
                }
                break;
            case R.id.name_del_btn: // 이름 삭제
                commonView.setClearEditText(mNameEdit);
                break;
            case R.id.phone_number_del_btn: // 전화번호 삭제
                commonView.setClearEditText(mPhoneEdit);
                break;
            case R.id.birthday_tv:  // 생년월일
                try {
                    if(mCurDate == null){
                        mCurDate = new Date();
                    }
                    mCalendar.setTime(mCurDate);
                    int nNowYear = mCalendar.get(Calendar.YEAR);
                    int nNowMonth = mCalendar.get(Calendar.MONTH);
                    int nNowDay = mCalendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurDate = mCalendar.getTime();
                        SimpleDateFormat format = new SimpleDateFormat(CommonData.PATTERN_YYYYMMDD);
                        mCurrentBirth = format.format(mCurDate);
                        format = new SimpleDateFormat(CommonData.PATTERN_DATE_KR);
                        mBirthTv.setText( format.format(mCurDate));
                        setConfirmBtn(isConfirm());
                    }, nNowYear, nNowMonth , nNowDay);
                    datePickerDialog.setCancelable(false);
                    datePickerDialog.showYearPickerFirst(true);
                    datePickerDialog.show(getFragmentManager(),"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.name_edit:    // 이름
//                commonView.setClearImageBt(mNameDelBtn, hasFocus);
                break;
            case R.id.phone_number_edit:	   // 전화번호
//                commonView.setClearImageBt(mPhoneDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
            mNotFoundTv.setVisibility(View.GONE);

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

}
