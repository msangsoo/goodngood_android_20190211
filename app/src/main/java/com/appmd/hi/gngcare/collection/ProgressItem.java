package com.appmd.hi.gngcare.collection;

import android.widget.TextView;

/**
 * Created by CP on 2018. 4. 21..
 */

public class ProgressItem {

    public int color;
    public float progressItemPercentage;
    public String pointStr;
    public String descStr;
    public String bottomStr;
}