package com.appmd.hi.gngcare.psychology;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.PsyHistoryAdapter;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.collection.PsyResultItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-04-18.
 *
 * @since 0, 1
 */
public class PsyHistoryFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, PsyMainActivity.onKeyBackPressedListener {

    private PsyMainActivity activity;
    private LinearLayout mVisibleLayout;
    private Button tabPsyBtn;
    private Button tabBrnBtn;
    private Button tabPrnBtn;
    private View view;
    private PullToRefreshListView mRefreshListView;
    private PsyHistoryAdapter mAdapter;
    private ArrayList<PsyResultItem> mItem;

    private LinearLayout startDateBtn;
    private LinearLayout endDateBtn;
    private TextView startDateTxt;
    private TextView endDateTxt;
    private Button searchBtn;

    private int firStartYear;
    private int firStartMonth;
    private int firStartDay;

    private int firEndYear;
    private int firEndMonth;
    private int firEndDay;

    private int secStartYear;
    private int secStartMonth;
    private int secStartDay;

    private int secEndYear;
    private int secEndMonth;
    private int secEndDay;

    // 현재 보고있는 페이지f
    private int mPageId = 1;
    private int mSearchPageId = 1;
    // 전체 페이지
    private int mTotalPage = 1;

    // header view
    private View mHeaderView;
    private TextView emptyTxt;
    //   private TextView mHeaderTitleTv, mHeaderEmptyTv;

    private JSONArray jsonArr;
    private boolean firstFlag = false;


    private String mLkCode = "LK01";
    private String firStartDate;
    private String firEndDate;

    private String secStartDate;
    private String secEndDate;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (PsyMainActivity) getActivity();
        view = inflater.inflate(R.layout.psy_history_fragment, null);
        init(view);
        // 헤더뷰
        addHeaderView();
        setEvent();

        /* 날짜 초기화 */
        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        Date currentTime = new Date();
     //   String mTime = mSimpleDateFormat.format(currentTime);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        String curDate = mSimpleDateFormat.format(calendar.getTime());
        String[] curDateStrArr = curDate.split("-");

        calendar.add(Calendar.MONTH, -1);

        String pDate = mSimpleDateFormat.format(calendar.getTime());
        String[] pDateStrArr = pDate.split("-");


        firStartYear = Integer.valueOf(pDateStrArr[0]);
        firStartMonth = Integer.valueOf(pDateStrArr[1]);
        firStartDay = Integer.valueOf(pDateStrArr[2]);

        firEndYear = Integer.valueOf(curDateStrArr[0]);
        firEndMonth = Integer.valueOf(curDateStrArr[1]);
        firEndDay = Integer.valueOf(curDateStrArr[2]);

        secStartYear = Integer.valueOf(pDateStrArr[0]);
        secStartMonth = Integer.valueOf(pDateStrArr[1]);
        secStartDay = Integer.valueOf(pDateStrArr[2]);

        secEndYear = Integer.valueOf(curDateStrArr[0]);
        secEndMonth = Integer.valueOf(curDateStrArr[1]);
        secEndDay = Integer.valueOf(curDateStrArr[2]);

        mLkCode = "LK01";

        startDateTxt.setText(getDateStr(firStartYear, firStartMonth, firStartDay));
        endDateTxt.setText(getDateStr(firEndYear, firEndMonth, firEndDay));

        firStartDate = getDateStr(firStartYear, firStartMonth, firStartDay);
        firEndDate = getDateStr(firEndYear, firEndMonth, firEndDay);
        requestPsyResultData(mLkCode, firStartDate, firEndDate);

        return view;
    }


    public String getIntToStringForDate(int tmp) {
        String result = String.valueOf(tmp);
        if (result.length() == 1) {
            result = "0" + result;
        }
        return result;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitleTxt(getString(R.string.psy_btn_text_check_history));
    }

    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {

        mRefreshListView = (PullToRefreshListView) view.findViewById(R.id.psy_result_listview);
        mVisibleLayout = (LinearLayout) view.findViewById(R.id.visible_layout);
        tabPsyBtn = (Button) view.findViewById(R.id.tabPsyBtn);
        tabBrnBtn = (Button) view.findViewById(R.id.tabBrnBtn);
        tabPrnBtn = (Button) view.findViewById(R.id.tabPrnBtn);
        tabPsyBtn.setSelected(true);
        tabBrnBtn.setSelected(false);
        tabPrnBtn.setSelected(false);

        startDateBtn = (LinearLayout) view.findViewById(R.id.startDateBtn);
        endDateBtn = (LinearLayout) view.findViewById(R.id.endDateBtn);
        startDateTxt = (TextView) view.findViewById(R.id.startDateTxt);
        endDateTxt = (TextView) view.findViewById(R.id.endDateTxt);
        searchBtn = (Button) view.findViewById(R.id.searchBtn);

        mItem = new ArrayList<PsyResultItem>();

    }

    private DatePickerDialog.OnDateSetListener startListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (mLkCode.compareTo("LK01") == 0) {
                firStartYear = year;
                firStartMonth = (monthOfYear + 1);
                firStartDay = dayOfMonth;
            } else if(mLkCode.compareTo("LK02") == 0) {
                secStartYear = year;
                secStartMonth = (monthOfYear + 1);
                secStartDay = dayOfMonth;
            } else{
                secStartYear = year;
                secStartMonth = (monthOfYear + 1);
                secStartDay = dayOfMonth;
            }
            startDateTxt.setText(getDateStr(year, (monthOfYear + 1), dayOfMonth));

        }
    };

    private DatePickerDialog.OnDateSetListener endListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (mLkCode.compareTo("LK01") == 0) {
                firEndYear = year;
                firEndMonth = (monthOfYear + 1);
                firEndDay = dayOfMonth;
            } else if(mLkCode.compareTo("LK02") == 0) {
                secEndYear = year;
                secEndMonth = (monthOfYear + 1);
                secEndDay = dayOfMonth;
            } else{
                secEndYear = year;
                secEndMonth = (monthOfYear + 1);
                secEndDay = dayOfMonth;
            }
            endDateTxt.setText(getDateStr(year, (monthOfYear + 1), dayOfMonth));

        }
    };


    /**
     * 이벤트 연결
     */
    public void setEvent() {

        tabPsyBtn.setOnClickListener(this);
        tabBrnBtn.setOnClickListener(this);
        tabPrnBtn.setOnClickListener(this);

        startDateBtn.setOnClickListener(this);
        endDateBtn.setOnClickListener(this);
        searchBtn.setOnClickListener(this);

        mRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Intent intent = null;
                    JSONObject obj = jsonArr.getJSONObject(i - 2); //header 2's
                    if(obj.getString(CommonData.JSON_M_CODE_F).compareTo("LK01002") == 0){
                        intent = new Intent(getActivity(), PsyCheckImgResultActivity.class);
                    }else{
                        intent = new Intent(getActivity(), PsyCheckResultActivity.class);
                    }
                    intent.putExtra("objSeq", obj.getString("MJ_SEQ"));
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        // 상단에서 내리면 새로고침 동작
        mRefreshListView.setOnRefreshListener(refreshView -> {
            // TODO Auto-generated method stub
            mItem.clear();
            mAdapter.clear();

            GLog.i("상단에서 내릴때--->", "dd");

            if (mLkCode.compareTo("LK01") == 0) {
                firStartDate = getDateStr(firStartYear, firStartMonth, firStartDay);
                firEndDate = getDateStr(firEndYear, firEndMonth, firEndDay);
                requestPsyResultData(mLkCode, firStartDate, firEndDate);

            } else if(mLkCode.compareTo("LK02") == 0) {
                secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);
                requestPsyResultData(mLkCode, secStartDate, secEndDate);
            } else{
                secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);
                requestPsyResultData(mLkCode, secStartDate, secEndDate);
            }


        });

        // 리스트 마지막 아이템일 경우
        mRefreshListView.setOnLastItemVisibleListener(() -> {
            // TODO Auto-generated method stub
//            switch (mMode) {
//                case MODE_NORMAL: // 기본모드
//                    if (mPageId != 1 && mPageId <= mTotalPage) {    // 현재 페이지가 1이 아니거나, 전체 페이지수보다 적을경우 호출
//                    //    requestFaqList(mSearchEdit.getText().toString());
//                    }
//                    break;
//
//            }
        });


    }

    /**
     * 리스트뷰 연결
     */
    public void setListAdapter() {
        try {
            mAdapter = new PsyHistoryAdapter(getActivity(), mItem);


            mRefreshListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /**
     * headerView
     */
    public void addHeaderView() {

        mHeaderView = mLayoutInflater.inflate(R.layout.psy_history_search_header_view, null, false);

        startDateBtn = (LinearLayout) mHeaderView.findViewById(R.id.startDateBtn);
        endDateBtn = (LinearLayout) mHeaderView.findViewById(R.id.endDateBtn);
        startDateTxt = (TextView) mHeaderView.findViewById(R.id.startDateTxt);
        endDateTxt = (TextView) mHeaderView.findViewById(R.id.endDateTxt);
        searchBtn = (Button) mHeaderView.findViewById(R.id.searchBtn);
        emptyTxt = (TextView) mHeaderView.findViewById(R.id.emptyTxt);

        mRefreshListView.getRefreshableView().addHeaderView(mHeaderView);

    }


    public String getDateStr(int y, int m, int d) {
        String result = getIntToStringForDate(y)
                + "-" + getIntToStringForDate(m)
                + "-" + getIntToStringForDate(d);
        return result;
    }

    public void requestPsyResultData(String lkCode, String startDate, String endDate) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_DOCNO_F, CommonData.JSON_APINM_HP006);
            object.put(CommonData.JSON_CHL_SN_F, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_L_CODE_F, lkCode);
            object.put("MBER_SN", commonData.getMberSn());


            String pNy = CommonData.getInstance().getIamChild();
            if ("N".equals(pNy)){
                pNy = "Y";
            }else{
                pNy = "N";
            }
            object.put(CommonData.JSON_P_YN, pNy);
            object.put(CommonData.JSON_START_DATE_F, startDate);
            object.put(CommonData.JSON_END_DATE_F, endDate);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));
            RequestApi.requestApi(getActivity(), NetworkConst.NET_PSY_CHECK_RESULT_LIST, NetworkConst.getInstance().getPsyDomain(), networkListener, params, activity.getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {

            case R.id.tabPsyBtn:
                emptyTxt.setVisibility(View.GONE);
                mAdapter.clear();
                tabPsyBtn.setSelected(true);
                tabBrnBtn.setSelected(false);
                tabPrnBtn.setSelected(false);
                mLkCode = "LK01";
                firStartDate = getDateStr(firStartYear, firStartMonth, firStartDay);
                firEndDate = getDateStr(firEndYear, firEndMonth, firEndDay);

                startDateTxt.setText(firStartDate);
                endDateTxt.setText(firEndDate);
                requestPsyResultData(mLkCode, firStartDate, firEndDate);
           //     BleUtil.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                break;
            case R.id.tabBrnBtn:
                emptyTxt.setVisibility(View.GONE);
                mAdapter.clear();
                tabPsyBtn.setSelected(false);
                tabBrnBtn.setSelected(true);
                tabPrnBtn.setSelected(false);
                mLkCode = "LK02";
                secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);

                startDateTxt.setText(secStartDate);
                endDateTxt.setText(secEndDate);
                requestPsyResultData(mLkCode, secStartDate, secEndDate);
            //    BleUtil.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                break;

            case R.id.tabPrnBtn:
                emptyTxt.setVisibility(View.GONE);
                mAdapter.clear();
                tabPsyBtn.setSelected(false);
                tabBrnBtn.setSelected(false);
                tabPrnBtn.setSelected(true);
                mLkCode = "LK03";
                secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);

                startDateTxt.setText(secStartDate);
                endDateTxt.setText(secEndDate);
                requestPsyResultData(mLkCode, secStartDate, secEndDate);
                //    BleUtil.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                break;

            case R.id.startDateBtn:
                showDatePickerDialog(0); //Start
                break;
            case R.id.endDateBtn:
                showDatePickerDialog(1); //End
                break;
            case R.id.searchBtn:

                if (mLkCode.compareTo("LK01") == 0) {
                    firStartDate = getDateStr(firStartYear, firStartMonth, firStartDay);
                    firEndDate = getDateStr(firEndYear, firEndMonth, firEndDay);

                    int startDateInt = Integer.valueOf(firStartDate.replaceAll("-",""));
                    int endDateInt = Integer.valueOf(firEndDate.replaceAll("-",""));
                    if(startDateInt > endDateInt){




                        Toast.makeText(getActivity(), "출산예정일은 미래 날짜로 입력해주세요.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    emptyTxt.setVisibility(View.GONE);
                    mItem.clear();
                    requestPsyResultData(mLkCode, firStartDate, firEndDate);
                } else if(mLkCode.compareTo("LK02") == 0) {
                    secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                    secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);

                    int secStartDateInt = Integer.valueOf(firStartDate.replaceAll("-",""));
                    int secEndDateInt = Integer.valueOf(firEndDate.replaceAll("-",""));

                    if(secStartDateInt > secEndDateInt){
                        Toast.makeText(getActivity(), "출산예정일은 미래 날짜로 입력해주세요.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    emptyTxt.setVisibility(View.GONE);
                    mItem.clear();
                    requestPsyResultData(mLkCode, secStartDate, secEndDate);

                } else {
                    secStartDate = getDateStr(secStartYear, secStartMonth, secStartDay);
                    secEndDate = getDateStr(secEndYear, secEndMonth, secEndDay);

                    int secStartDateInt = Integer.valueOf(firStartDate.replaceAll("-",""));
                    int secEndDateInt = Integer.valueOf(firEndDate.replaceAll("-",""));

                    if(secStartDateInt > secEndDateInt){
                        Toast.makeText(getActivity(), "기출산예정일은 미래 날짜로 입력해주세요.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    emptyTxt.setVisibility(View.GONE);
                    mItem.clear();
                    requestPsyResultData(mLkCode, secStartDate, secEndDate);
                }


                break;
        }
    }

    public void showDatePickerDialog(int type) {
        DatePickerDialog dialog;
        if (type == 0) {//Start
            if (mLkCode.compareTo("LK01") == 0)
                dialog = new DatePickerDialog(getActivity(), startListener, firStartYear, firStartMonth - 1, firStartDay); //1탭
            else if(mLkCode.compareTo("LK02") == 0)
                dialog = new DatePickerDialog(getActivity(), startListener, secStartYear, secStartMonth - 1, secStartDay); //2탭
            else
                dialog = new DatePickerDialog(getActivity(), startListener, secStartYear, secStartMonth - 1, secStartDay); //3탭
        } else { //End
            if (mLkCode.compareTo("LK01") == 0)
                dialog = new DatePickerDialog(getActivity(), endListener, firEndYear, firEndMonth - 1, firEndDay); //1탭
            else if(mLkCode.compareTo("LK02") == 0)
                dialog = new DatePickerDialog(getActivity(), endListener, secEndYear, secEndMonth - 1, secEndDay); //2탭
            else
                dialog = new DatePickerDialog(getActivity(), endListener, secEndYear, secEndMonth - 1, secEndDay); //3탭
        }
        dialog.show();

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch (id) {
            case R.id.search_edit:    // 검색
                //     CommonView.getInstance().setClearImageBt(mSearchDelBtn, hasFocus);
                break;
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            activity.hideProgress();
            switch (type) {
                case NetworkConst.NET_PSY_CHECK_RESULT_LIST:

                    mRefreshListView.onRefreshComplete();

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:

                            if(!firstFlag){
                                Util.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                                firstFlag = true;
                            }

                            try {
                                if (resultData.getString("RESULT_CODE").compareTo("0000") == 0) {
                                    jsonArr = resultData.getJSONArray("DATA");
                                    mTotalPage = resultData.getInt("DATA_LENGTH");
                                    GLog.i("mTotalPage = " + mTotalPage, "dd");

                                    mItem.clear();

                                    if (jsonArr.length() > 0) {    // 리스트가 있을 경우
                                        emptyTxt.setVisibility(View.GONE);
                                        for (int i = 0; i < jsonArr.length(); i++) {
                                            JSONObject object = jsonArr.getJSONObject(i);
                                            PsyResultItem item = new PsyResultItem(
                                                    object.getString("M_NAME"),
                                                    object.getString("REGDATE"),
                                                    object.getString("P_YN"));
                                            mItem.add(item);
                                        }
                                    } else {  // 리스트 데이터가 없는 경우
                                        emptyTxt.setText(getString(R.string.psy_result_row_empty));
                                        emptyTxt.setVisibility(View.VISIBLE);

                                    }
                                    setListAdapter();
                                    mRefreshListView.onRefreshComplete();

                                } else {
                                    //코드 에러
                                    setListAdapter();
                                    emptyTxt.setText(getString(R.string.psy_result_row_empty));
                                    emptyTxt.setVisibility(View.VISIBLE);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mRefreshListView.onRefreshComplete();

                            break;
                    }

            }
            activity.hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            activity.hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            activity.hideProgress();
            dialog.show();

        }
    };


    @Override
    public void onStart() {
        super.onStart();
        ((PsyMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        PsyMainActivity activity = (PsyMainActivity) getActivity();
        switchFragment(new PsyMainFragment());
    }
}
