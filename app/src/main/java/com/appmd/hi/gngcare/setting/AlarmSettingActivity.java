package com.appmd.hi.gngcare.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.kyleduo.switchbutton.SwitchButton;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 * 알림설정 클래스
 * @since 0, 1
 */
public class AlarmSettingActivity extends BackBaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private ImageView mBackImg;
    private SwitchButton mPushCb_1, mPushCb_2, mPushCb_3, mPushCb_4, mPushCb_5, mPushCb_6, mBellCb, mVibCb;

    // 1 : 체온 측정, 2: 건강뉴스, 3: 열지도, 4 : 일반 공지 5 : 다이어트 공지 6 : 알림 공지
    private boolean mCurrentPush_1, mCurrentPush_2, mCurrentPush_3, mCurrentPush_4, mCurrentPush_5, mCurrentPush_6, mCurrentBell, mCurrentVib;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_setting_activity);

        setTitle(getString(R.string.setting_alarm));

        init();
        setEvent();

        // 변경하기전 비교값
        mCurrentPush_1 = commonData.getPushAlarm();
        mCurrentPush_2 = commonData.getNewsPushAlarm();
        mCurrentPush_3 = commonData.getMapPushAlarm();
        mCurrentPush_4 = commonData.getNoticePushAlarm();
        mCurrentPush_5 = commonData.getDietPushAlarm();
        mCurrentPush_6 = commonData.getNotityPushAlarm();
        mCurrentBell = commonData.getAlarmMode() == CommonData.PUSH_MODE_BELL || commonData.getAlarmMode() == CommonData.PUSH_MODE_BELL_VIBRATE ? true : false;
        mCurrentVib = commonData.getAlarmMode() == CommonData.PUSH_MODE_VIBRATE || commonData.getAlarmMode() == CommonData.PUSH_MODE_BELL_VIBRATE ? true : false;

        if( !mCurrentPush_1 && !mCurrentPush_2 && !mCurrentPush_3 && !mCurrentPush_4 && !mCurrentPush_5 && !mCurrentPush_6){
            mPushCb_1.setChecked(false);
            mPushCb_2.setChecked(false);
            mPushCb_3.setChecked(false);
            mPushCb_4.setChecked(false);
            mPushCb_5.setChecked(false);
            mPushCb_6.setChecked(false);
            mBellCb.setChecked(false);
            mVibCb.setChecked(false);

            mBellCb.setEnabled(false);
            mVibCb.setEnabled(false);
        }else{
            if(mCurrentPush_1)  // 체온 측정 알람
                mPushCb_1.setChecked(true);

            if(mCurrentPush_2)  // 건강뉴스 알람
                mPushCb_2.setChecked(true);

            if(mCurrentPush_3)  // 열지도 알람
                mPushCb_3.setChecked(true);

            if(mCurrentPush_4)  // 일반 공지 알람
                mPushCb_4.setChecked(true);

            if(mCurrentPush_5)  // 다이어트 독려 공지 알람
                mPushCb_5.setChecked(true);

            if(mCurrentPush_6)  // 알림게시판 공지 알람
                mPushCb_6.setChecked(true);

            if(mCurrentBell)  // 벨소리를 설정했다면
                mBellCb.setChecked(true);

            if(mCurrentVib) // 진동을 설정했다면
                mVibCb.setChecked(true);

        }

    }

    /**
     * 초기화
     */
    public void init(){

        mBackImg    =   getBackImg();

        mPushCb_1 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_1);
        mPushCb_2 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_2);
        mPushCb_3 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_3);
        mPushCb_4 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_4);
        mPushCb_5 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_5);
        mPushCb_6 =   (SwitchButton)  findViewById(R.id.push_alarm_cb_6);
        mBellCb     =   (SwitchButton)  findViewById(R.id.bell_cb);
        mVibCb      =   (SwitchButton)  findViewById(R.id.vibrate_cb);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mBackImg.setOnClickListener(this);
        mPushCb_1.setOnCheckedChangeListener(this);
        mPushCb_2.setOnCheckedChangeListener(this);
        mPushCb_3.setOnCheckedChangeListener(this);
        mPushCb_4.setOnCheckedChangeListener(this);
        mPushCb_5.setOnCheckedChangeListener(this);
        mPushCb_6.setOnCheckedChangeListener(this);
        mBellCb.setOnCheckedChangeListener(this);
        mVibCb.setOnCheckedChangeListener(this);
    }

    /**
     * 알림 설정 변경값이 있는지 체크
     * @return boolean ( true - 변경, false - 변경하지 않음 )
     */
    public boolean isChange(){
        boolean change = false;

        if(mCurrentPush_1 != mPushCb_1.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentPush_2 != mPushCb_2.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentPush_3 != mPushCb_3.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentPush_4 != mPushCb_4.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentPush_5 != mPushCb_5.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentPush_6 != mPushCb_6.isChecked()){    // 푸시설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentBell != mBellCb.isChecked()){    // 벨소리 설정값을 변경했다면
            change = true;
            return change;
        }

        if(mCurrentVib != mVibCb.isChecked()){  // 진동 설정값을 변경했다면
            change = true;
            return change;
        }



        return change;

    }

    /**
     * 알림 설정
     */
    public void requestAlarmSetting(String push_1, String fx_mth, String push_2, String push_3, String push_4,String push_5,String push_6){
        GLog.i("requestAlarmSetting", "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CALL_REG_FX_ADD);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());
            object.put(CommonData.JSON_FX_YN, push_1);
            object.put(CommonData.JSON_FX_MTH, fx_mth);
            object.put(CommonData.JSON_NEWS_YN, push_2);
            object.put(CommonData.JSON_HEAT_YN, push_3);
            object.put(CommonData.JSON_NOTICE_YN, push_4);
            object.put(CommonData.JSON_DIET_YN, push_5);
            object.put(CommonData.JSON_NOTITY_YN, push_6);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(AlarmSettingActivity.this, NetworkConst.NET_CALL_REG_FX_ADD, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }

    @Override
    public void onBackPressed() {

        if(isChange()){
            // 알림설정 api 호출하자
            String push_1;
            String push_2;
            String push_3;
            String push_4;
            String push_5;
            String push_6;
            String bell_or_vib;


            if(mPushCb_1.isChecked()){    // 체온 푸시알림 설정 유무
                push_1 = CommonData.YES;
            }else{
                push_1 = CommonData.NO;
            }

            if(mPushCb_2.isChecked()){    // 건강뉴스 푸시알림 설정 유무
                push_2 = CommonData.YES;
            }else{
                push_2 = CommonData.NO;
            }

            if(mPushCb_3.isChecked()){    // 열지도 푸시알림 설정 유무
                push_3 = CommonData.YES;
            }else{
                push_3 = CommonData.NO;
            }

            if(mPushCb_4.isChecked()){    // 일반 공지 푸시알림 설정 유무
                push_4 = CommonData.YES;
            }else{
                push_4 = CommonData.NO;
            }

            if(mPushCb_5.isChecked()){    // 일반 공지 푸시알림 설정 유무
                push_5 = CommonData.YES;
            }else{
                push_5 = CommonData.NO;
            }

            if(mPushCb_6.isChecked()){    // 일반 공지 푸시알림 설정 유무
                push_6 = CommonData.YES;
            }else{
                push_6 = CommonData.NO;
            }

            if(mBellCb.isChecked() && !mVibCb.isChecked()){
                GLog.i("벨소리 설정 완료", "dd");
                bell_or_vib = "1";
            }else if(mVibCb.isChecked() && !mBellCb.isChecked()){
                GLog.i("진동 설정 완료", "dd");
                bell_or_vib = "2";
            }else if(mVibCb.isChecked() && mBellCb.isChecked()) {
                GLog.i("벨 진동 모두 설정", "dd");
                bell_or_vib = "3";
            }else{
                GLog.i("무음", "dd");
                bell_or_vib = "0";
            }

            requestAlarmSetting(push_1, bell_or_vib, push_2, push_3, push_4, push_5, push_6);
        }else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.common_left_btn:  // 뒤로가기
                if(isChange()){

                    // 알림설정 api 호출하자
                    String push_1;
                    String push_2;
                    String push_3;
                    String push_4;
                    String push_5;
                    String push_6;
                    String bell_or_vib;


                    if(mPushCb_1.isChecked()){    // 체온 푸시알림 설정 유무
                        push_1 = CommonData.YES;
                    }else{
                        push_1 = CommonData.NO;
                    }

                    if(mPushCb_2.isChecked()){    // 건강뉴스 푸시알림 설정 유무
                        push_2 = CommonData.YES;
                    }else{
                        push_2 = CommonData.NO;
                    }

                    if(mPushCb_3.isChecked()){    // 열지도 푸시알림 설정 유무
                        push_3 = CommonData.YES;
                    }else{
                        push_3 = CommonData.NO;
                    }

                    if(mPushCb_4.isChecked()){    // 일반 공지 푸시알림 설정 유무
                        push_4 = CommonData.YES;
                    }else{
                        push_4 = CommonData.NO;
                    }

                    if(mPushCb_5.isChecked()){    // 다이어트 독려 설정 유무
                        push_5 = CommonData.YES;
                    }else{
                        push_5 = CommonData.NO;
                    }

                    if(mPushCb_6.isChecked()){    // 알림 게시판 설정 유무
                        push_6 = CommonData.YES;
                    }else{
                        push_6 = CommonData.NO;
                    }

                    if(mBellCb.isChecked() && !mVibCb.isChecked()){
                        GLog.i("벨소리 설정 완료", "dd");
                        bell_or_vib = "1";
                    }else if(mVibCb.isChecked() && !mBellCb.isChecked()){
                        GLog.i("진동 설정 완료", "dd");
                        bell_or_vib = "2";
                    }else if(mVibCb.isChecked() && mBellCb.isChecked()) {
                        GLog.i("벨 진동 모두 설정", "dd");
                        bell_or_vib = "3";
                    }else{
                        GLog.i("무음", "dd");
                        bell_or_vib = "0";
                    }

                    requestAlarmSetting(push_1, bell_or_vib, push_2, push_3, push_4, push_5, push_6);

                }else{
                    finish();
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        GLog.i("onCheckedChanged", "dd");
        int id = buttonView.getId();

        switch( id ){
            case R.id.push_alarm_cb_1:    // 체온 푸시알림
                mPushCb_1.setChecked(isChecked);
                setAlarmOnOff();
                break;
            case R.id.push_alarm_cb_2:    // 건강뉴스 푸시알림
                mPushCb_2.setChecked(isChecked);
                setAlarmOnOff();
                break;
            case R.id.push_alarm_cb_3:    // 열지도 푸시알림
                if(isChecked){
                    if(commonData.getAddressDo().length() > 0 && commonData.getAddressGu().length() > 0){
                        mPushCb_3.setChecked(isChecked);
                        setAlarmOnOff();
                    }else{
                        CustomAlertDialog customAlertDialog = new CustomAlertDialog(AlarmSettingActivity.this, CustomAlertDialog.TYPE_B);
                        customAlertDialog.setTitle(getString(R.string.app_name_kr));
                        customAlertDialog.setContent(getString(R.string.popup_setting_address));
                        customAlertDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog, button) -> {
                            mPushCb_3.setChecked(false);
                            dialog.dismiss();
                        });
                        customAlertDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                            Intent intent = new Intent(AlarmSettingActivity.this, SettingAddressActivity.class);
                            startActivityForResult(intent, CommonData.REQUEST_ADDRESS_SETTING);
                            dialog.dismiss();
                        });
                        customAlertDialog.show();
                    }
                }
                break;
            case R.id.push_alarm_cb_4:    // 일반공지 푸시알림
                mPushCb_4.setChecked(isChecked);
                setAlarmOnOff();
                break;
            case R.id.push_alarm_cb_5:    // 다이어트 독려 푸시알림
                mPushCb_5.setChecked(isChecked);
                setAlarmOnOff();
                break;
            case R.id.push_alarm_cb_6:    // 알림게시판 푸시알림
                mPushCb_6.setChecked(isChecked);
                setAlarmOnOff();
                break;
            case R.id.bell_cb:  // 벨소리
                mBellCb.setChecked(isChecked);
                break;
            case R.id.vibrate_cb:   // 진동
                mVibCb.setChecked(isChecked);
                break;
        }

    }

    public void setAlarmOnOff(){
        if(!mPushCb_1.isChecked() && !mPushCb_2.isChecked() && !mPushCb_3.isChecked() && !mPushCb_4.isChecked() && !mPushCb_5.isChecked() && !mPushCb_6.isChecked()){
            mBellCb.setChecked(false);
            mVibCb.setChecked(false);

            mBellCb.setEnabled(false);
            mVibCb.setEnabled(false);
        }else{
            mBellCb.setEnabled(true);
            mVibCb.setEnabled(true);
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_CALL_REG_FX_ADD:									// 알림 설정

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");

                            commonData.setPushAlarm(mPushCb_1.isChecked());
                            commonData.setNewsPushAlarm(mPushCb_2.isChecked());
                            commonData.setMapPushAlarm(mPushCb_3.isChecked());
                            commonData.setNoticePushAlarm(mPushCb_4.isChecked());
                            commonData.setDietPushAlarm(mPushCb_5.isChecked());
                            commonData.setNotityPushAlarm(mPushCb_6.isChecked());

                            if(mBellCb.isChecked() && mVibCb.isChecked()){  // 벨+진동
                                commonData.setAlarmMode(CommonData.PUSH_MODE_BELL_VIBRATE);
                            }else if(mBellCb.isChecked()){  // 벨
                                commonData.setAlarmMode(CommonData.PUSH_MODE_BELL);
                            }else if(mVibCb.isChecked()){   // 진동
                                commonData.setAlarmMode(CommonData.PUSH_MODE_VIBRATE);
                            }else {
                                commonData.setAlarmMode(0);
                            }

                            GLog.i("변경함", "dd");
                            setResult(RESULT_OK);
                            finish();

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:	// 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:	// 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.i("requestCode = " + requestCode, "dd");
        GLog.i("resultCode = " +resultCode, "dd");
        GLog.i("data = " +data, "dd");

        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != Activity.RESULT_OK){
            mPushCb_3.setChecked(false);
            return;
        }
    }

}
