package com.appmd.hi.gngcare.collection;

/**
 * Created by MobileDoctor on 2016-11-04.
 */

public class MarkerItem {
    double lat;
    double lon;
    double fever;

    public MarkerItem(double lat, double lon, double fever){
        this.lat = lat;
        this.lon = lon;
        this.fever = fever;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getPm25() {
        return fever;
    }
}
