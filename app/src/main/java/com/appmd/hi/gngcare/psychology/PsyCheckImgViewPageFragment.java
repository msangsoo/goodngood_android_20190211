package com.appmd.hi.gngcare.psychology;


import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class PsyCheckImgViewPageFragment extends Fragment {

    private View view;
    //    private Button firSelOneBtn;
//    private Button firSelTwoBtn;
//    private Button firSelThreeBtn;
//    private Button firSelFourBtn;
//    private Button firSelFiveBtn;
    private TextView firQTitleTxt;
    private ImageView firQueImgView;

    private TextView secQTitleTxt;

    private Handler handler;
    private AQuery aq;

    private ArrayList<Button> firSelBtnArr;
    private ArrayList<ImageView> secSelRadioArr;

    private final int firBtnResId[] = {R.id.firSelOneBtn
            , R.id.firSelTwoBtn, R.id.firSelThreeBtn
            , R.id.firSelFourBtn, R.id.firSelFiveBtn};

    private final int secImgResId[] = {R.id.secAnsImgView01
            , R.id.secAnsImgView02, R.id.secAnsImgView03
            , R.id.secAnsImgView04};


    private final int secRadioResId[] = {R.id.secRadio01, R.id.secRadio02, R.id.secRadio03, R.id.secRadio04
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = (View) inflater.inflate(R.layout.psy_check_img_viewpage, container, false);

        Bundle extra = getArguments();

        int position = extra.getInt("position");

//        Toast.makeText(getContext(), "selectedIndex : " + selectedIndex, Toast.LENGTH_SHORT).show();

        String sharedData = Util.getSharedPreference(getContext(), "psyCheckData");


        firSelBtnArr = new ArrayList<>();
        for (int i = 0; i < firBtnResId.length; i++) {
            firSelBtnArr.add((Button) view.findViewById(firBtnResId[i]));
        }

        secSelRadioArr = new ArrayList<>();
        for (int i = 0; i < secRadioResId.length; i++) {
            secSelRadioArr.add((ImageView) view.findViewById(secRadioResId[i]));
        }

        int selectedIndex = -1;

        try {
            JSONArray tmp = new JSONArray(sharedData);
            if (tmp.length() != 0 && tmp.getString(position).compareTo("-1") != 0) {
                selectedIndex = Integer.valueOf(tmp.getString(position));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        PsyHandlerParcel handlerParcel = extra.getParcelable("psyHandlerParcel");
        LinearLayout qTypeLy01 = (LinearLayout) view.findViewById(R.id.qTypeLy01);
        LinearLayout qTypeLy02 = (LinearLayout) view.findViewById(R.id.qTypeLy02);

        qTypeLy01.setVisibility(View.GONE);
        qTypeLy02.setVisibility(View.GONE);

        handler = handlerParcel.getHandler();
        aq = new AQuery(getContext());

        if (handlerParcel.getmQueImgYn().compareTo("Y") == 0) { //유형1(이미지 1개)

            qTypeLy01.setVisibility(View.VISIBLE);
            qTypeLy02.setVisibility(View.GONE);

            firQueImgView = (ImageView) view.findViewById(R.id.firQueImgView);
            firQTitleTxt = (TextView) view.findViewById(R.id.firQTitleTxt);
            int btnCnt = Integer.valueOf(handlerParcel.getmAnsCnt());

            firQTitleTxt.setText(handlerParcel.getmQueNum() + "." + handlerParcel.getmQueTitle());
            Glide.with(getContext()).load(handlerParcel.getmQueImg())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(firQueImgView);
//            aq.id(firQueImgView).image(handlerParcel.getmQueImg());

            for (int i = 0; i < firBtnResId.length; i++) {
                view.findViewById(firBtnResId[i]).setOnClickListener(listener);
                view.findViewById(firBtnResId[i]).setVisibility(View.GONE);
            }


            for (int i = 0; i < btnCnt; i++) {
                view.findViewById(firBtnResId[i]).setVisibility(View.VISIBLE);
                if (i == 0) {
                    firSelBtnArr.get(0).setText(handlerParcel.getmAnsTitle1());
                } else if (i == 1) {
                    firSelBtnArr.get(1).setText(handlerParcel.getmAnsTitle2());
                } else if (i == 2) {
                    firSelBtnArr.get(2).setText(handlerParcel.getmAnsTitle3());
                } else if (i == 3) {
                    firSelBtnArr.get(3).setText(handlerParcel.getmAnsTitle4());
                } else if (i == 4) {
                    firSelBtnArr.get(4).setText(handlerParcel.getmAnsTitle5());
                }

                if (selectedIndex != -1 && selectedIndex == i + 1) {
                    firSelBtnArr.get(i).setSelected(true);
                }


            }

        } else if (handlerParcel.getmQueImgYn().compareTo("N") == 0) {
            qTypeLy02.setVisibility(View.VISIBLE);
            qTypeLy01.setVisibility(View.GONE);

            secQTitleTxt = (TextView) view.findViewById(R.id.secQTitleTxt);
            secQTitleTxt.setText(handlerParcel.getmQueNum() + "." + handlerParcel.getmQueTitle());

            int btnCnt = Integer.valueOf(handlerParcel.getmAnsCnt());

            for (int i = 0; i < secImgResId.length; i++) {
                view.findViewById(secImgResId[i]).setOnClickListener(listener);
                view.findViewById(secImgResId[i]).setVisibility(View.GONE);
            }
            for (int i = 0; i < btnCnt; i++) {
                view.findViewById(secImgResId[i]).setVisibility(View.VISIBLE);

                if (i == 0) {
                    ImageView ansimg1 = view.findViewById(secImgResId[i]);
                    Glide.with(getContext()).load(handlerParcel.getmAnsImg1())
                            .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)).into(ansimg1);
//                    aq.id(view.findViewById(secImgResId[i])).image(handlerParcel.getmAnsImg1());
                } else if (i == 1) {
                    ImageView ansimg2 = view.findViewById(secImgResId[i]);
                    Glide.with(getContext()).load(handlerParcel.getmAnsImg2())
                            .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)).into(ansimg2);
//                    aq.id(view.findViewById(secImgResId[i])).image(handlerParcel.getmAnsImg2());
                } else if (i == 2) {
                    ImageView ansimg3 = view.findViewById(secImgResId[i]);
                    Glide.with(getContext()).load(handlerParcel.getmAnsImg3())
                            .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)).into(ansimg3);
//                    aq.id(view.findViewById(secImgResId[i])).image(handlerParcel.getmAnsImg3());
                } else if (i == 3) {
                    ImageView ansimg4 = view.findViewById(secImgResId[i]);
                    Glide.with(getContext()).load(handlerParcel.getmAnsImg4())
                            .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)).into(ansimg4);
//                    aq.id(view.findViewById(secImgResId[i])).image(handlerParcel.getmAnsImg4());
                }

                if (selectedIndex != -1 && selectedIndex == i + 1) {
                    secSelRadioArr.get(i).setSelected(true);
                }

            }



        }

        return view;

    }


    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            for (int i = 0; i < firBtnResId.length; i++) {
                firSelBtnArr.get(i).setSelected(false);
            }

            for (int i = 0; i < secImgResId.length; i++) {
                secSelRadioArr.get(i).setSelected(false);
            }

            if (v.getId() == firBtnResId[0] || v.getId() == secImgResId[0]) {
                firSelBtnArr.get(0).setSelected(true);
                secSelRadioArr.get(0).setSelected(true);

                handler.sendEmptyMessage(1);
            } else if (v.getId() == firBtnResId[1] || v.getId() == secImgResId[1]) {
                firSelBtnArr.get(1).setSelected(true);
                secSelRadioArr.get(1).setSelected(true);
                handler.sendEmptyMessage(2);
            } else if (v.getId() == firBtnResId[2] || v.getId() == secImgResId[2]) {
                firSelBtnArr.get(2).setSelected(true);
                secSelRadioArr.get(2).setSelected(true);
                handler.sendEmptyMessage(3);
            } else if (v.getId() == firBtnResId[3] || v.getId() == secImgResId[3]) {
                firSelBtnArr.get(3).setSelected(true);
                secSelRadioArr.get(3).setSelected(true);
                handler.sendEmptyMessage(4);
            } else if (v.getId() == firBtnResId[4]) {
                firSelBtnArr.get(4).setSelected(true);
                secSelRadioArr.get(4).setSelected(true);
                handler.sendEmptyMessage(5);
            }

        }
    };


}