package com.appmd.hi.gngcare.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.PsyMediaItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;

import java.util.ArrayList;


/**
 * Created by jihoon on 2016-04-18.
 * 성장 FAQ 목록 어댑터
 * @since 0, 1
 */
public class PsyMediaAdapter extends ArraySwipeAdapter<PsyMediaItem> {

    private LayoutInflater mInflater;
    private ArrayList<PsyMediaItem> mData;
    private Activity mContext;
    private Handler handler;

    private AQuery aq;
//    private PsyMediaAdapter.EventListener mListener;

    public PsyMediaAdapter(Activity context, ArrayList<PsyMediaItem> object, Handler handler){
        super(context, 0, object);
        this.handler = handler;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = object;
        mContext = context;
        aq = new AQuery( mContext );
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final PsyMediaItem item = mData.get(position);

        PsyMediaAdapter.ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.psy_media_list_row, null);


            holder      =   new PsyMediaAdapter.ViewHolder();

            holder.mThumbImg       =       (ImageView) convertView.findViewById(R.id.thumbImg);
            holder.mPlayBtn          =       (ImageView)  convertView.findViewById(R.id.playBtn);
            holder.mTit      =       (TextView)  convertView.findViewById(R.id.tit);
            holder.mCon        =       (TextView)  convertView.findViewById(R.id.con);
            holder.mWr        =       (TextView)  convertView.findViewById(R.id.wr);
            convertView.setTag(holder);

        }else{
            holder  =   (PsyMediaAdapter.ViewHolder)    convertView.getTag();
        }


        final int index = position;

        holder.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message msg = new Message();
                msg.obj = view;
                msg.what = index;
                handler.sendMessage(msg);
            }
        });


        if (item != null) {
            String mTit, mCon, mWr, mThumbImgUrl;
            mTit = item.getmTitle();
            mCon = item.getmContent();
            mWr = item.getmWr();
            mThumbImgUrl = item.getmImg();

            holder.mTit.setText(mTit);
            holder.mCon.setText(mCon);
            holder.mWr.setText(mWr);


//            aq.id( holder.mThumbImg ).image(mThumbImgUrl);
            Glide.with(getContext()).load(mThumbImgUrl)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(holder.mThumbImg);

        }
        return convertView;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder{
        ImageView mThumbImg;
        ImageView mPlayBtn;
        TextView mTit , mCon, mWr;
    }

}
