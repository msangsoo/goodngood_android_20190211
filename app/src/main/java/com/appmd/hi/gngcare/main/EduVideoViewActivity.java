package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
// import com.tsengvn.typekit.TypekitContextWrapper;


public class EduVideoViewActivity extends AppCompatActivity {

    WebView mEduVier;
    ImageButton mCommonLeftBtn;
    TextView common_title_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu_video_view);

        initView();
        initEvent();
        startView();
        Intent intent = getIntent();
        String title = intent.getStringExtra("TITLE").replaceAll("\n", "").trim();
        common_title_tv.setText(title);
    }

    protected void initView(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        mEduVier = (WebView)findViewById(R.id.edu_webview);
        mEduVier.getSettings().setJavaScriptEnabled(true);
        mEduVier.getSettings().setPluginState(WebSettings.PluginState. ON);
        mEduVier.setWebViewClient(new WebClient());
        mEduVier.setWebChromeClient(new WebChromeClient());

        common_title_tv = (TextView) findViewById(R.id.common_title_tv);
    }

    protected void initEvent(){
        mCommonLeftBtn.setOnClickListener(v -> finish());
    }

    protected void startView(){
        Intent i = getIntent();
        String url = i.getStringExtra(CommonData.JSON_INFO_TITLE_URL);

        mEduVier.loadUrl(url);
    }


    public class WebClient extends WebViewClient{
        private AlertDialog show;
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return true;
        }
    }

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}
