package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.data.PressureData;

public interface PresureDataProvider extends BarLineScatterCandleBubbleDataProvider {

    PressureData getPresureData();
}
