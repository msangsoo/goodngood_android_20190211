package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.appmd.hi.gngcare.util.GLog;
import com.viewpagerindicator.CirclePageIndicator;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;

/**
 * Created by jihoon on 2016-03-21.
 * 프롤로그 클래스
 * @since 0, 1
 */
public class PrologueActivity extends IntroBaseActivity implements View.OnClickListener{

    public static final int ITEM_COUNT  = 6;

    private ViewPager mViewPager;
    private CustomAdapter mViewPagerAdapter;
    private CirclePageIndicator mIndicator;

    private Button mStartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.prologue_activity);

        init();
        setEvent();

    }

    /**
     * 초기화
     */
    public void init(){

        mStartBtn       =   (Button)    findViewById(R.id.start_btn);

        mViewPager           =  (ViewPager) findViewById(R.id.login_viewpager);

        mViewPagerAdapter   =   new CustomAdapter(PrologueActivity.this);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);


        mViewPager.setAdapter(mViewPagerAdapter);
        mIndicator.setViewPager(mViewPager);


        mIndicator.setFillColor(ContextCompat.getColor(PrologueActivity.this, R.color.bg_blue_light));  // viewpager 현재 페이지 아이콘
        mIndicator.setPageColor(ContextCompat.getColor(PrologueActivity.this, R.color.color_100ffffff));  // viewpager 전체 페이지 아이콘


    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mStartBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;
        switch(v.getId()){
            case R.id.start_btn:    // 서비스 시작하기
                GLog.i("start_btn", "dd");
                CommonData.getInstance().setGuideCheck(true);

                intent = new Intent(PrologueActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

                break;
        }
    }


    /**
     * ViewPager 어댑터
     */
    public class CustomAdapter extends PagerAdapter {

        LayoutInflater mInflater;
        Context mContext;

        public CustomAdapter(Context context){
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return ITEM_COUNT;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = null;

            final ImageView mViewpagerImg;

            view = mInflater.inflate(R.layout.prologue_viewpager_item, null);

            mViewpagerImg =   (ImageView) view.findViewById(R.id.photo_img);

            switch(position){
                case 0: // 첫번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_01);
                    break;
                case 1: // 두번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_02);
                    break;
                case 2: // 세번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_03);
                    break;
                case 3: // 세번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_04);
                    break;
                case 4: // 네번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_05);
                    break;
                case 5: // 다섯번째 프롤로그 화면
                    mViewpagerImg.setImageResource(R.drawable.intro_06);
                    break;
            }

            ((ViewPager) container).addView(view, 0);

            return view;
        }
    }
}
