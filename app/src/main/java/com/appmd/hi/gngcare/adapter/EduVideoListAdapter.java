package com.appmd.hi.gngcare.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;

import com.appmd.hi.gngcare.collection.EduVideoItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.main.EduVideoViewActivity;

import java.util.ArrayList;

/**
 * Created by MobileDoctor on 2017-06-07.
 */

public class EduVideoListAdapter extends RecyclerView.Adapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext;
    private ArrayList<EduVideoItem> eduVideoItem;
    private DisplayMetrics dm;

    private OnLoadMoreListener onLoadMoreListener;

    int visibleThreshold = 1;
    int totalItemCount, lastVisibleItem;
    private boolean loading;
    private String mCode;

    public EduVideoListAdapter(Context _context, ArrayList<EduVideoItem> _eduVideoItem, RecyclerView recyclerView, String code){
        mContext = _context;
        eduVideoItem = _eduVideoItem;
        dm = mContext.getResources().getDisplayMetrics();
        mCode = code;

        if(recyclerView.getLayoutManager() instanceof GridLayoutManager){
            final GridLayoutManager gridLayoutManager = (GridLayoutManager)recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = gridLayoutManager.getItemCount();
                    lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();

                    if(!loading && totalItemCount <= (lastVisibleItem+visibleThreshold)){
                        if(onLoadMoreListener != null){
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.edu_list_item, parent, false);
        vh = new EduItemViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof EduItemViewHolder){
            EduVideoItem curItem = eduVideoItem.get(position);

            ((EduItemViewHolder)holder).txt_title.setText(curItem.getmTit());

         //   ((EduItemViewHolder)holder).txt_category.setText(mContext.getResources().getStringArray(R.array.bbs_category)[curItem.getContent_typ()]);

            CustomImageLoader.clearCache();
            CustomImageLoader.displayImageDefault(mContext, curItem.getmImg(), ((EduItemViewHolder)holder).img_thum);

            if(CommonData.getInstance().getBBSUrlShowCheck(curItem.getmIUrl()))
                ((EduItemViewHolder)holder).img_new.setVisibility(View.INVISIBLE);
            else
                ((EduItemViewHolder)holder).img_new.setVisibility(View.VISIBLE);
            ((EduItemViewHolder)holder).frame_item_lay.setOnClickListener(v -> {
                Intent i = new Intent(mContext, EduVideoViewActivity.class);
                i.putExtra(CommonData.JSON_INFO_TITLE_URL, curItem.getmIUrl());
                i.putExtra("TITLE", curItem.getmTit());
            //    CommonData.getInstance().setEduUrlShowCheck(curItem.getInfo_title_url(), true);
                mContext.startActivity(i);
            });



            //click 저장
            OnClickListener mClickListener = new OnClickListener(null,((EduItemViewHolder)holder).frame_item_lay,mContext);

            //아이 성장
            ((EduItemViewHolder)holder).frame_item_lay.setOnTouchListener(mClickListener);


            int j = position + 1;

            if(mCode.equals("2")) //코드 부여(아이 성장)
                ((EduItemViewHolder)holder).frame_item_lay.setContentDescription("HL04_007_00"+ j +"_!");
            else if(mCode.equals("1"))  //코드 부여(아이 심리)
                ((EduItemViewHolder)holder).frame_item_lay.setContentDescription("HL06_009_00"+ j +"_!");
            else //코드 부여(아이 체온)
                ((EduItemViewHolder)holder).frame_item_lay.setContentDescription("HL05_006_00"+ j +"_!");

        }
    }

    @Override
    public int getItemCount() {
        return eduVideoItem.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class EduItemViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout frame_item_lay;
        public ImageView img_thum, img_new;
        public TextView txt_title;
        public TextView txt_category;

        public EduItemViewHolder(View itemView) {
            super(itemView);

            frame_item_lay = (FrameLayout)itemView.findViewById(R.id.frame_item_lay);
            img_thum = (ImageView)itemView.findViewById(R.id.img_thum);
            img_new = (ImageView)itemView.findViewById(R.id.img_new);
            txt_title = (TextView)itemView.findViewById(R.id.txt_title);
            txt_category = (TextView)itemView.findViewById(R.id.txt_category);
        }
    }
}
