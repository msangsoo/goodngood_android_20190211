package com.appmd.hi.gngcare.greencare.googleFitness;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by cldlt on 2018-04-02.
 */

public interface IConnectResult {
    void success(GoogleApiClient client);
    void fail();
}
