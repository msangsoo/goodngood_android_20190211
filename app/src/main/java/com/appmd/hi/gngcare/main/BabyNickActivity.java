package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-04-06.
 * 애칭 변경 화면
 * @since 0, 1
 */
public class BabyNickActivity extends BackBaseActivity implements View.OnClickListener , View.OnFocusChangeListener{

    private ImageView mBackImg;
    private EditText mNickEdit;
    private ImageButton mNickDelBtn, mSaveBtn;

    private String mChangeNick; // 변경한 닉네임

    private Intent intent = null;
    // intent 로 넘겨 받은 값
    private String mNick, mChlSn;
    private int mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_nick_activity);

        setTitle(getString(R.string.baby_nick));

        init();
        setEvent();

        intent = getIntent();
        if(intent != null){
            mNick   =   intent.getStringExtra(CommonData.EXTRA_NICK);
            mChlSn  =   intent.getStringExtra(CommonData.EXTRA_CHL_SN);
            mPosition=  intent.getIntExtra(CommonData.EXTRA_CHILD_INDEX, 0);

            mNickEdit.setText(mNick);
        }

    }

    /**
     * 초기화
     */
    public void init(){
        mBackImg        =   getBackImg();
        mNickEdit       =   (EditText)          findViewById(R.id.nick_edit);
        mNickDelBtn     =   (ImageButton)       findViewById(R.id.nick_del_btn);
        mSaveBtn        =   (ImageButton)       findViewById(R.id.save_btn);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mBackImg.setOnClickListener(this);
        mNickDelBtn.setOnClickListener(this);
        mSaveBtn.setOnClickListener(this);
        mNickEdit.setOnFocusChangeListener(this);
    }

    /**
     * 자녀 애칭 변경 api
     * @param chldrn_nm 변경할 자녀 별칭
     */
    public void requestChangeChlNm(String chldrn_nm){
        GLog.i("requestChangeChlNm chldrn_nm = " +chldrn_nm, "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CHLDRNCM);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());
            object.put(CommonData.JSON_CHL_SN, mChlSn);
            object.put(CommonData.JSON_CHLDRN_NM, chldrn_nm);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(BabyNickActivity.this, NetworkConst.NET_CHLDRNCM, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.common_left_btn:  // 뒤로가기
                finish();
                break;
            case R.id.nick_del_btn: // 애칭 삭제
                commonView.setClearEditText(mNickEdit);
                break;
            case R.id.save_btn:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        mChangeNick = mNickEdit.getText().toString().trim();

        if(!mChangeNick.equals(mNick)){    // 변경된 닉네임이 존재하면서, 변경하기전 닉네임과 다르다면 변경 api 호출
            requestChangeChlNm(mChangeNick);
        }else{
            finish();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.nick_edit:    // 닉네임
                commonView.setClearImageBt(mNickDelBtn, hasFocus);
                break;
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_CHLDRNCM:	// 자녀 애칭 변경 완료
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_CHLDRNCM", "dd");

                            try {
                                String reg_yn = resultData.getString(CommonData.JSON_REG_YN);

                                if(reg_yn.equals(CommonData.YES)){
                                    mDialog =   new CustomAlertDialog(BabyNickActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_change_nick_complete));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            
                                            MainActivity.mChildMenuItem.get(mPosition).setmChldrnNcm(mChangeNick);   // 변경한 닉네임을 캐쉬에 업데이트

                                            MainActivity.mChangeChild   =   true;   // 화면 전환시 새로고침 할 수 있는 파라미터

                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    });
                                    mDialog.show();
                                }else{
                                    mDialog =   new CustomAlertDialog(BabyNickActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_change_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }

                            }catch(Exception e){
                                GLog.e(e.toString());
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };

}
