package com.appmd.hi.gngcare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.PsyImgResultItem;
import com.appmd.hi.gngcare.collection.PsyResultItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;

import java.util.ArrayList;


/**
 * Created by jihoon on 2016-04-18.
 * 성장 FAQ 목록 어댑터
 * @since 0, 1
 */
public class PsyImgResultAdapter extends ArraySwipeAdapter<PsyImgResultItem> {

    private LayoutInflater mInflater;
    private ArrayList<PsyImgResultItem> mData;
    private Activity mContext;
    private AQuery aq;

    public PsyImgResultAdapter(Activity context, ArrayList<PsyImgResultItem> object){
        super(context, 0, object);
        aq = new AQuery(context);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = object;
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final PsyImgResultItem item = mData.get(position);

        PsyImgResultAdapter.ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.psy_img_result_list_row, null);

            holder      =   new PsyImgResultAdapter.ViewHolder();
            holder.psyThumbImg = (ImageView) convertView.findViewById(R.id.psyThumbImg) ;
            holder.psyNoTxt      =       (TextView)  convertView.findViewById(R.id.psyNoTxt);
            holder.psySelTxt        =       (TextView)  convertView.findViewById(R.id.psySelTxt);
            holder.psyContentTxt        =       (TextView)  convertView.findViewById(R.id.psyContentTxt);

            convertView.setTag(holder);

        }else{
            holder  =   (PsyImgResultAdapter.ViewHolder)    convertView.getTag();
        }

        if (item != null) {
            String psyThumbStr, psyNoStr, psySelStr, psyContentStr;
            psyThumbStr = item.getmThumb();
            psyNoStr = item.getmNo();
            psySelStr = item.getmSel();
            psyContentStr = item.getmContent();

            holder.psyNoTxt.setText(psyNoStr + mContext.getString(R.string.psy_result_no_tail));
            holder.psySelTxt.setText(psySelStr);
            if(psySelStr.length() == 0)
                holder.psySelTxt.setText("좌측 이미지 선택");
            holder.psyContentTxt.setText(psyContentStr);
            Glide.with(getContext()).load(psyThumbStr)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)).into(holder.psyThumbImg);
//            aq.id( holder.psyThumbImg ).image(psyThumbStr);
        }
        return convertView;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder{
        ImageView psyThumbImg;
        TextView psyNoTxt, psySelTxt , psyContentTxt;
    }

}
