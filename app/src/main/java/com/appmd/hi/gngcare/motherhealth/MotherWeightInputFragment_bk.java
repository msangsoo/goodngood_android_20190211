package com.appmd.hi.gngcare.motherhealth;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.bluetooth.device.WeightDeviceScan;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.BluetoothManager;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.component.CDatePicker;
import com.appmd.hi.gngcare.greencare.component.CDialog;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_hedctdata;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.util.TextWatcherUtil;
import com.appmd.hi.gngcare.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class MotherWeightInputFragment_bk extends BaseFragment {
    private static final String TAG = MotherWeightInputFragment_bk.class.getSimpleName();
    private WeightDeviceScan mWeightDevice;
    private EditText weightEt;
    private ImageView useweightbtn;
    private CustomAlertDialog mDialog;
    private CDialog mWeightDialog;
    private int cal_year;
    private int cal_month;
    private int cal_day;
    private int cal_hour;
    private int cal_min;
    private TextView mDateTv, mTimeTv;

    public static Fragment newInstance() {
        MotherWeightInputFragment_bk fragment = new MotherWeightInputFragment_bk();
        return fragment;
    }

    private void setActionBar() {
        // CommonActionBar actionBar 는 안 씀 한화꺼
        if (getActivity() instanceof DummyActivity) {
            DummyActivity activity = (DummyActivity) getActivity();

            TextView titleTv = (TextView)activity.findViewById(R.id.common_title_tv);
            titleTv.setText(getString(R.string.mother_health_wt_mesure1));

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_mother_weight_input, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setActionBar();

        /*String today = CDateUtil.getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
        java.util.Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(today);
        cal_year = cal.get(Calendar.YEAR);
        cal_month = cal.get(Calendar.MONTH);
        cal_day = cal.get(Calendar.DAY_OF_MONTH);*/




        //String today = CDateUtil.getToday_yyyy_MM_dd();
        //today = CDateUtil.getFormat_yyyy_MM_dd_ko(today);

        //mDateTv = (TextView) view.findViewById(R.id.input_date_textview);

        //        weightEt.addTextChangedListener(watcher);
        //mDateTvSet(cal_year, cal_month, cal_day);


        mDateTv = (TextView) view.findViewById(R.id.weight_input_date_textview);
        mTimeTv = (TextView) view.findViewById(R.id.weight_input_time_textview);

        String now_time = CDateUtil.getForamtyyyyMMddHHmm(new Date(System.currentTimeMillis()));
        Calendar cal = CDateUtil.getCalendar_yyyy_MM_dd_HH_mm(now_time);
        cal_year = cal.get(Calendar.YEAR);
        cal_month = cal.get(Calendar.MONTH);
        cal_day = cal.get(Calendar.DAY_OF_MONTH);
        cal_hour = cal.get(Calendar.HOUR_OF_DAY);
        cal_min = cal.get(Calendar.MINUTE);

        mDateTvSet(cal_year, cal_month, cal_day);
        mTimeTvSet(cal_hour, cal_min);

        mDateTv.setOnTouchListener(mTouchListener);
        mTimeTv.setOnTouchListener(mTouchListener);

        mDateTv.addTextChangedListener(watcher);
        mTimeTv.addTextChangedListener(watcher);



        useweightbtn = (ImageView) view.findViewById(R.id.use_weight_btn);

        weightEt = (EditText) view.findViewById(R.id.mom_weight_et);
        weightEt.setTag("U");
        weightEt.setText(CommonData.getInstance().getMotherWeight());
        new TextWatcherUtil().setTextWatcher(weightEt, 130, 2);
        weightEt.addTextChangedListener(watcher);
//        weightEt.addTextChangedListener(watcher);
        view.findViewById(R.id.use_weight_device_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(), WeightDeviceScan.class);
//                startActivity(i);
                deviceAlert().show();
                mWeightDevice = new WeightDeviceScan(getContext(), weightEt);
            }
        });

        view.findViewById(R.id.show_result_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(), "체중 등록", Toast.LENGTH_SHORT).show();
                beforeRegistWeight();
            }
        });

        useweightbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent(getString(R.string.howtouseweight));
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
            }
        });



    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mWeightDialog != null)
                mWeightDialog.dismiss();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public CDialog deviceAlert() {
        if (mWeightDialog == null) {
            mWeightDialog = CDialog.showDlg(getContext(), R.layout.alert_weight_device_wait_progress);
            mWeightDialog.setOkButton("취  소", null);
            mWeightDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (mWeightDevice != null)
                        mWeightDevice.stopScan();
                }
            });
        }
        return mWeightDialog;
    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int vId = v.getId();
                if (vId == R.id.weight_input_date_textview) {
                    showDatePicker(v);
                } else if (vId == R.id.weight_input_time_textview) {
                    showTimePicker();
                }
            }
            return false;
        }
    };

    private boolean DateTimeCheck(String type, int pram1, int pram2, int pram3){
        Calendar cal = Calendar.getInstance();

        if(type.equals("D")){
            cal.set(Calendar.YEAR, pram1);
            cal.set(Calendar.MONTH, pram2);
            cal.set(Calendar.DAY_OF_MONTH, pram3);
            cal.set(Calendar.HOUR_OF_DAY, cal_hour);
            cal.set(Calendar.MINUTE, cal_min);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });
                return false;
            }else{
                return true;
            }
        }else{
            cal.set(Calendar.YEAR, cal_year);
            cal.set(Calendar.MONTH, cal_month);
            cal.set(Calendar.DAY_OF_MONTH, cal_day);
            cal.set(Calendar.HOUR_OF_DAY, pram1);
            cal.set(Calendar.MINUTE, pram2);

            if(cal.getTimeInMillis() > System.currentTimeMillis()){
                CDialog.showDlg(getContext(), getString(R.string.message_nowtime_over), new CDialog.DismissListener() {
                    @Override
                    public void onDissmiss() {

                    }
                });

                return false;
            }else{
                return true;
            }
        }
    }

    private void showDatePicker(View v) {
        GregorianCalendar calendar = new GregorianCalendar();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = mDateTv.getTag().toString();
        if (TextUtils.isEmpty(date) == false) {
            year = StringUtil.getIntVal(date.substring(0 , 4));
            month = StringUtil.getIntVal(date.substring(4 , 6))-1;
            day = StringUtil.getIntVal(date.substring(6 , 8));
        }

        new CDatePicker(getContext(), dateSetListener, year, month, day, false).show();
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(DateTimeCheck("D",year, monthOfYear, dayOfMonth)) {
                cal_year = year;
                cal_month = monthOfYear;
                cal_day = dayOfMonth;
                mDateTvSet(year, monthOfYear, dayOfMonth);
            }
        }

    };

    private void mDateTvSet(int year, int monthOfYear, int dayOfMonth){
        String msg = String.format("%d.%d.%d", year, monthOfYear + 1, dayOfMonth);
        String tagMsg = String.format("%d%02d%02d", year, monthOfYear + 1, dayOfMonth);
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear + 1);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        mDateTv.setText(msg+" "+ CDateUtil.getDateToWeek(tagMsg)+"요일");
        mDateTv.setTag(tagMsg);
    }

    private void showTimePicker() {
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        Date now = new Date();
        cal.setTime(now);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String time = mTimeTv.getTag().toString();
        if (TextUtils.isEmpty(time) == false) {
            hour = StringUtil.getIntVal(time.substring(0, 2));
            minute = StringUtil.getIntVal(time.substring(2 , 4));

            Logger.i(TAG, "hour="+hour+", minute="+minute);
        }

        TimePickerDialog dialog = new TimePickerDialog(getContext(), listener, hour, minute, false);
        dialog.show();
    }


    /**
     * 시간 피커 완료
     */
    private TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(DateTimeCheck("S",hourOfDay, minute, 0)) {
                cal_hour = hourOfDay;
                cal_min = minute;
                mTimeTvSet(hourOfDay, minute);
            }
        }
    };

    private void mTimeTvSet(int hourOfDay, int minute){
        // 설정버튼 눌렀을 때
        String amPm = getString(R.string.text_morning);
        int hour = hourOfDay;
        if (hourOfDay > 11) {
            amPm = getString(R.string.text_afternoon);
            if (hourOfDay >= 13)
                hour -= 12;
        } else {
            hour = hour == 0 ? 12 : hour;
        }
        String tagMsg = String.format("%02d%02d", hourOfDay, minute);
        String timeStr = String.format("%02d:%02d", hour, minute);
        mTimeTv.setText(amPm + " " + timeStr);
        mTimeTv.setTag(tagMsg);
    }

    private void beforeRegistWeight() {

        String weight = weightEt.getText().toString();

        if (TextUtils.isEmpty(weight)) {
            mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.noti));
            mDialog.setContent("몸무게를 입력해 주세요.");
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            //CDialog.showDlg(getContext(), "몸무게를 입력해 주세요.");
            return;
        }

        if (TextUtils.isEmpty(weight) == false) {
//            String bmitext = String.valueOf(Math.round((Float.parseFloat(weight) / (((Float.parseFloat(tall)) * (Float.parseFloat(tall))) / 10000)) * 10d) / 10d);
//            SparseArray<WeightModel> model = makeWeightModel(weight);

            if (StringUtil.getIntVal(weight) > 0)
                uploadWeight(Util.checkLastDot(weight, "."),mDateTv.getTag().toString(),mTimeTv.getTag().toString());
            else{
                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent("체중 정보가 없습니다.");
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                mDialog.show();
            }
                //Toast.makeText(getContext(), "체중 정보가 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 체중 데이터 입력
     *
     */
    public void uploadWeight(String weight, String regDate , String timeStr) {
        showProgress();

        regDate += timeStr;
        CommonData commonData = CommonData.getInstance();

        Tr_get_hedctdata.DataList data = new Tr_get_hedctdata.DataList();
        data.bmr = "0";
        data.bodywater = "0"     ;
        data.bone = "0"          ;
        data.fat = "0"           ;
        data.heartrate = "0"     ;
        data.muscle = "0"        ;
        data.obesity = "0"       ;
        data.weight = "" + weight;
        data.bdwgh_goal = "" + commonData.getMotherGoalWeight();//weightModel.getBdwgh_goal();


        data.idx = CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis()));
        data.regtype = weightEt.getTag().toString();
        data.reg_de = regDate;

        List<Tr_get_hedctdata.DataList> datas = new ArrayList<>();
        datas.add(data);

        new DeviceDataUtil().uploadWeight(this, datas, new BluetoothManager.IBluetoothResult() {
            @Override
            public void onResult(boolean isSuccess) {
                Logger.i(TAG, "DeviceDataUtil().uploadWeight.weight="+weight);
                hideProgress();

                commonData.setMotherWeight(weight);

                String msg = isSuccess ? "저장되었습니다." : "저장에 실패 했습니다.";

                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent(msg);
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                    @Override
                    public void onClick(CustomAlertDialog dialog, Button button) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                });
                mDialog.show();
            }
        });

    }

//    public SparseArray<WeightModel> makeWeightModel(String mWeight) {
//        SparseArray<WeightModel> array = new SparseArray<>();
//        WeightModel dataModel = new WeightModel();
//        dataModel.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(System.currentTimeMillis())));
//        dataModel.setWeight(StringUtil.getFloatVal(mWeight));                 // 체중
////        dataModel.setFat(StringUtil.getFloatVal(mWeightBodyRate));      // 체지방률
////        dataModel.setObesity(StringUtil.getFloatVal(mWeightinner));     // 내장지방
////        dataModel.setBodyWater(StringUtil.getFloatVal(mWeightWater));   // 수분
////        dataModel.setMuscle(StringUtil.getFloatVal(mWeightMuscle));     // 근육
////        dataModel.setBdwgh_goal(StringUtil.getFloatVal(weightTarget));     // 근육
//        dataModel.setBmr(0);
//        dataModel.setBone(0);
//        dataModel.setHeartRate(0);
//        String regType = TextUtils.isEmpty(weightEt.getTag().toString()) ? "U" : weightEt.getTag().toString();//U사용자입력, D 체중계입력
//        dataModel.setRegType(regType);
//        dataModel.setRegDate(CDateUtil.getToday_yyyy_MM_dd());
//
//        array.append(0, dataModel);
//
//        return array;
//    }

    @Override
    public void onPause() {
        super.onPause();
        if (mWeightDevice != null)
            mWeightDevice.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWeightDevice != null)
            mWeightDevice.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mWeightDevice != null)
            mWeightDevice.stopScan();

        super.onBackPressed();
    }
}