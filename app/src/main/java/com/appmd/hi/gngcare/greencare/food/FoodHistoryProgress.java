package com.appmd.hi.gngcare.greencare.food;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.util.DisplayUtil;
import com.appmd.hi.gngcare.greencare.util.StringUtil;


public class FoodHistoryProgress extends LinearLayout {
    private final String TAG = getClass().getSimpleName();

    private ProgressBar mProgressBar;
    private TextView mMaxTextView;
    private TextView mValTextView;
    private TextView mMarker;
    private LinearLayout mMarkerLayout;

    public FoodHistoryProgress(Context context) {
        super(context);
        initView();
    }

    public FoodHistoryProgress(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public FoodHistoryProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void initView() {
        View progressView = inflate(getContext(), R.layout.food_history_progress, null);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        addView(progressView, params);
        mProgressBar = progressView.findViewById(R.id.food_progress_bar);
        mValTextView = progressView.findViewById(R.id.food_val_textview);
        mMaxTextView = progressView.findViewById(R.id.food_max_val_textview);
        mMarker = progressView.findViewById(R.id.food_progress_marker);
        mMarkerLayout = progressView.findViewById(R.id.food_progress_marker_layout);

//        setMarkerPosition(mProgressBar.getProgress());
    }

    private void setMarkerPosition(int progress) {
        int padding= mProgressBar.getPaddingLeft() + mProgressBar.getPaddingRight();
        int sPos = mProgressBar.getLeft() + mProgressBar.getPaddingLeft();
        int xPos = (mProgressBar.getWidth()-padding) * mProgressBar.getProgress() / mProgressBar.getMax() + sPos - (mMarker.getWidth()/2);
        mMarker.setText(StringUtil.getFormatPrice(""+progress));
        Log.i(TAG, mProgressBar.getMeasuredWidth()+"*"+ mProgressBar.getProgress()+"/"+ mProgressBar.getMax() );
        Log.i(TAG, "thumbPos="+xPos);

        mMarker.setVisibility(View.VISIBLE);

        xPos = xPos - mMarker.getWidth() + DisplayUtil.getDpToPix(getContext(),31); // 보정값(일반 센터에 맞으면 필요 없음)
        mMarkerLayout.setX(xPos);
    }


    public void setProgress(int progress) {
        int kcal = new DeviceDataUtil().getPregnancyRecommendCal();
        mProgressBar.setMax(kcal);
        mMaxTextView.setText(StringUtil.exchangeAmountToStringUnit(""+kcal)+" kcal");

        mProgressBar.setProgress(progress);
        mValTextView.setText(StringUtil.exchangeAmountToStringUnit(""+progress) +"kcal");



//        if(progress <= 0){
//            mValTextView.setTextColor(getContext().getResources().getColor(R.color.color_gray5));
//        }else if(progress <= kcal){
//            mValTextView.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
//        }else{
//            mValTextView.setTextColor(getContext().getResources().getColor(R.color.colorRed));
//        }

        mMarker.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setMarkerPosition(progress);
            }
        }, 500);


    }
}
