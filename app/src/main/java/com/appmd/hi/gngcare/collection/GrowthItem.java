package com.appmd.hi.gngcare.collection;

/**
 * Created by jihoon on 2016-03-22.
 * 자녀 리스트 아이템
 * @since 0, 1
 */
public class GrowthItem {

    private String mGrowth_SN;
    private String mInput_De;
    private String mChldrn_Month;
    private String mInput_Height;
    private String mInput_Weight;
    private String mInput_Head;
    private String mCm_Result;
    private String mKg_Result;
    private String mHead_Result;

    /**
     * 육아 다이어리 리스트
     * @param growth_sn  고유번호
     * @param input_de   날짜
     * @param chldrn_month   생후 개월수
     * @param input_height   키
     * @param input_bdwgh   몸무게
     * @param input_headcm   머리둘레
     * @param cm_result   키 % 결과값
     * @param cm_result   몸무게 % 결과값
     * @param cm_result   머리둘레 % 결과값
     */
    public GrowthItem(String growth_sn,
                      String input_de,
                      String chldrn_month,
                      String input_height,
                      String input_bdwgh,
                      String input_headcm,
                      String cm_result,
                      String kg_result,
                      String head_result){
        this.mGrowth_SN     =   growth_sn;
        this.mInput_De      =   input_de;
        this.mChldrn_Month  =   chldrn_month;
        this.mInput_Height  =   input_height;
        this.mInput_Weight  =   input_bdwgh;
        this.mInput_Head    =   input_headcm;
        this.mCm_Result        =   cm_result;
        this.mKg_Result        =   kg_result;
        this.mHead_Result        =   head_result;
    }

    public String getmGrowth_SN() {
        return mGrowth_SN;
    }

    public void setmGrowth_SN(String mGrowth_SN) {
        this.mGrowth_SN = mGrowth_SN;
    }

    public String getmInput_De() {
        return mInput_De;
    }

    public void setmInput_De(String mInput_De) {
        this.mInput_De = mInput_De;
    }

    public String getmChldrn_Month() {
        return mChldrn_Month;
    }

    public void setmChldrn_Month(String mChldrn_Month) {
        this.mChldrn_Month = mChldrn_Month;
    }

    public String getmInput_Height() {
        return mInput_Height;
    }

    public void setmInput_Height(String mInput_Height) {
        this.mInput_Height = mInput_Height;
    }

    public String getmInput_Weight() {
        return mInput_Weight;
    }

    public void setmInput_Weight(String mInput_Weight) {
        this.mInput_Weight = mInput_Weight;
    }

    public String getmInput_Head() {
        return mInput_Head;
    }

    public void setmInput_Head(String mInput_Head) {
        this.mInput_Head = mInput_Head;
    }

    public String getmCm_Result() {
        return mCm_Result;
    }
    public void setmCm_Result(String mCm_Result) {
        this.mCm_Result = mCm_Result;
    }

    public String getmKg_Result() {
        return mKg_Result;
    }
    public void setmKg_Result(String mKg_Result) {
        this.mKg_Result = mKg_Result;
    }

    public String getmHead_Result() {
        return mHead_Result;
    }
    public void setmHead_Result(String mHead_Result) {
        this.mHead_Result = mHead_Result;
    }
}
