package com.appmd.hi.gngcare.greencare.food;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.CommonActionBar;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.util.DisplayUtil;
import com.appmd.hi.gngcare.util.GLog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodManageFragment extends BaseFragment {

    private TextView tvFoodWriting;
    private TextView tvFoodHistory;

    private FoodManageWriteView mRegistView;
    private FoodManageHistoryView mHistoryView;

    private View mVisibleView1;
    private View mVisibleView2;
    private View mVisibleView3;
    private View mVisibleView4;
    private View mVisibleView5;
    private View mVisibleView6;
    private ScrollView mContentScrollView;
    private LinearLayout mChartFrameLayout;
    private ImageView mChartCloseBtn, mChartZoomBtn;

    private CoordinatorLayout BreakfastCL;
    private CoordinatorLayout BreakfastDisertCL;
    private CoordinatorLayout LauchCL;
    private CoordinatorLayout LauchDisertCL;
    private CoordinatorLayout DinnerCL;
    private CoordinatorLayout DinnerDisertCL;

    public static Fragment newInstance() {
        FoodManageFragment fragment = new FoodManageFragment();
        return fragment;
    }

    @Override
    public void loadActionbar(CommonActionBar actionBar) {
    }

    @Nullable
    @Override//activity_food_manage
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_food_manage, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View registView = view.findViewById(R.id.layout_food_write);
        View historyView = view.findViewById(R.id.layout_food_history);


        // 등록하기 화면
        mRegistView = new FoodManageWriteView(FoodManageFragment.this, registView);
        // 히스토리 화면
        mHistoryView = new FoodManageHistoryView(FoodManageFragment.this, historyView);

        this.tvFoodHistory = (TextView) view.findViewById(R.id.tvFoodHistory);
        this.tvFoodWriting = (TextView) view.findViewById(R.id.tvFoodWriting);

        this.tvFoodWriting = (TextView) view.findViewById(R.id.tvFoodWriting);

        view.findViewById(R.id.tvFoodHistory).setOnClickListener(mClickListener);
        view.findViewById(R.id.tvFoodWriting).setOnClickListener(mClickListener);


        //초기 탭버튼 설정
        mRegistView.setVisibility(View.VISIBLE);
        mHistoryView.setVisibility(View.GONE);
        tvFoodWriting.setSelected(true);
        tvFoodHistory.setSelected(false);


//        setGoneHcall_btn();

        // 차트 전체 화면 처리
        mVisibleView1 = view.findViewById(R.id.visible_layout_1);
        mVisibleView2 = view.findViewById(R.id.visible_layout_2);
        mVisibleView3 = view.findViewById(R.id.visible_layout_3);
        mVisibleView4 = view.findViewById(R.id.visible_layout_4);
        mVisibleView5 = view.findViewById(R.id.visible_layout_5);
        mVisibleView6 = view.findViewById(R.id.radiogroup_period_type);
        mContentScrollView = view.findViewById(R.id.view_scrollview);
        mChartFrameLayout = view.findViewById(R.id.chart_frame_layout);
        mChartCloseBtn = view.findViewById(R.id.chart_close_btn);
        mChartZoomBtn = view.findViewById(R.id.landscape_btn);

        mChartZoomBtn.setOnClickListener(mClickListener);
        mChartCloseBtn.setOnClickListener(mClickListener);


        //원모양 처리
        BreakfastCL = view.findViewById(R.id.coordinatorLayout_1);
        BreakfastDisertCL = view.findViewById(R.id.coordinatorLayout_2);
        LauchCL = view.findViewById(R.id.coordinatorLayout_3);
        LauchDisertCL = view.findViewById(R.id.coordinatorLayout_4);
        DinnerCL = view.findViewById(R.id.coordinatorLayout_5);
        DinnerDisertCL = view.findViewById(R.id.coordinatorLayout_6);

        BreakfastCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        BreakfastDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        LauchCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        LauchDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        DinnerCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);
        DinnerDisertCL.getViewTreeObserver().addOnGlobalLayoutListener(myOnGlobalLayoutListener);

        setVisibleOrientationLayout();

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, getContext());

        //엄마 건강
        view.findViewById(R.id.tvFoodWriting).setOnTouchListener(ClickListener);
        view.findViewById(R.id.tvFoodHistory).setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        view.findViewById(R.id.tvFoodWriting).setContentDescription(getString(R.string.tvFoodWriting));
        view.findViewById(R.id.tvFoodHistory).setContentDescription(getString(R.string.tvFoodHistory));
    }

    ViewTreeObserver.OnGlobalLayoutListener myOnGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            ViewGroup.LayoutParams params = BreakfastCL.getLayoutParams();
            params.height = (int)(BreakfastCL.getMeasuredWidth());
            BreakfastCL.setLayoutParams(params);
            BreakfastCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params2 = BreakfastDisertCL.getLayoutParams();
            params2.height = (int)(BreakfastDisertCL.getMeasuredWidth());
            BreakfastDisertCL.setLayoutParams(params2);
            BreakfastDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params3 = LauchCL.getLayoutParams();
            params3.height = (int)(LauchCL.getMeasuredWidth());
            LauchCL.setLayoutParams(params3);
            LauchCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params4 = LauchDisertCL.getLayoutParams();
            params4.height = (int)(LauchDisertCL.getMeasuredWidth());
            LauchDisertCL.setLayoutParams(params4);
            LauchDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params5 = DinnerCL.getLayoutParams();
            params5.height = (int)(DinnerCL.getMeasuredWidth());
            DinnerCL.setLayoutParams(params5);
            DinnerCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            ViewGroup.LayoutParams params6 = DinnerDisertCL.getLayoutParams();
            params6.height = (int)(DinnerDisertCL.getMeasuredWidth());
            DinnerDisertCL.setLayoutParams(params6);
            DinnerDisertCL.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();

            if (vId == R.id.tvFoodHistory) {
                // 히스토리 화면
                mHistoryView.setVisibility(View.VISIBLE);
                mRegistView.setVisibility(View.GONE);
//                Hcallbtn.setVisibility(View.VISIBLE);
                tvFoodWriting.setSelected(false);
                tvFoodHistory.setSelected(true);

                mHistoryView.getData();
            } else if (vId == R.id.tvFoodWriting) {
                // 기록하기 화면
                mRegistView.setVisibility(View.VISIBLE);
                mHistoryView.setVisibility(View.GONE);
//                Hcallbtn.setVisibility(View.GONE);
                tvFoodWriting.setSelected(true);
                tvFoodHistory.setSelected(false);

                mHistoryView.getData();
            } else if(vId == R.id.landscape_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else if(vId == R.id.chart_close_btn) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    };

    private boolean isLandScape = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        GLog.i("onConfigurationChanged="+newConfig.orientation, "");
        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE: //가로 모드
                isLandScape = true;
                break;
            case Configuration.ORIENTATION_PORTRAIT: //세로 모드
                isLandScape = false;
                break;
        }

        setVisibleOrientationLayout();
    }

    /**
     * 가로, 세로모드일때 불필요한 화면 Visible 처리
     */
    private void setVisibleOrientationLayout() {
        mVisibleView1.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView2.setVisibility(isLandScape ? View.INVISIBLE : View.VISIBLE);
        mVisibleView3.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView4.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView5.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mVisibleView6.setVisibility(isLandScape ? View.GONE : View.VISIBLE);
        mChartCloseBtn.setVisibility(isLandScape ? View.VISIBLE : View.GONE);
        mChartZoomBtn.setVisibility(isLandScape ? View.GONE : View.VISIBLE);

        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mChartFrameLayout.getLayoutParams();
        android.util.Log.i(TAG, "isLandScape="+isLandScape+", dm.widthPixels="+dm.widthPixels+", dm.heightPixels="+dm.heightPixels );

//        int height = (int) (dm.heightPixels - mDateLayout.getLayoutParams().height);//(dm.heightPixels *0.20)); // 15% 작게
        int landHeight = (int) (dm.heightPixels - dm.heightPixels * 0.30); // 가로모드 세로사이즈 30% 작게
        int portHeight = DisplayUtil.getDpToPix(getContext(), 230);    // 세로모드일때 사이즈 230dp
        params.height = isLandScape ? landHeight : portHeight;

        mChartFrameLayout.setLayoutParams(params);
        // 가로모드일때 스크롤뷰 막기
        mContentScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isLandScape;
            }
        });
        //가로모드 전환 시 스크롤 상단으로 위치
        mContentScrollView.smoothScrollTo(0,0);
    }

    /**
     * 원형 이미지 만들기
     * @param drawable
     * @param iv
     */
    private void setCircleImage(Drawable drawable, ImageView iv) {
        Glide.with(getActivity())
                .load("")
                .apply(new RequestOptions().circleCrop().centerCrop())
                .into(iv);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mRegistView != null && mRegistView.getVisibility() == View.VISIBLE) {
            mRegistView.onResume();
        }

    }
}
