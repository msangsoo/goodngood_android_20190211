package com.appmd.hi.gngcare.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.greencare.base.CommonActionBar;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.component.FullImageFragment;

public class MedicareActivity extends BackBaseActivity {

    ImageButton[] mBtnMediTabs;
    ImageButton mBtnSearch, mBtnMedicare, mCommonLeftBtn;
    LinearLayout mMediContents, mBottomLay;
    ScrollView mMediScroll;
     private LinearLayout btnmeditab6,btnmeditab5,btnmeditab4,btnmeditab3,btnmeditab2,btnmeditab1;


    int[] mImgTab = {R.drawable.btn_medicare_01, R.drawable.btn_medicare_02, R.drawable.btn_medicare_03, R.drawable.btn_medicare_04};
    int[] mImgTabSel = {R.drawable.btn_medicare_01_sel, R.drawable.btn_medicare_02_sel, R.drawable.btn_medicare_03_sel, R.drawable.btn_medicare_04_sel};

    int[] mImgContents = {R.drawable.btn_medicare_01_text, R.drawable.btn_medicare_02_text, R.drawable.btn_medicare_03_text, R.drawable.btn_medicare_04_text, R.drawable.btn_medicare_05_text, R.drawable.btn_medicare_06_text};

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicare);


        init();

    }

    protected void init(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);

       findViewById(R.id.btn_medi_tab_6).setOnClickListener(mOnClickListener);
       findViewById(R.id.btn_medi_tab_5).setOnClickListener(mOnClickListener);
       findViewById(R.id.btn_medi_tab_4).setOnClickListener(mOnClickListener);
       findViewById(R.id.btn_medi_tab_3).setOnClickListener(mOnClickListener);
       findViewById(R.id.btn_medi_tab_2).setOnClickListener(mOnClickListener);
       findViewById(R.id.btn_medi_tab_1).setOnClickListener(mOnClickListener);


    }
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
    @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btn_medi_tab_1:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT1));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[0]);
                    bundle.putInt("Type", 1);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.btn_medi_tab_2:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT2));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[1]);
                    bundle.putInt("Type", 2);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.btn_medi_tab_3:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT3));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[2]);
                    bundle.putInt("Type", 3);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.btn_medi_tab_4:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT4));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[3]);
                    bundle.putInt("Type", 4);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.btn_medi_tab_5:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT5));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[4]);
                    bundle.putInt("Type", 5);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.btn_medi_tab_6:
                    bundle = new Bundle();
                    bundle.putString("Title", getString(R.string.main_serviceT6));
                    bundle.putInt(FullImageFragment.DRAWABLE_IMG, mImgContents[5]);
                    bundle.putInt("Type", 6);
                    DummyActivity.startActivity(MedicareActivity.this, FullImageFragment.class, bundle);
                    break;
                case R.id.common_left_btn:
                    finish();
                    break;
            }

        }
    };

    /*protected void setTab(int type){
        mMediContents.removeAllViews();
        for(int i = 0; i < 4; i++){
            if(type == i){
                mBtnMediTabs[i].setImageResource(mImgTabSel[i]);
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setAdjustViewBounds(true);
                imageView.setImageResource(mImgContents[i]);
                mMediContents.addView(imageView);
            }else{
                mBtnMediTabs[i].setImageResource(mImgTab[i]);
            }
        }
        if( type == 0 ){
            mBottomLay.setVisibility(View.VISIBLE);
            mBtnSearch.setVisibility(View.GONE);
            mBtnMedicare.setVisibility(View.VISIBLE);
        }else if(type == 3){
            mBottomLay.setVisibility(View.VISIBLE);
            mBtnSearch.setVisibility(View.VISIBLE);
            mBtnMedicare.setVisibility(View.GONE);
        }
        else
            mBottomLay.setVisibility(View.GONE);

        mMediScroll.smoothScrollTo(0,0);
        mMediScroll.smoothScrollTo(0,0);
    }*/

    /*@Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_medi_tab_1:
                setTab(0);
                break;
            case R.id.btn_medi_tab_2:
                setTab(1);
                break;
            case R.id.btn_medi_tab_3:
                setTab(2);
                break;
            case R.id.btn_medi_tab_4:
                setTab(3);
                break;
            case R.id.btn_search:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_higngkids))));
                break;
            case R.id.btn_medicare:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_medicare))));
                break;
            case R.id.common_left_btn:
                finish();
                break;
        }
    }

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }*/
}
