package com.appmd.hi.gngcare.diary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.GrowthListAdapter;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.collection.GrowthItem;
import com.appmd.hi.gngcare.collection.GrowthRecordMonthGraphItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import lecho.lib.hellocharts.formatter.SimpleAxisValueFormatter;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by jihoon on 2016-03-21.
 * 성장기록 클래스
 *
 * @since 0, 1
 */
public class GrowthRecordLastHeightFragment extends BaseFragment implements GrowthMainActivity.onKeyBackPressedListener {

    //그래프
    private LineChartView chart;
    private LineChartData data;
    private int numberOfLines = 4;
    private int maxNumberOfLines = 4;
    private int numberOfPoints;

    float[][] randomNumbersTab;

    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLines = true;
    private boolean hasPoints = false;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean isFilled = false;
    private boolean hasLabels = false;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;
    private int mDifference , mDifference2;
    private boolean mPointFlag = false;
    // 공통
    private boolean mAgeFlag = false; // false =  24개월 미만 /  true = 24개월 이상
    int mType = 0; // 0 = 키 , 1 = 몸무게 , 2 = 머리둘레
    boolean mTypeFlag = false; // false = 그래프 , true = 도표
    boolean mFirstFlag = false; // false = 최초 , ture = 최초 외
    boolean mDataFlag = false; // false = 데이터가 없을시 , true = 데이터가 있을 시
    private View view;
    double mTwoDateCompare;
    private float mHeightMax = 0f , mHeightMin = 999f  , mWeightMax = 0f , mWeightMin = 999f , mHeadMax = 0f , mHeadMin = 999f;
    private TextView mUnitTv , mUnitTv2;
    // 도표
    private ArrayList<GrowthItem> mGrowthListItem = new ArrayList<GrowthItem>();
    private ArrayList<GrowthRecordMonthGraphItem> mGrowthGraphListItem = new ArrayList<GrowthRecordMonthGraphItem>();
    private GrowthListAdapter mAdapter;
    private PullToRefreshListView mPullListView;
    private ListView mListView;

    private int mPageId = 1;    // 현재 보고있는 페이지
    private int mTotalPage = 1;// 전체 페이지
    private int mDelPosition;
    private String mYear, mMonth, mDay;
    // 성장 정보가 없을 경우
    private LinearLayout mDefaultLay;

    // 성장 정보가 있을 경우
    private LinearLayout mBtnLay; // 하단 버튼 레이아웃
    private RelativeLayout mDataLay; // 도표&그래프 레이아웃
    // 그래프 관련
    private RelativeLayout mGraphLay;
    // 도표 관련
//    LinearLayout mDiagramLay;
//    TextView mTypeTv; // 키 몸무게 머리둘레

    // 그래프&도표 공용
    private Button mGrahphBtn, mDiagramBtn; // 상단 버튼
    private ImageView mGraphImg, mTimelineImg;
    // 성장 정보 입력 버튼
    private ImageButton mGrowthrecordlastheightBtn;
    private int mTotalMonth;
    // header view
    private View mHeaderView;
    private TextView mHeaderTypeTv , mEtcTv;

    private boolean mDelFlag = false;
    private boolean mHeadFlag = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.growth_record_lastheight_fragment, null);
        init(view);
        setEvent();

        // 헤더뷰
        addHeaderView();
        mListView.addHeaderView(mHeaderView);

        return view;
    }

    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {

        chart = (LineChartView) view. findViewById(R.id.chart);
        chart.setZoomEnabled(false);
        chart.setViewportCalculationEnabled(false);
        chart.setVisibility(View.INVISIBLE);
        mDefaultLay = (LinearLayout) view.findViewById(R.id.default_lay); // 성장 정보 없을 시
        mDataLay = (RelativeLayout) view.findViewById(R.id.data_lay); // 성장 정보 있을 시 전체 레이아웃
        mGraphLay = (RelativeLayout) view.findViewById(R.id.graph_lay); // 성장 정보 있을 시 그래프 레이아웃
//        mDiagramLay     =       (LinearLayout)  view.   findViewById(R.id.diagram_lay); // 성장 정보 있을 시 도표 레이아웃
        mBtnLay = (LinearLayout) view.findViewById(R.id.btn_lay); // 성장 정보 있을 시 하단 버튼 레이아웃

        // 성장 정보 있을 시 공통 버튼
        mGrahphBtn = (Button) view.findViewById(R.id.graph_btn); // 그래프
        mDiagramBtn = (Button) view.findViewById(R.id.diagram_btn); // 도표
        mGrowthrecordlastheightBtn = (ImageButton) view.findViewById(R.id.growthlastheightrecord_btn); // 성장 정보 입력

        mEtcTv          =       (TextView)       view.    findViewById(R.id.etc_tv); // 도표 리스트에 뿌려지는 키/몸무게/머리둘레
        mUnitTv         =       (TextView)       view.    findViewById(R.id.unit_tv);
        mUnitTv2        =       (TextView)       view.    findViewById(R.id.unit_tv2);
        mGraphImg = (ImageView)view.findViewById(R.id.graph_img);
        mTimelineImg = (ImageView)view.findViewById(R.id.timeline_img);

        mPullListView = (PullToRefreshListView) view.findViewById(R.id.diagram_listview);

        mListView = mPullListView.getRefreshableView();
        mListView.setDivider(null);

        GLog.i("size- --> " + MainActivity.mChildMenuItem.size(), "dd");

        if (MainActivity.mChildMenuItem.size() < 1) {
            mDialog = new CustomAlertDialog(getActivity(), CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
            mDialog.setContent(getString(R.string.popup_dialog_no_child_register));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();

            mDefaultLay.setVisibility(View.VISIBLE);
            mDataLay.setVisibility(View.GONE);
            mGrowthrecordlastheightBtn.setEnabled(false);
        } else {
            Date imsiDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
            int year = imsiDate.getYear() + 1900;
            int month = imsiDate.getMonth()+1;
            int day = imsiDate.getDate();

            String imsiBirth = "" + year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
            GLog.i("imsiBirth = " +imsiBirth, "dd");


            String childebirth = Util.getDateSpecialCharacter(getActivity(), imsiBirth, 4);
            mTwoDateCompare = Util.getTwoDateCompare(childebirth , CommonData.PATTERN_DATE);
            GLog.i("mTwoDateCompare  -->   " + mTwoDateCompare, "dd");
            if (mTwoDateCompare <= 730){
                mAgeFlag = false;
                mEtcTv.setVisibility(View.INVISIBLE);
            } else {
                mAgeFlag = true;
                mEtcTv.setVisibility(View.VISIBLE);
            }

            GLog.i("!!!!!!!!!!!! " + childebirth, "dd");

            // 자녀의 생일과 현재날짜의 개월수 차이구하기
            long now = System.currentTimeMillis();
            Date date = new Date(now);
            SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMMdd");
            String nowDate = CurDateFormat.format(date);
            int childeYear = Integer.parseInt(childebirth.substring(0, 4));
            String childeMonth = Util.getTwoDateFormat(Integer.parseInt(childebirth.substring(5, 7)));
            int nowYear = Integer.parseInt(nowDate.substring(0, 4));
            String nowMonth = Util.getTwoDateFormat(Integer.parseInt(nowDate.substring(4, 6)));

            GLog.i("아이생일달" + childeYear + childeMonth, "dd");
            GLog.i("현재달" + nowYear + nowMonth, "dd");

            mTotalMonth = (nowYear - childeYear)* 12 + (Integer.parseInt(nowMonth) - Integer.parseInt(childeMonth));
            GLog.i("아이생일과 현재날짜 개월수 차이 --> " + mTotalMonth, "dd");
            requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
        }


    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {

        view.findViewById(R.id.growthrecord_btn).setOnClickListener(btnListener);
        view.findViewById(R.id.graph_btn).setOnClickListener(btnListener);
        view.findViewById(R.id.diagram_btn).setOnClickListener(btnListener);

        // 상단에서 내리면 새로고침 동작
        mPullListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                // TODO Auto-generated method stub
                mGrowthListItem.clear();
                mPageId = 1;
                GLog.i("상단에서 내릴때--->", "dd");

                requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
            }
        });

        // 리스트 마지막 아이템일 경우
        mPullListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                // TODO Auto-generated method stub
                if (mPageId != 1 && mPageId <= mTotalPage) {    // 현재 페이지가 1이 아니거나, 전체 페이지수보다 적을경우 호출
                    requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
                }
            }
        });

    }

    /**
     * 도표 headerView
     */
    public void addHeaderView() {

        mHeaderView = mLayoutInflater.inflate(R.layout.growth_diagram_header_view, null, false);
        mHeaderTypeTv = (TextView) mHeaderView.findViewById(R.id.type_tv);
    }

    /**
     * 리스트뷰 연결
     *
     * @param type     키, 몸무게, 머리둘레 관련 타입 0 = 키 , 1 = 몸무게 , 2 = 머리둘레
     * @param typeflag 그래프 도표 관련 플래그 false = 그래프 , true = 도표
     */
    public void setListAdapter(int type, boolean typeflag, boolean firstflag) {
        if (typeflag) {
            mPullListView.setVisibility(View.VISIBLE);
            mAdapter = new GrowthListAdapter(getActivity(), mGrowthListItem, type, typeflag, new GrowthListAdapter.EventListener() {
                @Override
                public void delete(final int position) {
                    mYear = mGrowthListItem.get(position).getmInput_De().substring(2, 4);
                    mMonth = mGrowthListItem.get(position).getmInput_De().substring(4, 6);
                    mDay = mGrowthListItem.get(position).getmInput_De().substring(6, 8);

                    mDialog = new CustomAlertDialog(getActivity(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(mYear + CommonData.STRING_SLASH + mMonth + CommonData.STRING_SLASH + mDay + getString(R.string.popup_dialog_growth_delete_content));
                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {

                        @Override
                        public void onClick(CustomAlertDialog dialog, Button button) {
                            requestGrowthRecordDelApi(mGrowthListItem.get(position).getmGrowth_SN() , mType);
                            mDelPosition = position;
                            dialog.dismiss();
                        }
                    });
                    mDialog.show();
                }
            });
            mPullListView.setAdapter(mAdapter);

        } else {
            mPullListView.setVisibility(View.GONE);
        }
    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            switch (v.getId()) {
                case R.id.growthrecord_btn:   // 성장 정보 입력
                    intent = new Intent(getContext(), GrowthRecordInPut.class);
                    getActivity().startActivityForResult(intent, CommonData.REQUEST_CODE_INPUT);
                    Util.BackAnimationStart(getActivity());
                    break;
                case R.id.graph_btn:
                    if (mDelFlag){
                        requestGrowthGraphApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), CommonData.GRAPH_PAGENUMBER);
                        mDelFlag = false;
                        mTypeFlag = false;
                    }else {
                        mTypeFlag = false;
                        setListAdapter(mType, mTypeFlag, mFirstFlag);
                        TypeEdit(mTypeFlag, mFirstFlag);
                    }
                    mGraphImg.setVisibility(View.VISIBLE);
                    mTimelineImg.setVisibility(View.INVISIBLE);
                    break;
                case R.id.diagram_btn:
                    mTypeFlag = true;
                    setListAdapter(mType, mTypeFlag, mFirstFlag);
                    TypeEdit( mTypeFlag, mFirstFlag);
                    mGraphImg.setVisibility(View.INVISIBLE);
                    mTimelineImg.setVisibility(View.VISIBLE);
                    if(!CommonData.getInstance().getGrowthTimeLineHelp()){
                        GrowthMainActivity activity = (GrowthMainActivity)getActivity();
                        activity.swichHelpLay();
                    }
                    break;
            }
        }
    };

    /**
     * 그래프 & 도표 관련 화면 UI
     * @param typeflag 그래프 도표 관련 플래그 false = 그래프 , true = 도표
     * @param firstflag true = 최초 화면 진입시 false = 아닐시
     */
    public void TypeEdit(boolean typeflag, boolean firstflag) {
        // 데이터가 있고 처음 들어왔을 때
        GLog.i("typeflag--> " + typeflag, "dd");
        GLog.i("firstflag--> " + firstflag, "dd");

        if (!firstflag) {
            mDefaultLay.setVisibility(View.GONE);
            mDataLay.setVisibility(View.VISIBLE);
            mGraphLay.setVisibility(View.VISIBLE);
//            mDiagramLay.setVisibility(View.GONE);
            mHeaderView.setVisibility(View.GONE);
            mBtnLay.setVisibility(View.VISIBLE);
            mFirstFlag = true;
            requestGrowthGraphApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), CommonData.GRAPH_PAGENUMBER);

        } else {
            // 그래프 일 때
            if (!typeflag) {
                mGraphLay.setVisibility(View.VISIBLE);
                mHeaderView.setVisibility(View.GONE);
//                mDiagramLay.setVisibility(View.GONE);

            } else {
                mGraphLay.setVisibility(View.GONE);
//            mDiagramLay.setVisibility(View.VISIBLE);
                mHeaderView.setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * 그래프에 표시될 데이터 담는 부분
     * @param dataflag true= 데이터가 있을 시 , false = 없을 시
     *//*
    private void generateValues(boolean dataflag) {
        GLog.i("dataflag ---> " + dataflag);
        mPointFlag = false;

        int imsi = mDifference - 24;

        if (dataflag){
            maxNumberOfLines = 4;
        }else {
            maxNumberOfLines = 3;
        }

        if (dataflag && imsi > 0) {
            GLog.i("imsi --> " + imsi);
            GLog.i("mGrowthGraphListItem.size() --> " + mGrowthGraphListItem.size());

            if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) {

                for (int i = imsi; i < mGrowthGraphListItem.size(); i++) {
                    if (mHeightMin > Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(i).getmValue())) {
                        mHeightMin = (int) Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(i).getmValue());
                    }
                    if (mWeightMin > Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(i).getmValue())) {
                        mWeightMin = (int) Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(i).getmValue());
                    }
                    if (81 > i) {
                        if (mHeadMin > Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(i).getmValue())) {
                            mHeadMin = (int) Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(i).getmValue());
                        }
                    }else {
                        mHeadFlag =true;
                        mHeadMin = 1f;
                    }
                }

            } else {

                for (int i = imsi; i < mGrowthGraphListItem.size(); i++) {
                    if (mHeightMin > Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(i).getmValue())) {
                        mHeightMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(i).getmValue());
                    }
                    if (mWeightMin > Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(i).getmValue())) {
                        mWeightMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(i).getmValue());
                    }
                    if (81 > i) {
                        if (mHeadMin > Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(i).getmValue())) {
                            mHeadMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(i).getmValue());
                        }
                    }else {
                        mHeadFlag =true;
                        mHeadMin = 1f;
                    }
                }
            }
        }else {

            if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) {
                for (int i = 0; i < 25; i++) {
                    if (mHeightMin > Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(i).getmValue())) {
                        mHeightMin = (int)Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(i).getmValue());
                    }
                    if (mWeightMin > Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(i).getmValue())) {
                        mWeightMin = (int)Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(i).getmValue());
                    }
                    if (mHeadMin > Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(i).getmValue())) {
                        mHeadMin = (int)Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(i).getmValue());
                    }
                }

            } else {

                for (int i = 0; i < 25; i++) {
                    if (mHeightMin > Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(i).getmValue())) {
                        mHeightMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(i).getmValue());
                    }
                    if (mWeightMin > Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(i).getmValue())) {
                        mWeightMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(i).getmValue());
                    }
                    if (mHeadMin > Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(i).getmValue())) {
                        mHeadMin = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(i).getmValue());
                    }
                }
            }
        }



        //남자면
        if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) {
            if (type == 0) { //키
                if (!mAgeFlag) {
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue());
                                if (mHeightMax < Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue())){
                                    mHeightMax = (int)Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
                                }
                            }
                        }
                    }
                } else {
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue());
                                if (mHeightMax < Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue())){
                                    mHeightMax = (int)Float.parseFloat(GrowthMainActivity.mMaleHeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHeight_3.get(j).getmValue());
                            } else if (i == 3) {
                                randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                GLog.i("mGrowthGraphListItem.get(" + j + ").getmInput_Height() --- > " + mGrowthGraphListItem.get(j).getmInput_Height());
                            }
                        }
                    }
                }
            } else if (type == 1) { // 몸무게
                if (!mAgeFlag) {
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                            for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue());
                                if (mWeightMax < Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue())) {
                                    mWeightMax = (int)Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Weight());
                                }
                            }
                        }
                    }
                } else {
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue());
                                if (mWeightMax < Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue())) {
                                    mWeightMax = (int)Float.parseFloat(GrowthMainActivity.mMaleWeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleWeight_3.get(j).getmValue());
                            } else if (i == 3) {
                                randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Weight());
//                                GLog.i("mGrowthGraphListItem.get(" + j + ").getmInput_Height() --- > " + mGrowthGraphListItem.get(j).getmInput_Height());
                            }
                        }
                    }
                }
            } else { // 머리둘레
                if (!mAgeFlag) {
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                if (mHeadMax < Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue())) {
                                    mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }
                    }
                } else {
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        if (!mHeadFlag) {
                            for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                                if (i == 0) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                    if (mHeadMax < Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue())) {
                                        mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                    }
                                } else if (i == 1) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_50.get(j).getmValue());
                                } else if (i == 2) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(j).getmValue());
                                } else if (i == 3) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }else {
                            for (int j = 0; j < 81; j++) {
                                if (i == 0) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                    if (mHeadMax < Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue())) {
                                        mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mMaleHead_90.get(j).getmValue());
                                    }
                                } else if (i == 1) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_50.get(j).getmValue());
                                } else if (i == 2) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mMaleHead_3.get(j).getmValue());
                                } else if (i == 3) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }
                    }
                }
            }
        } else { // 여자면
            if (type == 0) { //키
                if (!mAgeFlag) {
                    GLog.i("여자고 24개월 미만일 때");
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue());
                                if (mHeightMax < Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue())){
                                    mHeightMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
                                }
                            }
                        }
                    }
                } else {
                    GLog.i("여자고 24개월 이상일 때");
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue());
                                if (mHeightMax < Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue())){
                                    mHeightMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHeight_3.get(j).getmValue());
                            } else if (i == 3) {
                                randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                GLog.i("mGrowthGraphListItem.get(" + j + ").getmInput_Height() --- > " + mGrowthGraphListItem.get(j).getmInput_Height());
                            }
                        }
                    }
                }
            } else if (type == 1) { // 몸무게
                if (!mAgeFlag) {
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue());
                                if (mWeightMax < Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue())) {
                                    mWeightMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Weight());
                                }
                            }
                        }
                    }
                } else {
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue());
                                if (mWeightMax < Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue())) {
                                    mWeightMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleWeight_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleWeight_3.get(j).getmValue());
                            } else if (i == 3) {
                                randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Weight());
//                                GLog.i("mGrowthGraphListItem.get(" + j + ").getmInput_Height() --- > " + mGrowthGraphListItem.get(j).getmInput_Height());
                            }
                        }
                    }
                }
            } else { // 머리둘레
                if (!mAgeFlag) {
                    numberOfPoints = 25;
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        for (int j = 0; j < numberOfPoints; j++) {
                            if (i == 0) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                if (mHeadMax < Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue())) {
                                    mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                }
                            } else if (i == 1) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_50.get(j).getmValue());
                            } else if (i == 2) {
                                randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(j).getmValue());

                            } else if (i == 3) {
//                                if (mGrowthGraphListItem.size() > 25) {
//                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Height());
//                                } else {
                                if (j < mGrowthGraphListItem.size()) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }
                    }
                } else {
                    numberOfPoints = mGrowthGraphListItem.size();
                    randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
                    for (int i = 0; i < maxNumberOfLines; i++) {
                        if (!mHeadFlag) {
                            for (int j = 0; j < mGrowthGraphListItem.size(); j++) {
                                if (i == 0) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                    if (mHeadMax < Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue())) {
                                        mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                    }
                                } else if (i == 1) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_50.get(j).getmValue());
                                } else if (i == 2) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(j).getmValue());
                                } else if (i == 3) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }else {
                            for (int j = 0; j < 81; j++) {
                                if (i == 0) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                    if (mHeadMax < Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue())) {
                                        mHeadMax = (int)Float.parseFloat(GrowthMainActivity.mFeMaleHead_90.get(j).getmValue());
                                    }
                                } else if (i == 1) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_50.get(j).getmValue());
                                } else if (i == 2) {
                                    randomNumbersTab[i][j] = Float.parseFloat(GrowthMainActivity.mFeMaleHead_3.get(j).getmValue());
                                } else if (i == 3) {
                                    randomNumbersTab[i][j] = Float.parseFloat(mGrowthGraphListItem.get(j).getmInput_Head());
                                }
                            }
                        }
                    }
                }
            }
        }


        mBtnLay.postDelayed(new Runnable() {
            @Override
            public void run() {
                generateData(mType, mDataFlag);
                resetViewport(mType, mDataFlag);

            }
        }, CommonData.ANI_DELAY_100);


    }*/


    /**
     * 그래프 그리기
     */
    private void generateData(int type, boolean dataflag) {
//        for(int i=0; i<randomNumbersTab.length; i++) {
//            for (int j = 0; j < numberOfPoints; ++j) {
//
//                GLog.i("randomNumbersTab[ " + i + " ][ " + j + "] = " + randomNumbersTab[i][j]);
//            }
//        }
        // 각 해당 되는 그래프선의 색깔

//        GLog.i("mHeightMax = " +mHeightMax);
//        GLog.i("mHeightMin = " + mHeightMin);
//        GLog.i("mWeightMax = " +mWeightMax);
//        GLog.i("mWeightMin = " + mWeightMin);
//        GLog.i("mHeadMax = " +mHeadMax);
//        GLog.i("mHeadMin = " + mHeadMin);

        int[] COLORS = {Color.parseColor("#C0905C"), Color.parseColor("#9FA1A4"), Color.parseColor("#FFFFFF"), Color.parseColor("#231F20")};

        List<Line> lines = new ArrayList<Line>();
        // 총 그래프의 개수 = numberOfLines
        for (int i = 0; i < numberOfLines; ++i) {
            List<PointValue> values = new ArrayList<PointValue>();

            if (i == 3)
                mPointFlag = true;
            else
                mPointFlag = false;

            if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) { //남자면
                if (type == 0) { // 키
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                } else if (type == 1) { // 몸무게
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                } else { // 머리둘레
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                }
            } else { // 여자면
                if (type == 0) { // 키
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                } else if (type == 1) { // 몸무게
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                } else { // 머리둘레
                    if (!mAgeFlag) { // 24개월전
                        if (mGrowthGraphListItem.size() > 25) {
                            for (int j = 0; j < numberOfPoints; ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        } else {
                            if (i == 3) {
                                mPointFlag = true;
                                for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                    values.add(new PointValue(k, randomNumbersTab[i][k]));
                                }
                            } else {
                                mPointFlag = false;
                                for (int j = 0; j < numberOfPoints; ++j) {
                                    values.add(new PointValue(j, randomNumbersTab[i][j]));
                                }
                            }
                        }
                    } else { // 24개월 후
                        if (i == 3) {
                            mPointFlag = true;
                            for (int k = 0; k < mGrowthGraphListItem.size(); ++k) {
                                values.add(new PointValue(k, randomNumbersTab[i][k]));
                            }
                        } else {
                            mPointFlag = false;
                            for (int j = 0; j < mGrowthGraphListItem.size(); ++j) {
                                values.add(new PointValue(j, randomNumbersTab[i][j]));
                            }
                        }
                    }
                }
            }

            Line line = new Line(values);
            line.setColor(COLORS[i]);
            line.setStrokeWidth(2);
            line.setShape(shape);
            line.setCubic(isCubic);
            line.setFilled(isFilled);
            line.setHasLabels(hasLabels);
            line.setHasLabelsOnlyForSelected(hasLabelForSelected);
            if (mPointFlag){
                line.setHasPoints(true);
                line.setPointRadius(Util.pixelFromDP(getActivity(), 1));
                line.setHasLines(false);
            }else {
                line.setHasPoints(false);
                line.setHasLines(true);
            }

//            line.setPointRadius(BleUtil.pixelFromDP(getActivity(), 1));
            lines.add(line);
        }

        data = new LineChartData(lines);

        if (hasAxes) {
            Axis axisX;
            Axis axisY;


//            String c = String.format("%.2f" , mTwoDateCompare / 360);
//            GLog.i("c  -> " + c);
//            float d = Float.parseFloat(c) - 2f;
            if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) { //남자
                if (type == 0) { // 키
                    if (!mAgeFlag) { // 24개월미만
                        axisX = Axis.generateAxisFromRange(0f, 24f , 3f);
                        axisY = Axis.generateAxisFromRange(mHeightMin , mHeightMax + 1f, 1f);
                    } else { // 이상
                        axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                        axisY = Axis.generateAxisFromRange(mHeightMin , mHeightMax + 1f, 1f);
                    }
                } else if (type == 1) { // 몸무게
                    if (!mAgeFlag) {
                        axisX = Axis.generateAxisFromRange(0f, 24f, 3f);
                        axisY = Axis.generateAxisFromRange(mWeightMin , mWeightMax + 1f, 1f);
                    } else {
                        axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                        axisY = Axis.generateAxisFromRange(mWeightMin , mWeightMax +1f , 1f);
                    }
                } else { // 머리둘레
                    if (!mAgeFlag) {
                        axisX = Axis.generateAxisFromRange(0f, 24f, 3f);
                        axisY = Axis.generateAxisFromRange(mHeadMin , mHeadMax + 1f, 1f);
                    } else {
                        axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                        axisY = Axis.generateAxisFromRange(mHeadMin , mHeadMax +1f , 1f);
                    }
                }
            } else { // 여자
                if (type == 0) { // 키
                    if (!mAgeFlag) { // 24개월미만
                        axisX = Axis.generateAxisFromRange(0f, 24f , 3f);
                        axisY = Axis.generateAxisFromRange(mHeightMin , mHeightMax +1f , 1f);
                    } else { // 이상
                        axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                        axisY = Axis.generateAxisFromRange(mHeightMin , mHeightMax + 1f, 1f);
                    }
                } else if (type == 1) { // 몸무게
                    if (!mAgeFlag) {
                        axisX = Axis.generateAxisFromRange(0f, 24f, 3f);
                        axisY = Axis.generateAxisFromRange(mWeightMin , mWeightMax + 1f, 1f);
                    } else {
                        axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                        axisY = Axis.generateAxisFromRange(mWeightMin , mWeightMax +1f, 1f);
                    }
                } else { // 머리둘레
                    if (!mAgeFlag) {
                        axisX = Axis.generateAxisFromRange(0f, 24f, 3f);
                        axisY = Axis.generateAxisFromRange(mHeadMin , mHeadMax + 1f, 1f);
                    } else {
                        if (!mHeadFlag){
                            axisX = Axis.generateAxisFromRange((float) mTotalMonth - 24f, (float) mTotalMonth, 3f);
                            axisY = Axis.generateAxisFromRange(mHeadMin, mHeadMax + 1f, 1f);
                        }else {
                            axisX = Axis.generateAxisFromRange((float)mTotalMonth - 24f, (float)mTotalMonth, 3f);
                            axisY = Axis.generateAxisFromRange(25f , 55f , 3f);
                        }
                    }
                }
            }


            axisX.setHasSeparationLine(false);
            axisX.setTextColor(ContextCompat.getColor(getActivity(), R.color.txt_dark_gray)); // 색상
            axisX.setHasLines(true);
            axisX.setLineColor(R.color.txt_dark_gray);
            SimpleAxisValueFormatter formatter = new SimpleAxisValueFormatter(0);
            axisY.setHasSeparationLine(false);
            axisY.setTextColor(ContextCompat.getColor(getActivity(), R.color.txt_dark_gray)); // 색상
            axisY.setHasLines(true);
            axisY.setLineColor(R.color.txt_dark_gray);

            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        chart.setLineChartData(data);
        chart.setVisibility(View.VISIBLE);

    }

    /**
     * 그래프의 상하좌우 크기 셋팅
     */
    private void resetViewport(int type , boolean dataflag) {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        GLog.i("mHeightMax = " + mHeightMax, "dd");
        GLog.i("mHeightMin = " + mHeightMin, "dd");
        GLog.i("mWeightMax = " + mWeightMax, "dd");
        GLog.i("mWeightMin = " + mWeightMin, "dd");
        GLog.i("mHeadMax = " + mHeadMax, "dd");
        GLog.i("mHeadMin = " + mHeadMin, "dd");

        // 시작점0f, 상단점30.5f(유동) , 끝점24.5f, 하단점0f 크기를 표현함. 1f 당 하나의 픽셀이라고 보면됨 크기를 정하는건 axisX = Axis.generateAxisFromRange(0f, 24f, 3f); axisY = Axis.generateAxisFromRange(40f, 93f, 5f);


        //데이터가 있을 시
//        if (dataflag) {
        //남자면
//            if (GrowthMainActivity.mChildMenuItem.get(GrowthMainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) {
        if (type == 0) { // 키
            if (!mAgeFlag) {
                v.set(0f, mHeightMax, 25f, mHeightMin);
            } else {
                v.set((float) mTotalMonth - 24f, mHeightMax + 1.3f, (float) mTotalMonth + 1f , mHeightMin);
            }
        } else if (type == 1) { // 몸무게
            if (!mAgeFlag) {
                v.set(0f, mWeightMax, 25f, mWeightMin);
            } else {
                v.set((float) mTotalMonth - 24f, mWeightMax + 1.3f, (float) mTotalMonth + 1f, mWeightMin);
            }
        } else { // 머리둘레
            if (!mAgeFlag) {
                v.set(0f, mHeadMax, 25f, mHeadMin);
            } else {
                if (!mHeadFlag) {
                    v.set((float) mTotalMonth - 24f, mHeadMax + 1.3f, (float) mTotalMonth + 1f, mHeadMin);
                }else {
                    v.set((float) mTotalMonth - 24f, 60f, (float) mTotalMonth + 1f, 25f);
                }
            }
        }
//            } else {
//                if (type == 0) { // 키
//                    if (!mAgeFlag) {
//                        v.set(0f, mHeightMax, 25f, mHeightMin);
//                    } else {
//                        v.set((float) mTotalMonth - 24f, mHeightMax , (float) mTotalMonth, mHeightMin);
//                    }
//                } else if (type == 1) { // 몸무게
//                    if (!mAgeFlag) {
//                        v.set(0f, mWeightMax, 25f, mWeightMin);
//                    } else {
//                        v.set((float)mTotalMonth - 24f , mWeightMax + 5f, (float)mTotalMonth, mWeightMin);
//                    }
//                } else { // 머리둘레
//                    if (!mAgeFlag) {
//                        v.set(0f, mHeightMax, 25f, mHeightMin);
//                    } else {
//                        v.set((float)mTotalMonth - 24f, mHeadMax , (float)mTotalMonth, mHeadMin);
//                    }
//                }
//            }
//        }else {
//            if (GrowthMainActivity.mChildMenuItem.get(GrowthMainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.STRING_ONE)) {
//                if (type == 0) { // 키
//                    if (!mAgeFlag) {
//                        v.set(0f, 94f, 25f, 40f);
//                    } else {
//                        v.set((float)mTotalMonth - 24f , mHeightMax, (float)mTotalMonth , mHeightMin);
//                    }
//                } else if (type == 1) { // 몸무게
//                    if (!mAgeFlag) {
//                        v.set(0f, 16f, 25f, 0f);
//                    } else {
//                        v.set((float)mTotalMonth - 24f , mWeightMax , (float)mTotalMonth, mWeightMin);
//                    }
//                } else { // 머리둘레
//                    if (!mAgeFlag) {
//                        v.set(0f, 51f, 25f, 30f);
//                    } else {
//                        v.set((float)mTotalMonth - 24f, mHeadMax , (float)mTotalMonth, mHeadMin);
//                    }
//                }
//            } else {
//                if (type == 0) { // 키
//                    if (!mAgeFlag) {
//                        v.set(0f, 93f, 25f, 40f);
//                    } else {
//                        v.set((float) mTotalMonth - 24f, mHeightMax , (float) mTotalMonth, mHeightMin);
//                    }
//                } else if (type == 1) { // 몸무게
//                    if (!mAgeFlag) {
//                        v.set(0f, 16f, 25f, 0f);
//                    } else {
//                        v.set((float) mTotalMonth - 24f, mWeightMax , (float) mTotalMonth, mWeightMin);
//                    }
//                } else { // 머리둘레
//                    if (!mAgeFlag) {
//                        v.set(0f, 51f, 25f, 30f);
//                    } else {
//                        v.set((float) mTotalMonth - 24f, mHeadMax , (float) mTotalMonth, mHeadMin);
//                    }
//                }
//            }
//        }

        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }


    /**
     * 육아 다이어리 리스트 가져오기
     */
    public void requestGrowthRecordApi(String chl_sn, int pagerNum, int typeNum) {
//        GLog.i("requestAppInfo");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {   "api_code": "chldrn_growth_list",   "insures_code": "106", "mber_sn": "10035"  ,"chl_sn": "1000" ,"pageNumber": "1" , "growth_typ": "1"}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_GET_GROWTH_LIST_TEST);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);          //  os
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값
            object.put(CommonData.JSON_PAGENUMBER, String.valueOf(pagerNum));         //  페이지 넘버
            object.put(CommonData.JSON_GROWTH_TYP, String.valueOf(typeNum));               //  리스트타입

            GLog.i("String.valueOf(typeNum) --> " + String.valueOf(typeNum), "dd");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_RECORD, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 육아 다이어리 리스트(그래프) 가져오기
     */
    public void requestGrowthGraphApi(String chl_sn, int pagerNum) {
        // {   "api_code": "chldrn_growth_list_month_grp",   "insures_code": "106", "mber_sn": "10035"  ,"chl_sn": "1000" ,"pageNumber": "1"}
        GLog.i("requestAppInfo", "dd");

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_GET_GROWTH_MONTH_GRP_TEST);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN, commonData.getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값
            object.put(CommonData.JSON_PAGENUMBER, String.valueOf(pagerNum));         //  페이지 넘버

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_RECORD_GRAPH, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 육아 다이어리 삭제
     * @param value 데이터 고유 값
     * @param type 데이터 삭제 타입 0 = 키 , 1 = 몸무게 , 2 = 머리둘레
     */
    public void requestGrowthRecordDelApi(String value , int type) {
        GLog.i("requestAppInfo", "dd");
        GLog.i("value-->" + value, "dd");
        String mtype = "";
        if (type == 0){
            mtype = CommonData.STRING_ONE;
        }else if (type == 1){
            mtype = CommonData.STRING_TWO;
        }else {
            mtype = CommonData.STRING_THR;
        }

        GLog.i("mtype -- > "  + mtype, "dd");

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {   "api_code": "chldrn_growth_del",   "insures_code": "106", "mber_sn": "10035"  ,"growth_sn": "1006"}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_GET_GROWTH_DEL_TEST);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN, commonData.getMberId());             //  회원고유값
            object.put(CommonData.JSON_GROWTH_SN, value);               //  데이터 고유 값
            object.put(CommonData.JSON_GROWTH_TYP, mtype);               //  삭제 타입

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_RECORD_DEL, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 새로고침
     */
    public void refreshUi() {
        GLog.i("refreshUi", "dd");

        Date imsiDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
        int year = imsiDate.getYear() + 1900;
        int month = imsiDate.getMonth()+1;
        int day = imsiDate.getDate();

        String imsiBirth = "" + year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
        GLog.i("imsiBirth = " +imsiBirth, "dd");


        String childebirth = Util.getDateSpecialCharacter(getActivity(), imsiBirth, 4);
        mTwoDateCompare = Util.getTwoDateCompare(childebirth , CommonData.PATTERN_DATE);
        GLog.i("mTwoDateCompare  -->   " + mTwoDateCompare, "dd");
        if (mTwoDateCompare <= 730){
            mAgeFlag = false;
            mEtcTv.setVisibility(View.INVISIBLE);
        } else {
            mAgeFlag = true;
            mEtcTv.setVisibility(View.VISIBLE);
        }

        // 자녀의 생일과 현재날짜의 개월수 차이구하기
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMMdd");
        String nowDate = CurDateFormat.format(date);

        int childeYear = Integer.parseInt(childebirth.substring(0, 4));
        String childeMonth = Util.getTwoDateFormat(Integer.parseInt(childebirth.substring(5, 7)));
        int nowYear = Integer.parseInt(nowDate.substring(0, 4));
        String nowMonth = Util.getTwoDateFormat(Integer.parseInt(nowDate.substring(4, 6)));

        GLog.i("아이생일달" + childeYear + childeMonth, "dd");
        GLog.i("현재달" + nowYear + nowMonth, "dd");

        mTotalMonth = (nowYear - childeYear)* 12 + (Integer.parseInt(nowMonth) - Integer.parseInt(childeMonth));
        GLog.i("아이생일과 현재날짜 개월수 차이 --> " + mTotalMonth, "dd");


        mGrowthGraphListItem.clear();
        mGrowthListItem.clear();

        requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
        requestGrowthGraphApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), CommonData.GRAPH_PAGENUMBER);
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch (type) {
                case NetworkConst.NET_RECORD:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);

                                if (data_yn.equals(CommonData.YES)) {

                                    mDataFlag = true;

                                    mTotalPage = resultData.getInt(CommonData.JSON_MAXPAGENUMBER);
                                    if (mPageId == 1) {   // 첫번째 페이지라면 데이터 초기화
                                        mGrowthListItem.clear();
                                    }

                                    JSONArray growthArr = resultData.getJSONArray(CommonData.JSON_GROWTH_LIST);
                                    // 데이터가 있을 시
                                    if (growthArr.length() > 0) {

                                        for (int i = 0; i < growthArr.length(); i++) {
                                            JSONObject growthObject = growthArr.getJSONObject(i);

                                            GrowthItem item = new GrowthItem(growthObject.getString(CommonData.JSON_GROWTH_SN),
                                                    growthObject.getString(CommonData.JSON_INPUT_DE),
                                                    growthObject.getString(CommonData.JSON_CHIDRN_MONTH),
                                                    growthObject.getString(CommonData.JSON_INPUT_HEIGHT),
                                                    growthObject.getString(CommonData.JSON_INPUT_BDWGH),
                                                    growthObject.getString(CommonData.JSON_INPUT_HEADCM),
                                                    growthObject.getString(CommonData.JSON_CM_REUSLT),
                                                    growthObject.getString(CommonData.JSON_KG_REUSLT),
                                                    growthObject.getString(CommonData.JSON_HEAD_REUSLT));
                                            mGrowthListItem.add(item);

//                                            GLog.i("고유번호 --> " + item.getmGrowth_SN());
//                                            GLog.i("개월수 --> " + item.getmChldrn_Month());
//                                            GLog.i("%결과 --> " + item.getmResult());
//                                            GLog.i("키 --> " + item.getmInput_Height());
//                                            GLog.i("날짜 --> " + item.getmInput_De());
                                        }



                                        if (mPageId == 1) {
                                            setListAdapter(mType, mTypeFlag, mFirstFlag);
                                            //TypeEdit(mType, mTypeFlag, mFirstFlag);
//                                            GLog.i("리스트아답터로 넘어갈때--->");
                                            mPullListView.onRefreshComplete();
                                        } else {
                                            mAdapter.notifyDataSetChanged();
                                        }

                                        ++mPageId;      // 페이지 증가

                                    }
                                }else {
                                    mGrowthListItem.clear();
                                    if (mPageId == 1) {
                                        setListAdapter(mType, mTypeFlag, mFirstFlag);
                                       // TypeEdit(mType, mTypeFlag, mFirstFlag);
//                                            GLog.i("리스트아답터로 넘어갈때--->");
                                        mPullListView.onRefreshComplete();
                                    } else {
                                        mAdapter.notifyDataSetChanged();
                                    }

                                    /*chart.setVisibility(View.GONE);
                                    mDefaultLay.setVisibility(View.VISIBLE);
                                    mDataLay.setVisibility(View.GONE);
                                    mGrowthrecordBtn.setEnabled(true);*/
                                }

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;

                case NetworkConst.NET_RECORD_GRAPH:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                GLog.i("그래프!!", "dd");
                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);

                                if (data_yn.equals(CommonData.YES)) {
                                    mDataFlag = true;
                                    mGrowthGraphListItem.clear();
                                    JSONArray growthgraphArr = resultData.getJSONArray(CommonData.JSON_GROWTH_MONTH_LIST);
                                    // 데이터가 있을 시
//                                    String childeBirth = CommonData.STRING_TWOTEN + GrowthMainActivity.mChildMenuItem.get(GrowthMainActivity.mChildChoiceIndex).getmChldrnLifyea();
                                    Date imsiDate = Util.getDateFormat(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnLifyea(), CommonData.PATTERN_YYMMDD);
                                    int year = imsiDate.getYear() + 1900;
                                    int month = imsiDate.getMonth()+1;
                                    int day = imsiDate.getDate();

                                    String childeBirth = "" + year+Util.getTwoDateFormat(month)+Util.getTwoDateFormat(day);
                                    GLog.i("childeBirth = " +childeBirth, "dd");

                                    if (growthgraphArr.length() > 0) {

                                        for (int i = 0; i < growthgraphArr.length(); i++) {

                                            JSONObject growthgraphObject = growthgraphArr.getJSONObject(i);

                                            GrowthRecordMonthGraphItem item = new GrowthRecordMonthGraphItem(growthgraphObject.getString(CommonData.JSON_YYMM),
                                                    growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT),
                                                    growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH),
                                                    growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM));
                                            mGrowthGraphListItem.add(item);


                                            if (mHeightMax < Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT))) {
                                                mHeightMax = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT));
                                            }

                                            if (mHeightMin > Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT)) && Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT)) != 0) {
                                                mHeightMin = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEIGHT));
                                            }

                                            if (mWeightMax < Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH))) {
                                                mWeightMax = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH));
                                            }

                                            if(mWeightMin > Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH))  && Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH)) != 0) {
                                                mWeightMin = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_BDWGH));
                                            }

                                            if(mHeadMax < Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM))) {
                                                mHeadMax = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM));
                                            }

                                            if(mHeadMin > Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM)) && Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM)) != 0) {
                                                mHeadMin = (int)Float.parseFloat(growthgraphObject.getString(CommonData.JSON_INPUT_HEADCM));
                                            }

                                        }

                                        GLog.i("mHeightMax = " + mHeightMax, "dd");
                                        GLog.i("mHeightMin = " + mHeightMin, "dd");
                                        GLog.i("mWeightMax = " + mWeightMax, "dd");
                                        GLog.i("mWeightMin = " + mWeightMin, "dd");
                                        GLog.i("mHeadMax = " + mHeadMax, "dd");
                                        GLog.i("mHeadMin = " + mHeadMin, "dd");


                                        GrowthRecordMonthGraphItem item;

                                        // 자녀의 생일과 마지막 데이터 개월수 차이구하기
                                        int lastyear = Integer.parseInt(mGrowthGraphListItem.get(mGrowthGraphListItem.size()-1).getmYYMM().substring(0, 4));
                                        int lastmonth = Integer.parseInt(mGrowthGraphListItem.get(mGrowthGraphListItem.size() - 1).getmYYMM().substring(4, 6));

                                        int childeYear = Integer.parseInt(childeBirth.substring(0, 4));
                                        String childeMonth = Util.getTwoDateFormat(Integer.parseInt(childeBirth.substring(4, 6)));

                                        mDifference = (lastyear - childeYear)* 12 + (lastmonth - Integer.parseInt(childeMonth));
                                        GLog.i("아이생일과 마지막 데이터 년월의 차이 --> " + mDifference, "dd");


                                        // 자녀의 생일과 처음데이터 개월수 차이구하기
                                        int firstyear = Integer.parseInt(mGrowthGraphListItem.get(0).getmYYMM().substring(0, 4));
                                        int firstmonth = Integer.parseInt(mGrowthGraphListItem.get(0).getmYYMM().substring(4, 6));

                                        mDifference2 = (firstyear - childeYear)* 12 + (firstmonth - Integer.parseInt(childeMonth));
                                        GLog.i("아이생일과 처음 데이터 년월의 차이 --> " + mDifference2, "dd");

                                        for (int i = 0; i < mDifference2; i++){
                                            item = new GrowthRecordMonthGraphItem(CommonData.STRING_ZERO, CommonData.STRING_ZERO , CommonData.STRING_ZERO, CommonData.STRING_ZERO);
                                            mGrowthGraphListItem.add(i, item);
                                        }
                                        //generateValues(mType, mDataFlag);
                                    }
                                }
                                //generateValues(mType , mDataFlag);

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;

                case NetworkConst.NET_RECORD_DEL:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");

                            try {

                                if (resultData.getString(CommonData.JSON_REG_YN).equals(CommonData.YES)) {
                                    GLog.i("삭제완료", "dd");
                                    mDelFlag = true;
                                    if (mType == 0) {
                                        mGrowthListItem.get(mDelPosition).setmInput_Height(CommonData.STRING_ZERO);
                                    }else if (mType == 1) {
                                        mGrowthListItem.get(mDelPosition).setmInput_Weight(CommonData.STRING_ZERO);
                                    }else {
                                        mGrowthListItem.get(mDelPosition).setmInput_Head(CommonData.STRING_ZERO);
                                    }

                                    mGrowthListItem.clear();
                                    mPageId = 1;
                                    GLog.i("상단에서 내릴때--->", "dd");

                                    requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
                                }

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        GLog.i("requestCode = " + requestCode, "dd");
        GLog.i("resultCode = " + resultCode, "dd");
        GLog.i("data = " + data, "dd");

        if (resultCode != Activity.RESULT_OK) {
            GLog.i("resultCode != RESULT_OK", "dd");
            return;
        }

        switch (requestCode) {
            case CommonData.REQUEST_CODE_INPUT:
                GLog.i("requestCode != REQUEST_CODE_INPUT", "dd");

                mPageId = 1;
                requestGrowthRecordApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId , mType + 1);
                requestGrowthGraphApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn(), mPageId);

                //refreshUi();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((GrowthMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        GrowthMainActivity activity = (GrowthMainActivity)getActivity();
        activity.switchActionBarTheme(GrowthMainActivity.THEME_DARK_BLUE);
        switchFragment(new GrowthMainFragment());
    }
}
