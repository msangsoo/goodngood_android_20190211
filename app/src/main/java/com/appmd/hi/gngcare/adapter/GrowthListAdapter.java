package com.appmd.hi.gngcare.adapter;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.GrowthItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;


/**
 * Created by suwun on 2016-03-31.
 * 유아성장 도표 아답터
 * @since 0, 1
 */
public class GrowthListAdapter extends ArraySwipeAdapter<GrowthItem> {

    private LayoutInflater mInflater;
    private ArrayList<GrowthItem> mData;
    private Activity mContext;
    private int mType;
    private boolean mTypeFlag;

    private EventListener mListener;

    public GrowthListAdapter(Activity context, ArrayList<GrowthItem> object, int type , boolean typeflag, EventListener listener){
        super(context, 0, object);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = object;
        mContext = context;
        mType = type;
        mTypeFlag = typeflag;
        mListener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final GrowthItem item = mData.get(position);

        ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.growth_list_diagram, null);

            holder      =   new ViewHolder();

            holder.mResultLay       =       (LinearLayout) convertView.findViewById(R.id.result_lay);
            holder.mDateTv          =       (TextView)  convertView.findViewById(R.id.date_tv);
            holder.mMonthOldTv      =       (TextView)  convertView.findViewById(R.id.monthold_tv);
            holder.mHeightTv        =       (TextView)  convertView.findViewById(R.id.height_tv);
            holder.mResultTv        =       (TextView)  convertView.findViewById(R.id.result_tv);

            holder.swipeLayout      =       (SwipeLayout)   convertView.findViewById(R.id.swipe);
            holder.buttonDelete     =       (ImageButton) convertView.findViewById(R.id.trash);

            holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

            holder.buttonDelete.setOnClickListener(view -> mListener.delete(position));

            convertView.setTag(holder);

        }else{
            holder  =   (ViewHolder)    convertView.getTag();
        }

        if (item != null) {
            String year , month , day;
            year = item.getmInput_De().substring(2,4);
            month = item.getmInput_De().substring(4,6);
            day = item.getmInput_De().substring(6,8);

            holder.mDateTv.setText(year + CommonData.STRING_SLASH + month + CommonData.STRING_SLASH + day);
            holder.mMonthOldTv.setText(item.getmChldrn_Month());
            if (mType == 0){
                holder.mResultTv.setText(item.getmCm_Result());
                if (item.getmInput_Height().equals(CommonData.STRING_ZERO)){
                    holder.mResultLay.setVisibility(View.GONE);
                }else {
                    holder.mHeightTv.setText(item.getmInput_Height() + getContext().getString(R.string.cm));
                }
            }else if (mType == 1) {
                holder.mResultTv.setText(item.getmKg_Result());
                if (item.getmInput_Weight().equals(CommonData.STRING_ZERO)){
                    holder.mResultLay.setVisibility(View.GONE);
                }else {
                    holder.mHeightTv.setText(item.getmInput_Weight() + getContext().getString(R.string.kg));
                }
            }else {
                holder.mResultTv.setText(item.getmHead_Result());
                if (item.getmInput_Head().equals(CommonData.STRING_ZERO)){
                    holder.mResultLay.setVisibility(View.GONE);
                }else {
                    holder.mHeightTv.setText(item.getmInput_Head() + getContext().getString(R.string.cm));
                }

            }

        }
        return convertView;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder{
        TextView mDateTv , mMonthOldTv , mHeightTv , mResultTv;
        LinearLayout mResultLay;

        SwipeLayout swipeLayout;
        ImageButton buttonDelete;
    }

    public interface EventListener
    {
        void delete(int position);
    }
}
