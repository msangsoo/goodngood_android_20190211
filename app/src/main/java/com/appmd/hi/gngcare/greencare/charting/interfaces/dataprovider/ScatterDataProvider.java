package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
