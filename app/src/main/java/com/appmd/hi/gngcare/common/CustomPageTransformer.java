package com.appmd.hi.gngcare.common;

import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by jihoon on 2016-03-21.
 * 공유 데이터 클래스
 * @since 0, 1
 */
public class CustomPageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.75f;
    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();


        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
//            view.setAlpha(0f);

        } else if (position <= 0) { // [-1,0]

            findAllEdittexts((ViewGroup) view, position);

        } else if (position <= 1) { // (0,1]

            findAllEdittexts((ViewGroup) view, position);

        } else { // (1,+Infinity]



        }
    }

    private void findAllEdittexts(ViewGroup viewGroup, float position) {

        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllEdittexts((ViewGroup) view, position);
            else if (view instanceof TextView) {
                TextView edittext = (TextView) view;
                edittext.setTranslationX (( float ) position * 200);

            }
        }

    }

}