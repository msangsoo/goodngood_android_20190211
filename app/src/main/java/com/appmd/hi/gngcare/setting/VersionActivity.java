package com.appmd.hi.gngcare.setting;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by jihoon on 2016-03-28.
 * 설정 클래스
 * @since 0, 1
 */
public class VersionActivity extends BackBaseActivity implements View.OnClickListener {

    private TextView mNowVersionTv, mNewVersionTv;
    private Button mVersionBtn;
    private boolean mVersionFlag = false;
    private PackageInfo pi	= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.version_impormation_activity);

        setTitle(getString(R.string.version_info));

        init();
        setEvent();

        }

    /**
     * 초기화
     */
    public void init(){

        mNowVersionTv           =   (TextView)          findViewById(R.id.nowversion_tv);
        mNewVersionTv           =   (TextView)          findViewById(R.id.newversion_tv);

        mVersionBtn             =   (Button)            findViewById(R.id.version_btn);

        boolean isAppUpdate = false;

        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            mNowVersionTv.setText(CommonData.STRING_SPACE + pi.versionName.toString());
            isAppUpdate = Util.isAppUpdate(pi.versionName.toString(), commonData.getAppVersion()); // 앱버전과 로그인 시 받아오는 버전 체크
            GLog.i("appver  = " + pi.versionName.toString(), "dd");
            GLog.i("pi.packagename = " + pi.packageName, "dd");

        } catch (Exception e) {
            GLog.e(e.toString());
        }

        if (pi != null && isAppUpdate) {    // 업데이트가 필요하다면
            mVersionBtn.setBackgroundResource(R.drawable.btn_5_6bb0d7);
            mNewVersionTv.setText(getString(R.string.version_guide) + pi.versionName.toString());
            mVersionFlag = true;
        }else{ // 최신버전이면
            mVersionBtn.setBackgroundResource(R.drawable.background_5_506bb0d7);
            mNewVersionTv.setText(getString(R.string.version_newest));
            mVersionFlag = false;
        }


    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mVersionBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch(v.getId()){
            case R.id.version_btn: // 알림설정
                if (mVersionFlag){
                    Uri uri = Uri.parse("market://details?id=" + pi.packageName);   // 패키지명
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }else {

                }
                break;
        }
    }
}
