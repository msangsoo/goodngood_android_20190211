package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.ApplinkDialog;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.KakaoLinkUtil;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-29.
 * 최종 키 예측 결과 클래스
 * @since 0, 1
 */
public class GrowthRecordLastHeightResult extends BackBaseActivity {

    private TextView mHeightTv, mHeightMaleTv, mHeightFemaleTv, mLeftTv , mCenterTv , mRightTv, mTxtTitleLastheight; // 자녀이름 , 키 표시 , 프로그래스바 키표시, 타이틀
    private RelativeLayout mMoveLay; // 퍼센티지 레이아웃 부모
    private LinearLayout mPerLay; // 그래프 퍼센티지 표시 레이아웃
    private LinearLayout mOneLay, mTwoLay;  // 태아의 경우 성별 2가지 모두 보여줘야 함
    private ImageButton mConfirmBtn, mBtnShare; // 확인&저장 버튼
    private View mView;
    private ProgressBar mHeight_Pbar;
    private ImageView mImgTriCenter, mImgTriRight;
    private int graphLine;

    private String mMyCm , mStartCm, mEndCm , mMotherHeight , mFatherHeight;

    private boolean mAgeFlag = false;
    private TextView mResultTv1 , mResultTv2;
//    private Handler mHandler = new Handler();
//    private TimerTask mTask = new TimerTask() {
//
//        @Override
//        public void run() {
//            mMove_Lay1.setVisibility(View.INVISIBLE);
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.growth_record_lastheightresult);

        init();
        setEvent();
    }

    /**
     * 초기화
     */
    public void init() {

        mTxtTitleLastheight =       (TextView)      findViewById(R.id.txt_title_lastheight);

        mView                   =       (View)          findViewById(R.id.common_back_top);
        // 그래프 퍼센티지 부모 레이아웃
        mMoveLay                =       (RelativeLayout)  findViewById(R.id.move_lay);
        // 그래프 퍼센티지 표시 레이아웃
        mPerLay                 =       (LinearLayout)  findViewById(R.id.per_lay);

        mOneLay                 =       (LinearLayout)  findViewById(R.id.one_lay);
        mTwoLay                 =       (LinearLayout)  findViewById(R.id.two_lay);

        // 성장 결과 Text (순서대로 출생정보 -> 출생정보 키 몸무게 머리둘레 // 키 몸무게 머리둘레 결과물 표시
        mLeftTv                 =       (TextView)      findViewById(R.id.left_tv);
        mCenterTv               =       (TextView)      findViewById(R.id.center_tv);
        mRightTv                =       (TextView)      findViewById(R.id.right_tv);
        mHeightTv               =       (TextView)      findViewById(R.id.height_tv);
        mHeightMaleTv           =       (TextView)      findViewById(R.id.height_male_tv);
        mHeightFemaleTv           =       (TextView)      findViewById(R.id.height_female_tv);
        mResultTv1              =       (TextView)      findViewById(R.id.result_tv1);
        mResultTv2              =       (TextView)      findViewById(R.id.result_tv2);

        mConfirmBtn             =       (ImageButton)    findViewById(R.id.confirm_btn);

        mHeight_Pbar            =       (ProgressBar)   findViewById(R.id.height_pbar);

        mImgTriCenter           =       (ImageView)     findViewById(R.id.img_tri_center);
        mImgTriRight           =       (ImageView)     findViewById(R.id.img_tri_right);

        mBtnShare               =       (ImageButton)   findViewById(R.id.share_btn);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        Intent intent = getIntent();
        mAgeFlag = intent.getExtras().getBoolean(CommonData.JSON_MBER_FLAG);

        if (mAgeFlag) {
            mOneLay.setVisibility(View.VISIBLE);
            mTwoLay.setVisibility(View.INVISIBLE);

            mMyCm = intent.getExtras().getString(CommonData.JSON_MBER_LAST_CM); // 나의예상 키
            mStartCm = intent.getExtras().getString(CommonData.JSON_MBER_START_PER); // 최고 키
            mEndCm = intent.getExtras().getString(CommonData.JSON_MBER_END_PER); // 최저 키
            mLeftTv.setText(mStartCm+"cm");
            mRightTv.setText(mEndCm+"cm");
            //mCenterTv.setText(mMyCm);
            mHeightTv.setText(mMyCm);

            if(mEndCm.equals(mMyCm)){
                mHeight_Pbar.setProgress(100);
                mImgTriCenter.setVisibility(View.INVISIBLE);
                mImgTriRight.setVisibility(View.VISIBLE);
                mCenterTv.setVisibility(View.INVISIBLE);
            }else{
                graphLine = (int)((StringUtil.getFloat(mMyCm)-StringUtil.getFloat(mStartCm)) / (StringUtil.getFloat(mEndCm) - StringUtil.getFloat(mStartCm))*100);
                mHeight_Pbar.setProgress(graphLine);
                android.util.Log.i("GrowthRecordLastHresult", "Pbar: " + graphLine);
                mImgTriCenter.setVisibility(View.VISIBLE);
                mImgTriRight.setVisibility(View.INVISIBLE);
                mCenterTv.setVisibility(View.VISIBLE);

            }

            mResultTv1.setText(getString(R.string.lastheight_result));
            mTxtTitleLastheight.setText(R.string.final_key_prediction_1);
        }else {
            mMotherHeight = intent.getExtras().getString(CommonData.JSON_MBER_MOTHER); // 엄마키
            mFatherHeight = intent.getExtras().getString(CommonData.JSON_MBER_FATHER); // 아빠키
            Double reulstheight;


            if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)){
                mOneLay.setVisibility(View.INVISIBLE);
                mTwoLay.setVisibility(View.VISIBLE);

                mHeightMaleTv.setText(String.valueOf((Double.parseDouble(mFatherHeight) + Double.parseDouble(mMotherHeight) + 13 ) / 2));
                mHeightFemaleTv.setText(String.valueOf((Double.parseDouble(mFatherHeight) + Double.parseDouble(mMotherHeight) - 13 ) / 2));

            }else{
                if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnSex().equals(CommonData.MALE)){
                    reulstheight= (Double.parseDouble(mFatherHeight) + Double.parseDouble(mMotherHeight) + 13 ) / 2;

                }else {
                    reulstheight = (Double.parseDouble(mFatherHeight) + Double.parseDouble(mMotherHeight) - 13 ) / 2;
                }

                mHeightTv.setText(String.valueOf(reulstheight));

                mOneLay.setVisibility(View.VISIBLE);
                mTwoLay.setVisibility(View.INVISIBLE);
            }



            mMoveLay.setVisibility(View.GONE);
            mPerLay.setVisibility(View.GONE);
            mTxtTitleLastheight.setText(R.string.final_key_prediction_2);
            mResultTv1.setText(getString(R.string.lastheight_result3));
        }

        String nickName = MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNcm();

        setTitle(R.string.lastheight_title);
        findViewById(R.id.confirm_btn).setOnClickListener(btnListener);
        mView.findViewById(R.id.common_left_btn).setOnClickListener(btnListener);
        mOneLay.setOnClickListener(btnListener);
        mTwoLay.setOnClickListener(btnListener);
        mBtnShare.setOnClickListener(btnListener);

//        mHandler.postDelayed(mTask, 2000);

    }
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            setSeekValPosition();
        }

    }

    private void setSeekValPosition() {

        int padding = (int) (mHeight_Pbar.getResources().getDimension(R.dimen._4_dp) + mHeight_Pbar.getResources().getDimension(R.dimen._4_dp));
        int startPos = mHeight_Pbar.getLeft() + (int)mHeight_Pbar.getResources().getDimension(R.dimen._4_dp);
        int moveX = (mHeight_Pbar.getWidth()-padding) * mHeight_Pbar.getProgress() / mHeight_Pbar.getMax() + startPos - (mImgTriCenter.getWidth()/2);
        int pbar_width = mHeight_Pbar.getWidth();
        Logger.i("GrowthRecordLastHResult", "setSeekValPosition.seekBar.moveX="+moveX);
        Logger.i("GrowthRecordLastHResult", "setSeekValPosition.seekBar.getWidth()="+mHeight_Pbar.getWidth());
        Logger.i("GrowthRecordLastHResult", "setSeekValPosition.mSeekValTv.getWidth()="+mImgTriCenter.getWidth());

//        if ((moveX+mSeekValTv.getWidth()) > mSeekbar.getWidth()) {
//            moveX = mSeekbar.getWidth() - mSeekValTv.getWidth();
//        }

        if (moveX <= mHeight_Pbar.getX()) {
            mImgTriCenter.setX(mHeight_Pbar.getX() + mImgTriCenter.getWidth()/2);
        } else {

            if ((moveX+mImgTriCenter.getWidth()) >= mHeight_Pbar.getWidth()) {
                moveX = mHeight_Pbar.getWidth() - mImgTriCenter.getWidth();
            }

            float calX = ((float)graphLine / 100) * (float)pbar_width;
            moveX = (int)calX;
            Logger.i("GrowthRecordLasthResult","MoveX: " + moveX + "/" + graphLine );
            mImgTriCenter.setX(moveX);
        }
    }

    /**
     * 우리아이 성장예측 공유
     */
    public void requestGrowthLastRecordDataShareApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {"api_code":"asstb_mber_cntr_height_last"
        //,"insures_code":"108"
        //,"cntr_typ":"11"
        //,"mber_sn":"115232"
        //,"last_height":"168"
        //}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, "asstb_mber_cntr_height_last");    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_CNTR_TYP, "11");
            object.put(CommonData.JSON_MBER_SN, CommonData.getInstance().getMberSn());             //  회원고유값
            object.put("last_height",mAgeFlag? mHeightTv.getText().toString() : mHeightMaleTv.getText().toString());
            object.put("last_height_girl",mAgeFlag? "" : mHeightFemaleTv.getText().toString());

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(GrowthRecordLastHeightResult.this, NetworkConst.NET_ASSTB_MBER_CNTR_HEIGHT_LAST, NetworkConst.getInstance().getDefDomain(), networkListener, params, new MakeProgress(this));
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch (type) {
                case NetworkConst.NET_ASSTB_MBER_CNTR_HEIGHT_LAST:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);

                                if (data_yn.equals(CommonData.YES)) {
                                    String imgUrl = "https://wkd.walkie.co.kr/HL_FV/info/image/01_11.png";

                                    String cntr_url = resultData.getString("cntr_url");
                                    if(cntr_url.contains("https://wkd.walkie.co.kr"));
                                    String param = cntr_url.replace("https://wkd.walkie.co.kr","");

                                    View view = LayoutInflater.from(GrowthRecordLastHeightResult.this).inflate(R.layout.applink_dialog_layout, null);
                                    ApplinkDialog dlg = ApplinkDialog.showDlg(GrowthRecordLastHeightResult.this, view);
                                    dlg.setSharing(imgUrl, "img", "", "","[현대해상 "+ KakaoLinkUtil.getAppname(GrowthRecordLastHeightResult.this.getPackageName(),GrowthRecordLastHeightResult.this)+"]","우리 아이 성장 예측키","자세히보기","",false,"chl_growth_prediction.png",param,cntr_url);
                                } else {

                                }
                            } catch (Exception e) {
                                GLog.e(e.toString());

                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");

                            break;
                    }
                    break;
            }

        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;

            switch (v.getId()) {
                case R.id.common_left_btn:
                case R.id.confirm_btn:
                    GLog.i("--confirm_btn--", "dd");
                    GrowthRecordLastHeight.resume_flag = true;
                    finish();
                    break;
                case R.id.one_lay:
                case R.id.two_lay:
                    intent = new Intent(GrowthRecordLastHeightResult.this, BackWebViewActivity.class);
                    intent.putExtra(CommonData.EXTRA_URL, "http://www.naver.com");
                    intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.baby_growth_status));
                    startActivity(intent);
                    Util.BackAnimationStart(GrowthRecordLastHeightResult.this);
                    break;
                case R.id.share_btn:
                    requestGrowthLastRecordDataShareApi();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        GrowthRecordLastHeight.resume_flag = true;
        finish();
    }

}
