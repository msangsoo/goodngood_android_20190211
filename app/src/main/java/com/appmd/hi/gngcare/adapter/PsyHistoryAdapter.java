package com.appmd.hi.gngcare.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.PsyResultItem;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;

import java.util.ArrayList;


/**
 * Created by jihoon on 2016-04-18.
 * 성장 FAQ 목록 어댑터
 * @since 0, 1
 */
public class PsyHistoryAdapter extends ArraySwipeAdapter<PsyResultItem> {

    private LayoutInflater mInflater;
    private ArrayList<PsyResultItem> mData;
    private Activity mContext;


    public PsyHistoryAdapter(Activity context, ArrayList<PsyResultItem> object){
        super(context, 0, object);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = object;
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final PsyResultItem item = mData.get(position);

        PsyHistoryAdapter.ViewHolder holder = null;

        if(convertView == null){
            convertView =   mInflater.inflate(R.layout.psy_history_list_row, null);

            holder      =   new PsyHistoryAdapter.ViewHolder();

            holder.mTit      =       (TextView)  convertView.findViewById(R.id.psy_title);
            holder.mDate        =       (TextView)  convertView.findViewById(R.id.psy_date);
            holder.mIsChild_Image   = (ImageView) convertView.findViewById(R.id.is_child_image);

            convertView.setTag(holder);

        }else{
            holder  =   (PsyHistoryAdapter.ViewHolder)    convertView.getTag();
        }

        if(position % 2 == 0){
            convertView.setBackgroundResource(R.color.color_97b376);
        }else{
            convertView.setBackgroundResource(R.color.color_b9cba1);
        }

        if (item != null) {
            String mTit, mCon, mPyn;
            mTit = item.getmTitle();
            mCon = item.getmDate();
            mPyn = item.getmPyn();

            holder.mTit.setText(mTit);
            holder.mDate.setText(mCon);
            if ("Y".equals(mPyn)){
                holder.mIsChild_Image.setVisibility(View.INVISIBLE);
            }else{
                holder.mIsChild_Image.setVisibility(View.VISIBLE);
            }
        }
        return convertView;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder{

        TextView mTit , mDate;
        ImageView mIsChild_Image;
    }

}
