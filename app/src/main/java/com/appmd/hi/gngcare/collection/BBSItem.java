package com.appmd.hi.gngcare.collection;

/**
 * Created by MobileDoctor on 2017-06-07.
 */

public class BBSItem {
    private String info_day;
    private String info_title_url;
    private String info_subject;
    private int content_typ;
    private String content_typ_mobile_img;

    public BBSItem(){}

    public BBSItem(String info_subject, int content_typ, String info_title_url, String content_typ_mobile_img, String info_day){
        this.info_subject = info_subject;
        this.content_typ = content_typ;
        this.info_title_url = info_title_url;
        this.content_typ_mobile_img = content_typ_mobile_img;
        this.info_day = info_day;
    }

    public String getInfo_day() {
        return info_day;
    }

    public void setInfo_day(String info_day) {
        this.info_day = info_day;
    }

    public String getInfo_title_url() {
        return info_title_url;
    }

    public void setInfo_title_url(String info_title_url) {
        this.info_title_url = info_title_url;
    }

    public String getInfo_subject() {
        return info_subject;
    }

    public void setInfo_subject(String info_subject) {
        this.info_subject = info_subject;
    }

    public int getContent_typ() {
        return content_typ;
    }

    public void setContent_typ(int content_typ) {
        this.content_typ = content_typ;
    }

    public String getContent_typ_mobile_img() {
        return content_typ_mobile_img;
    }

    public void setContent_typ_mobile_img(String content_typ_mobile_img) {
        this.content_typ_mobile_img = content_typ_mobile_img;
    }
}
