package com.appmd.hi.gngcare.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_asstb_mber_keep_member_chl_info;
import com.appmd.hi.gngcare.intro.LoginActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-28.
 *
 * @since 0, 1
 */
public class SwitchMember2Activity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener{

    private Button mConfirmBtn;
    private Intent intent = null;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ImageButton mBackimg;

    private String mberno ="";
    private String memId ="";
    private String memId_site_bool ="";
    private String memId_app_bool ="";
    private int lastCheckedPosition = 0;
    List<Tr_asstb_mber_keep_member_chl_info.chldrn> mList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.switch_member_activity2);

        setTitle(getString(R.string.member_switch2));

        init();
        setEvent();


//        mNextBtn.setEnabled(false);  // 인증 버튼 비활성화 기본
        setConfirmBtn(isConfirm());

        intent = getIntent();

        if (intent != null) {
            mberno  =   intent.getStringExtra(CommonData.EXTRA_MBERNO);
            memId = intent.getStringExtra(CommonData.EXTRA_MEMID);
            memId_site_bool = intent.getStringExtra(CommonData.EXTRA_MEMID_SITE_BOOL);
            memId_app_bool = intent.getStringExtra(CommonData.EXTRA_MEMID_APP_BOOL);
        } else {
            GLog.i("intent = null", "dd");
        }

        requestChlList();

    }
    /**
     * 초기화
     */
    public void init(){

        mConfirmBtn     =   (Button)            findViewById(R.id.confirm_btn);

        mRecyclerView = findViewById(R.id.recyclerview);
        mBackimg = findViewById(R.id.common_left_btn);

        mBackimg.setOnClickListener(v -> finish());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(SwitchMember2Activity.this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);


        mLayoutManager = new LinearLayoutManager(SwitchMember2Activity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        //mBirthTv.setOnClickListener(this);
        mConfirmBtn.setOnClickListener(this);
    }

    /**
     * 인증 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildCerfiti(){
        boolean bool = true;
//
//        if(mNameEdit.getText().toString().trim().equals("")){    // 이름 입력이 없다면
//            mNameEdit.requestFocus();
//            mDialog =   new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
//            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
//            mDialog.setContent("보험 가입 정보를 입력해 주세요.");//getString(R.string.popup_dialog_name_error)
//            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
//            mDialog.show();
//            bool = false;
//            return bool;
//        }





        return bool;

    }

    /**
     * * 회원인증 가능한지 체크
    * @return boolean ( true - 저장 가능, false - 저장 불가 )
    */
    public boolean isConfirm(){
        boolean bool = true;

//        if(mNameEdit.getText().toString().trim().equals("")){    // 이름 입력이 없다면
//            bool = false;
//            return bool;
//        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

//        if(bool) {
        mConfirmBtn.setEnabled(true);

        mConfirmBtn.setBackgroundResource(R.drawable.btn_5_22396c);
        mConfirmBtn.setTextColor(ContextCompat.getColorStateList(SwitchMember2Activity.this, R.color.color_ffffff_btn));
//        }else{
//            mNextBtn.setEnabled(false);
//
//            mNextBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
//            mNextBtn.setTextColor(ContextCompat.getColorStateList(SwitchMemberActivity.this, R.color.color_ffffff_btn));
//        }

    }

    /**
     * 아이리스트
     *
     */
    public void requestChlList() {
//json={"api_code":"asstb_mber_keep_member_chl_info"
//                                    ,"insures_code":"108"
//                                    ,"mber_sn":"115232"
//                                    ,"mber_no":"9999999999970"
//                                    }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_KEEP_MEMBER_CHL_INFO);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_MBER_NO,     mberno);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember2Activity.this, NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_CHL_INFO, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }

    }


    /**
     * 아이맵핑
     *
     */
    public void requestChlMapping() {
//json={"api_code":"asstb_mber_keep_member_chl_info_mapping"
//                                    ,"insures_code":"108"
//                                    ,"mber_sn":"115232"
//                                    ,"chl_sn":"97819"
//                                    ,"chldrn_nm":"홍길동Y"
//                                    ,"mber_no":"9999999999970"
//                                    ,"full_join_sn":"224302"
//                                    ,"full_chldrn_nm":"태아"
//                                    ,"full_chldrn_joinserial":"L0171077777700002"
//                                    }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_KEEP_MEMBER_CHL_INFO_MAPPING);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_CHL_SN,     MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_CHLDRN_NM,     MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNm());
            object.put(CommonData.JSON_MBER_NO,     mList.get(lastCheckedPosition).mber_no);
            object.put("full_join_sn",     mList.get(lastCheckedPosition).full_join_sn);
            object.put("full_chldrn_nm",     mList.get(lastCheckedPosition).full_chldrn_nm);
            object.put("full_chldrn_joinserial",     mList.get(lastCheckedPosition).full_chldrn_joinserial);


            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember2Activity.this, NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_CHL_INFO_MAPPING, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }


    }


    /**
     * 아이디 선택 후 아이맵핑
     *
     */
    public void requestSelectIdChlMapping() {
//              json={
//              "api_code":"asstb_mber_keep_member_id_change_chl_info_mapping"
//              ,"insures_code":"108"
//              ,"mber_sn":"115232"
//              ,"chl_sn":"97819"
//              ,"chldrn_nm":"홍길동Y"
//              ,"mber_no":"9999999999970"
//              ,"full_join_sn":"224302"
//              ,"full_chldrn_nm":"태아"
//              ,"full_chldrn_joinserial":"L0171077777700002"
//              ,"sel_site_yn":"N"
//              ,"app_site_yn":"Y"
//              ,"sel_memid":"tjhong@gchealthcare.com"
//              }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE_CHL_INFO_MAPPING);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_SN,     commonData.getMberSn());
            object.put(CommonData.JSON_CHL_SN,     MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
            object.put(CommonData.JSON_CHLDRN_NM,     MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnNm());
            object.put(CommonData.JSON_MBER_NO,     mList.get(lastCheckedPosition).mber_no);
            object.put("full_join_sn",     mList.get(lastCheckedPosition).full_join_sn);
            object.put("full_chldrn_nm",     mList.get(lastCheckedPosition).full_chldrn_nm);
            object.put("full_chldrn_joinserial",     mList.get(lastCheckedPosition).full_chldrn_joinserial);

            object.put(CommonData.JSON_SEL_SITE_YN,     memId_site_bool);
            object.put(CommonData.JSON_APP_SITE_YN,     memId_app_bool);
            object.put(CommonData.JSON_SEL_MEMID,     memId);


            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember2Activity.this, NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE_CHL_INFO_MAPPING, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }


    }

    /**
     * 로그인
     *
     * @param mber_id  아이디
     * @param mber_pwd 비밀번호
     */
    public void requestLogin(String mber_id, String mber_pwd) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(SwitchMember2Activity.this, NetworkConst.NET_LOGIN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    private Object parseJSON(JSONObject result) {
        Gson gson = new Gson();
        return gson.fromJson(result.toString(), Tr_asstb_mber_keep_member_chl_info.class);
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);

            switch ( type ) {
                case NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_CHL_INFO:   // 아이리스트
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 아이리스트
                                    Object obj = parseJSON(resultData);
                                    if(obj instanceof Tr_asstb_mber_keep_member_chl_info){
                                        Tr_asstb_mber_keep_member_chl_info data = (Tr_asstb_mber_keep_member_chl_info) obj;


                                        mAdapter.setData(data.chldrn_list);
                                    }


                                }else {  // 현대해상 미가입자
                                    mDialog = new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_not_found_chl));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;

                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;

                case NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_CHL_INFO_MAPPING :
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 아이맵칭
                                    requestLogin(commonData.getMberId(), commonData.getMberPwd());
                                    MainActivity.mJunChangeChild = true;

                                }else {  // 현대해상 미가입자
                                    mDialog = new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_chl_content));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;

                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;


                case NetworkConst.NET_ASSTB_MBER_KEEP_MEMBER_ID_CHANGE_CHL_INFO_MAPPING :
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:

                            try {

                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                                if(data_yn.equals(CommonData.YES)){   // 아이맵칭
                                    requestLogin(commonData.getMberId(), commonData.getMberPwd());
                                    MainActivity.mJunChangeChild = true;

                                }else {  // 현대해상 미가입자
                                    mDialog = new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_chl_content));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                        }
                                    });
                                    mDialog.show();
                                }


                            } catch ( Exception e ) {
                                e.printStackTrace();
                            }


                            break;

                        default:
                            if(dialog != null){
                                dialog.show();
                            }

                            break;
                    }
                    break;

                case NetworkConst.NET_LOGIN:    // 로그인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_LOGIN", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);


                                if(data_yn.equals(CommonData.YES)){	// 로그인 성공
                                    GLog.i("NET_LOGIN SUCCESS", "dd");

                                    mDialog =   new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setHtmlContent(getString(R.string.popup_dialog_contactor_complete4));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            commonData.setMberId(commonData.getMberId());
                                            commonData.setMberPwd(commonData.getMberPwd());

                                            if (!commonData.getMberId().equals("")) {    // 아이디가 있다면 부모
                                                commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                            } else {    // 없다면 자녀
                                                commonData.setLoginType(CommonData.LOGIN_TYPE_CHILD);
                                            }

                                            loginSuccess(SwitchMember2Activity.this, resultData,true);

                                        }
                                    });
                                    mDialog.show();

                                }else{	// 로그인 실패
                                    GLog.i("NET_LOGIN FAIL", "dd");
                                    //초기화
                                    commonData.setMberId("");
                                    commonData.setMberPwd("");
                                    mDialog	=	new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            // 로그인 실패 시 내용을 전부 지운다.
                                            dialog.dismiss();
                                            // 로그인 실패 시 내용을 전부 지운다.
                                            commonData.setMberPwd(null);
                                            commonData.setMain_Category("");
                                            commonData.setAutoLogin(false);
                                            commonData.setRememberId(false);
                                            commonData.setMberSn("");

                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_bef_cm", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_bef_kg", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_kg","");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_term_kg", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_chl_birth_de", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_milk_yn", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_birth_due_de", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_chl_typ", "");
                                            Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_actqy", "");


                                            commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                            mDialog.dismiss();
                                            finish();

                                            Intent intent2 = new Intent(SwitchMember2Activity.this, LoginActivity.class);
                                            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent2);

                                        }
                                    });
                                    mDialog.show();

                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                                //초기화
                                commonData.setMberId("");
                                commonData.setMberPwd("");
                                GLog.i("NET_LOGIN Exception", "dd");
                                mDialog	=	new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.popup_dialog_switch_member_error));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(CustomAlertDialog dialog, Button button) {
                                        // 로그인 실패 시 내용을 전부 지운다.
                                        dialog.dismiss();
                                        // 로그인 실패 시 내용을 전부 지운다.
                                        commonData.setMberPwd(null);
                                        commonData.setMain_Category("");
                                        commonData.setAutoLogin(false);
                                        commonData.setRememberId(false);
                                        commonData.setMberSn("");

                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_bef_cm", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_bef_kg", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_kg","");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_term_kg", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_chl_birth_de", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_milk_yn", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_birth_due_de", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_mber_chl_typ", "");
                                        Util.setSharedPreference(SwitchMember2Activity.this, "MonJin_actqy", "");


                                        commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                                        mDialog.dismiss();
                                        finish();

                                        Intent intent2 = new Intent(SwitchMember2Activity.this, LoginActivity.class);
                                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent2);

                                    }
                                });
                                mDialog.show();
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            if (dialog != null) {
                                dialog.show();
                            }
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            dialog.show();

        }
    };




    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {



        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.switch_member_list_item, parent, false);
            return new ViewHolder(itemView);
        }

        public void setData(List<Tr_asstb_mber_keep_member_chl_info.chldrn> dataList) {
            mList.clear();
            mList.addAll(dataList);
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {
            final Tr_asstb_mber_keep_member_chl_info.chldrn data = mList.get(position);

            holder.mChlRb.setText(String.format(getString(R.string.chl_list),position+1));
            holder.mNameTv.setText(data.full_chldrn_nm);

            if(data.full_chl_exist_yn.equals("Y")) {
                if(data.full_chldrn_lifyea == null || data.full_chldrn_lifyea.equals("")
                        || data.full_chldrn_lifyea.equals("000000") || data.full_chldrn_lifyea.equals("00000000") || data.full_chldrn_lifyea.equals("19000000")){
                    holder.mBirthTv.setText("");
                }else {
                    holder.mBirthTv.setText(data.full_chldrn_lifyea);
                }
                holder.mBirth_tv_1.setText("생년월일");
                holder.mSexTv.setText(data.full_chldrn_sex.equals("1")? "남" : "여");
            }
            else {
                if(data.full_chldrn_aft_lifyea == null || data.full_chldrn_aft_lifyea.equals("")
                        || data.full_chldrn_aft_lifyea.equals("000000") || data.full_chldrn_aft_lifyea.equals("00000000") || data.full_chldrn_aft_lifyea.equals("19000000")){
                    holder.mBirthTv.setText("");
                }else {
                    holder.mBirthTv.setText(data.full_chldrn_aft_lifyea);
                }
                holder.mBirth_tv_1.setText("출산예정일");
                holder.mSexTv.setText("태아");
            }




            holder.mChlRb.setChecked(position == lastCheckedPosition);

            if(mList.size() == position+1){
                holder.mLine.setVisibility(View.GONE);
            }else{
                holder.mLine.setVisibility(View.VISIBLE);
            }




        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            RadioButton mChlRb;
            TextView mNameTv;
            TextView mBirthTv, mBirth_tv_1;
            TextView mSexTv;
            View mLine;


            public ViewHolder(View itemView) {
                super(itemView);

                mChlRb = itemView.findViewById(R.id.list_btn);
                mNameTv = itemView.findViewById(R.id.name_tv);
                mBirthTv = itemView.findViewById(R.id.birth_tv);
                mSexTv = itemView.findViewById(R.id.sex_tv);
                mLine = itemView.findViewById(R.id.view_line);
                mBirth_tv_1 = itemView.findViewById(R.id.birth_tv_1);

                mChlRb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lastCheckedPosition = getAdapterPosition();
                        notifyDataSetChanged();

                    }
                });


            }
        }
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.confirm_btn: // 인증 버튼
                GLog.i("certifi_btn", "dd");
                mDialog = new CustomAlertDialog(SwitchMember2Activity.this, CustomAlertDialog.TYPE_B);
                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                mDialog.setHtmlContent(getString(R.string.member_switch_complete));
                mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog,button)->{
                    Intent intent1 = new Intent(SwitchMember2Activity.this, SettingActivity.class);
                    startActivity(intent1);
                    activityClear();
                    finish();
                    dialog.dismiss();
                });
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                    if( memId != null && !memId.equals("")){
                        requestSelectIdChlMapping();
                    }else {
                        requestChlMapping();
                    }
                    dialog.dismiss();
                });
                mDialog.show();

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if(isChecked){
            if(isConfirm()){
               setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }
        }else{
            setConfirmBtn(false);
        }
    }
}
