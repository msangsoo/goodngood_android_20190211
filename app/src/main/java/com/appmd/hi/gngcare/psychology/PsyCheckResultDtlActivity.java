package com.appmd.hi.gngcare.psychology;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by CP on 2018. 4. 7..
 */

public class PsyCheckResultDtlActivity extends PsyBaseActivity {

    public static Activity PSY_CHECK_RESULT_ACTIVITY;
    public LayoutInflater mLayoutInflater;

    private Intent intent;
    private LinearLayout containLy;
    private TextView mTitleTxt;
    private String gTitle;
    private String gComment;
    //private ArrayList<LinearLayout> linearArr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.psy_check_result_dtl_activity);
        PSY_CHECK_RESULT_ACTIVITY = PsyCheckResultDtlActivity.this;
        init();
        intent = getIntent();
        gTitle = intent.getStringExtra("G_TITLE");

        gComment = intent.getStringExtra("G_COMMENT");

        String[] lineStrArr = gComment.split("\r\n●");


        mTitleTxt.setText(gTitle);
        setRow(lineStrArr);


    }

    public void onBackBtn(View view){
        finish();
    }

    public void setRow(String[] rowStrArr){

        //전개자로 xml파일을 가져옴

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for(int i=0; i<rowStrArr.length; i++) {
            rowStrArr[i] = rowStrArr[i].replace("●", "");
            rowStrArr[i] = rowStrArr[i].replaceAll("●", "");

            LinearLayout linear = (LinearLayout) inflater.inflate(R.layout.psy_result_decs_row, null);
            TextView rowText = (TextView) linear.findViewById(R.id.contentText);
            if(rowStrArr[i].length()==0)
                rowText.setVisibility(View.GONE);


            rowText.setText(rowStrArr[i]);

            String tmp = rowText.getText().toString().trim();

            if(tmp.length() != 0){
                containLy.addView(linear);
            }
            //파라미터를 세팅해줌
//            LinearLayout.LayoutParams paramlinear = new LinearLayout.LayoutParams(
//
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//
//                    LinearLayout.LayoutParams.MATCH_PARENT
//
//            );
//            paramlinear.setMargins(60,60,60,60);
//            containLy.setLayoutParams(paramlinear);




        }


    }


    /**
     * 초기화
     */
    public void init() {

        containLy = (LinearLayout)findViewById(R.id.containLy);
        mTitleTxt = (TextView) findViewById(R.id.mTitleTxt1);


    }



    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GLog.i("onStop", "dd");

    }




    @Override
    public void onBackPressed() {
        if(mOnKeyBackPressedListener != null){
            mOnKeyBackPressedListener.onBack();
        }else{
            finish();
        }
    }

    @Override
    public void finish(){
        super.finish();
        Util.BackAnimationEnd(PsyCheckResultDtlActivity.this);
    }

    public interface onKeyBackPressedListener {
        void onBack();
    }
    private PsyMainActivity.onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(PsyMainActivity.onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        GLog.i("onNewIntent", "dd");

        if ( CommonData.getInstance().getMemberId() == 0 ) {
            GLog.i("CommonData.getInstance().getMemberId() == 0", "dd");
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }



}
