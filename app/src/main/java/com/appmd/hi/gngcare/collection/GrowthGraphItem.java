package com.appmd.hi.gngcare.collection;

/**
 * Created by jihoon on 2016-04-19.
 * 그래프 평균 데이터 아이템
 * @since 0, 1
 */
public class GrowthGraphItem {

    private String mValue;

    /**
     * 그래프 평균 데이터
     * @param value 개월수에 해당하는 값
     */
    public GrowthGraphItem(String value){
        this.mValue =   value;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }
}
