package com.appmd.hi.gngcare.motherhealth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.util.JsonLogPrint;
import com.appmd.hi.gngcare.intro.IntroActivity;
import com.appmd.hi.gngcare.main.BabyInfoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.psychology.PsyBaseActivity;
import com.appmd.hi.gngcare.psychology.PsyMainActivity;
import com.appmd.hi.gngcare.psychology.PsyMainFragment;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by CP on 2018. 4. 6..
 */

public class MotherHealthRegActivity extends PsyBaseActivity {

    //   public static Activity PSY_CHECK_START_ACTIVITY;
    public LayoutInflater mLayoutInflater;
    public Fragment mContentFragment;
    public FrameLayout mFrameContainer;
    private Toolbar toolbar;


    // 네비바
    public RelativeLayout mBgActionBar;
    private ImageButton mLefeBtn;
    private RelativeLayout mRightLayout;
    private FrameLayout mBgBabyFace;
    private ImageView mRightImg;
    private TextView mTitleTv;



    private boolean selBeforeFlag = true;

    private final int pageCnt = 2;
    private int curPosition = 0;


//    private ViewPager viewPager;
//    private RegViewPagerAdapter viewPagerAdapter;
//    private ImageView mIndicator;

    private Intent intent;

    private String apiCode = "";
    private String insuresCode = "";
    private String mberSn = "";
    private int TabNum = 0;
    private Bundle bundle;


    public String getApiCode() {
        return apiCode;
    }

    public String getInsuresCode() {
        return insuresCode;
    }

    public String getMberSn() {
        return mberSn;
    }


    //"api_code":"mber_mother_reg_ok","insures_code":"108","mber_sn": "21182"


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mother_health_reg_activity);
        //       PSY_CHECK_START_ACTIVITY = MatherHealthRegDataActivity.this;
        mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init();
        setEvent();

        mTitleTv.setText(getString(R.string.mother_health_reg_title_text));
        intent = getIntent();

//        if(intent != null){
//            TabNum = intent.getIntExtra("Num",0);
//        }

        /*호출시 받아야 할 데이터*/
        apiCode = "mber_mother_reg_ok";
        insuresCode = "108";
        mberSn = CommonData.getInstance().getMberSn();

        // 자녀 데이터가 있는경우 UI 세팅
        if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
            CustomImageLoader.displayImage(MotherHealthRegActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
            //    mRightLayout.setVisibility(View.VISIBLE);
        } else {
            mRightLayout.setVisibility(View.GONE);
        }
        requestMotherMonJinDataApi();

//        viewPagerAdapter = new RegViewPagerAdapter(getSupportFragmentManager());
//        viewPager.setAdapter(viewPagerAdapter);

    }


    public void setMatherBeforeFlag(boolean flag) {
        selBeforeFlag = flag;
    }

    public boolean getMatherBeforeFlag() {
        return selBeforeFlag;
    }


    /**
     * 초기화
     */
    public void init() {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_no_main);

        // start custom actionbar leftmargin remove
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        // end custom actionbar leftmargin remove
        mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);

        mBgActionBar = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_action_bar);
        mBgActionBar.setBackgroundColor(getResources().getColor(R.color.color_fd0ea2));
        mLefeBtn = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.left_btn);
        mRightLayout = (RelativeLayout) getSupportActionBar().getCustomView().findViewById(R.id.right_layout);
        mBgBabyFace = (FrameLayout) getSupportActionBar().getCustomView().findViewById(R.id.bg_baby_face);
        mRightImg = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.photo_img);

        mRightLayout.setVisibility(View.GONE);


        mTitleTv = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.title_tv);

//        viewPager = (ViewPager) findViewById(R.id.viewpager);
//
//        mIndicator = (ImageView) findViewById(R.id.mIndicator);

    }




    /* 타이틀 변경 */
    public void setTitleTxt(String title) {
        mTitleTv.setText(title);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {

        mRightLayout.setOnClickListener(btnListener);
        mLefeBtn.setOnClickListener(btnListener);
        mRightImg.setOnClickListener(btnListener);


//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int i, float v, int i1) {
//
//            }
//
//            @Override
//            public void onPageSelected(int i) {
//                //Toast.makeText(this(), "select position : " + i, 200).show();
//                if (i == 0) {
//                    mIndicator.setImageResource(R.drawable.mother_health_point01);
//                } else {
//                    mIndicator.setImageResource(R.drawable.mother_health_point02);
//                    regStep02Fragment.setLayout();
//                    regStep02Fragment.initCurDate();
//                }
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int i) {
//
//            }
//        });

    }


    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


        GLog.i("onResume", "dd");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        GLog.i("onStop", "dd");

    }

    /**
     * 버튼 클릭 리스너
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            Intent intent = null;
            String str = "";
            Fragment fragment = null;

            switch (v.getId()) {
                case R.id.left_btn:
                    onBackPressed();
                    break;
                case R.id.photo_img:
                    intent = new Intent(MotherHealthRegActivity.this, BabyInfoActivity.class);
                    startActivityForResult(intent, CommonData.REQUEST_CHILD_MANAGE);
                    Util.BackAnimationStart(MotherHealthRegActivity.this);
                    break;
//                case R.id.prevBtn:
//                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
//                    break;
//                case R.id.nextBtn:
//                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
//                    break;

            }

            if (!str.equals("")) {
                mTitleTv.setText(str);
            }
        }
    };

    public void replaceFragment(final Fragment fragment, final boolean isReplace, boolean isAnim, Bundle bundle) {

        android.util.Log.i("MotherMainA", "replaceFragment: " + fragment);

//        BaseFragment.newInstance(this);
//        if (isReplace)
//            removeAllFragment();

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (isAnim)
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left  , R.anim.slide_in_left, R.anim.slide_out_right);
        if (bundle != null)
            fragment.setArguments(bundle);

        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
        if (!isFinishing()) {
            if (isReplace == false)
                transaction.addToBackStack(null);

            transaction.commit();
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isReplace == false)
                        transaction.addToBackStack(null);

                    transaction.commitAllowingStateLoss();
                }
            }, 100);
        }
    }


    MotherHealthRegStepOneFragment regStep01Fragment = new MotherHealthRegStepOneFragment();
    MotherHealthRegStepTwoFragment regStep02Fragment = new MotherHealthRegStepTwoFragment();

//    private class RegViewPagerAdapter extends FragmentPagerAdapter {
//        public RegViewPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//
//        @Override
//        public Fragment getItem(int position) {
//
//            Fragment f = null;
//
//            if (position == 0) {
//                f = regStep01Fragment;
//            } else {
//                f = regStep02Fragment;
//            }
//            return f;
//        }
//
//        @Override
//        public int getCount() {
//            return pageCnt;
//        }
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (mOnKeyBackPressedListener != null) {
//            mOnKeyBackPressedListener.onBack();
//        } else {
//            finish();
//        }
    }

    @Override
    public void finish() {
        super.finish();
        Util.BackAnimationEnd(MotherHealthRegActivity.this);
    }

    public interface onKeyBackPressedListener {
        void onBack();
    }

    private PsyMainActivity.onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(PsyMainActivity.onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }

    /**
     * Fragment 변경
     *
     * @param fragment 변경할 fragment
     */
    public void switchContent(Fragment fragment) {
        mContentFragment = fragment;

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
        } else {
            GLog.e("Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        if (CommonData.getInstance().getMemberId() == 0) {
            Intent introIntent = new Intent(getApplicationContext(), IntroActivity.class);
            startActivity(introIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.i("requestCode = " + requestCode, "dd");
        GLog.i("resultCode = " + resultCode, "dd");
        GLog.i("data = " + data, "dd");

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CommonData.REQUEST_CHILD_MANAGE:   // 자녀관리
                GLog.i("REQUEST_CHILD_MANAGE", "dd");
                mFrameContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 자녀 데이터가 있는경우 UI 세팅
                        if (!MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn().equals("")) {
                            CustomImageLoader.displayImage(MotherHealthRegActivity.this, MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChldrnOrgImage(), mRightImg);
                            //    mRightLayout.setVisibility(View.VISIBLE);
                        } else {
                            mRightLayout.setVisibility(View.GONE);
                        }

                        setResult(RESULT_OK);
                        switchContent(new PsyMainFragment());
                    }
                }, CommonData.ANI_DELAY_500);
                break;

        }

        if (mContentFragment != null) {
            GLog.i("mContentFragment != null", "dd");
            mContentFragment.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        // // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }


    /**
     * 문진 정보 가져오기
     */
    public void requestMotherMonJinDataApi() {
        Log.i("MotherRegTwo", "requestMotherMonJinDataApi: 호출 ");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // { ""api_code"": ""goalqy_hl_sel"",""insures_code"": ""108"",""mber_sn"": ""76503""}
        // "api_code": "mber_mother_sel","insures_code": "108","mber_sn": "76503","goalqy_sn": "706","birth_chl_yn": "N","actqy": "1","bef_cm": "168","bef_kg": "50","mber_kg": "75","ber_kg": "75","mber_term_kg": "0","mber_birth_due_de": "20181208","mber_chl_birth_de": "","mber_chl_typ": "2","mber_milk_yn": "","goal_brssr_contraction": "0","goal_brssr_relax": "0","goal_bef_bdsg": "0","goal_aft_bdsg": "0","goal_bdwgh": "0","goal_lcltyrt": "0","goal_ntr_calory": "0","goal_mvm_calory": "176","goal_mvm_stepcnt": "6000","goal_water_ntkqy": "0","goal_water_goalqy": "0","input_de": "20180508","input_de": "20180508","data_yn": "Y"
        try {
            JSONObject object = new JSONObject();
            object.put("api_code", "mber_mother_sel");    //  api 코드명
            object.put("insures_code","108");
            object.put("mber_sn",  CommonData.getInstance().getMberSn());             //  회원고유값

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            JsonLogPrint.printJson(params.toString());
            RequestApi.requestApi(MotherHealthRegActivity.this, NetworkConst.NET_MOTHER_REVC_DATA, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            hideProgress();
            switch (type) {
                case NetworkConst.NET_MOTHER_REVC_DATA:             // 문진정보 가져오기
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            try {
                                //    Toast.makeText(getActivity(), resultData.toString(), Toast.LENGTH_LONG).show();
                                bundle = new Bundle();
                                bundle.putInt("TabNum",TabNum);

                                if (resultData.getString("data_yn").compareTo("Y") == 0) {

                                    String MonJin_bef_cm = resultData.getString("bef_cm");
                                    String MonJin_bef_kg = resultData.getString("bef_kg");
                                    String MonJin_mber_kg = resultData.getString("mber_kg");
                                    String MonJin_mber_term_kg = resultData.getString("mber_term_kg");
                                    String MonJin_mber_chl_birth_de = resultData.getString("mber_chl_birth_de");
                                    String MonJin_mber_milk_yn = resultData.getString("mber_milk_yn");
                                    String MonJin_mber_birth_due_de = resultData.getString("mber_birth_due_de");
                                    String MonJin_mber_chl_typ = resultData.getString("mber_chl_typ");
                                    String MonJin_actqy = resultData.getString("actqy");
                                    String MonJin_birth_chl_yn = resultData.getString("birth_chl_yn");


                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_bef_cm", MonJin_bef_cm);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_bef_kg", MonJin_bef_kg);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_kg", MonJin_mber_kg);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_term_kg", MonJin_mber_term_kg);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_chl_birth_de", MonJin_mber_chl_birth_de);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_milk_yn", MonJin_mber_milk_yn);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_birth_due_de", MonJin_mber_birth_due_de);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_mber_chl_typ", MonJin_mber_chl_typ);
                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_actqy", MonJin_actqy);
                                    commonData.setbirth_chl_yn(MonJin_birth_chl_yn);
//                                    Util.setSharedPreference(MotherHealthRegActivity.this, "MonJin_birth_chl_yn", MonJin_birth_chl_yn);
                                    if(commonData.getMberGrad().equals("10")) {

                                        replaceFragment(new MotherHealthRegStepOneFragment(), true, true, bundle);
                                    }else{
                                        if(commonData.getbirth_chl_yn().equals(CommonData.YES)){
                                            setMatherBeforeFlag(false); //출산 후
                                        }else{
                                            setMatherBeforeFlag(true); //임신 중
                                        }

                                        replaceFragment(new MotherHealthRegStepTwoFragment(), true, true, null);
                                    }

                                } else {
                                   if(commonData.getMberGrad().equals("10")) {
                                        replaceFragment(new MotherHealthRegStepOneFragment(), true, true, bundle);
                                    }else{
                                        if(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.YES)){
                                            setMatherBeforeFlag(false); //출산 후
                                        }else{
                                            setMatherBeforeFlag(true); //임신 중
                                        }

                                        replaceFragment(new MotherHealthRegStepTwoFragment(), true, true, null);
                                    }
                                    //코드 에러
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type,
                                   int httpResultCode, CustomAlertDialog dialog) {
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog
                dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };


}