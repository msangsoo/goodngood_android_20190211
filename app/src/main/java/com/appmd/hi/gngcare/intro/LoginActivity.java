package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-30.
 * 로그인 화면
 * @since 0, 1
 */
public class LoginActivity extends IntroBaseActivity implements View.OnClickListener , CompoundButton.OnCheckedChangeListener , View.OnFocusChangeListener{
    private final String TAG = LoginActivity.class.getSimpleName();


    private EditText mIdEdit, mPwdEdit;
    private ImageButton mIdDelBtn, mPwdDelBtn;

    private CheckBox mAutoLoginCb, mRememberIdCb;

    private Button mLoginBtn, mJoinBtn;

    private TextView mFindIdTv, mFindPwdTv;

    public CustomAlertDialog mDialog;
    private String temp="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        setTitle(getString(R.string.login));

        init();
        setEvent();

        Util.setTextViewCustom(LoginActivity.this, mFindIdTv, mFindIdTv.getText().toString(), mFindIdTv.getText().toString(), 0, 15, Typeface.BOLD, true);
        Util.setTextViewCustom(LoginActivity.this, mFindPwdTv, mFindPwdTv.getText().toString(), mFindPwdTv.getText().toString(), 0, 15, Typeface.BOLD, true);

        if(commonData.getRememberId()){ // 아이디 저장이라면 아이디 표시
            mIdEdit.setText(commonData.getMberId());
        }

        mRememberIdCb.setChecked(commonData.getRememberId());
        mAutoLoginCb.setChecked(commonData.getAutoLogin());

        commonData.setMberSn("");
    }

    /**
     * 초기화
     */
    public void init(){

        mFindIdTv        =   (TextView)          findViewById(R.id.find_id_tv);
        mFindPwdTv      =   (TextView)          findViewById(R.id.find_pwd_tv);

        mIdEdit       =   (EditText)          findViewById(R.id.id_edit);
        mPwdEdit      =   (EditText)          findViewById(R.id.pwd_edit);

        mIdDelBtn     =   (ImageButton)       findViewById(R.id.id_del_btn);
        mPwdDelBtn    =   (ImageButton)       findViewById(R.id.pwd_del_btn);

        mAutoLoginCb      =   (CheckBox)          findViewById(R.id.auto_login_cb);
        mRememberIdCb     =   (CheckBox)          findViewById(R.id.remember_id_cb);

        mLoginBtn       =   (Button)            findViewById(R.id.login_btn);
        mJoinBtn        =   (Button)            findViewById(R.id.join_btn);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mFindIdTv.setOnClickListener(this);
        mFindPwdTv.setOnClickListener(this);
        mIdDelBtn.setOnClickListener(this);
        mPwdDelBtn.setOnClickListener(this);

        mLoginBtn.setOnClickListener(this);
        mJoinBtn.setOnClickListener(this);

        mAutoLoginCb.setOnCheckedChangeListener(this);
        mRememberIdCb.setOnCheckedChangeListener(this);

        mIdEdit.setOnFocusChangeListener(this);
        mPwdEdit.setOnFocusChangeListener(this);

        mIdEdit.addTextChangedListener(new MyTextWatcher());
        mPwdEdit.addTextChangedListener(new MyTextWatcher());
    }

    /**
     * 로그인 가능한지 체크
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildLogin(String mber_id, String mber_pwd){
        boolean bool = true;

        if(mIdEdit.getText().toString().trim().equals("")){    // 아이디 미입력
            mIdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(LoginActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_login_input_id));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(mPwdEdit.getText().toString().trim().equals("")){   // 비밀번호 미입력
            mPwdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(LoginActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_login_input_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }

        if(!Util.checkEmail(mber_id)){  // 이메일 형식이 아니라면
            mIdEdit.requestFocus();
            mDialog =   new CustomAlertDialog(LoginActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));;
            mDialog.setContent(getString(R.string.popup_dialog_mail_format_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.show();
            bool = false;
            return bool;
        }


        return bool;

    }

    /**
     * 로그인 가능한지 체크
     * @return boolean ( true - 저장 가능, false - 저장 불가 )
     */
    public boolean isConfirm(){
        boolean bool = true;

        if(mIdEdit.getText().toString().trim().equals("")){    // 이름 입력이 없다면
            bool = false;
            return bool;
        }

        if(mPwdEdit.getText().toString().trim().equals("")){   // 휴대폰 번호
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool){

        if(bool) {
            mLoginBtn.setEnabled(true);

            mLoginBtn.setBackgroundResource(R.drawable.btn_5_6bb0d7);
            mLoginBtn.setTextColor(ContextCompat.getColorStateList(LoginActivity.this, R.color.color_ffffff_btn));
        }else{
            mLoginBtn.setEnabled(false);

            mLoginBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
            mLoginBtn.setTextColor(ContextCompat.getColorStateList(LoginActivity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 로그인
     * @param mber_id 아이디
     * @param mber_pwd  비밀번호
     */
    public void requestLogin(String mber_id, String mber_pwd){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID + Build.VERSION.RELEASE);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            Log.i(TAG, "deviceToken = " + RCApplication.deviceToken);
            if(RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            Log.i(TAG, "앱버전 : "+commonData.getAppVersion());

            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(LoginActivity.this, NetworkConst.NET_LOGIN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        }catch(Exception e){
            Log.i(TAG, e.toString());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();

        switch (id){
            case R.id.auto_login_cb: // 자동로그인
                if(isChecked){
                    commonData.setAutoLogin(true);
                    commonData.setRememberId(true);
                    mRememberIdCb.setChecked(true);
                }else { // 자동로그인 해제시 기존에 저장해두었던 ID, PW 삭제
                    commonData.setAutoLogin(false);
                    commonData.setMberId("");
                    commonData.setMberPwd("");
                }
                break;
            case R.id.remember_id_cb:    // 아이디 저장
                if(!isChecked) {    // 아이디 저장 해제시
                    commonData.setMberId(""); //  아이디 저장 해제시 기존에 저장해두었던 ID 삭제

                    if (mAutoLoginCb.isChecked()){
                        mRememberIdCb.setChecked(true);

                        Toast.makeText(LoginActivity.this, getString(R.string.toast_auto_remember_id_error), Toast.LENGTH_SHORT).show();
                    }else{
                        commonData.setRememberId(false);
                    }
                }else{
                    commonData.setRememberId(true);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()){
            case R.id.login_btn: // 로그인
                Log.i(TAG, "login_btn");

                String mber_id, mber_pwd;

                mber_id     =   mIdEdit.getText().toString().trim();
                mber_pwd    =   mPwdEdit.getText().toString().trim();

                if(invaildLogin(mber_id, mber_pwd)) {   // 로그인 가능하다면
                    // 로그인 api 호출
                    Log.i(TAG, "로그인 가능");
                    requestLogin(mber_id, mber_pwd);
                }
                break;
            case R.id.join_btn: // 가입하기
                intent = new Intent(LoginActivity.this, JoinIntroActivity.class);
                startActivity(intent);
//                finish();
                break;
            case R.id.id_del_btn: // 아이디 삭제
                commonView.setClearEditText(mIdEdit);
                break;
            case R.id.pwd_del_btn: // 비밀번호 삭제
                commonView.setClearEditText(mPwdEdit);
                break;
            case R.id.find_id_tv:  // 아이디 찾기
                Log.i(TAG, "find_id_tv");
                intent = new Intent(LoginActivity.this, FindIdActivity.class);
                startActivityForResult(intent, CommonData.REQUEST_LOGIN_ID);
                Util.BackAnimationStart(LoginActivity.this);
                break;
            case R.id.find_pwd_tv:  // 비밀번호 찾기
                Log.i(TAG, "find_pwd_tv");
                intent = new Intent(LoginActivity.this, FindPwdActivity.class);
                startActivity(intent);
                Util.BackAnimationStart(LoginActivity.this);
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.id_edit:    // 이메일
                commonView.setClearImageBt(mIdDelBtn, hasFocus);
                break;
            case R.id.pwd_edit:	   // 패스워드
                commonView.setClearImageBt(mPwdDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_LOGIN:	// 로그인
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            Log.i(TAG, "NET_LOGIN");

                            try {
                                String data_yn = resultData.getString(CommonData.JSON_DATA_YN);



                                if(data_yn.equals(CommonData.YES)){	// 로그인 성공
                                    Log.i(TAG, "NET_LOGIN SUCCESS");
                                    String id = mIdEdit.getText().toString();
                                    temp = CommonData.getInstance().getLink();
                                    if (id.equals(commonData.getMberId()) == false) {
                                        Logger.i(TAG,  "아이디 변경 기존 데이터 삭제 기존아이디:"+commonData.getMberId()+", 새아이디:"+id);
                                        DBHelper helper = new DBHelper(LoginActivity.this);
                                        helper.deleteAll();



                                        SharedPref.getInstance(LoginActivity.this).removeAllPreferences();    // sharedprefrence 초기화
                                    }

                                    CommonData.getInstance().setLink(temp);


                                    //정회원 전환시 필요정보(2019.03.04)
                                    commonData.setMberId(mIdEdit.getText().toString().trim());
                                    commonData.setMberPwd(mPwdEdit.getText().toString().trim());


                                    if(mAutoLoginCb.isChecked()){   // 자동로그인 체크했다면 ID, PWD 저장
                                        commonData.setMberId(id);
                                        commonData.setMberPwd(mPwdEdit.getText().toString().trim());
                                    }

                                    if(mRememberIdCb.isChecked()){  // 아이디기억 체크했다면 ID 저장
                                        commonData.setMberId(mIdEdit.getText().toString().trim());
                                        commonData.setMberPwd(mPwdEdit.getText().toString().trim());
                                    }

                                    commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);

                                    loginSuccess(LoginActivity.this, resultData,false);

                                }else{	// 로그인 실패
                                    Log.i(TAG, "NET_LOGIN FAIL");
                                    temp = CommonData.getInstance().getLink();
                                    mDialog	=	new CustomAlertDialog(LoginActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_login_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            // 로그인 실패 시 내용을 전부 지운다.
                                            dialog.dismiss();

                                        }
                                    });
                                    mDialog.show();

                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                                temp = CommonData.getInstance().getLink();
                                Log.i(TAG, "NET_LOGIN Exception");
                                mDialog	=	new CustomAlertDialog(LoginActivity.this, CustomAlertDialog.TYPE_A);
                                mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                mDialog.setContent(getString(R.string.popup_dialog_login_error));
                                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(CustomAlertDialog dialog, Button button) {
                                        // 로그인 실패 시 내용을 전부 지운다.
                                        dialog.dismiss();

                                    }
                                });
                                mDialog.show();
                            }

                            break;

                        default:
                            Log.i(TAG, "JOIN FAIL");

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };


    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            if(isConfirm()){
                setConfirmBtn(true);
            }else{
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        GLog.i("resultCode : " + resultCode, "dd");
        GLog.i("requestCode :" + requestCode, "dd");

        if (resultCode != RESULT_OK) {
            GLog.i("resultCode != RESULT_OK", "dd");
            return;
        }

        switch (requestCode) {
            case CommonData.REQUEST_LOGIN_ID: // 음식사진 등록 날짜
                mIdEdit.setText(commonData.getMberId());
                break;
        }
    }
}
