package com.appmd.hi.gngcare.intro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.RCApplication;
import com.appmd.hi.gngcare.base.IntroBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAlertDialogInterface;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by jihoon on 2016-03-30.
 * 회원가입 화면
 *
 * @since 0, 1
 */
public class JoinActivity extends IntroBaseActivity implements View.OnClickListener, View.OnFocusChangeListener {
    public final String TAG = JoinActivity.class.getSimpleName();

    private EditText mIdEdit, mPwdEdit, mPwdReEdit;
    private ImageButton mIdDelBtn, mPwdDelBtn, mPwdReDelBtn;

    private Button mJoinBtn;

    private String mBerId, mBerPwd;

    private Intent intent = null;
    private String mBerNm, mBirthday, mBerNo, mBerHp, mGrad, mMberSn,mMberJob;
    private int mBerNation = 1;
    private int mGender = 1;

    private ImageButton mBackimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_activity);

        setTitle(getString(R.string.make));

        init();
        setEvent();

        mBackimg = findViewById(R.id.common_left_btn);
        mBackimg.setVisibility(View.VISIBLE);

        mBackimg.setOnClickListener(v -> finish());

        intent = getIntent();

        if (intent != null) {

            mBerNm = intent.getStringExtra(CommonData.EXTRA_MBERNM);
            mBirthday = intent.getStringExtra(CommonData.EXTRA_BIRTHDAY);
            mBerNation = intent.getIntExtra(CommonData.EXTRA_MBERNATION, 1);
            mGender = intent.getIntExtra(CommonData.EXTRA_GENDER, 1);
            mBerNo = intent.getStringExtra(CommonData.EXTRA_MBERNO);
            mBerHp = intent.getStringExtra(CommonData.EXTRA_PHONENO);
            mGrad = intent.getStringExtra(CommonData.EXTRA_GLAD);
            mMberSn = intent.getStringExtra(CommonData.EXTRA_MBERSN);
            mMberJob = intent.getStringExtra(CommonData.EXTRA_JOB);


        }


    }

    /**
     * 초기화
     */
    public void init() {

        mIdEdit = (EditText) findViewById(R.id.id_edit);
        mPwdEdit = (EditText) findViewById(R.id.pwd_edit);
        mPwdReEdit = (EditText) findViewById(R.id.pwd_re_edit);

        mIdDelBtn = (ImageButton) findViewById(R.id.id_del_btn);
        mPwdDelBtn = (ImageButton) findViewById(R.id.pwd_del_btn);
        mPwdReDelBtn = (ImageButton) findViewById(R.id.pwd_re_del_btn);

        mJoinBtn = (Button) findViewById(R.id.join_btn);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent() {
        mIdDelBtn.setOnClickListener(this);
        mPwdDelBtn.setOnClickListener(this);
        mPwdReDelBtn.setOnClickListener(this);

        mJoinBtn.setOnClickListener(this);

        mIdEdit.setOnFocusChangeListener(this);
        mPwdEdit.setOnFocusChangeListener(this);
        mPwdReEdit.setOnFocusChangeListener(this);

        mIdEdit.addTextChangedListener(new MyTextWatcher());
        mPwdEdit.addTextChangedListener(new MyTextWatcher());
        mPwdReEdit.addTextChangedListener(new MyTextWatcher());
    }

    /**
     * 회원가입 가능한지 체크
     *
     * @param id         아이디
     * @param pw         비밀번호
     * @param pw_confirm 비밀번호 확인
     * @return bool ( true - 인증가능, false - 인증 불가 )
     */
    public boolean invaildLogin(String id, String pw, String pw_confirm) {
        boolean bool = true;

        if (id.equals("")) {    // 아이디 미입력
            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_login_input_id));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mIdEdit.requestFocus();
                }
            });
            mDialog.show();
            bool = false;
            return bool;
        }

        if (!Util.checkEmail(id)) {   // 아이디 형식 맞지 않음
            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_mail_format_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mIdEdit.requestFocus();
                    commonView.setSelectionCursor(mIdEdit, mIdEdit.length());
                }
            });
            mDialog.show();
            return false;
        }

        if (pw.equals("") || !Util.checkPasswd(pw)) {   // 비밀번호 미입력

            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_invalid_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mPwdEdit.requestFocus();
                }
            });
            mDialog.show();
            bool = false;
            return bool;
        }

        if (pw_confirm.equals("") || !Util.checkPasswd(pw_confirm)) {   // 비밀번호 확인 미입력
            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_invalid_pwd));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mPwdReEdit.requestFocus();
                }
            });
            mDialog.show();
            bool = false;
            return bool;
        }

        if (!pw.equals(pw_confirm)) {   // 비밀번호 미일치
            commonView.setSelectionCursor(mPwdReEdit, mPwdReEdit.length());
            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_login_pwd_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mPwdReEdit.requestFocus();
                }
            });
            mDialog.show();
            bool = false;
            return bool;
        }


        return bool;

    }

    /**
     * 가입 가능한지 체크
     *
     * @return boolean ( true - 저장 가능, false - 저장 불가 )
     */
    public boolean isConfirm() {
        boolean bool = true;

        if (mIdEdit.getText().toString().trim().equals("")) {    // 아이디
            bool = false;
            return bool;
        }

        if (mPwdEdit.getText().toString().trim().equals("")) {   // 비밀번호
            bool = false;
            return bool;
        }

        if (mPwdReEdit.getText().toString().trim().equals("")) {   // 비밀번호 확인
            bool = false;
            return bool;
        }

        return bool;
    }

    /**
     * 확인 버튼 설정
     *
     * @param bool ( true - 활성화, false - 비활성화 )
     */
    public void setConfirmBtn(boolean bool) {

        if (bool) {
            mJoinBtn.setEnabled(true);

            mJoinBtn.setBackgroundResource(R.drawable.btn_5_6bb0d7);
            mJoinBtn.setTextColor(ContextCompat.getColorStateList(JoinActivity.this, R.color.color_ffffff_btn));
        } else {
            mJoinBtn.setEnabled(false);

            mJoinBtn.setBackgroundResource(R.drawable.background_5_e6e7e8);
            mJoinBtn.setTextColor(ContextCompat.getColorStateList(JoinActivity.this, R.color.color_ffffff_btn));
        }

    }

    /**
     * 이메일 중복 검사
     *
     * @param email 로그인 이메일 계정
     */
    public void emailVerify(String email) {

        if (!Util.checkEmail(email)) {   // 아이디 형식 맞지 않음
            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));

            mDialog.setContent(getString(R.string.popup_dialog_mail_format_error));
            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mIdEdit.requestFocus();
                    commonView.setSelectionCursor(mIdEdit, mIdEdit.length());
                }
            });
            mDialog.show();
            return;
        }


        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

//        {   "api_code": "mber_reg_check_id",   "insures_code": "106", "token": "deviceToken",  "mber_id": "tjhong@gchealthcare.com"   }

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_REG_CHECK_ID);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, email);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JoinActivity.this, NetworkConst.NET_MBER_REG_CHECK_ID, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }

    }

    /**
     * 회원가입
     *
     * @param mber_nm     이름
     * @param mber_lifyea 생년월일
     * @param mber_hp     휴대폰 번호
     * @param mber_nation 내국 & 외국 ( 1 - 내국인, 2 - 외국인 )
     * @param mber_sex    성별 ( 1 - 남자, 2 - 여자 )
     * @param mber_no     회원인증 키값 ( 회원인증 하면 서버에서 제공하는 고유 키값 13자리 )
     * @param mber_id     회원 id
     * @param mber_pwd    회원 비밀번호
     */
    public void requestMberRegOk(String mber_nm, String mber_lifyea, String mber_hp, String mber_nation, String mber_sex,
                                 String mber_no, String mber_id, String mber_pwd, String mber_job) {
//        {"api_code": "mber_reg_ok","insures_code": "106", "token": "deviceToken",  "app_code": "android19" ,
// "mber_nm": "테스트" ,"mber_lifyea": "20140101","mber_hp": "010758421333", "mber_nation": "1"  ,  "mber_sex": "2"
// ,"mber_no": "9999999999999", "phone_model": "SM-N910S","pushk": "0", "app_ver": "0.28" , "mber_id": "tjhong22@gchealthcare.com" , "mber_pwd": "tes222tpwd"  }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_MBER_REG_OK);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID + Build.VERSION.RELEASE);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_NM, mber_nm);
            object.put(CommonData.JSON_MBER_LIFYEA, mber_lifyea);
            object.put(CommonData.JSON_MBER_HP, mber_hp);
            object.put(CommonData.JSON_MBER_NATION, mber_nation);
            object.put(CommonData.JSON_MBER_SEX, mber_sex);
            object.put(CommonData.JSON_MBER_NO, mber_no);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);
            object.put(CommonData.JSON_MBER_JOB_YN, mber_job);
            object.put(CommonData.JSON_PUSH_K, "0");

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JoinActivity.this, NetworkConst.NET_MBER_REG_OK, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 준회원가입
     *
     */
    public void requestAsstbRegIdPwdInput(String mber_sn, String mber_grad, String mber_id, String mber_pwd) {
//       {
//            "api_code":"asstb_reg_id_pwd_input","insures_code":"108"
//              ,"app_code":"android"
//              ,"token":"APA91bHCUpphD7XglAYaBx6YXkUwMvVxydBB2pNMSg_z-N13kQ4_1TObbhHt-Aoju6_YqguAQHKQQ2IGxFOgQODYGhkSuBxY7QSQtvm3hm_05lyGl7tnEmTnyQEUBWiF8KRErkoQ3BN8"
//              ,"mber_sn":"115232"
//              ,"mber_grad":"20"
//              ,"mber_id" : "tjhong@gchealthcare.com"
//              ,"mber_pwd":"test1234!"
//   }

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_ASSTB_REG_ID_PWD_INPUT);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID + Build.VERSION.RELEASE);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_GRAD,     mber_grad);
            object.put(CommonData.JSON_MBER_SN,     mber_sn);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JoinActivity.this, NetworkConst.NET_ASSTB_REG_ID_PWD_INPUT, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 로그인
     *
     * @param mber_id  아이디
     * @param mber_pwd 비밀번호
     */
    public void requestLogin(String mber_id, String mber_pwd) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_LOGIN);
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);
            object.put(CommonData.JSON_MBER_ID, mber_id);
            object.put(CommonData.JSON_MBER_PWD, mber_pwd);

            GLog.i("deviceToken = " + RCApplication.deviceToken, "dd");
            if (RCApplication.deviceToken != null) {
                object.put(CommonData.JSON_TOKEN, RCApplication.deviceToken);
            }
            object.put(CommonData.JSON_PHONE_MODEL, Build.MODEL);
            //TODO 테스트 시 버전 99
            object.put(CommonData.JSON_APP_VER, commonData.getAppVersion());
//            object.put(CommonData.JSON_APP_VER, "99");

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(JoinActivity.this, NetworkConst.NET_LOGIN, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgressLayout());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.join_btn: // 아이디 만들기
                GLog.i("아이디 생성", "dd");

                mBerId = mIdEdit.getText().toString().trim();
                mBerPwd = mPwdEdit.getText().toString().trim();
                String pwdConfirm = mPwdReEdit.getText().toString().trim();

                if (invaildLogin(mBerId, mBerPwd, pwdConfirm)) { // 회원가입 가능한 상태라면

                    //TelephonyManager telephony = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    //String  telPhoneNo = telephony.getLine1Number();    // 전화번호 가져오기

                    //if(telPhoneNo == null || telPhoneNo.equals("")) {   // usim 정보를 가져올 수 없다면
                    //   telPhoneNo = "0000";
                    //}

//                    requestMberRegOk(commonData.getMberNm(), commonData.getBirthDay(), telPhoneNo, commonData.getMberNation(), commonData.getGender(), commonData.getMberNo(),
//                                    mBerId, mBerPwd);

                    if(mGrad.equals("20")){
                        requestAsstbRegIdPwdInput(mMberSn,mGrad,mBerId, mBerPwd);
                    }else{
                        requestMberRegOk(mBerNm, mBirthday, mBerHp, String.valueOf(mBerNation), String.valueOf(mGender), mBerNo, mBerId, mBerPwd, mMberJob);
                    }
                }

                break;
            case R.id.id_del_btn: // 아이디 삭제
                commonView.setClearEditText(mIdEdit);
                break;
            case R.id.pwd_del_btn: // 비밀번호 삭제
                commonView.setClearEditText(mPwdEdit);
                break;
            case R.id.pwd_re_del_btn:   // 비밀번호 확인 삭제
                commonView.setClearEditText(mPwdReEdit);
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch (id) {
            case R.id.id_edit:    // 아이디
                commonView.setClearImageBt(mIdDelBtn, hasFocus);

                if (!hasFocus && !mIdEdit.getText().toString().trim().equals(""))
                    emailVerify(mIdEdit.getText().toString().trim());

                break;
            case R.id.pwd_edit:       // 비밀번호
                commonView.setClearImageBt(mPwdDelBtn, hasFocus);
                break;
            case R.id.pwd_re_edit:       // 비밀번호 확인
                commonView.setClearImageBt(mPwdReDelBtn, hasFocus);
                break;


        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            if (getProgressLayout() != null)
                getProgressLayout().setVisibility(View.GONE);
            switch (type) {
                case NetworkConst.NET_MBER_REG_CHECK_ID:   // 아이디 중복 체크
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_MBER_REG_CHECK_ID", "dd");

                            try {
                                String mber_id_yn = resultData.getString(CommonData.JSON_MBER_ID_YN);
                                if (mber_id_yn.equals(CommonData.YES)) {   // 아이디 중복이라면

                                    mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_already_id));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            mIdEdit.requestFocus();

                                        }
                                    });
                                    mDialog.show();
                                } else {
                                    mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_id_complete));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            mPwdEdit.requestFocus();
                                        }
                                    });
                                    mDialog.show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            break;


                        default:
                            if (dialog != null) {
                                dialog.show();
                            }

                            break;
                    }
                    break;
                case NetworkConst.NET_MBER_REG_OK:   // 회원 가입
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_MBER_REG_CHECK_ID", "dd");

                            try {
                                String regyn = resultData.getString(CommonData.JSON_REG_YN);
                                if (regyn.equals(CommonData.YES)) {   // 회원 가입에 성공이라면
                                    Logger.i(TAG,  "회원가입 기존 sqlite 데이터 삭제");
                                    DBHelper helper = new DBHelper(JoinActivity.this);
                                    helper.deleteAll();
                                    SharedPref.getInstance(JoinActivity.this).removeAllPreferences();    // sharedprefrence 초기화

                                    commonData.setMberSn(resultData.getString(CommonData.JSON_MBER_SN));   // api 호출시 키값 저장
                                    requestLogin(mBerId, mBerPwd);

                                } else {
                                    mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
                                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                                    mDialog.setContent(getString(R.string.popup_dialog_sign_error));
                                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(CustomAlertDialog dialog, Button button) {
                                            dialog.dismiss();
                                            mPwdEdit.requestFocus();
                                        }
                                    });
                                    mDialog.show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            break;


                        default:
                            if (dialog != null) {
                                dialog.show();
                            }

                            break;
                    }
                    break;
                case NetworkConst.NET_LOGIN:    // 로그인
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_LOGIN", "dd");

                            commonData.setMberId(mBerId);
                            commonData.setMberPwd(mBerPwd);

                            if (!commonData.getMberId().equals("")) {    // 아이디가 있다면 부모
                                commonData.setLoginType(CommonData.LOGIN_TYPE_PARENTS);
                            } else {    // 없다면 자녀
                                commonData.setLoginType(CommonData.LOGIN_TYPE_CHILD);
                            }

                            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setCancelable(false);
                            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                            mDialog.setContent(getString(R.string.popup_dialog_join_complete));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog1, button) -> {
                                loginSuccess(JoinActivity.this, resultData,false);
                            });
                            mDialog.show();

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");
                            if (dialog != null) {
                                dialog.show();
                            }
                            break;
                    }
                    break;
                case NetworkConst.NET_ASSTB_REG_ID_PWD_INPUT : // 준회원가입
                    try {
                        String data_yn = resultData.getString(CommonData.JSON_DATA_YN);
                        if (data_yn.equals(CommonData.YES)) {   // 회원 가입에 성공이라면
                            Logger.i(TAG,  "회원가입 기존 sqlite 데이터 삭제");
                            DBHelper helper = new DBHelper(JoinActivity.this);
                            helper.deleteAll();
                            SharedPref.getInstance(JoinActivity.this).removeAllPreferences();    // sharedprefrence 초기화

                            commonData.setMberSn(resultData.getString(CommonData.JSON_MBER_SN));   // api 호출시 키값 저장
                            requestLogin(mBerId, mBerPwd);

                        } else {
                            mDialog = new CustomAlertDialog(JoinActivity.this, CustomAlertDialog.TYPE_A);
                            mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                            mDialog.setContent(getString(R.string.popup_dialog_sign_error));
                            mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), new CustomAlertDialogInterface.OnClickListener() {
                                @Override
                                public void onClick(CustomAlertDialog dialog, Button button) {
                                    dialog.dismiss();
                                    mPwdEdit.requestFocus();
                                }
                            });
                            mDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };


    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (isConfirm()) {
                setConfirmBtn(true);
            } else {
                setConfirmBtn(false);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

}
