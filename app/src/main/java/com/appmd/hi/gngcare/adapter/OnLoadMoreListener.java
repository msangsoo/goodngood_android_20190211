package com.appmd.hi.gngcare.adapter;

/**
 * Created by MobileDoctor on 2017-06-07.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
