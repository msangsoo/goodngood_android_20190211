package com.appmd.hi.gngcare.collection;

/**
 * Created by jihoon on 2018-04-01.
 * 음원 Item
 * @since 0, 1
 */
public class PsyResultItem {

    private String mTitle;
    private String mDate;
    private String mPyn;

    /**
     * 심리 결과
     * @param tit   제목
     * @param date
     */
    public PsyResultItem(String tit,
                         String date,
                         String pyn){
        this.mTitle = tit;
        this.mDate = date;
        this.mPyn = pyn;
    }


    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmPyn() {
        return mPyn;
    }

    public void setmPyn(String mpyn) {
        this.mPyn = mpyn;
    }


}
