package com.appmd.hi.gngcare.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.collection.BBSItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomImageLoader;
import com.appmd.hi.gngcare.main.BBSViewActivity;

import java.util.ArrayList;

/**
 * Created by MobileDoctor on 2017-06-07.
 */

public class BBSListAdapter extends RecyclerView.Adapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext;
    private ArrayList<BBSItem> mBBSListItem;
    private DisplayMetrics dm;

    private OnLoadMoreListener onLoadMoreListener;

    int visibleThreshold = 1;
    int totalItemCount, lastVisibleItem;
    private boolean loading;

    public BBSListAdapter(Context _context, ArrayList<BBSItem> _mBBSListItem, RecyclerView recyclerView){
        mContext = _context;
        mBBSListItem = _mBBSListItem;
        dm = mContext.getResources().getDisplayMetrics();

        if(recyclerView.getLayoutManager() instanceof GridLayoutManager){
            final GridLayoutManager gridLayoutManager = (GridLayoutManager)recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = gridLayoutManager.getItemCount();
                    lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();

                    if(!loading && totalItemCount <= (lastVisibleItem+visibleThreshold)){
                        if(onLoadMoreListener != null){
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bbs_list_item, parent, false);
        vh = new BBSItemViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BBSItemViewHolder){
            BBSItem curItem = mBBSListItem.get(position);

            ((BBSItemViewHolder)holder).txt_title.setText(curItem.getInfo_subject());
            ((BBSItemViewHolder)holder).txt_category.setText(mContext.getResources().getStringArray(R.array.bbs_category)[curItem.getContent_typ()]);

            CustomImageLoader.clearCache();
            CustomImageLoader.displayImageDefault(mContext, curItem.getContent_typ_mobile_img(), ((BBSItemViewHolder)holder).img_thum);

            if(CommonData.getInstance().getBBSUrlShowCheck(curItem.getInfo_title_url()))
                ((BBSItemViewHolder)holder).img_new.setVisibility(View.INVISIBLE);
            else
                ((BBSItemViewHolder)holder).img_new.setVisibility(View.VISIBLE);

            ((BBSItemViewHolder)holder).frame_item_lay.setOnClickListener(v -> {
                Intent i = new Intent(mContext, BBSViewActivity.class);
                i.putExtra(CommonData.JSON_INFO_TITLE_URL, curItem.getInfo_title_url());
                CommonData.getInstance().setBBSUrlShowCheck(curItem.getInfo_title_url(), true);
                mContext.startActivity(i);
            });
        }
    }

    @Override
    public int getItemCount() {
        return mBBSListItem.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class BBSItemViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout frame_item_lay;
        public ImageView img_thum, img_new;
        public TextView txt_title;
        public TextView txt_category;

        public BBSItemViewHolder(View itemView) {
            super(itemView);

            frame_item_lay = (FrameLayout)itemView.findViewById(R.id.frame_item_lay);
            img_thum = (ImageView)itemView.findViewById(R.id.img_thum);
            img_new = (ImageView)itemView.findViewById(R.id.img_new);
            txt_title = (TextView)itemView.findViewById(R.id.txt_title);
            txt_category = (TextView)itemView.findViewById(R.id.txt_category);
        }
    }
}
