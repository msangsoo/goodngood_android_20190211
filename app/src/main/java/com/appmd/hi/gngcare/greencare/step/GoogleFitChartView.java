/*
 * Copyright (C) 2016 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appmd.hi.gngcare.greencare.step;

import android.Manifest;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.value.TypeDataSet;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.BluetoothManager;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.bluetooth.model.BandModel;
import com.appmd.hi.gngcare.greencare.charting.data.BarEntry;
import com.appmd.hi.gngcare.greencare.chartview.valueFormat.AxisValueFormatter;
import com.appmd.hi.gngcare.greencare.chartview.walk.BarChartView;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperPPG;
import com.appmd.hi.gngcare.greencare.database.DBHelperStep;
import com.appmd.hi.gngcare.greencare.googleFitness.GoogleFitInstance;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.ChartTimeUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.text.DateFormat.getDateTimeInstance;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class GoogleFitChartView {
    public static final String TAG = GoogleFitChartView.class.getSimpleName();

    protected List<BarEntry> mYVals = new ArrayList<>();
    protected Map<Integer, Integer> mGoogleFitStepMap = new HashMap<>();           // 구글 피트니스 조회 데이터 저장
    private Map<Integer, Integer> mDbResultMap = new HashMap<>();   // Sqlite 조회 내용 저장

    private BaseFragment mBaseFragment;
    private Context mContext;

    public RadioGroup mTypeRg;

    private boolean mChartType;

    private int mArrIdx = 0;
    public ChartTimeUtil mTimeClass;

//    public GoogleApiClient mClient = null;
    protected BarChartView mChart;

    protected ImageButton mNextbtn;
    protected ImageButton mPrebtn;
    private TextView mDateTv;

    private Long mTotalVal = 0L;
    private Long mGoalStep = 0L;
//    private DecimalFormat mFloatFormat = new DecimalFormat("#,###.00");

    private TextView tvStepBoxTitle01;
    private TextView tvStepBoxTitle02;
    private TextView tvStepBoxTitle03;
    private TextView tvStepBoxTitle04;
    private TextView tvStepBoxValue01;
    private TextView tvStepBoxValue02;
    private TextView tvStepBoxValue03;
    private TextView tvStepBoxValue04;

    private TextView mGoalDescTv;

    private ImageView IvStepBoxImage02;
    private ImageView IvStepBoxImage03;
    private ImageView IvStepBoxImage04;

    private TextView tvActiveTitle;
    private TextView tvActiveValue;
    private TextView tvActiveValueTag;
    private TextView tvTargetValue;
    private TextView tvTargetValueTag;
    private TextView chartunit;
    //private LinearLayout mChangeLine;

    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private OnDataPointListener mListener;

    int tmp=0;
    String _startDate;
    String _endDate;

    private RadioButton radioBtnDay;
    private RadioButton radioBtnWeek;
    private RadioButton radioBtnMonth;


    private LinearLayout ll_continue_layer; //최장 연속활동 시간 레이어 (칼로리 일때는 안보이기 위해)

    public GoogleFitChartView(BaseFragment baseFragment, View view) {
        mContext = baseFragment.getContext();
        mBaseFragment = baseFragment;
        initView(view);
    }

    private void initView(View view) {
        mChartType = false;

        mTypeRg = (RadioGroup) view.findViewById(R.id.type_radio_group);
        RadioButton radioBtnCal = (RadioButton) view.findViewById(R.id.radio_btn_calory);
        radioBtnCal.setChecked(true);

        mPrebtn = (ImageButton) view.findViewById(R.id.pre_btn);
        mNextbtn = (ImageButton) view.findViewById(R.id.next_btn);
        mDateTv = (TextView) view.findViewById(R.id.period_date_textview);

        mGoalDescTv = (TextView) view.findViewById(R.id.goal_desc_tv);

        RadioGroup periodRg = (RadioGroup) view.findViewById(R.id.period_radio_group);
        radioBtnDay = (RadioButton) view.findViewById(R.id.period_radio_btn_day);
        radioBtnWeek = (RadioButton) view.findViewById(R.id.period_radio_btn_week);
        radioBtnMonth = (RadioButton) view.findViewById(R.id.period_radio_btn_month);
        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth);

        mChart = new BarChartView(mContext, view);
        mChart.setDefaultDummyData(mTimeClass);

        tvStepBoxTitle01 = (TextView) view.findViewById(R.id.tvStepBoxTitle01);   //목표 달성율
        tvStepBoxTitle02 = (TextView) view.findViewById(R.id.tvStepBoxTitle02);   //총이동거리
        tvStepBoxTitle03 = (TextView) view.findViewById(R.id.tvStepBoxTitle03);   //칼로리비교
        tvStepBoxTitle04 = (TextView) view.findViewById(R.id.tvStepBoxTitle04);   //최장연속활동시간
        tvStepBoxValue01 = (TextView) view.findViewById(R.id.tvStepBoxValue01);   // 타이틀
        tvStepBoxValue02 = (TextView) view.findViewById(R.id.tvStepBoxValue02);
        tvStepBoxValue03 = (TextView) view.findViewById(R.id.tvStepBoxValue03);
        tvStepBoxValue04 = (TextView) view.findViewById(R.id.tvStepBoxValue04);

        //mCarorieLayout = (LinearLayout) view.findViewById(R.id.calorie_layout);
        //mStepLayout = (LinearLayout) view.findViewById(R.id.step_layout);
        //mChangeLine = (LinearLayout) view.findViewById(R.id.changeLine);

        IvStepBoxImage02 = (ImageView)view.findViewById(R.id.IvStepBoxImage02);
        IvStepBoxImage03 = (ImageView)view.findViewById(R.id.IvStepBoxImage03);
        IvStepBoxImage04 = (ImageView)view.findViewById(R.id.IvStepBoxImage04);

        chartunit = (TextView) view.findViewById(R.id.chart_unit);
        tvActiveTitle = (TextView) view.findViewById(R.id.tvActiveTitle);
        tvActiveValue = (TextView) view.findViewById(R.id.tvActiveValue);
        tvActiveValueTag = (TextView) view.findViewById(R.id.tvActiveValueaTag);
        tvTargetValue = (TextView) view.findViewById(R.id.tvTargetValue);
        tvTargetValueTag = (TextView) view.findViewById(R.id.tvTargetValueTag);

        mPrebtn.setOnClickListener(mClickListener);
        mNextbtn.setOnClickListener(mClickListener);
        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);
        mTypeRg.setOnCheckedChangeListener(mTypeCheckedChangeListener);

//        mBaseFragment.showProgress();
        setNextButtonVisible();

        // 건강 메시지 세팅 하기
        ((TextView)view.findViewById(R.id.result_tip_textview)).setText(new DeviceDataUtil().getStepMessage());

        ll_continue_layer = view.findViewById(R.id.ll_continue_layer);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, mBaseFragment.getContext());
        //엄마 건강
        view.findViewById(R.id.radio_btn_calory).setOnTouchListener(ClickListener);
        view.findViewById(R.id.radio_btn_step).setOnTouchListener(ClickListener);
        radioBtnDay.setOnTouchListener(ClickListener);
        radioBtnWeek.setOnTouchListener(ClickListener);
        radioBtnMonth.setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        view.findViewById(R.id.radio_btn_calory).setContentDescription(mBaseFragment.getContext().getString(R.string.radio_btn_calory));
        view.findViewById(R.id.radio_btn_step).setContentDescription(mBaseFragment.getContext().getString(R.string.radio_btn_step));

    }

    /**
     * 걸음수 조회 하기
     */
    public DataReadRequest queryFitnessData() {
        DataType dataType1 = null;
        DataType dataType2 = null;

        /* 조회 타입 설정(칼로리, 걸음수) */
        if (getType() == TypeDataSet.Type.TYPE_CALORY) {
            // 칼로리
            dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
            dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;

            //mStepLayout.setVisibility(View.GONE);
            //ssshin 수정 막아둠 mStepLayout GONE한 이유를 모르겠음.
            //mStepLayout.setVisibility(View.VISIBLE);

            //ssshin 수정 막아둠 mCarorieLayout사용되는지 의문
            //mCarorieLayout.setVisibility(View.VISIBLE);
            //mCarorieLayout.setVisibility(View.GONE);
            IvStepBoxImage04.setVisibility(View.GONE);
            //mChangeLine.setBackgroundResource(R.drawable.linetop);
        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            // 걸음수
            dataType1 = DataType.TYPE_STEP_COUNT_DELTA;
            dataType2 = DataType.AGGREGATE_STEP_COUNT_DELTA;
            //mCarorieLayout.setVisibility(View.GONE);
            //mStepLayout.setVisibility(View.VISIBLE);
            IvStepBoxImage04.setVisibility(View.VISIBLE);
            //mChangeLine.setBackgroundResource(R.drawable.textline);
        }

        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        DateFormat dateFormat = getDateTimeInstance();
        Log.i(TAG, "Range Start: " + dateFormat.format(startTime));
        Log.i(TAG, "Range End: " + dateFormat.format(endTime));

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(dataType1, dataType2)
                .bucketByTime(1, mTimeClass.getTimeUnit())
                .setTimeRange(startTime, endTime, MILLISECONDS)
                .build();

        return readRequest;
    }


    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            mTimeClass.clearTime();         // 날자 초기화

            AxisValueFormatter xFormatter = new AxisValueFormatter(periodType);
            mChart.setXValueFormat(xFormatter);

            if (mYVals != null)
                mYVals.clear();

            getData();
        }
    };

    public RadioGroup.OnCheckedChangeListener mTypeCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            getTime();
            setBottomField();

            if (mYVals != null)
                mYVals.clear();

            getData();
        }
    };


    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.pre_btn) {
                mTimeClass.calTime(-1);
            } else if (vId == R.id.next_btn) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
            }

            if (mYVals != null)
                mYVals.clear();

            getData();
            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            mNextbtn.setVisibility(View.INVISIBLE);
        }else{
            mNextbtn.setVisibility(View.VISIBLE);
        }
    }


    protected void getTime() {
        if (mTimeClass != null)
            mTimeClass.getTime();
    }

    /**
     * 날자 계산 후 조회
     */
    public void getData() {
        if (getType() == TypeDataSet.Type.TYPE_CALORY) {
            mChart.setUnitStr("kcal");
            radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryDay));
            radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryWeek));
            radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryMonth));
        } else {
            mChart.setUnitStr("step");
            radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepDay));
            radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepWeek));
            radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepMonth));
        }
        mBaseFragment.showProgress();

        mChart.getBarChart().setDrawMarkers(false);  // 새로운 조회시 마커 사라지게 하기
        // 활동소모 초기화
        mFoodCalories = new ArrayList<>();
        String[] name = mContext.getResources().getStringArray(R.array.life_food);
        String[] unit = mContext.getResources().getStringArray(R.array.life_food_unit);
        String[] value = mContext.getResources().getStringArray(R.array.life_food_calorie);

        for (int i = 0; i < name.length; i++)
            mFoodCalories.add(new FoodCalories(name[i], unit[i], Integer.valueOf(value[i])));

        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        _startDate = sdf.format(startTime);
        _endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(_startDate);
        } else {
            mDateTv.setText(_startDate + " ~ " + _endDate);
        }

        if (GoogleFitInstance.isFitnessAuth(mBaseFragment.getActivity())) {
            Fitness.getHistoryClient(mBaseFragment.getActivity(),
                    GoogleSignIn.getLastSignedInAccount(mBaseFragment.getActivity()))
                    .readData(queryFitnessData())
                    .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                        @Override
                        public void onSuccess(DataReadResponse response) {
                            readData(response.getBuckets());
                            fintnessResult();
                        }
                    });
        } else {
            Log.e(TAG, "구글 피트니스 Noti 작동 태스트 중 계정인증 안됨.");
            GoogleFitInstance.requestGoogleFitnessAuth(mBaseFragment.getActivity());
        }
    }

    private void fintnessResult() {
        Logger.i(TAG, "mTotalVal=" + mTotalVal);
        mChart.setData(mYVals, mTimeClass);
        mChart.animateY();
        TypeDataSet.Period period = mTimeClass.getPeriodType();
        CommonData login = CommonData.getInstance();
        Long dayDate = 1L;
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            dayDate = 1L;
        } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
            dayDate = 7L;
        } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
            dayDate = (long) mTimeClass.getDayOfMonth();
        }

        Long goalKcal = StringUtil.getLong(new DeviceDataUtil().getStepGoalCal());
        goalKcal = goalKcal * dayDate;
        if (StringUtil.getIntVal(login.getMotherGoalCal()) > 0  ) {
            // 로그인 정보에 목표 칼로리가 있는 경우
            goalKcal = StringUtil.getLong(login.getMotherGoalCal());
            goalKcal = goalKcal * dayDate;
        }

        long goalStep = StringUtil.getLong(mBaseFragment.getStepTargetCalulator(goalKcal));
        if (StringUtil.getIntVal(login.getMotherGoalStep()) > 0  ) {
            // 로그인 정보에 목표 걸음이 있는 경우
            goalStep = StringUtil.getLong(login.getMotherGoalStep());
            goalStep = goalStep * dayDate;
        }

//            Long Goal_Cal =  StringUtil.getLong(CommonData.getInstance().getMotherGoalCal()) * dayDate;
//            Long Goal_Step = StringUtil.getLong(CommonData.getInstance().getMotherGoalStep()) * dayDate;


        int sex = StringUtil.getIntVal(login.getGender());
        float weight = StringUtil.getFloatVal(login.getMberKg());
        DBHelper helper = new DBHelper(mBaseFragment.getContext());
//            DBHelperWeight weightDb = helper.getWeightDb();
        DBHelperPPG ppgDB = helper.getPPGDb();

//            DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
//            if (!bottomData.getWeight().isEmpty()) {
//                weight = StringUtil.getFloatVal(login.getMberKg());
//            } else {
//                weight = StringUtil.getFloatVal(login.getMberKg());
//            }
        float height = StringUtil.getFloat(login.getBefCm());

        float avgStepDistance = (StringUtil.getFloat(login.getBefCm()) - 100f); // 평균보폭
        if (getType() == TypeDataSet.Type.TYPE_CALORY) {    // 칼로리

            ll_continue_layer.setVisibility(View.GONE);

            int totalActiveTime = (int) ((avgStepDistance * mTotalStep) * 1.8);
            int maxActiveTime = (int) ((avgStepDistance * mMaxStep) * 1.8);

            // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
//                tvActiveValue.setText(StringUtil.getFormatPrice("" + mBaseFragment.getCalroriTargetCalulator(sex, height, weight, StringUtil.getIntVal("" + mTotalVal))));
//                Logger.d(TAG, mTotalVal + "");


            // 구글피트의 칼로리 계산시을 사용한 칼로리계산
            tvActiveValue.setText(StringUtil.getFormatPrice("" + mTotalVal));
//                tvTargetValue.setText(StringUtil.getFormatPrice("" + Goal_Cal));

            float value = (StringUtil.getFloatVal(tvActiveValue.getText().toString()) / (float) goalKcal) * 100;
            if (Float.isInfinite(value) || Float.isNaN(value)) {
                value = 0f;
            }

            if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                tvStepBoxValue01.setText("-");
            } else {
                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                    tvStepBoxValue01.setText("-");
                } else {
                    tvStepBoxValue01.setText(String.format("%,.1f", value) + "%");
                }
            }
            value = (totalActiveTime / 60f) * 0.01f;

//                tvStepBoxValue03.setText(String.format("%,d", Math.abs((int) value)) + "분");   // 총활동시간
//                tvStepBoxValue04.setText(calcClaorieCompare(mTotalVal.intValue()));
            //ssshin 수정

            tvStepBoxValue02.setText(calcClaorieCompare(mTotalVal.intValue())); //활동 소모 칼로리 비교
            tvStepBoxValue03.setText(String.format("%,d", Math.abs((int) value)) + "분");   // 총활동시간

            if (mBaseFragment == null || mBaseFragment.getActivity() == null)
                return;

            DBHelperPPG.PPGValue ppgvalue = null;
            if (period == TypeDataSet.Period.PERIOD_DAY) {  // 최근
                ppgvalue = ppgDB.getResultFastPPG(helper, _startDate, _endDate);
            }else{                                          // 주간, 월간
                ppgvalue = ppgDB.getResultPPG(_startDate, _endDate);
            }
            if (ppgvalue.hrm.equals("0")){
                tvStepBoxValue04.setText("-");
                //tvStepBoxValue02.setText("");
            }else{
                tvStepBoxValue04.setText(StringUtil.getIntVal(ppgvalue.hrm) + "회/분");   // 심박수 (구 최장연속활동시간)
                //tvStepBoxValue02.setText("");
            }

//                String stepGoalCal = new DeviceDataUtil().getStepGoalCal();
//                if (StringUtil.getIntVal(login.getMotherGoalCal()) > 0  ) {
//                    stepGoalCal = login.getMotherGoalCal();
//                }
//                int goalVal = (int) (StringUtil.getIntVal(stepGoalCal) * dayDate);
            tvTargetValue.setText(String.format("%,d", goalKcal));

        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            ll_continue_layer.setVisibility(View.VISIBLE);
            float totalMoveDistance = (avgStepDistance * mTotalVal) * 0.1f;
            Logger.i(TAG, "avgStepDistince=" + avgStepDistance + ", mTotalVal=" + mTotalVal);
            tvActiveValue.setText(StringUtil.getFormatPrice("" + mTotalVal));
            tvTargetValue.setText(StringUtil.getFormatPrice("" + goalStep));
            float value = ((float) mTotalVal / (float) goalStep) * 100f;
            if (Float.isInfinite(value) || Float.isNaN(value)) {
                value = 0f;
            }

            if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                tvStepBoxValue01.setText("-");
            } else {

                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
                    tvStepBoxValue01.setText("-");
                } else {
                    tvStepBoxValue01.setText(String.format("%,.1f", value) + "%");
                }
            }
            float dispTotalDistance = totalMoveDistance / 1000f;
            value = dispTotalDistance * 0.1f;
            tvStepBoxValue02.setText(String.format("%,.2f", Math.abs(value)) + "km");

//                tmp += 200;
//                dispTotalDistance = dispTotalDistance + (tmp);
//                CDialog.showDlg(mBaseFragment.getContext(), "dispTotalDistance:"+dispTotalDistance +", tmp:" +tmp);

            float savedMoney = calcSaveMoney(dispTotalDistance*100);
            tvStepBoxValue03.setText(StringUtil.getFormatPrice("" + (int)savedMoney) + "원");
            float kmcm = StringUtil.getFloatVal(String.format("%.2f", value)) * 100000;
            if(kmcm == 0.0f){
                tvStepBoxValue04.setText("0cm");
            }else{
                tvStepBoxValue04.setText(String.format("%.1f", (kmcm / mTotalVal)) + "cm");
            }
        }

//            uploadGoogleFitStepData();
        setNextButtonVisible();

        mBaseFragment.hideProgress();
    }


//    /**
//     * 날자 계산 후 조회
//     */
//    public void getData() {
//        if (getType() == TypeDataSet.Type.TYPE_CALORY) {
//            mChart.setUnitStr("kcal");
//            radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryDay));
//            radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryWeek));
//            radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtncaloryMonth));
//        } else {
//            mChart.setUnitStr("step");
//            radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepDay));
//            radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepWeek));
//            radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnstepMonth));
//        }
//        mChart.getBarChart().setDrawMarkers(false);  // 새로운 조회시 마커 사라지게 하기
//        Log.i(TAG, "getType="+getType().name() +", getUnitStr="+ ((AxisValueFormatter) mChart.getXValueFormat()).getUnitStr());
//
//        mBaseFragment.showProgress();
//        GoogleFitInstance.getInstance().buildApiClient(mBaseFragment, new IConnectResult() {
//            @Override
//            public void success(GoogleApiClient client) {
////                mBaseFragment.hideProgress();
////                mClient = client;
//
//                // 활동소모 초기화
//                mFoodCalories = new ArrayList<>();
//                String[] name = mContext.getResources().getStringArray(R.array.life_food);
//                String[] unit = mContext.getResources().getStringArray(R.array.life_food_unit);
//                String[] value = mContext.getResources().getStringArray(R.array.life_food_calorie);
//
//                for (int i = 0; i < name.length; i++)
//                    mFoodCalories.add(new FoodCalories(name[i], unit[i], Integer.valueOf(value[i])));
//
//                long startTime = mTimeClass.getStartTime();
//                long endTime = mTimeClass.getEndTime();
//
//                String format = "yyyy.MM.dd";
//                SimpleDateFormat sdf = new SimpleDateFormat(format);
//
//                _startDate = sdf.format(startTime);
//                _endDate = sdf.format(endTime);
//
//                if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
//                    mDateTv.setText(_startDate);
//                } else {
//                    mDateTv.setText(_startDate + " ~ " + _endDate);
//                }
//
//                new QeuryVerifyDataTask(client).execute();
//
//            }
//            @Override
//            public void fail() {
//                mBaseFragment.hideProgress();
//            }
//        });
//    }
//    /**
//     * 구글피트니스 데이터 조회 하기
//     */
//    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, Void> {
//        DataReadRequest readRequest;
//        private GoogleApiClient mClient;
//        public QeuryVerifyDataTask(GoogleApiClient client) {
//            mClient = client;
//            mGoogleFitStepMap.clear();
//            mDbResultMap.clear();
//
//            mBaseFragment.showProgress();
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            setBottomField();
//        }
//
//        protected Void doInBackground(Void... params) {
//            // Create the query.
//            readRequest = queryFitnessData();
//
//            if (mClient != null) {
//                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
//                readData(dataReadResult);
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//
//            Logger.i(TAG, "mTotalVal=" + mTotalVal);
//            mChart.setData(mYVals, mTimeClass);
//            mChart.animateY();
//            TypeDataSet.Period period = mTimeClass.getPeriodType();
//            CommonData login = CommonData.getInstance();
//            Long dayDate = 1L;
//            if (period == TypeDataSet.Period.PERIOD_DAY) {
//                dayDate = 1L;
//            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
//                dayDate = 7L;
//            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
//                dayDate = (long) mTimeClass.getDayOfMonth();
//            }
//
//            Long goalKcal = StringUtil.getLong(new DeviceDataUtil().getStepGoalCal());
//            goalKcal = goalKcal * dayDate;
//            if (StringUtil.getIntVal(login.getMotherGoalCal()) > 0  ) {
//                // 로그인 정보에 목표 칼로리가 있는 경우
//                goalKcal = StringUtil.getLong(login.getMotherGoalCal());
//                goalKcal = goalKcal * dayDate;
//            }
//
//            long goalStep = StringUtil.getLong(mBaseFragment.getStepTargetCalulator(goalKcal));
//            if (StringUtil.getIntVal(login.getMotherGoalStep()) > 0  ) {
//                // 로그인 정보에 목표 걸음이 있는 경우
//                goalStep = StringUtil.getLong(login.getMotherGoalStep());
//                goalStep = goalStep * dayDate;
//            }
//
////            Long Goal_Cal =  StringUtil.getLong(CommonData.getInstance().getMotherGoalCal()) * dayDate;
////            Long Goal_Step = StringUtil.getLong(CommonData.getInstance().getMotherGoalStep()) * dayDate;
//
//
//            int sex = StringUtil.getIntVal(login.getGender());
//            float weight = StringUtil.getFloatVal(login.getMberKg());
//            DBHelper helper = new DBHelper(mBaseFragment.getContext());
////            DBHelperWeight weightDb = helper.getWeightDb();
//            DBHelperPPG ppgDB = helper.getPPGDb();
//
////            DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
////            if (!bottomData.getWeight().isEmpty()) {
////                weight = StringUtil.getFloatVal(login.getMberKg());
////            } else {
////                weight = StringUtil.getFloatVal(login.getMberKg());
////            }
//            float height = StringUtil.getFloat(login.getBefCm());
//
//            float avgStepDistance = (StringUtil.getFloat(login.getBefCm()) - 100f); // 평균보폭
//            if (getType() == TypeDataSet.Type.TYPE_CALORY) {    // 칼로리
//
//                ll_continue_layer.setVisibility(View.GONE);
//
//                int totalActiveTime = (int) ((avgStepDistance * mTotalStep) * 1.8);
//                int maxActiveTime = (int) ((avgStepDistance * mMaxStep) * 1.8);
//
//                // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
////                tvActiveValue.setText(StringUtil.getFormatPrice("" + mBaseFragment.getCalroriTargetCalulator(sex, height, weight, StringUtil.getIntVal("" + mTotalVal))));
////                Logger.d(TAG, mTotalVal + "");
//
//
//                // 구글피트의 칼로리 계산시을 사용한 칼로리계산
//                tvActiveValue.setText(StringUtil.getFormatPrice("" + mTotalVal));
////                tvTargetValue.setText(StringUtil.getFormatPrice("" + Goal_Cal));
//
//                float value = (StringUtil.getFloatVal(tvActiveValue.getText().toString()) / (float) goalKcal) * 100;
//                if (Float.isInfinite(value) || Float.isNaN(value)) {
//                    value = 0f;
//                }
//
//                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
//                    tvStepBoxValue01.setText("-");
//                } else {
//                    if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
//                        tvStepBoxValue01.setText("-");
//                    } else {
//                        tvStepBoxValue01.setText(String.format("%,.1f", value) + "%");
//                    }
//                }
//                value = (totalActiveTime / 60f) * 0.01f;
//
////                tvStepBoxValue03.setText(String.format("%,d", Math.abs((int) value)) + "분");   // 총활동시간
////                tvStepBoxValue04.setText(calcClaorieCompare(mTotalVal.intValue()));
//                //ssshin 수정
//
//                tvStepBoxValue02.setText(calcClaorieCompare(mTotalVal.intValue())); //활동 소모 칼로리 비교
//                tvStepBoxValue03.setText(String.format("%,d", Math.abs((int) value)) + "분");   // 총활동시간
//
//                if (mBaseFragment == null || mBaseFragment.getActivity() == null)
//                    return;
//
//                DBHelperPPG.PPGValue ppgvalue = null;
//                if (period == TypeDataSet.Period.PERIOD_DAY) {  // 최근
//                    ppgvalue = ppgDB.getResultFastPPG(helper, _startDate, _endDate);
//                }else{                                          // 주간, 월간
//                    ppgvalue = ppgDB.getResultPPG(_startDate, _endDate);
//                }
//                if (ppgvalue.hrm.equals("0")){
//                    tvStepBoxValue04.setText("-");
//                    //tvStepBoxValue02.setText("");
//                }else{
//                    tvStepBoxValue04.setText(StringUtil.getIntVal(ppgvalue.hrm) + "회/분");   // 심박수 (구 최장연속활동시간)
//                    //tvStepBoxValue02.setText("");
//                }
//
////                String stepGoalCal = new DeviceDataUtil().getStepGoalCal();
////                if (StringUtil.getIntVal(login.getMotherGoalCal()) > 0  ) {
////                    stepGoalCal = login.getMotherGoalCal();
////                }
////                int goalVal = (int) (StringUtil.getIntVal(stepGoalCal) * dayDate);
//                tvTargetValue.setText(String.format("%,d", goalKcal));
//
//            } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
//                ll_continue_layer.setVisibility(View.VISIBLE);
//                float totalMoveDistance = (avgStepDistance * mTotalVal) * 0.1f;
//                Logger.i(TAG, "avgStepDistince=" + avgStepDistance + ", mTotalVal=" + mTotalVal);
//                tvActiveValue.setText(StringUtil.getFormatPrice("" + mTotalVal));
//                tvTargetValue.setText(StringUtil.getFormatPrice("" + goalStep));
//                float value = ((float) mTotalVal / (float) goalStep) * 100f;
//                if (Float.isInfinite(value) || Float.isNaN(value)) {
//                    value = 0f;
//                }
//
//                if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
//                    tvStepBoxValue01.setText("-");
//                } else {
//
//                    if (value == 0.0f || Float.isNaN(value) || Float.isInfinite(value)) {
//                        tvStepBoxValue01.setText("-");
//                    } else {
//                        tvStepBoxValue01.setText(String.format("%,.1f", value) + "%");
//                    }
//                }
//                float dispTotalDistance = totalMoveDistance / 1000f;
//                value = dispTotalDistance * 0.1f;
//                tvStepBoxValue02.setText(String.format("%,.2f", Math.abs(value)) + "km");
//
////                tmp += 200;
////                dispTotalDistance = dispTotalDistance + (tmp);
////                CDialog.showDlg(mBaseFragment.getContext(), "dispTotalDistance:"+dispTotalDistance +", tmp:" +tmp);
//
//                float savedMoney = calcSaveMoney(dispTotalDistance*100);
//                tvStepBoxValue03.setText(StringUtil.getFormatPrice("" + (int)savedMoney) + "원");
//                float kmcm = StringUtil.getFloatVal(String.format("%.2f", value)) * 100000;
//                if(kmcm == 0.0f){
//                    tvStepBoxValue04.setText("0cm");
//                }else{
//                    tvStepBoxValue04.setText(String.format("%.1f", (kmcm / mTotalVal)) + "cm");
//                }
//            }
//
////            uploadGoogleFitStepData();
//            setNextButtonVisible();
//
//            mBaseFragment.hideProgress();
//        }
//    }


    /**
     * 구글 걸음 데이터를 서버에 전송 및 Sqlite에 저장하기
     * 일별 조회 일때만 저장하기
     *
     * 20190214 인트로에서 업로드 하도록 수정
     */
    @Deprecated
    private void uploadGoogleFitStepData() {
        if (mGoogleFitStepMap == null)
            return;

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            int hour = 0;
            List<BandModel> dataModelArr = new ArrayList<>();

            Iterator<Integer> iter = mGoogleFitStepMap.keySet().iterator();
           try {
               while(iter.hasNext()){
                   int key = iter.next();
//                System.out.println("Map Value:"+mGoogleFitStepMap.get(key));

                   hour = key;
                   int step = mGoogleFitStepMap.get(key);
                   System.out.println( String.format("key(hour)=%s, value=%s", key, step)+", mDbResultMap.get("+hour+")="+mDbResultMap.get(hour));

                   Calendar calendar = Calendar.getInstance();
                   calendar.setTimeInMillis(mTimeClass.getStartTime());

                   calendar.set(Calendar.HOUR, hour);
                   calendar.set(Calendar.MINUTE, 0);
                   calendar.set(Calendar.SECOND, 0);
                   calendar.set(Calendar.MILLISECOND, 0);

                   BandModel model = new BandModel();
                   model.setStep(step);
                   model.setRegtype("G");  // 구글 피트니스
                   model.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(calendar.getTimeInMillis())));
                   model.setRegDate(CDateUtil.getForamtyyyyMMddHHmmss(new Date(calendar.getTimeInMillis())));

//                Logger.i(TAG, "StepGoogle.regDate=" + model.getRegDate() + ", idx=" + model.getIdx() + ", step=" + model.getStep()
//                        +", mDbResultMap.get("+hour+") ="+mDbResultMap.get(hour) );

                   // Sqlite에서 조회 했던 결과가 없으면 서버저장 전문에 사용할 데이터와
                   // Sqlite에서에 저장할 데이터를 생성
                   if (mDbResultMap.get(hour) == null) {
                       if (isToday() && getNowHour() == hour) {
                           // 현재 시간은 저장하지 않음
                       } else {
                           dataModelArr.add(model);
                       }
                   }
               }
           } catch (Exception e) {
               e.printStackTrace();
           }

//            for(int key : mGoogleFitStepMap.keySet() ){
//                hour = key;
//                int step = mGoogleFitStepMap.get(key);
//                System.out.println( String.format("key(hour)=%s, value=%s", key, step)+", mDbResultMap.get("+hour+")="+mDbResultMap.get(hour));
//
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTimeInMillis(mTimeClass.getStartTime());
//
//                calendar.set(Calendar.HOUR, hour);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//
//                BandModel model = new BandModel();
//                model.setStep(step);
//                model.setRegtype("G");  // 구글 피트니스
//                model.setIdx(CDateUtil.getForamtyyMMddHHmmssSS(new Date(calendar.getTimeInMillis())));
//                model.setRegDate(CDateUtil.getForamtyyyyMMddHHmmss(new Date(calendar.getTimeInMillis())));
//
////                Logger.i(TAG, "StepGoogle.regDate=" + model.getRegDate() + ", idx=" + model.getIdx() + ", step=" + model.getStep()
////                        +", mDbResultMap.get("+hour+") ="+mDbResultMap.get(hour) );
//
//                // Sqlite에서 조회 했던 결과가 없으면 서버저장 전문에 사용할 데이터와
//                // Sqlite에서에 저장할 데이터를 생성
//                if (mDbResultMap.get(hour) == null) {
//                    if (isToday() && getNowHour() == hour) {
//                        // 현재 시간은 저장하지 않음
//                    } else {
//                        dataModelArr.add(model);
//                    }
//                 }
//            }

            if (dataModelArr.size() <= 0)
                return;

            DBHelper helper = new DBHelper(mContext);
            DBHelperStep db = helper.getStepDb();
            List<BandModel> newModelArr = db.getResultRegistData(dataModelArr);

            if (newModelArr.size() > 0)
                new DeviceDataUtil().uploadStepData(mContext, newModelArr, new BluetoothManager.IBluetoothResult() {
                    @Override
                    public void onResult(boolean isSuccess) {
                        mBaseFragment.hideProgress();
                    }
                });
        }
    }

    /*
        Save Money 계산 함수
     */
    private float calcSaveMoney(float distance) {
        if (distance == 0)
            return 0.0f;

//        distance = distance * 1000;

        final int initDist = 2000;
        final int initFee = 3800;
        final int unitDist = 132;
        final int unitDistFee = 100;

        if (distance <= initDist)
            return initFee;

        distance -= initDist;
        float tmp = (initFee + (distance / unitDist + 1) * unitDistFee);
        return tmp;
    }

    // 활동소모 칼로리비교 가져오기 : 삼계탕 0.0인분
    private String calcClaorieCompare(int calories) {
        if (calories == 0)
            return "-";

        Random random = new Random();
        int r = random.nextInt(mFoodCalories.size());

        FoodCalories target = mFoodCalories.get(r);

        String value = String.format("%.1f", calories / (float) target.calories);

        if (value != null && value.endsWith(".0"))
            value = value.substring(0, value.length() - 2);

        String result = target.name + " " + value + " " + target.unit;
        return result;
    }

    // 활동소모 선언
    private ArrayList<FoodCalories> mFoodCalories;

    private class FoodCalories {
        FoodCalories(String name, String unit, int calories) {
            this.name = name;
            this.unit = unit;
            this.calories = calories;
        }

        String name, unit;
        int calories;
    }

    private int mMaxStep = 0;
    private int mTotalStep = 0;

//    /**
//     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
//     * @param dataReadResult
//     */
//    public void readData(DataReadResult dataReadResult) {
//        mArrIdx = 0;
//        mTotalVal = 0L;
//
//        mMaxStep = 0;
//        mTotalStep = 0;
//
//        mGoogleFitStepMap.clear();
//        if (dataReadResult.getBuckets().size() > 0) {
//            Log.i(TAG, "BucketSize=" + dataReadResult.getBuckets().size());
//            for (Bucket bucket : dataReadResult.getBuckets()) {
//                List<DataSet> dataSets = bucket.getDataSets();
//                for (DataSet dataSet : dataSets) {
//                    mTotalVal += dumpDataSet(dataSet);
//                }
//            }
//            TypeDataSet.Period period = mTimeClass.getPeriodType();
//            // 주간 조회시 남은 주간 채워주기
//            int max = 10;
//            if (period == TypeDataSet.Period.PERIOD_WEEK) {
//                max = 7 - (mYVals.size());
//            } else if (period == TypeDataSet.Period.PERIOD_DAY) {
//                max = 24 - (mYVals.size());
//            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
//                max = mTimeClass.getDayOfMonth() - (mYVals.size());
//            }
//
//            for (int i = 0; i < max; i++) {
//                mYVals.add(new BarEntry(mArrIdx++, 0f));
//                Log.i(TAG, "PERIOD.size=" + mYVals.size() + ", idx=" + mArrIdx);
//            }
//
//        } else if (dataReadResult.getDataSets().size() > 0) {
//            Log.i(TAG, "DataSetsSize=" + dataReadResult.getBuckets().size());
//            for (DataSet dataSet : dataReadResult.getDataSets()) {
//                mTotalVal += dumpDataSet(dataSet);
//            }
//        }
//    }

    /**
     * 조회된 구글 피트니스 데이터 데이터 형태로 파싱하기
     * @param buckets
     */
    public void readData(List<Bucket> buckets) {
        mArrIdx = 0;
        mTotalVal = 0L;

        mMaxStep = 0;
        mTotalStep = 0;
        if (buckets.size() > 0) {
            Log.i(TAG, "BucketSize=" + buckets.size());
            for (Bucket bucket : buckets) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    mTotalVal += dumpDataSet(dataSet);
                }
            }
            TypeDataSet.Period period = mTimeClass.getPeriodType();
            // 주간 조회시 남은 주간 채워주기
            int max = 10;
            if (period == TypeDataSet.Period.PERIOD_WEEK) {
                max = 7 - (mYVals.size());
            } else if (period == TypeDataSet.Period.PERIOD_DAY) {
                max = 24 - (mYVals.size());
            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
                max = mTimeClass.getDayOfMonth() - (mYVals.size());
            }

            for (int i = 0; i < max; i++) {
                mYVals.add(new BarEntry(mArrIdx++, 0f));
                Log.i(TAG, "PERIOD.size=" + mYVals.size() + ", idx=" + mArrIdx);
            }


//        } else if (buckets.size() > 0) {
//            Log.i(TAG, "DataSetsSize=" + buckets.size());
//            for (DataSet dataSet : buckets) {
//                mTotalVal += dumpDataSet(dataSet);
//            }
        }
    }

    /**
     * 구글 피트니스 데이터 복잡한 데이터 타입에서 필요한 데이터만 뽑아서 차트 및 데이터에 사용할 데이터 추출
     * @param dataSet
     * @return
     */
    private long dumpDataSet(DataSet dataSet) {
        Logger.i(TAG, "====================");

        DateFormat dateFormat = getDateTimeInstance();

        long dumpTotalVal = 0;   // 총 칼로리, 총 걸음수
        mYVals.add(new BarEntry(mArrIdx, 0f));
        CommonData login = CommonData.getInstance();
        int sex = StringUtil.getIntVal(login.getGender());
        float height = StringUtil.getFloat(login.getBefCm());
        float weight = StringUtil.getFloatVal(login.getMotherWeight());

//        DBHelper helper = new DBHelper(mBaseFragment.getContext());
//        DBHelperWeight weightDb = helper.getWeightDb();
//        DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic(helper);
//        if (!bottomData.getWeight().isEmpty()) {
//            weight = StringUtil.getFloatVal(login.getMotherWeight());
//        } else {
//            weight = StringUtil.getFloatVal(login.getMotherWeight());
//        }

        for (DataPoint dp : dataSet.getDataPoints()) {
            Logger.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(MILLISECONDS)));
            Logger.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\t Value:" + field.getName() + "=" + dp.getValue(field) + "[" + mArrIdx + "]");
                // sqlite에 저장 여부를 판단할 용도로 사용할 Map
                mGoogleFitStepMap.put(mArrIdx, (int) StringUtil.getFloat(dp.getValue(field).toString()));

                if (getType() == TypeDataSet.Type.TYPE_CALORY) {

                    int value = 0;
                    float calorie = 0;
                    try {
                        value = dp.getValue(field).asInt(); // 걸음수
                        // 일반적인 계산식 (기존) - 구글피트계산방식.
                        // calorie = getMileToCalorie(value, height, weight);

                        // 녹십자 헬스케어의 계산식을 사용한 칼로리계산
                        calorie = StringUtil.getFloatVal(mBaseFragment.getCalroriTargetCalulator(value));

                        mMaxStep = mMaxStep < value ? value : mMaxStep; // 최대 걸음수
                        mTotalStep += value;                       // 걸음수 총 합
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mYVals.set(mArrIdx, new BarEntry(mArrIdx, calorie));
                    Logger.i(TAG, "calorie[" + mArrIdx + "]" + calorie);

                    dumpTotalVal += calorie;
                } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
                    int steps = 0;
                    try {
                        steps += StringUtil.getFloat(dp.getValue(field).toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mYVals.set(mArrIdx, new BarEntry(mArrIdx, (float) steps));
                    dumpTotalVal += steps;

                    // mBaseFragment.getStepTargetCalulator(steps);
                }
            }
        }
        Logger.i(TAG, "mDumpValue=" + dumpTotalVal);
        mArrIdx++;

        return dumpTotalVal;
    }

    /**
     * 마일당 칼로리 구하기
     *
     * @param stepCnt
     * @param height
     * @param weight
     * @return
     */
    private float getMileToCalorie(int stepCnt, float height, float weight) {

        float moveDistance = (height - 100) * stepCnt / 100; // (키-100 * 걸음수) / 100
        float mc = (float) (3.7103f + weight + (0.0359f * (weight * 60 * 0.0006213) * 2) * weight);
        float calorie = (float) ((moveDistance * mc) * 0.006213);
        return calorie;
    }

    /**
     * 칼로리, 걸음 여부 판단
     *
     * @return
     */
    public TypeDataSet.Type getType() {
        if (mTypeRg != null) {
            if (mTypeRg.getCheckedRadioButtonId() == R.id.radio_btn_calory) {
                //mStepLayout.setVisibility(View.GONE);
                //mCarorieLayout.setVisibility(View.VISIBLE);
                IvStepBoxImage04.setVisibility(View.GONE);
                //mChangeLine.setBackgroundResource(R.drawable.linetop);
                return TypeDataSet.Type.TYPE_CALORY;
            } else if (mTypeRg.getCheckedRadioButtonId() == R.id.radio_btn_step) {
                //mCarorieLayout.setVisibility(View.GONE);
                //mStepLayout.setVisibility(View.VISIBLE);
                IvStepBoxImage04.setVisibility(View.VISIBLE);
                //mChangeLine.setBackgroundResource(R.drawable.textline);
                return TypeDataSet.Type.TYPE_STEP;
            }
        }
        return TypeDataSet.Type.TYPE_CALORY;
    }

    protected void onResume() {
        mBaseFragment.reqPermissions(permissions, new BaseFragment.IPermission() {
            @Override
            public void result(boolean isGranted) {
                if (isGranted) {

                    getData();
//                    FitnessOptions fitnessOptions = FitnessOptions.builder()
//                                    .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                                    .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
//                                    .build();
//                    if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(mBaseFragment.getActivity()), fitnessOptions)) {
//                        GoogleSignIn.requestPermissions(
//                                mBaseFragment.getActivity(),
//                                GoogleFitInstance.REQUEST_OAUTH_REQUEST_CODE,
//                                GoogleSignIn.getLastSignedInAccount(mBaseFragment.getActivity()),
//                                fitnessOptions);
//                    } else {
//                        new GoogleFitInstance().subscribe(mBaseFragment.getActivity(), new ApiData.IStep() {
//                            @Override
//                            public void next(Object obj) {
//                                getData();
//                            }
//                        });
//                    }

                } else {
                    mBaseFragment.hideProgress();
                    mBaseFragment.onBackPressed();
                }
            }
        });
    }

    public void onStop() {
//        Logger.i(TAG, "onStop.mClient=" + mClient);
        GoogleFitInstance.getInstance().stopClient(mBaseFragment);
//        mClient = null;
//        if (mClient != null) {
//            Logger.i(TAG, "onDetach.mClient=" + mClient + ", isConnected()=" + mClient.isConnected());
//            mClient.stopAutoManage(mBaseFragment.getActivity());
//            mClient.disconnect();
//            mClient = null;
//        }
    }

    public void onDetach() {
//        Logger.i(TAG, "onDetach.mClient=" + mClient);
//        if (mClient != null) {
//            Logger.i(TAG, "onDetach.mClient=" + mClient + ", isConnected()=" + mClient.isConnected());
//            mClient.stopAutoManage(mBaseFragment.getActivity());
//            mClient.disconnect();
//            mClient = null;
//        }
    }

    /**
     * 칼로리,걸음 하단 화면 초기화
     */
    private void setBottomField() {


        if (getType() == TypeDataSet.Type.TYPE_CALORY) {    // 칼로리
            tvActiveTitle.setText("활동 소모 칼로리");

            tvStepBoxTitle01.setText("목표 달성률");
//            tvStepBoxTitle02.setText("");
//            tvStepBoxTitle03.setText("총 활동 시간");
//            tvStepBoxTitle04.setText("활동 소모 칼로리 비교");
            //ssshin 수정 20190129
            tvStepBoxTitle02.setText("활동 소모 칼로리 비교");
            tvStepBoxTitle03.setText("총 활동 시간");
            tvStepBoxTitle04.setText("최장 연속 활동 시간");

            mGoalDescTv.setText("(소모 칼로리 / 목표 칼로리)");

            tvStepBoxValue01.setText("0%");
            tvActiveValueTag.setText(" Kcal");
            tvTargetValueTag.setText(" Kcal");

            IvStepBoxImage02.setImageResource(R.drawable.icon_st3);
            IvStepBoxImage03.setImageResource( R.drawable.icon_st2);
            IvStepBoxImage04.setImageResource(0);
            chartunit.setText("kcal");
        } else if (getType() == TypeDataSet.Type.TYPE_STEP) {
            tvActiveTitle.setText("총 걸음 수");

            tvStepBoxTitle01.setText("목표 달성률");
            tvStepBoxTitle02.setText("총 이동 거리");
            tvStepBoxTitle03.setText("Save Money");
            tvStepBoxTitle04.setText("평균 보폭");
            tvStepBoxValue01.setText("0%");
            tvActiveValueTag.setText(" Step");
            tvTargetValueTag.setText(" Step");

            mGoalDescTv.setText("(진행 걸음수 / 목표 걸음수)");

            IvStepBoxImage02.setImageResource(R.drawable.icon_st5);
            IvStepBoxImage03.setImageResource(R.drawable.icon_st6);
            IvStepBoxImage04.setImageResource(R.drawable.icon_st7);
            chartunit.setText("step");
        }

        Logger.i(TAG, "TYPE_CALORY="+(getType() == TypeDataSet.Type.TYPE_CALORY));
    }

    /**
     * 오늘인지 여부
     *
     * @return
     */
    private boolean isToday() {
        // 오늘일 경우
        return mTimeClass.getCalTime() == 0;
    }

    /**
     * 현재 시간을 구한다
     * @return
     */
    private int getNowHour() {
            Calendar nowCal = Calendar.getInstance();
            nowCal.setTimeInMillis(System.currentTimeMillis());

        return nowCal.get(Calendar.HOUR_OF_DAY);
    }

}
