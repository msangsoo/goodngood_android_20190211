package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
// import com.tsengvn.typekit.TypekitContextWrapper;

public class BBSViewActivity extends AppCompatActivity {

    WebView mBbsVier;
    ImageButton mCommonLeftBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbsview);

        initView();
        initEvent();
        startView();
    }

    protected void initView(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        mBbsVier = (WebView)findViewById(R.id.bbs_webview);
        mBbsVier.getSettings().setJavaScriptEnabled(true);
        mBbsVier.getSettings().setPluginState(WebSettings.PluginState. ON);
        mBbsVier.setWebViewClient(new WebClient());
        mBbsVier.setWebChromeClient(new WebChromeClient());
    }

    protected void initEvent(){
        mCommonLeftBtn.setOnClickListener(v -> finish());
    }

    protected void startView(){
        Intent i = getIntent();
        String url = i.getStringExtra(CommonData.JSON_INFO_TITLE_URL);

        mBbsVier.loadUrl(url);
    }


    public class WebClient extends WebViewClient{
        private AlertDialog show;
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return true;
        }
    }

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}
