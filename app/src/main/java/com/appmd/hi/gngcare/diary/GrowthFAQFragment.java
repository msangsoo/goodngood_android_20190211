package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appmd.hi.gngcare.util.GLog;
import com.handmark.pulltorefresh.library.PullToRefreshExpandableListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.GrowthFaqAdapter;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.collection.GrowthFaqItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CommonView;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.Util;

/**
 * Created by jihoon on 2016-04-18.
 *
 * @since 0, 1
 */
public class GrowthFAQFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, GrowthMainActivity.onKeyBackPressedListener{

    public static final int     MODE_NORMAL =   0;  // 기본 모드
    public static final int     MODE_SEARCH =   1;  // 검색 모드

    private LinearLayout mVisibleLayout;

    private EditText mSearchEdit;
    private ImageButton mSearchDelBtn, mSearchBtn;

    private View view;
    private PullToRefreshExpandableListView mRefreshListView;
    private ExpandableListView mListView;
    private GrowthFaqAdapter mAdapter;
    private ArrayList<GrowthFaqItem> mItem;

    // 현재 보고있는 페이지
    private int mPageId = 1;
    private int mSearchPageId = 1;
    // 전체 페이지
    private int mTotalPage = 1;
    private int mSearchTotalPage = 1;

    private int mClickPosition = 0;

    private String mSearchStr;    // 검색 문자열

    // header view
    private View mHeaderView;
    private TextView mHeaderTitleTv, mHeaderEmptyTv;

    private int mMode = MODE_NORMAL;  // 0 - 기본, 1 - 검색

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.growth_faq_fragment, null);

        init(view);
        setEvent();

        // 헤더뷰
        addHeaderView();
        mListView.addHeaderView(mHeaderView);

        requestFaqList("");

        return view;
    }

    /**
     * 객체 초기화
     * @param view view
     */
    public void init(View view){

        mRefreshListView   =       (PullToRefreshExpandableListView) view.findViewById(R.id.list_view);
        mVisibleLayout      =       (LinearLayout)  view.findViewById(R.id.visible_layout);

        mSearchEdit         =       (EditText)      view.findViewById(R.id.search_edit);
        mSearchDelBtn       =       (ImageButton)   view.findViewById(R.id.search_del_btn);
        mSearchBtn          =       (ImageButton)   view.findViewById(R.id.search_btn);

        mListView = mRefreshListView.getRefreshableView();
        mItem   =   new ArrayList<GrowthFaqItem>();
//        mListView.setDividerHeight(BleUtil.pixelFromDP(getActivity(), 29));

    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        mSearchEdit.setOnFocusChangeListener(this);
        mSearchEdit.addTextChangedListener(new MyTextWatcher());

        mSearchDelBtn.setOnClickListener(this);
        mSearchBtn.setOnClickListener(this);



        // 상단에서 내리면 새로고침 동작
        mRefreshListView.setOnRefreshListener(refreshView -> {
            // TODO Auto-generated method stub
            mItem.clear();

            mPageId = 1;
            mSearchPageId = 1;
            GLog.i("상단에서 내릴때--->", "dd");

            requestFaqList(mSearchEdit.getText().toString());
        });

        // 리스트 마지막 아이템일 경우
        mRefreshListView.setOnLastItemVisibleListener(() -> {
            // TODO Auto-generated method stub
            switch (mMode) {
                case MODE_NORMAL: // 기본모드
                    if (mPageId != 1 && mPageId <= mTotalPage) {    // 현재 페이지가 1이 아니거나, 전체 페이지수보다 적을경우 호출
                        requestFaqList(mSearchEdit.getText().toString());
                    }
                    break;
                case MODE_SEARCH: // 검색모드
                    if (mSearchPageId != 1 && mSearchPageId <= mSearchTotalPage) {    // 현재 페이지가 1이 아니거나, 전체 페이지수보다 적을경우 호출
                        requestFaqList(mSearchEdit.getText().toString());
                    }
                    break;
            }
        });

        mListView.setOnGroupExpandListener(groupPosition -> {
            GLog.i("g Expand = " + groupPosition, "dd");

            int groupCount = mAdapter.getGroupCount();

            // 한 그룹을 클릭하면 나머지 그룹들은 닫힌다.
            for (int i = 0; i < groupCount; i++) {
                if (!(i == groupPosition))
                    mListView.collapseGroup(i);
            }
        });

    }

    /**
     * 리스트뷰 연결
     */
    public void setListAdapter(){
        try{
            mAdapter = new GrowthFaqAdapter(getActivity(), mItem);
//        mRefreshListView.setAdapter(mAdapter);
            mListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            mListView.setOnItemClickListener((parent, view1, position, id) -> {
                mListView.smoothScrollToPosition(position);
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 성장 FAQ headerView
     */
    public void addHeaderView(){

        mHeaderView     =   mLayoutInflater.inflate(R.layout.child_care_note_header_view, null, false);
        mHeaderTitleTv  =   (TextView)  mHeaderView.findViewById(R.id.title_tv);
        mHeaderEmptyTv  =   (TextView)  mHeaderView.findViewById(R.id.empty_tv);

        mHeaderTitleTv.setText(getString(R.string.faq_desc));
        mHeaderEmptyTv.setVisibility(View.GONE);

    }

    /**
     * 육아 노트 리스트 api
     * @param faq_kwrd  검색어
     */
    public void requestFaqList(String faq_kwrd){
        GLog.i("requestFaqList", "dd");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//        {   "api_code": "chldrn_growth_faq",   "insures_code": "106","pageNumber": "1", "faq_kwrd": "" }
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CHLDRN_GROWTH_FAQ);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_FAQ_KWRD, faq_kwrd);

            if(faq_kwrd.equals("")){    //  검색어가 없다면 기본 모드
                object.put(CommonData.JSON_PAGENUMBER, String.valueOf(mPageId));
                mMode   =   MODE_NORMAL;
            }else{                      //  검색어가 있다면 검색 모드
                object.put(CommonData.JSON_PAGENUMBER, String.valueOf(mSearchPageId));
                mMode   =   MODE_SEARCH;
            }

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_CHLDRN_GROWTH_FAQ, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());

        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()){
            case R.id.search_del_btn:   // 검색 삭제
                GLog.i("search_del_btn", "dd");
                CommonView.getInstance().setClearEditText(mSearchEdit);
                break;
            case R.id.search_btn:   // 검색
                mSearchStr = mSearchEdit.getText().toString().trim();
                if(!mSearchStr.equals("")){  // 검색 내용이 있다면
                    Util.hideKeyboard(getActivity(), mSearchEdit);
                    requestFaqList(mSearchStr);
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch ( id ) {
            case R.id.search_edit:    // 검색
                CommonView.getInstance().setClearImageBt(mSearchDelBtn, hasFocus);
                break;
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_CHLDRN_GROWTH_FAQ:  // 성장FAQ

                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            try {

                                switch (mMode){
                                    case MODE_NORMAL:    // 기본 모드
                                        String data_yn  =   resultData.getString(CommonData.JSON_DATA_YN);

                                        if(data_yn.equals(CommonData.YES)) {
                                            JSONArray jsonArr = resultData.getJSONArray(commonData.JSON_NOTICE);

                                            mTotalPage = resultData.getInt(CommonData.JSON_MAXPAGENUMBER);
                                            GLog.i("mTotalPage = " + mTotalPage, "dd");
                                            GLog.i("mPageId = " + mPageId, "dd");

                                            if (mPageId == 1) {
                                                mItem.clear();
                                            }

                                            if (jsonArr.length() > 0) {    // 리스트가 있을 경우
                                                mHeaderEmptyTv.setVisibility(View.GONE);
                                                for (int i = 0; i < jsonArr.length(); i++) {
                                                    JSONObject object = jsonArr.getJSONObject(i);

                                                    GrowthFaqItem item = new GrowthFaqItem(object.getString(CommonData.JSON_FAQ_SN),
                                                            object.getString(CommonData.JSON_FAQ_QESTN),
                                                            object.getString(CommonData.JSON_FAQ_ANSWER));
                                                    mItem.add(item);
                                                }
                                            } else {  // 리스트 데이터가 없는 경우
                                                if (mPageId == 1) {
                                                    mHeaderEmptyTv.setText(getString(R.string.child_care_note_empty));
                                                    mHeaderEmptyTv.setVisibility(View.VISIBLE);
                                                }
                                            }

                                            if (mPageId == 1) {   // 첫번쨰 리스트 호출
                                                setListAdapter();
                                                mRefreshListView.onRefreshComplete();
                                            } else {
                                                mAdapter.notifyDataSetChanged();
                                            }

                                            mPageId++;  // 페이지 증가

                                            if (mItem.size() > 0 && mItem.size() <= CommonData.PAGE_MAX_COUNT) {
                                                Util.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                                            }
                                        }else{
                                            mHeaderEmptyTv.setText(getString(R.string.faq_empty));
                                            mHeaderEmptyTv.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    case MODE_SEARCH:    // 검색 모드

                                        String search_data_yn  =   resultData.getString(CommonData.JSON_DATA_YN);

                                        if(search_data_yn.equals(CommonData.YES)){ // 데이터가 있다면
                                            JSONArray searchArr  =   resultData.getJSONArray(commonData.JSON_NOTICE);

                                            mSearchTotalPage  =   resultData.getInt(CommonData.JSON_MAXPAGENUMBER);
                                            GLog.i("mSearchTotalPage = " +mSearchTotalPage, "dd");
                                            GLog.i("mSearchPageId = " +mSearchPageId, "dd");

                                            if(mSearchPageId  ==   1){
                                                mItem.clear();
                                            }

                                            if(searchArr.length() > 0){    // 리스트가 있을 경우
                                                for(int i=0; i<searchArr.length(); i++){
                                                    JSONObject object = searchArr.getJSONObject(i);

                                                    GrowthFaqItem   item = new GrowthFaqItem(object.getString(CommonData.JSON_FAQ_SN),
                                                            object.getString(CommonData.JSON_FAQ_QESTN),
                                                            object.getString(CommonData.JSON_FAQ_ANSWER));
                                                    mItem.add(item);
                                                }
                                                mHeaderTitleTv.setText(String.format(getString(R.string.child_care_note_search_desc), mSearchEdit.getText().toString()));
                                                mHeaderTitleTv.setVisibility(View.VISIBLE);
                                                mHeaderEmptyTv.setVisibility(View.GONE);
                                            }else{  // 리스트 데이터가 없는 경우
                                                if(mSearchPageId  == 1){
                                                    mHeaderTitleTv.setText(getString(R.string.faq_search_empty));
                                                    mHeaderTitleTv.setVisibility(View.VISIBLE);
                                                    mHeaderEmptyTv.setVisibility(View.GONE);
                                                }
                                            }

                                            if(mSearchPageId == 1){   // 첫번쨰 리스트 호출
                                                setListAdapter();
                                                mRefreshListView.onRefreshComplete();
                                            }else{
                                                mAdapter.notifyDataSetChanged();
                                            }

                                            mSearchPageId++;  // 페이지 증가

                                            if(mItem.size() > 0 && mItem.size() <= CommonData.PAGE_MAX_COUNT){
                                                Util.setAlphaAni(mVisibleLayout, CommonData.ANI_DELAY_1000, false, CommonData.ANI_DELAY_2000);
                                            }
                                        }else{

                                            mItem.clear();
                                            mAdapter.notifyDataSetChanged();

                                            mHeaderTitleTv.setText(getString(R.string.faq_search_empty));
                                            mHeaderTitleTv.setVisibility(View.VISIBLE);
                                            mHeaderEmptyTv.setVisibility(View.GONE);
                                        }

                                        break;
                                }



                            }catch(Exception e){
                                GLog.e(e.toString());
                            }
                            mRefreshListView.onRefreshComplete();

                            break;
                        default:
                            if(dialog != null)
                                dialog.show();

                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };

    /**
     * 입력창 리스너
     */
    class MyTextWatcher implements TextWatcher
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

            GLog.i("s = " +s.toString() +"\n start = " +start +"\n before = " +before +"\n count = " +count, "dd");

            if(mSearchEdit.getText().toString().length() == 0){  // 검색어 입력 후, 텍스트 삭제시 기본 화면으로 원복
                mPageId = 1;
                mSearchPageId = 1;

                requestFaqList("");
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((GrowthMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        GrowthMainActivity activity = (GrowthMainActivity)getActivity();
        activity.switchActionBarTheme(GrowthMainActivity.THEME_DARK_BLUE);
        activity.switchActionBarTitle(getString(R.string.title_growth));
        switchFragment(new GrowthMainFragment());
    }
}
