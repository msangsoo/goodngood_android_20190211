package com.appmd.hi.gngcare.collection;

/**
 * Created by MobileDoctor on 2017-03-21.
 */

public class AllDataItem {
    private String mDataSn;
    private String mFilter;
    private String mInputDe;
    private String mFever;
    private String mInputKind;
    private String mInputType;
    private String mInputVolume;
    private String mInputCode;
    private String mInputNum;
    private String mInputMemo;

    private boolean mIsDate = false;

    public boolean getmIsDate() {
        return mIsDate;
    }

    public void setmIsDate(boolean date) {
        mIsDate = date;
    }

    public String getmDataSn() {
        return mDataSn;
    }

    public void setmDataSn(String mDataSn) {
        this.mDataSn = mDataSn;
    }

    public String getmFilter() {
        return mFilter;
    }

    public void setmFilter(String mFilter) {
        this.mFilter = mFilter;
    }

    public String getmInputDe() {
        return mInputDe;
    }

    public void setmInputDe(String mInputDe) {
        this.mInputDe = mInputDe;
    }

    public String getmFever() {
        return mFever;
    }

    public void setmFever(String mFever) {
        this.mFever = mFever;
    }

    public String getmInputKind() {
        return mInputKind;
    }

    public void setmInputKind(String mInputKind) {
        this.mInputKind = mInputKind;
    }

    public String getmInputType() {
        return mInputType;
    }

    public void setmInputType(String mInputType) {
        this.mInputType = mInputType;
    }

    public String getmInputVolume() {
        return mInputVolume;
    }

    public void setmInputVolume(String mInputVolume) {
        this.mInputVolume = mInputVolume;
    }

    public String getmInputCode() {
        return mInputCode;
    }

    public void setmInputCode(String mInputCode) {
        this.mInputCode = mInputCode;
    }

    public String getmInputNum() {
        return mInputNum;
    }

    public void setmInputNum(String mInputNum) {
        this.mInputNum = mInputNum;
    }

    public String getmInputMemo() {
        return mInputMemo;
    }

    public void setmInputMemo(String mInputMemo) {
        this.mInputMemo = mInputMemo;
    }
}
