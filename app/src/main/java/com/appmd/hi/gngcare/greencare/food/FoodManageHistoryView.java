package com.appmd.hi.gngcare.greencare.food;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.value.TypeDataSet;
import com.appmd.hi.gngcare.greencare.bluetooth.manager.DeviceDataUtil;
import com.appmd.hi.gngcare.greencare.charting.data.BarEntry;
import com.appmd.hi.gngcare.greencare.charting.data.RadarEntry;
import com.appmd.hi.gngcare.greencare.chartview.food.RadarChartView;
import com.appmd.hi.gngcare.greencare.chartview.valueFormat.AxisValueFormatter;
import com.appmd.hi.gngcare.greencare.chartview.walk.BarChartView;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperFoodMain;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_get_meal_input_data;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.greencare.util.ChartTimeUtil;
import com.appmd.hi.gngcare.greencare.util.Logger;
import com.appmd.hi.gngcare.greencare.util.StringUtil;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 *  히스토리화면
 * Created by mrsohn on 2017. 3. 14..
 */

public class FoodManageHistoryView {
    private final String TAG = FoodManageHistoryView.class.getSimpleName();

    int recommandCalrori;   //권장칼로리.
    public ChartTimeUtil mTimeClass;

    private BarChartView mChart;
    private RadarChartView mRadarChart;
    private TextView mDateTv;

    private BaseFragment mBaseFragment;
    private View mView;

    private TextView mBottomCalBreakfastTv;
    private TextView mBottomCalLunchTv;
    private TextView mBottomCalDinnerTv;
    private TextView mBottomMinuteBreakfastTv;
    private TextView mBottomMinuteLunchTv;
    private TextView mBottomMinuteDinnerTv;

    private TextView mCalTitleTv;
    private TextView mMinuteTitleTv;
    private TextView mRadarTitleTv;

    private TextView txttakecal;
    private TextView txtrecomcal;

    private ImageButton imgPre_btn;
    private ImageButton imgNext_btn;

    private TextView mTipTv;
    private LinearLayout mHCallBtn;

    public FoodManageHistoryView(BaseFragment baseFragment, View view) {
        mBaseFragment = baseFragment;
        mView = view;

        mDateTv = (TextView) view.findViewById(R.id.txtCurrDate);

        RadioGroup periodRg = (RadioGroup) view.findViewById(R.id.radiogroup_period_type);
        RadioButton radioBtnMonth = (RadioButton) view.findViewById(R.id.radio_food_type_month);
        RadioButton radioBtnWeek = (RadioButton) view.findViewById(R.id.radio_food_type_week);
        RadioButton radioBtnDay = (RadioButton) view.findViewById(R.id.radio_food_type_day);

        mBottomCalBreakfastTv = (TextView) view.findViewById(R.id.food_cal_breakfast);
        mBottomCalLunchTv = (TextView) view.findViewById(R.id.food_cal_lunch);
        mBottomCalDinnerTv = (TextView) view.findViewById(R.id.food_cal_dinner);
        mBottomMinuteBreakfastTv = (TextView) view.findViewById(R.id.food_minute_blackfast);
        mBottomMinuteLunchTv = (TextView) view.findViewById(R.id.food_minute_lunch);
        mBottomMinuteDinnerTv = (TextView) view.findViewById(R.id.food_minute_dinner);

        mCalTitleTv = (TextView) view.findViewById(R.id.food_cal_title);
        mMinuteTitleTv = (TextView) view.findViewById(R.id.food_minute_title);
        mRadarTitleTv = (TextView) view.findViewById(R.id.food_radar_title);

        periodRg.setOnCheckedChangeListener(mCheckedChangeListener);

        imgPre_btn                  = (ImageButton) view.findViewById(R.id.btn_hiscalLeft);
        imgNext_btn                 = (ImageButton) view.findViewById(R.id.btn_hiscalRight);

        view.findViewById(R.id.btn_hiscalLeft).setOnClickListener(mClickListener);
        view.findViewById(R.id.btn_hiscalRight).setOnClickListener(mClickListener);


        mTimeClass = new ChartTimeUtil(radioBtnDay, radioBtnWeek, radioBtnMonth);
        mRadarChart = new RadarChartView(mBaseFragment.getContext(), view);
        mChart = new BarChartView(mBaseFragment.getContext(), view);
        if (mChart.getXValueFormat() instanceof AxisValueFormatter) {
            ((AxisValueFormatter) mChart.getXValueFormat()).setUnitStr("kcal");
        }

        txttakecal = (TextView) view.findViewById(R.id.txt_takecal);    //  섭취칼로리
        txtrecomcal = (TextView) view.findViewById(R.id.txt_recomcal);  //  권장칼로리

        setRecomandCal();
        setNextButtonVisible();

        // 건강 메시지 세팅 하기
        mTipTv = (TextView) view.findViewById(R.id.result_tip_textview);

        // 상담연결하기
        mHCallBtn = view.findViewById(R.id.Hcall_btn);
        mHCallBtn.setOnClickListener(mClickListener);

        //click 저장
        OnClickListener ClickListener = new OnClickListener(mClickListener, view, mBaseFragment.getContext());
        //엄마 건강
        radioBtnDay.setOnTouchListener(ClickListener);
        radioBtnWeek.setOnTouchListener(ClickListener);
        radioBtnMonth.setOnTouchListener(ClickListener);
        mHCallBtn.setOnTouchListener(ClickListener);

        //코드 부여(엄마 건강)
        radioBtnDay.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodDay));
        radioBtnWeek.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodWeek));
        radioBtnMonth.setContentDescription(mBaseFragment.getContext().getString(R.string.radioBtnfoodMonth));
        mHCallBtn.setContentDescription(mBaseFragment.getContext().getString(R.string.HCallBtn9));


    }

    private void setRecomandCal() {

        try {
//            CommonData login = CommonData.getInstance();                               // 로그인 정보
//            String nowYear = CDateUtil.getFormattedString_yyyy(System.currentTimeMillis());     // 현재년도
//            String sex = login.getGender();                                                        // 성별
//            float height = login.getHeight();                           // 키
//            float weight = StringUtil.getFloat(login.getMotherWeight()); //StringUtil.getFloatVal(login.mber_bdwgh);                            // 몸무게
//
//            float bmi = 0; //StringUtil.getFloatVal(login.());                                 // bmi 측정치
//            int rAge = Integer.parseInt(login.getBirthDay().substring(0,4));                      // 회원 생년
//            String mber_actqy  = login.getActqy();                                              //활동량 1,2,3
//
//            float mWeight;   //표준체중
//            float mHeight = height * 0.01f;
//            mHeight = StringUtil.getFloatVal(String.format("%.2f", mHeight));
//
//            if (sex.equals("2")){
//                //여성
//                mWeight = StringUtil.getFloatVal(String.format("%.1f",(mHeight*mHeight) *21));
//            }else {
//                //남성
//                mWeight = StringUtil.getFloatVal(String.format("%.1f",(mHeight*mHeight) *22));
//            }
//
//
//            bmi = weight/(height*height);
//
//            int bmiStep = 0;
//            if(bmi < 18.5){
//                bmiStep=1;
//            }else if(bmi >= 18.5 && bmi <=22.9){
//                bmiStep=2;
//            }else { //if(bmi >= 23.0 )ƒ√
//                bmiStep=3;
//            }
//
//
//            float tmpCal = 0.0f;
//            /*
//            if (mber_actqy.equals("1")){    // 가벼운활동
//                if (bmiStep==1){
//                    tmpCal = 35.0f;
//                }else if (bmiStep==2){
//                    tmpCal = 30.0f;
//                }else if (bmiStep==3){
//                    tmpCal = 25.0f;
//                }
//            }else if (mber_actqy.equals("2")) { // 보통활동
//                if (bmiStep==1){
//                    tmpCal = 40.0f;
//                }else if (bmiStep==2){
//                    tmpCal = 35.0f;
//                }else if (bmiStep==3){
//                    tmpCal = 30.0f;
//                }
//            }else if (mber_actqy.equals("3")) {     // 힘든활동
//                if (bmiStep==1){
//                    tmpCal = 45.0f;
//                }else if (bmiStep==2){
//                    tmpCal = 40.0f;
//                }else if (bmiStep==3){
//                    tmpCal = 35.0f;
//                }
//            }*/
//
//            // 이번 프로잭트에서는 가벼운운동, 보통활동, 힘든운동을 받지 않기때문에 보통으로 고정(박서언대리 2018.4.20)
//            if (bmiStep==1){
//                tmpCal = 40.0f;
//            }else if (bmiStep==2){
//                tmpCal = 35.0f;
//            }else if (bmiStep==3){
//                tmpCal = 30.0f;
//            }
//            // 하루 권장섭취량 (표준체중 * tmpCal)
//            int recomCal = (int)(mWeight * tmpCal);  // 권장섭취량
//
//            if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_WEEK) {
//
//                Calendar cal = Calendar.getInstance();
//                int nWeek = cal.get(Calendar.DAY_OF_WEEK);
//                recomCal = recomCal * nWeek;
//            }else if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_MONTH){
//
//                Calendar cal = Calendar.getInstance();
//                int day = cal.get(Calendar.DAY_OF_MONTH);
//                recomCal = recomCal * day;
//            }
//            recommandCalrori = recomCal;
//            txtrecomcal.setText(StringUtil.exchangeAmountToStringUnit(""+recomCal)+" kcal");

            int dayCnt = 1;
            TypeDataSet.Period period = mTimeClass.getPeriodType();
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                dayCnt = 1;
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                dayCnt = 7;
            }else if (period == TypeDataSet.Period.PERIOD_MONTH){
                dayCnt = mTimeClass.getDayOfMonth();
            }

            int kcal = new DeviceDataUtil().getPregnancyRecommendCal();
            kcal = dayCnt * kcal;   // 권장 섭취 칼로리


            txtrecomcal.setText(StringUtil.exchangeAmountToStringUnit(""+kcal)+" kcal");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setVisibility(int visibility) {
        mView.setVisibility(visibility);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.btn_hiscalLeft) {
                mTimeClass.calTime(-1);
                getData();
                setRecomandCal();
            } else if (vId == R.id.btn_hiscalRight) {
                // 초기값 일때 다음날 데이터는 없으므로 리턴
                if (mTimeClass.getCalTime() == 0)
                    return;

                mTimeClass.calTime(1);
                getData();
                setRecomandCal();
            } else if(vId == R.id.Hcall_btn){
                if(CommonData.getInstance().getMberGrad().equals("10")) {
                    CustomAlertDialog mDialog = new CustomAlertDialog(mBaseFragment.getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(mBaseFragment.getContext().getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(mBaseFragment.getContext().getString(R.string.do_call_center));
                    mDialog.setNegativeButton(mBaseFragment.getContext().getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(mBaseFragment.getContext().getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                        String tel = "tel:" + mBaseFragment.getContext().getString(R.string.call_center_number);
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(tel));
                        mBaseFragment.getContext().startActivity(intent);
                        dialog.dismiss();
                    });

                    mDialog.show();
                }else{
                    CustomAlertDialog mDialog = new CustomAlertDialog(mBaseFragment.getContext(), CustomAlertDialog.TYPE_B);
                    mDialog.setTitle(mBaseFragment.getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(mBaseFragment.getString(R.string.call_center2));
                    mDialog.setNegativeButton(mBaseFragment.getString(R.string.popup_dialog_button_cancel), null);
                    mDialog.setPositiveButton(mBaseFragment.getString(R.string.do_call), (dialog, button) -> {
                        String tel = "tel:" + mBaseFragment.getString(R.string.call_center_number2);
//                        startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                        Intent intentCall = new Intent(Intent.ACTION_DIAL);
                        intentCall.setData(Uri.parse(tel));
                        mBaseFragment.startActivity(intentCall);
                        dialog.dismiss();
                    });
                    mDialog.show();
                }
            }
            setNextButtonVisible();
        }
    };

    private void setNextButtonVisible(){
        // 초기값 일때 다음날 데이터는 없으므로 리턴
        if (mTimeClass.getCalTime() == 0) {
            imgNext_btn.setVisibility(View.INVISIBLE);
        }else{
            imgNext_btn.setVisibility(View.VISIBLE);
        }
    }
    /**
     * 일간,주간,월간
     */
    public RadioGroup.OnCheckedChangeListener mCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // 일간, 주간, 월간
            TypeDataSet.Period periodType = mTimeClass.getPeriodType();
            AxisValueFormatter xFormatter = new AxisValueFormatter(periodType);
            xFormatter.setUnitStr("kcal");
            mChart.setXValueFormat(xFormatter);
            mTimeClass.clearTime();         // 날자 초기화

            getData();   // 날자 세팅 후 조회
            setRecomandCal();
        }
    };

    /**
     * 날자 계산 후 조회
     */
    public void getData() {
        long startTime = mTimeClass.getStartTime();
        long endTime = mTimeClass.getEndTime();

        mChart.getBarChart().setDrawMarkers(false); // 새로운 조회시 마커 사라지게 하기

        String format = "yyyy.MM.dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        String startDate = sdf.format(startTime);
        String endDate = sdf.format(endTime);

        if (mTimeClass.getPeriodType() == TypeDataSet.Period.PERIOD_DAY) {
            mDateTv.setText(startDate);
        } else {
            mDateTv.setText(startDate + " ~ " + endDate);
        }

        format = "yyyy-MM-dd";
        sdf = new SimpleDateFormat(format);
        startDate = sdf.format(startTime);
        endDate = sdf.format(endTime);

        getBottomDataLayout(startDate, endDate);


    }

    /**
     * 하단 데이터 세팅하기
     *
     * @param startDate
     * @param endDate
     */
    private void getBottomDataLayout(String startDate, String endDate) {
        mBottomCalBreakfastTv.setText("0 kcal");
        mBottomCalLunchTv.setText("0 kcal");
        mBottomCalDinnerTv.setText("0 kcal");
        mBottomMinuteBreakfastTv.setText("0 분");
        mBottomMinuteLunchTv.setText("0 분");
        mBottomMinuteDinnerTv.setText("0 분");

        DBHelper helper = new DBHelper(mBaseFragment.getContext());

        DBHelperFoodMain db = helper.getFoodMainDb();
        int[] datas = db.getMealSum(startDate, endDate);

        TypeDataSet.Period period = mTimeClass.getPeriodType();
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            // 일별 하단 데이터 세팅
            Tr_get_meal_input_data foodData = db.getResultDay(startDate);
            int morningCal = 0;
            int lunchCal = 0;
            int dinnerCal = 0;

            for (Tr_get_meal_input_data.ReceiveDatas recv : foodData.data_list) {
                String mealType = recv.mealtype;
                Logger.i(TAG, "Tr_get_meal_input_data.mealType="+mealType+", calorie="+recv.calorie);
                if (mBaseFragment.getContext().getString(R.string.text_breakfast_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_breakfast_snack_code).equals(mealType)) {
                    morningCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalBreakfastTv.setText(String.format("%,d", morningCal) + " kcal");
                    mBottomMinuteBreakfastTv.setText(recv.amounttime + " 분");
                } else if (mBaseFragment.getContext().getString(R.string.text_lunch_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_lunch_snack_code).equals(mealType)) {
                    lunchCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalLunchTv.setText(String.format("%,d", lunchCal) + " kcal");
                    mBottomMinuteLunchTv.setText(recv.amounttime+" 분");
                } else if (mBaseFragment.getContext().getString(R.string.text_dinner_code).equals(mealType)
                        || mBaseFragment.getContext().getString(R.string.text_dinner_snack_code).equals(mealType)) {
                    dinnerCal += StringUtil.getIntger(recv.calorie);
                    mBottomCalDinnerTv.setText(String.format("%,d", dinnerCal) + " kcal");
                    mBottomMinuteDinnerTv.setText(recv.amounttime+" 분");
                }
            }
        } else {
            mBottomCalBreakfastTv.setText(String.format("%,d", datas[0])+" kcal");
            mBottomCalLunchTv.setText(String.format("%,d", datas[1])+" kcal");
            mBottomCalDinnerTv.setText(String.format("%,d", datas[2])+" kcal");
            mBottomMinuteBreakfastTv.setText(""+datas[3]+" 분");
            mBottomMinuteLunchTv.setText(""+datas[4]+" 분");
            mBottomMinuteDinnerTv.setText(""+datas[5]+" 분");
        }

        // 섭취칼로리
        int totTakeCal = datas[0] + datas[1] + datas[2];
        txttakecal.setText(StringUtil.exchangeAmountToStringUnit(""+totTakeCal)+" kcal");




        int dayCnt = 1;
        float resultRecommand = (float)new DeviceDataUtil().getPregnancyRecommendCal();
        if (period == TypeDataSet.Period.PERIOD_DAY) {
            dayCnt = 1;
            mTipTv.setText(new DeviceDataUtil().getCalroMessage(totTakeCal));

        } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
            dayCnt = 7;
            mTipTv.setText(new DeviceDataUtil().getCalroMessage(totTakeCal/dayCnt));
            resultRecommand = (float)new DeviceDataUtil().getPregnancyRecommendCal()*dayCnt;
        }else if (period == TypeDataSet.Period.PERIOD_MONTH){
            dayCnt = mTimeClass.getDayOfMonth();
            mTipTv.setText(new DeviceDataUtil().getCalroMessage(totTakeCal/dayCnt));
            resultRecommand = (float)new DeviceDataUtil().getPregnancyRecommendCal()*dayCnt;
        }




        List<RadarEntry> entries = db.getRadial(startDate, endDate, (float)totTakeCal, resultRecommand, dayCnt);

        int idx = 0;
        for (RadarEntry entry : entries) {
            Logger.i(TAG, "entry["+(idx++)+"]="+entry.getValue());
        }

        mRadarChart.setData(entries);

        new QeuryVerifyDataTask(db).execute();
    }

    public class QeuryVerifyDataTask extends AsyncTask<Void, Void, Void> {

        private DBHelperFoodMain db;
        public QeuryVerifyDataTask(DBHelperFoodMain db) {
            this.db = db;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBaseFragment.showProgress();
        }

        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mBaseFragment.hideProgress();

            TypeDataSet.Period period = mTimeClass.getPeriodType();
            if (period == TypeDataSet.Period.PERIOD_DAY) {
                String toDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                List<BarEntry> yVals1 = db.getResultDay(toDay, 24);
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("일간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("일간 평균 식사 소요시간");
                mRadarTitleTv.setText("일간 영양 섭취 균형도");
            } else if (period == TypeDataSet.Period.PERIOD_WEEK) {
                String startDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getStartTime());
                String endDay = CDateUtil.getFormattedString_yyyy_MM_dd(mTimeClass.getEndTime());
                List<BarEntry> yVals1 = db.getResultWeek(startDay, endDay, 7);
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("주간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("주간 평균 식사 소요시간");
                mRadarTitleTv.setText("주간 영양 섭취 균형도");
            } else if (period == TypeDataSet.Period.PERIOD_MONTH) {
                String year = CDateUtil.getFormattedString_yyyy(mTimeClass.getStartTime());
                String month = CDateUtil.getFormattedString_MM(mTimeClass.getStartTime());
                List<BarEntry> yVals1 = db.getResultMonth(year, month, mTimeClass.getDayOfMonth());
                mChart.setData(yVals1, mTimeClass);

                mCalTitleTv.setText("월간 영양 섭취 칼로리");
                mMinuteTitleTv.setText("월간 평균 식사 소요시간");
                mRadarTitleTv.setText("월간 영양 섭취 균형도");
            }
            mChart.animateY();
            setNextButtonVisible();
        }
    }
}
