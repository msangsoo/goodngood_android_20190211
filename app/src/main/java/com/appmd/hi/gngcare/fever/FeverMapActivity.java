package com.appmd.hi.gngcare.fever;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.collection.LocationItem;
import com.appmd.hi.gngcare.common.ApplinkDialog;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.util.CDateUtil;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.push.FirebaseMessagingService;
import com.appmd.hi.gngcare.setting.SettingAddressActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.GpsInfo;
import com.appmd.hi.gngcare.util.KakaoLinkUtil;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class FeverMapActivity extends BackBaseActivity implements View.OnClickListener, OnMapReadyCallback {

    Button mMapBtn, mEpidemicBtn;

    LinearLayout mLinearTabMap, mLinearTabEpidemic;

    LinearLayout mEpidemicRankLay;
    TextView mTxtNullEpidemic;
    LinearLayout [] mEpRankList;
    TextView[] mTxtDiseNmList, mTxtDiseCntList;

    ImageButton mBtnAlarm, mBtnShare;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;

    View mMarkerRootView; TextView marker;
    private Intent mIntent;
    private int mFragmentNum = 0;

    private View view;

    //hsh start
//    TextView textView9;
    //hsh end

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fever_map_activity);

        setTitle(getString(R.string.fever_map));

        init();
        setEvent();
        initView();

        initRank();

        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(new Date());
        mCalendar.add(Calendar.DAY_OF_MONTH, -365);
        requestMapData(mCalendar.getTime(), new Date());

        mIntent = getIntent();

        if(mIntent != null){
            mFragmentNum = mIntent.getIntExtra("tabNum",0);
        }

        setTab(mFragmentNum);

    }

    /**
     * 초기화
     */
    public void init(){

        mMapBtn = (Button)findViewById(R.id.map_btn);
        mEpidemicBtn = (Button)findViewById(R.id.epidemic_btn);

        mBtnAlarm = (ImageButton)findViewById(R.id.btn_alarm);
        mBtnShare = (ImageButton)findViewById(R.id.share_btn);

        mLinearTabMap = (LinearLayout)findViewById(R.id.linear_tab_map);
        mLinearTabEpidemic = (LinearLayout)findViewById(R.id.linear_tab_epidemic);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mEpidemicRankLay = (LinearLayout)findViewById(R.id.epidemic_rank_lay);
        mTxtNullEpidemic = (TextView)findViewById(R.id.txt_null_epidemic);

        mEpRankList = new LinearLayout[10];
        mEpRankList[0] = (LinearLayout)findViewById(R.id.ep_rank_1);
        mEpRankList[1] = (LinearLayout)findViewById(R.id.ep_rank_2);
        mEpRankList[2] = (LinearLayout)findViewById(R.id.ep_rank_3);
        mEpRankList[3] = (LinearLayout)findViewById(R.id.ep_rank_4);
        mEpRankList[4] = (LinearLayout)findViewById(R.id.ep_rank_5);
        mEpRankList[5] = (LinearLayout)findViewById(R.id.ep_rank_6);
        mEpRankList[6] = (LinearLayout)findViewById(R.id.ep_rank_7);
        mEpRankList[7] = (LinearLayout)findViewById(R.id.ep_rank_8);
        mEpRankList[8] = (LinearLayout)findViewById(R.id.ep_rank_9);
        mEpRankList[9] = (LinearLayout)findViewById(R.id.ep_rank_10);

        mTxtDiseNmList = new TextView[10];
        mTxtDiseNmList[0] = (TextView)findViewById(R.id.txt_dise_nm_1);
        mTxtDiseNmList[1] = (TextView)findViewById(R.id.txt_dise_nm_2);
        mTxtDiseNmList[2] = (TextView)findViewById(R.id.txt_dise_nm_3);
        mTxtDiseNmList[3] = (TextView)findViewById(R.id.txt_dise_nm_4);
        mTxtDiseNmList[4] = (TextView)findViewById(R.id.txt_dise_nm_5);
        mTxtDiseNmList[5] = (TextView)findViewById(R.id.txt_dise_nm_6);
        mTxtDiseNmList[6] = (TextView)findViewById(R.id.txt_dise_nm_7);
        mTxtDiseNmList[7] = (TextView)findViewById(R.id.txt_dise_nm_8);
        mTxtDiseNmList[8] = (TextView)findViewById(R.id.txt_dise_nm_9);
        mTxtDiseNmList[9] = (TextView)findViewById(R.id.txt_dise_nm_10);

        mTxtDiseCntList = new TextView[10];
        mTxtDiseCntList[0] = (TextView)findViewById(R.id.txt_dise_cnt_1);
        mTxtDiseCntList[1] = (TextView)findViewById(R.id.txt_dise_cnt_2);
        mTxtDiseCntList[2] = (TextView)findViewById(R.id.txt_dise_cnt_3);
        mTxtDiseCntList[3] = (TextView)findViewById(R.id.txt_dise_cnt_4);
        mTxtDiseCntList[4] = (TextView)findViewById(R.id.txt_dise_cnt_5);
        mTxtDiseCntList[5] = (TextView)findViewById(R.id.txt_dise_cnt_6);
        mTxtDiseCntList[6] = (TextView)findViewById(R.id.txt_dise_cnt_7);
        mTxtDiseCntList[7] = (TextView)findViewById(R.id.txt_dise_cnt_8);
        mTxtDiseCntList[8] = (TextView)findViewById(R.id.txt_dise_cnt_9);
        mTxtDiseCntList[9] = (TextView)findViewById(R.id.txt_dise_cnt_10);

        view = findViewById(R.id.root_view);

        //hsh start
//        textView9 = (TextView)findViewById(R.id.textView9);
        //hsh end


    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){
        mMapBtn.setOnClickListener(this);
        mBtnAlarm.setOnClickListener(this);
        mEpidemicBtn.setOnClickListener(this);
        mBtnShare.setOnClickListener(this);

        //hsh start
//        textView9.setOnClickListener(this);
        //hsh end



        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view,FeverMapActivity.this);

        //열지도
        mMapBtn.setOnTouchListener(mClickListener);
        mEpidemicBtn.setOnTouchListener(mClickListener);
        mBtnAlarm.setOnTouchListener(mClickListener);
        mBtnShare.setOnTouchListener(mClickListener);

        //코드 부여(열지도)
        mMapBtn.setContentDescription(getString(R.string.MapBtn));
        mEpidemicBtn.setContentDescription(getString(R.string.EpidemicBtn));
        mBtnAlarm.setContentDescription(getString(R.string.BtnAlarm));
        mBtnShare.setContentDescription(getString(R.string.BtnShare));

    }

    public void initView(){

    }

    public void initRank(){
        if(MainActivity.mEpidemicList.size() > 0){
            for (int i = 0; i < 10; i ++){
                mTxtDiseNmList[i].setText(MainActivity.mEpidemicList.get(i).getDzName());
                mTxtDiseCntList[i].setText(""+MainActivity.mEpidemicList.get(i).getRatio()+"%");
            }
        }else{
            mEpidemicRankLay.setVisibility(View.GONE);
            mTxtNullEpidemic.setVisibility(View.VISIBLE);
        }
    }

    public void setTab(int _tabNum){
        if(_tabNum == 0){         // 맵
            mLinearTabMap.setVisibility(View.VISIBLE);
            mLinearTabEpidemic.setVisibility(View.GONE);
            mMapBtn.setTextColor(getResources().getColor(R.color.h_orange));
            mMapBtn.setBackgroundResource(R.drawable.underline_fever);
            mEpidemicBtn.setTextColor(Color.WHITE);
            mEpidemicBtn.setBackgroundColor(getResources().getColor(R.color.bg_yellow_light));
        }else{
            mLinearTabMap.setVisibility(View.GONE);
            mLinearTabEpidemic.setVisibility(View.VISIBLE);
            mEpidemicBtn.setTextColor(getResources().getColor(R.color.h_orange));
            mEpidemicBtn.setBackgroundResource(R.drawable.underline_fever);
            mMapBtn.setTextColor(Color.WHITE);
            mMapBtn.setBackgroundColor(getResources().getColor(R.color.bg_yellow_light));
        }
    }

    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch(v.getId()){
            case R.id.map_btn: // 그래프 보기
                setTab(0);
                break;
            case R.id.epidemic_btn:
                setTab(1);
                break;
            case R.id.btn_alarm:
                intent = new Intent(FeverMapActivity.this, SettingAddressActivity.class);
                startActivity(intent);
                break;

//            //hsh start
//            case R.id.textView9:
//                GLog.i("click", "dd");
//                intent = new Intent(FeverMapActivity.this, FeverInputActivity.class);
//                startActivity(intent);
//                break;
//
//            //hsh end
            case R.id.share_btn:
                requestSharedisease();
                break;
        }
    }


    private Marker addMarker(LocationItem feverMapItem){
        double fever = Double.parseDouble(feverMapItem.getAvg_fever());
        LatLng position = new LatLng(Double.parseDouble(feverMapItem.getLoc_1()), Double.parseDouble(feverMapItem.getLoc_2()));

        marker.setText(feverMapItem.getLoc_nm_2()+"\n"+"평균:"+feverMapItem.getAvg_fever());

        if(fever < 35.5d){
            marker.setBackgroundResource(R.drawable.bg_marker_lv_1);
        }else if(fever < 37.5d){
            marker.setBackgroundResource(R.drawable.bg_marker_lv_2);
        }else if(fever < 38d){
            marker.setBackgroundResource(R.drawable.bg_marker_lv_3);
        }else if(fever < 39d){
            marker.setBackgroundResource(R.drawable.bg_marker_lv_4);
        }else{
            marker.setBackgroundResource(R.drawable.bg_marker_lv_5);
        }

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.title(""+fever);
        markerOptions.position(position);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, mMarkerRootView)));

        return mMap.addMarker(markerOptions);
    }

    // View를 Bitmap으로 변환
    private Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    /**
     * 열지도 데이터 가져오기
     */
    public void requestMapData(Date startDate, Date ednDate){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE_F, CommonData.JSON_APINM_HJ002);

            params.add(new BasicNameValuePair(CommonData.JSON_STRJSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_GET_MAP_DATA, NetworkConst.getInstance().getFeverDomain(), networkListener, params, new MakeProgress(this));
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 유의질환 공유
     */
    public void requestSharedisease(){
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        try {
            JSONArray array = new JSONArray();
            JSONObject object = new JSONObject();

            object.put(CommonData.JSON_API_CODE, "asstb_mber_cntr_diss");
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_CNTR_TYP, "15");
            object.put(CommonData.JSON_MBER_SN, CommonData.getInstance().getMberSn());             //  회원고유값
            object.put("diss_view_de", CDateUtil.getToday_yyyy_MM_dd());


            if(MainActivity.mEpidemicList.size() > 0 && MainActivity.mEpidemicList.size() > 9){
                object.put("data_length", "10");
                for (int i = 0; i < 10; i ++){
                    JSONObject epidObject = new JSONObject();
                    epidObject.put("rank", String.valueOf(i+1));
                    epidObject.put("rank_nm", MainActivity.mEpidemicList.get(i).getDzName());
                    epidObject.put("rank_per", "" + MainActivity.mEpidemicList.get(i).getRatio()+"%");

                    array.put(epidObject);
                }
            } else if(MainActivity.mEpidemicList.size() > 0 && MainActivity.mEpidemicList.size() < 10){
                object.put("data_length", MainActivity.mEpidemicList.size());
                for (int i = 0; i <  MainActivity.mEpidemicList.size(); i ++){
                    JSONObject epidObject = new JSONObject();
                    epidObject.put("rank", String.valueOf(i+1));
                    epidObject.put("rank_nm", MainActivity.mEpidemicList.get(i).getDzName());
                    epidObject.put("rank_per", "" + MainActivity.mEpidemicList.get(i).getRatio()+"%");

                    array.put(epidObject);
                }
            } else{
                object.put("data_length", "10");
                for (int i = 0; i < 10; i ++){
                    JSONObject epidObject = new JSONObject();
                    epidObject.put("rank", String.valueOf(i+1));
                    epidObject.put("rank_nm", "");
                    epidObject.put("rank_per", "0.0%");

                    array.put(epidObject);
                }
            }


            object.put("data", array);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(this, NetworkConst.NET_ASSTB_MBER_CNTR_DISS, NetworkConst.getInstance().getDefDomain(), networkListener, params, new MakeProgress(this));
        }catch(Exception e){
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_GET_MAP_DATA:
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN_F);

                                if (data_yn.equals(CommonData.YES)) {
                                    JSONArray mapArr = resultData.getJSONArray(CommonData.JSON_DATA_F);
                                    // 데이터가 있을 시
                                    if (mapArr.length() > 0) {
                                        for(int i = 0; i < mapArr.length(); i++){
                                            try {
                                                JSONObject object = mapArr.getJSONObject(i);

                                                LocationItem item = new LocationItem();
                                                item.setLoc_nm_1(object.getString(CommonData.JSON_LOC_NM_1));
                                                item.setLoc_nm_2(object.getString(CommonData.JSON_LOC_NM_2));
                                                item.setAvg_fever(object.getString(CommonData.JSON_AVG_FEVER));
                                                item.setLoc_1(object.getString(CommonData.JSON_LOC_1));
                                                item.setLoc_2(object.getString(CommonData.JSON_LOC_2));

                                                if( Double.parseDouble(item.getAvg_fever()) > 0)
                                                    addMarker(item);
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }
                                        //hsh start
                                        setTabShow();
                                        //hsh end
                                    }
                                }

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;

                case NetworkConst.NET_ASSTB_MBER_CNTR_DISS:
                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);
                                if (data_yn.equals(CommonData.YES)) {
                                    String imgUrl = "https://wkd.walkie.co.kr/HL_FV/info/image/01_15.png";
                                    String cntr_url = resultData.getString("cntr_url");

                                    if(cntr_url.contains("https://wkd.walkie.co.kr"));
                                    String param = cntr_url.replace("https://wkd.walkie.co.kr","");

                                    View view = LayoutInflater.from(FeverMapActivity.this).inflate(R.layout.applink_dialog_layout, null);
                                    ApplinkDialog dlg = ApplinkDialog.showDlg(FeverMapActivity.this, view);
                                    dlg.setSharing(imgUrl, "img", "", "","[현대해상 "+ KakaoLinkUtil.getAppname(FeverMapActivity.this.getPackageName(),FeverMapActivity.this)+"]","유의질환 발생 빈도","자세히보기","",false,"disease.png",param,cntr_url);

                                }else {
                                }

                            } catch (Exception e) {
                                GLog.e(e.toString());
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            break;
                    }
                    break;
            }
            hideProgress();
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            hideProgress();
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            hideProgress();
            dialog.show();

        }
    };

    //hsh start
    private void setTabShow() {
        Intent i = getIntent();
        int push_type = i.getIntExtra(CommonData.EXTRA_PUSH_TYPE, 0);
        int type = i.getIntExtra(CommonData.EXTRA_MAIN_TYPE,0);
        if((push_type!=0 && push_type == FirebaseMessagingService.DIESEASE) || type!=0){
            setTab(1);
        }else{
            setTab(0);
        }
        getIntent().removeExtra(CommonData.EXTRA_PUSH_TYPE);
        getIntent().removeExtra(CommonData.EXTRA_INFO_SN);
    }
    //hsh end

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        GpsInfo gps = new GpsInfo(this);
        if(gps.isGetLocation()) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(gps.getLatitude(), gps.getLongitude()), 11));
        }

        setCustomMarkerView();
    }

    private void setCustomMarkerView() {
        mMarkerRootView = LayoutInflater.from(this).inflate(R.layout.custom_marker, null);
        marker = (TextView) mMarkerRootView.findViewById(R.id.marker);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapFragment != null)
            mapFragment.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mapFragment != null)
            mapFragment.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mMap != null)
            mMap.clear();

            if (mapFragment != null)
                mapFragment.onDestroyView();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
