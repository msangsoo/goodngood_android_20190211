package com.appmd.hi.gngcare.greencare.step;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.CommonActionBar;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.base.value.Define;
import com.appmd.hi.gngcare.greencare.database.DBHelper;
import com.appmd.hi.gngcare.greencare.database.DBHelperWeight;
import com.appmd.hi.gngcare.greencare.network.tr.ApiData;
import com.appmd.hi.gngcare.greencare.network.tr.data.Tr_mvm_goalqy;
import com.appmd.hi.gngcare.greencare.util.SharedPref;
import com.appmd.hi.gngcare.greencare.util.StringUtil;
import com.appmd.hi.gngcare.greencare.util.ViewUtil;

import java.text.DecimalFormat;

/**
 * Created by insystemscompany on 2017. 2. 28..
 */

public class StepInputFragment extends BaseFragment {

    private EditText mCalEt, mTargetStepEt;
    private ImageButton typeButton;

//    private CheckBox mGoogleFitCb;
//    private CheckBox mBandCb;

    private CheckBox chkCaledit;
    private CheckBox chkStepedit;

    private View mCheckedEditText;

    private InputMethodManager imm;

    CustomAlertDialog mDialog;


    public static Fragment newInstance() {
        StepInputFragment fragment = new StepInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_walk_input, container, false);

        mCalEt = (EditText) view.findViewById(R.id.step_edittext);
        mTargetStepEt = (EditText) view.findViewById(R.id.step_target_edittext);

//        mGoogleFitCb = (CheckBox) view.findViewById(R.id.checkbox_google);
//        mBandCb = (CheckBox) view.findViewById(R.id.checkbox_urban);

        chkCaledit = (CheckBox) view.findViewById(R.id.checkbox_caledit);      // 칼로리수정 체크박스
        chkStepedit = (CheckBox) view.findViewById(R.id.checkbox_stepedit); // 걸음수수정 체크박스

        imm = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);

        mCalEt.setEnabled(false);
        mCalEt.setFocusableInTouchMode(false);
        mTargetStepEt.setEnabled(false);
        mTargetStepEt.setFocusableInTouchMode(false);

//        mGoogleFitCb.setChecked(false);
//        mBandCb.setChecked(false);

        /** font Typeface 적용 */
        typeButton = (ImageButton)view.findViewById(R.id.walk_input_save_btn);
        ViewUtil.setTypefacenotosanskr_bold(getContext(), typeButton);
        typeButton.setOnClickListener(mClickListener);

        CommonData login = CommonData.getInstance();
        if(TextUtils.isEmpty(login.getMotherGoalCal()) == false){
            mCalEt.setHint(makeStringComma(login.getMotherGoalCal().replace(",","")));
        }

        if(TextUtils.isEmpty(login.getMotherGoalStep()) == false){
            mTargetStepEt.setHint(makeStringComma(login.getMotherGoalStep().replace(",","")));
        }

        int dataSource = SharedPref.getInstance(getContext()).getPreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_GOOGLE_FIT);
//        if (dataSource != -1) {
//            if (Define.STEP_DATA_SOURCE_GOOGLE_FIT == dataSource) {
//                // 구글 피트니스 소스
//                mGoogleFitCb.setChecked(true);
//                mBandCb.setChecked(false);
//            } else if (Define.STEP_DATA_SOURCE_BAND == dataSource) {
//                // 스마트 밴드
//                mGoogleFitCb.setChecked(false);
//                mBandCb.setChecked(true);
//            }
//        }
//
//        mGoogleFitCb.setOnClickListener(mCheckClickListener);
//        mBandCb.setOnClickListener(mCheckClickListener);
        chkCaledit.setOnClickListener(mCheckClickListener);
        chkStepedit.setOnClickListener(mCheckClickListener);

        mCalEt.addTextChangedListener(watcher);
        mTargetStepEt.addTextChangedListener(watcher);

        mCalEt.setOnFocusChangeListener(mFocusChangeListener);
        mTargetStepEt.setOnFocusChangeListener(mFocusChangeListener);


        mCalEt.setOnKeyListener(new View.OnKeyListener() {
              public boolean onKey(View v, int keyCode, KeyEvent event) {
                  if(event.getKeyCode()== KeyEvent.KEYCODE_ENTER) {
                      if((StringUtil.getIntVal(mCalEt.getText().toString()) < 150 || StringUtil.getIntVal(mCalEt.getText().toString()) > 1000) && mCalEt.getText().length() > 0){
                          Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                      }
                  }

                  return false;
              }
        });

        mTargetStepEt.setOnKeyListener( new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getKeyCode()== KeyEvent.KEYCODE_ENTER) {
                    if((StringUtil.getIntVal(mCalEt.getText().toString()) < 150 || StringUtil.getIntVal(mCalEt.getText().toString()) > 1000) && mCalEt.getText().length() > 0){
                        Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                    }
                }

                return false;
            }
        });

        setActionBar();

        return view;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void loadActionbar(CommonActionBar actionBar) {
    }

    private void setActionBar() {
        // CommonActionBar actionBar 는 안 씀 한화꺼
        if (getActivity() instanceof DummyActivity) {
            DummyActivity activity = (DummyActivity) getActivity();

            TextView titleTv = (TextView)activity.findViewById(R.id.common_title_tv);
            titleTv.setText(getString(R.string.text_target_modify));

        }
    }

    /*
     **문자열 포맷
     */
    public String makeStringComma(String str) {
        if (str.length() == 0)
            return "";
        long value = Long.parseLong(str);
        DecimalFormat format = new DecimalFormat("#,###");
        return format.format(value);
    }

    View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            mCheckedEditText = v;
            if (v == mCalEt && hasFocus == false) {
                if((StringUtil.getIntVal(mCalEt.getText().toString()) < 150 || StringUtil.getIntVal(mCalEt.getText().toString()) > 1000) && mCalEt.getText().length() > 0){
                    Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                }
            } else if(v == mTargetStepEt && hasFocus == false) {
                if((StringUtil.getIntVal(mCalEt.getText().toString()) < 150 || StringUtil.getIntVal(mCalEt.getText().toString()) > 1000) && mCalEt.getText().length() > 0){
                    Toast.makeText(getContext(), getContext().getString(R.string.step_input_over), Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
            if (vId == R.id.walk_input_save_btn) {
                final String mStep = mCalEt.getText().toString();
                if (TextUtils.isEmpty(mStep) || StringUtil.getIntVal(mStep) <= 0) {
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.noti));
                    mDialog.setContent("하루 소모 칼로리를 정확히 입력해 주세요.");
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                    //CDialog.showDlg(getContext(), "하루 소모 칼로리를 정확히 입력해 주세요.");
                    return;
                }
                if (StringUtil.getIntVal(mCalEt.getText().toString()) < 150 || StringUtil.getIntVal(mCalEt.getText().toString()) > 1000) {
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.noti));
                    mDialog.setContent(getContext().getString(R.string.step_input_over));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                    //CDialog.showDlg(getContext(), getContext().getString(R.string.step_input_over));
                    return;
                }
                final String mTargetStep = mTargetStepEt.getText().toString();
                if (TextUtils.isEmpty(mTargetStep) || (StringUtil.getIntVal(mTargetStep) <= 0 || StringUtil.getIntVal(mTargetStep) > 500000)) {
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.noti));
                    mDialog.setContent("하루 목표 걸음수를 정확히 입력해 주세요.");
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                    //CDialog.showDlg(getContext(), "하루 목표 걸음수를 정확히 입력해 주세요.");
                    return;
                }

                mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_B);
                mDialog.setTitle(getString(R.string.noti));
                mDialog.setContent("하루 목표 칼로리 및 목표 걸음 수를 수정 하시겠습니까?");
                mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
                mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                    doInputStep(mStep, mTargetStep);
                    dialog.dismiss();
                });
                mDialog.show();

                /*CDialog.showDlg(getContext(), "하루 목표 칼로리 및 목표 걸음 수를 수정 하시겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doInputStep(mStep, mTargetStep);
                    }
                }, null);*/
            }
        }
    };


    View.OnClickListener mCheckClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final CheckBox chkBtn = (CheckBox)v;
            setInputText(chkBtn);
        }
    };


    private void setInputText(CheckBox chkBtn) {
        if (chkBtn == chkCaledit ) {
            if (chkCaledit.isChecked() == true) {
                chkCaledit.setChecked(true);
                chkStepedit.setChecked(false);

                mTargetStepEt.setEnabled(false);
                mTargetStepEt.setFocusable(false);
                mTargetStepEt.setFocusableInTouchMode(false);
                mTargetStepEt.requestFocus();
                imm.hideSoftInputFromInputMethod(mTargetStepEt.getWindowToken(), 0);

                mCalEt.setEnabled(true);
                mCalEt.setFocusable(true);
                mCalEt.setFocusableInTouchMode(true);
                mCalEt.requestFocus();
                imm.showSoftInput(mCalEt, InputMethodManager.SHOW_IMPLICIT);

            }else {
                chkCaledit.setChecked(false);

                mCalEt.setEnabled(false);
                mCalEt.setFocusable(false);
                mCalEt.setFocusableInTouchMode(false);
                mCalEt.requestFocus();
                imm.hideSoftInputFromInputMethod(mCalEt.getWindowToken(), 0);
            }
        }else if (chkBtn == chkStepedit){
            if (chkStepedit.isChecked() == true) {
                chkStepedit.setChecked(true);
                chkCaledit.setChecked(false);

                mCalEt.setEnabled(false);
                mCalEt.setFocusable(false);
                mCalEt.setFocusableInTouchMode(false);
                mCalEt.requestFocus();
                imm.hideSoftInputFromInputMethod(mCalEt.getWindowToken(), 0);

                mTargetStepEt.setEnabled(true);
                mTargetStepEt.setFocusable(true);
                mTargetStepEt.setFocusableInTouchMode(true);
                mTargetStepEt.requestFocus();
                imm.showSoftInput(mTargetStepEt, InputMethodManager.SHOW_IMPLICIT);
            }else {
                chkStepedit.setChecked(false);

                mTargetStepEt.setEnabled(false);
                mTargetStepEt.setFocusable(false);
                mTargetStepEt.setFocusableInTouchMode(false);
                mTargetStepEt.requestFocus();
                imm.hideSoftInputFromInputMethod(mTargetStepEt.getWindowToken(), 0);
            }
        }
    }


    private void doInputStep(String mStep , String mTargetStep) {

        CommonData login = CommonData.getInstance();

        Tr_mvm_goalqy.RequestData requestData = new Tr_mvm_goalqy.RequestData();
        requestData.mber_sn = login.getMberSn();
        requestData.goal_mvm_calory = mStep;
        requestData.goal_mvm_stepcnt = mTargetStep;
        requestData.goal_bdwgh = login.getMotherGoalWeight();

        getData(getContext(), Tr_mvm_goalqy.class, requestData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {
                if (obj instanceof Tr_mvm_goalqy) {
                    Tr_mvm_goalqy data = (Tr_mvm_goalqy)obj;
                    if ("Y".equals(data.reg_yn)) {
                        login.setMotherGoalStep(data.goal_mvm_stepcnt);
                        login.setMotherGoalCal(data.goal_mvm_calory);
                        login.setMotherGoalWeight(data.goal_bdwgh);

                        mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.noti));
                        mDialog.setContent("수정 되었습니다.");
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
                            mCalEt.setText("");
                            mTargetStepEt.setText("");
                            onBackPressed();
                            dialog.dismiss();
                        });
                        mDialog.show();
                        /*CDialog.showDlg(getContext(), "수정 되었습니다.", new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
                                CommonData login = CommonData.getInstance();
                                login.setMotherGoalCal(mCalEt.getText().toString());
                                login.setMotherGoalStep(mTargetStepEt.getText().toString());

                                mCalEt.setText("");
                                mTargetStepEt.setText("");
                                onBackPressed();
                            }
                        });*/
                    } else {
                        mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.noti));
                        mDialog.setContent("등록에 실패 했습니다.");
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();
                        //CDialog.showDlg(getContext(), "등록에 실패 했습니다.");
                    }

                }
            }
        });
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(s.toString().length() > 0){

                CommonData login = CommonData.getInstance();
                int sex             = StringUtil.getIntVal(login.getGender());
                DBHelper helper = new DBHelper(getContext());
                DBHelperWeight weightDb = helper.getWeightDb();
//                DBHelperWeight.WeightStaticData bottomData = weightDb.getResultStatic();
                float weight        = StringUtil.getFloat(login.getMotherWeight());

//                if(TextUtils.isEmpty(bottomData.getWeight()) == false) {
//                    weight          = StringUtil.getFloatVal(login.getMotherWeight());
//                } else{
//                    weight          = StringUtil.getFloatVal(login.getMotherGoalWeight());
//                }

                float height        = StringUtil.getFloat(login.getBefCm());
                int sInt            = Integer.parseInt(s.toString());

                if(chkCaledit.isChecked() == true){
                    if(mCalEt.getText().toString().length() > 0){
                        mTargetStepEt.removeTextChangedListener(this);
                        mTargetStepEt.setText(""+getStepTargetCalulator(sInt));
                        mTargetStepEt.addTextChangedListener(this);
                    }

                }else{
                    if(mTargetStepEt.getText().toString().length() > 0) {
                        mCalEt.removeTextChangedListener(this);
                        mCalEt.setText("" + getCalroriTargetCalulator(sInt));
                        mCalEt.addTextChangedListener(this);
                    }
                }
            }else{
                if(chkCaledit.isChecked() == true){
                    mTargetStepEt.removeTextChangedListener(this);
                    mTargetStepEt.setText("");
                    mTargetStepEt.addTextChangedListener(this);
                }else{
                    mCalEt.removeTextChangedListener(this);
                    mCalEt.setText("");
                    mCalEt.addTextChangedListener(this);
                }
            }

            if (isValidStep() && isValidTargetStep()) {
                typeButton.setEnabled(true);
            } else {
                typeButton.setEnabled(false);
            }
        }
    };

    private boolean isValidStep() {
        String text = mCalEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    private boolean isValidTargetStep() {
        String text = mTargetStepEt.getText().toString();
        return TextUtils.isEmpty(text) == false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Define.STEP_DATA_SOURCE_BAND) {
            int typeSource = SharedPref.getInstance(getContext()).getPreferences(SharedPref.STEP_DATA_SOURCE_TYPE, Define.STEP_DATA_SOURCE_GOOGLE_FIT);
//            if (Define.STEP_DATA_SOURCE_BAND == typeSource) {
//                // 밴드 등록 성공인 경우
//                mGoogleFitCb.setChecked(false);
//                mBandCb.setChecked(true);
//                restartMainActivity();
//            } else {
//                mGoogleFitCb.setChecked(true);
//                mBandCb.setChecked(false);
//            }
        }
    }
}