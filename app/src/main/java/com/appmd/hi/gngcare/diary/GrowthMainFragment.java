package com.appmd.hi.gngcare.diary;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.common.ApplinkDialog;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.main.EduVideoActivity;
import com.appmd.hi.gngcare.main.MainActivity;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.setting.SettingActivity;
import com.appmd.hi.gngcare.util.GLog;
import com.appmd.hi.gngcare.util.KakaoLinkUtil;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by MobileDoctor on 2017-02-28.
 */

public class GrowthMainFragment extends BaseFragment implements View.OnClickListener, GrowthMainActivity.onKeyBackPressedListener {


    private View view;

    private ImageButton mBtnGrowthPut, mBtnGrowthPredict, mBtnGrowthFaq, mBtnGrowthHx, mBtnGrowthEat, mBtnGrowthVideo;
    private TextView mTxtHeightMain, mTxtWeightMain, mTxtHeightRank, mTxtWeightRank, mTxtHeightDe, mTxtWeightDe;

    private ImageButton mShareBtn;


    CustomAlertDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.growth_main_fragment, null);

        init(view);
        setEvent();

        return view;
    }

    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {
        mBtnGrowthHx = (ImageButton) view.findViewById(R.id.btn_growth_hx);
        mBtnGrowthPut = (ImageButton) view.findViewById(R.id.btn_growth_put);
        mBtnGrowthPredict = (ImageButton) view.findViewById(R.id.btn_growth_predict);
        mBtnGrowthFaq = (ImageButton) view.findViewById(R.id.btn_growth_faq);
        mBtnGrowthEat = (ImageButton) view.findViewById(R.id.btn_growth_eat);
        mBtnGrowthVideo = (ImageButton) view.findViewById(R.id.btn_growth_video);

        mTxtHeightMain = (TextView) view.findViewById(R.id.txt_height_main);
        mTxtWeightMain = (TextView) view.findViewById(R.id.txt_weight_main);
        mTxtHeightRank = (TextView) view.findViewById(R.id.txt_height_rank);
        mTxtWeightRank = (TextView) view.findViewById(R.id.txt_weight_rank);
        mTxtHeightDe = (TextView) view.findViewById(R.id.txt_height_de);
        mTxtWeightDe = (TextView) view.findViewById(R.id.txt_weight_de);

        mShareBtn = (ImageButton) view.findViewById(R.id.share_btn);
    }

    public void setEvent() {
        mBtnGrowthHx.setOnClickListener(this);
        mBtnGrowthPut.setOnClickListener(this);
        mBtnGrowthPredict.setOnClickListener(this);
        mBtnGrowthFaq.setOnClickListener(this);
        mBtnGrowthEat.setOnClickListener(this);
        mBtnGrowthVideo.setOnClickListener(this);
        mShareBtn.setOnClickListener(this);


        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view, getContext());

        //아이 성장
        mShareBtn.setOnTouchListener(mClickListener);
        mBtnGrowthPut.setOnTouchListener(mClickListener);
        mBtnGrowthHx.setOnTouchListener(mClickListener);
        mBtnGrowthPredict.setOnTouchListener(mClickListener);
        mBtnGrowthEat.setOnTouchListener(mClickListener);
        mBtnGrowthVideo.setOnTouchListener(mClickListener);
        mBtnGrowthFaq.setOnTouchListener(mClickListener);

        //코드 부여(아이 성장)
        mShareBtn.setContentDescription(getString(R.string.ShareBtn4));
        mBtnGrowthPut.setContentDescription(getString(R.string.BtnGrowthPut));
        mBtnGrowthHx.setContentDescription(getString(R.string.BtnGrowthHx));
        mBtnGrowthPredict.setContentDescription(getString(R.string.BtnGrowthPredict));
        mBtnGrowthEat.setContentDescription(getString(R.string.BtnGrowthEat));
        mBtnGrowthVideo.setContentDescription(getString(R.string.BtnGrowthVideo));
        mBtnGrowthFaq.setContentDescription(getString(R.string.BtnGrowthFaq));
}

    @Override
    public void onClick(View v) {
        Intent intent;

        GrowthMainActivity activity = (GrowthMainActivity) getActivity();

        switch (v.getId()) {
            case R.id.btn_growth_hx:
                if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.call_center));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                } else {
                    switchFragment(new GrowthRecordFragment());
                    activity.switchActionBarTheme(GrowthMainActivity.THEME_BLUE);
                    activity.switchActionBarTitle(getString(R.string.title_growth_hx));
                }
                break;
            case R.id.btn_growth_put:
                if (MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlExistYn().equals(CommonData.NO)) {
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.call_center));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                } else {
                    intent = new Intent(getContext(), GrowthRecordInPut.class);
                    getActivity().startActivityForResult(intent, CommonData.REQUEST_CODE_INPUT);
                    Util.BackAnimationStart(getActivity());
                }
                break;
            case R.id.btn_growth_predict:
                switchFragment(new GrowthRecordLastHeight());
                activity.switchActionBarTheme(GrowthMainActivity.THEME_BLUE);
                activity.switchActionBarTitle(getString(R.string.title_growth_predict));
                break;
            case R.id.btn_growth_eat:

                // TODO : 만 나이로 3~8세까지 이외에는 들어갈 수 없음.
                //맞춤형 식단은 만 3~18세의 고객님께 제공합니다.
//                현재 1세 자녀를 선택 후 식단으로 들어가면 안드로이드는 1200kcal식단이 나오고, iOS는 식단 표에 빈칸으로 나오고 있습니다.
//                    맞춤형 식단 알고리즘에 의하면 3~18세까지 해당하는 고객에게 맞춤형으로 식단을 제공합니다. 따라서 3세 미만 자녀를 선택후 맞춤형 식단 메뉴에 진입 시 진입 못하게 막고 팝업(다른데 적용한 것과 동일) 생성하여 안내를 해주도록 해주세요. 문구는 "맞춤형 식단은 3~18세의 고객님께 제공합니다."

                String text = Util.getSharedPreference(getContext(),"age");
                GLog.i(" text = " , text);
                int age = 0;
                if(text.length() > 0 || !text.equals("")){
                    age = Integer.parseInt(text);
                }
                else{
                    age = 0;
                }


                if(age >= 3 && age <=18){
                    if(MainActivity.mLastHeight == null
                            || MainActivity.mLastHeight.equals("0")
                            || MainActivity.mLastHeight.length() <= 1
                            ){
                        mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                        mDialog.setContent(getString(R.string.growth_eat_dialog));
                        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                        mDialog.show();
                    }else{
                        intent = new Intent(getActivity(), GrowthEatActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    mDialog = new CustomAlertDialog(getContext(), CustomAlertDialog.TYPE_A);
                    mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
                    mDialog.setContent(getString(R.string.growth_eat_dialog1));
                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), null);
                    mDialog.show();
                }






                break;
            case R.id.btn_growth_video:
                intent = new Intent(getActivity(), EduVideoActivity.class);
                intent.putExtra("TITLE", getString(R.string.edu_video_title_03));
                intent.putExtra("ML_MCODE", "2");
                startActivity(intent);
                break;

            case R.id.btn_growth_faq:
                switchFragment(new GrowthFAQFragment());
                activity.switchActionBarTheme(GrowthMainActivity.THEME_BLUE);
                activity.switchActionBarTitle(getString(R.string.title_growth_faq));
                break;

            case R.id.share_btn:
                requestGrowthLastDataShareApi();
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        requestGrowthLastDataApi(MainActivity.mChildMenuItem.get(MainActivity.mChildChoiceIndex).getmChlSn());
    }

    @Override
    public void onStart() {
        super.onStart();
        ((GrowthMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        GrowthMainActivity activity = (GrowthMainActivity) getActivity();
        activity.finish();
    }

    /**
     * 최근 키 몸무게 가져오기
     */
    public void requestGrowthLastDataApi(String chl_sn) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {"api_code":"chldrn_growth_last_cm_height","insures_code":"108","app_code":"android","mber_sn":"18622","chl_sn":"1312"}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CHLDRN_GROWTH_LAST_CM_HEIGHT);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);          //  os
            object.put(CommonData.JSON_MBER_SN, CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_CHL_SN, chl_sn);               //  자녀키값

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_GROWTH_LAST_DATA, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 우리아이 성장현황 공유
     */
    public void requestGrowthLastDataShareApi() {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        // {"api_code":"asstb_mber_cntr_height_growth"
        //,"insures_code":"108"
        //,"cntr_typ":"10"
        //,"mber_sn":"115232"
        //,"height":"160"
        //,"bdwgh":"40"
        //,"height_pt":"89"
        //,"bdwgh_pt":"100"
        //,"height_de":"20190103"
        //,"bdwgh_de":"20190103"
        //}
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, "asstb_mber_cntr_height_growth");    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_CNTR_TYP, "10");
            object.put(CommonData.JSON_MBER_SN, CommonData.getInstance().getMberSn());             //  회원고유값
            object.put("height",MainActivity.mLastHeight);
            object.put("bdwgh",MainActivity.mLastWeight);
            object.put("height_pt",MainActivity.mLastCmResult);
            object.put("bdwgh_pt",MainActivity.mLastKgResult);
            object.put("height_de",MainActivity.mLastHeightDe);
            object.put("bdwgh_de",MainActivity.mLastWeightDe);

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(getActivity(), NetworkConst.NET_ASSTB_MBER_CNTR_HEIGHT_GROWTH, NetworkConst.getInstance().getDefDomain(), networkListener, params, getProgress());
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }


    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch (type) {
                case NetworkConst.NET_GROWTH_LAST_DATA:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);
                                if (data_yn.equals(CommonData.YES)) {
                                    MainActivity.mLastHeight = resultData.getString(CommonData.JSON_LAST_HRIGTH);
                                    MainActivity.mLastWeight = resultData.getString(CommonData.JSON_LAST_BDWGH);
                                    MainActivity.mLastCmResult = resultData.getString(CommonData.JSON_CM_REUSLT);
                                    MainActivity.mLastKgResult = resultData.getString(CommonData.JSON_KG_REUSLT);
                                    MainActivity.mLastHeightDe = resultData.getString(CommonData.JSON_LAST_HRIGTH_DE);
                                    MainActivity.mLastWeightDe = resultData.getString(CommonData.JSON_LAST_BDWGH_DE);
                                } else {
                                    MainActivity.mLastHeight = "0";
                                    MainActivity.mLastWeight = "0";
                                    MainActivity.mLastCmResult = "";
                                    MainActivity.mLastKgResult = "";
                                    MainActivity.mLastHeightDe = "";
                                    MainActivity.mLastWeightDe = "";
                                }
                            } catch (Exception e) {
                                GLog.e(e.toString());
                                MainActivity.mLastHeight = "0";
                                MainActivity.mLastWeight = "0";
                                MainActivity.mLastCmResult = "";
                                MainActivity.mLastKgResult = "";
                                MainActivity.mLastHeightDe = "";
                                MainActivity.mLastWeightDe = "";
                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");
                            MainActivity.mLastHeight = "0";
                            MainActivity.mLastWeight = "0";
                            MainActivity.mLastCmResult = "";
                            MainActivity.mLastKgResult = "";
                            MainActivity.mLastHeightDe = "";
                            MainActivity.mLastWeightDe = "";
                            break;
                    }

                    if (MainActivity.mLastHeight.equals("0") || TextUtils.isEmpty(MainActivity.mLastHeight) == true ) {
                        mTxtHeightMain.setText("0");
                        mTxtHeightRank.setVisibility(View.INVISIBLE);
                        mTxtHeightDe.setVisibility(View.INVISIBLE);
                    } else {
                        mTxtHeightMain.setText(MainActivity.mLastHeight);
                        mTxtHeightRank.setText(MainActivity.mLastCmResult + "%");
                        mTxtHeightDe.setText(MainActivity.mLastHeightDe.substring(0, 4) + "." + MainActivity.mLastHeightDe.substring(4, 6) + "." + MainActivity.mLastHeightDe.substring(6, 8));
                        mTxtHeightRank.setVisibility(View.VISIBLE);
                        mTxtHeightDe.setVisibility(View.VISIBLE);

                    }

                    if (MainActivity.mLastWeight.equals("0") || TextUtils.isEmpty(MainActivity.mLastWeight) == true) {
                        mTxtWeightMain.setText("0");
                        mTxtWeightRank.setVisibility(View.INVISIBLE);
                        mTxtWeightDe.setVisibility(View.INVISIBLE);
                    } else {
                        mTxtWeightMain.setText(MainActivity.mLastWeight);
                        mTxtWeightRank.setText(MainActivity.mLastKgResult + "%");
                        mTxtWeightDe.setText(MainActivity.mLastWeightDe.substring(0, 4) + "." + MainActivity.mLastWeightDe.substring(4, 6) + "." + MainActivity.mLastWeightDe.substring(6, 8));
                        mTxtWeightRank.setVisibility(View.VISIBLE);
                        mTxtWeightDe.setVisibility(View.VISIBLE);
                    }

                    break;

                case NetworkConst.NET_ASSTB_MBER_CNTR_HEIGHT_GROWTH:

                    switch (resultCode) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_APP_INFO API_SUCCESS", "dd");
                            try {
                                String data_yn = resultData.getString(CommonData.JSON_REG_YN);


                                if (data_yn.equals(CommonData.YES)) {
                                    String imgUrl = "https://wkd.walkie.co.kr/HL_FV/info/image/01_10.png";

                                    String cntr_url = resultData.getString("cntr_url");
                                    if(cntr_url.contains("https://wkd.walkie.co.kr"));
                                    String param = cntr_url.replace("https://wkd.walkie.co.kr","");

                                    View view = LayoutInflater.from(getContext()).inflate(R.layout.applink_dialog_layout, null);
                                    ApplinkDialog dlg = ApplinkDialog.showDlg(getContext(), view);
                                    dlg.setSharing(imgUrl, "img", "", "","[현대해상 "+ KakaoLinkUtil.getAppname(getContext().getPackageName(),getContext())+"]","우리 아이 성장 현황","자세히보기","",false,"chl_growth_status.png",param,cntr_url);

                                } else {

                                }
                            } catch (Exception e) {
                                GLog.e(e.toString());

                            }

                            break;
                        case CommonData.API_ERROR_SYSTEM_ERROR:    // 시스템 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_SYSTEM_ERROR", "dd");

                            break;
                        case CommonData.API_ERROR_INPUT_DATA_ERROR:    // 입력 데이터 오류
                            GLog.i("NET_GET_APP_INFO API_ERROR_INPUT_DATA_ERROR", "dd");
                            break;

                        default:
                            GLog.i("NET_GET_APP_INFO default", "dd");

                            break;
                    }
                    break;
            }

        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.

            dialog.show();

        }
    };

}
