package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.data.CombinedData;

/**
 * Created by philipp on 11/06/16.
 */
public interface CombinedDataProvider extends LineDataProvider, BarDataProvider, BubbleDataProvider, CandleDataProvider, ScatterDataProvider {

    CombinedData getCombinedData();
}
