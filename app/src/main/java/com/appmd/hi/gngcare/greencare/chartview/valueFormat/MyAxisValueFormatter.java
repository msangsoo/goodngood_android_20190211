package com.appmd.hi.gngcare.greencare.chartview.valueFormat;


import com.appmd.hi.gngcare.greencare.charting.components.AxisBase;
import com.appmd.hi.gngcare.greencare.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public MyAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " $";
    }
}
