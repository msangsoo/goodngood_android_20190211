
package com.appmd.hi.gngcare.greencare.charting.renderer;


import com.appmd.hi.gngcare.greencare.charting.utils.ViewPortHandler;
import com.appmd.hi.gngcare.greencare.util.ChartTimeUtil;

/**
 * Abstract baseclass of all Renderers.
 * 
 * @author Philipp Jahoda
 */
public abstract class Renderer {

    /**
     * the component that handles the drawing area of the chart and it's offsets
     */
    protected ViewPortHandler mViewPortHandler;

    public Renderer(ViewPortHandler viewPortHandler) {
        this.mViewPortHandler = viewPortHandler;
    }

    public void setTimeClass(ChartTimeUtil timeClass) {
    }
}
