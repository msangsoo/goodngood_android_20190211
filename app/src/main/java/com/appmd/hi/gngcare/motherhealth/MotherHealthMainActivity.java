package com.appmd.hi.gngcare.motherhealth;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BackBaseActivity;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.greencare.base.BaseFragment;
import com.appmd.hi.gngcare.greencare.base.DummyActivity;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.greencare.food.FoodManageFragment;
import com.appmd.hi.gngcare.greencare.pressure.PressureInputMainFragment;
import com.appmd.hi.gngcare.greencare.pressure.PressureManageFragment;
import com.appmd.hi.gngcare.greencare.step.StepInputFragment;
import com.appmd.hi.gngcare.greencare.step.StepManageFragment;
import com.appmd.hi.gngcare.greencare.sugar.SugarInputMainFragment;
import com.appmd.hi.gngcare.greencare.sugar.SugarManageFragment;
import com.appmd.hi.gngcare.greencare.weight.WeightManageFragment;
import com.appmd.hi.gngcare.webview.TipWebViewActivity;

public class MotherHealthMainActivity extends BackBaseActivity implements View.OnClickListener {
    private int mTabNum = 0;
    private int mInputNum = -1;    // 인풋 타입


    private int mFragmentNum;
    private Intent mIntent;

    private RadioGroup mMenuRadioGroup;
    private View mActionbar;
//    private ImageView mActionbarBtn;
    private ImageView mActionbarTipBtn;

    protected Fragment[] fragments = new Fragment[5];
    private LinearLayout[] contentLayouts = new LinearLayout[5];
    private RadioButton mRadiobtn0;
    private RadioButton mRadiobtn1;
    private RadioButton mRadiobtn2;
    private RadioButton mRadiobtn3;
    private RadioButton mRadiobtn4;
    private Fragment Ori_fragment;  //그래프 확대
//    private ImageView Hcall_btn;

    private MakeProgress mProgress			=	null;
    private boolean exsit_aft_baby;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mother_health_main_activity);
        if (mProgress == null)
            mProgress = new MakeProgress(this);

        mIntent = getIntent();

        if(mIntent != null){
            mFragmentNum = mIntent.getIntExtra("Num",0);
        }

        setTitle(getString(R.string.main_brn_txt_12));
        mMenuRadioGroup = (RadioGroup) findViewById(R.id.mother_health_main_radio_group);
        mRadiobtn0 = (RadioButton) findViewById(R.id.radio0);
        mRadiobtn1 = (RadioButton) findViewById(R.id.radio1);
        mRadiobtn2 = (RadioButton) findViewById(R.id.radio2);
        mRadiobtn3 = (RadioButton) findViewById(R.id.radio3);
        mRadiobtn4 = (RadioButton) findViewById(R.id.radio4);

        contentLayouts[0] = findViewById(R.id.mother_health_main_content_layout);
        contentLayouts[1] = findViewById(R.id.mother_health_main_content_layout2);
        contentLayouts[2] = findViewById(R.id.mother_health_main_content_layout3);
        contentLayouts[3] = findViewById(R.id.mother_health_main_content_layout4);
        contentLayouts[4] = findViewById(R.id.mother_health_main_content_layout5);

        mActionbar = (View)findViewById(R.id.common_back_top);
//        mActionbarBtn = (ImageView) mActionbar.findViewById(R.id.action_btn);
//        mActionbarBtn.setOnClickListener(mOnClickListener);
//        mActionbarBtn.setVisibility(View.GONE);

        mActionbarTipBtn = mActionbar.findViewById(R.id.actionbar_tip_btn);
        mActionbarTipBtn.setOnClickListener(mOnClickListener);
        mActionbarTipBtn.setVisibility(View.VISIBLE);

        //click 저장
        OnClickListener mClickListener = new OnClickListener(mOnClickListener,mActionbar, MotherHealthMainActivity.this);

        //엄마 건강
        mActionbarTipBtn.setOnTouchListener(mClickListener);

        //코드 부여(엄마 건강)
        mActionbarTipBtn.setContentDescription(getString(R.string.ActionbarTipBtn));

//        Hcall_btn = (ImageView) findViewById(R.id.Hcall_btn);
//        Hcall_btn.setOnClickListener(mOnClickListener);

        fragments[0] =  WeightManageFragment.newInstance();     // 체중 화면
        fragments[1] =  StepManageFragment.newInstance();       // 활동 화면
        fragments[2] =  FoodManageFragment.newInstance();       // 식사 화면
        fragments[3] =  PressureManageFragment.newInstance();   // 혈압 화면
        fragments[4] =  SugarManageFragment.newInstance();      // 혈당 화면

        mMenuRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int id = group.indexOfChild(findViewById(checkedId));

                switch (id){
                    case 0 :
//                        mActionbarBtn.setVisibility(View.GONE);
                        mActionbarTipBtn.setVisibility(View.VISIBLE);
//                        Hcall_btn.setVisibility(View.VISIBLE);
//                        mActionbarBtn.setImageResource(R.drawable.icon_targget);
                        setTitle(getString(R.string.main_brn_txt_12));

                        // 임신중일경우 목표 체중 안 띄움
//                        if ("N".equals(commonData.getbirth_chl_yn())) {
//                            mActionbarBtn.setVisibility(View.GONE);
//                        } else {
//                            mActionbarBtn.setVisibility(View.VISIBLE);
//                        }

                        break;
                    case 1 :
//                        mActionbarBtn.setVisibility(View.VISIBLE);
                        mActionbarTipBtn.setVisibility(View.VISIBLE);
//                        Hcall_btn.setVisibility(View.VISIBLE);
//                        mActionbarBtn.setImageResource(R.drawable.icon_targget);
                        setTitle(getString(R.string.main_brn_txt_08));
                        break;
                    case 2 :
//                        mActionbarBtn.setVisibility(View.GONE);
                        setTitle(getString(R.string.main_brn_txt_09));
                        break;
                    case 3 :
//                        mActionbarBtn.setVisibility(View.VISIBLE);
                        mActionbarTipBtn.setVisibility(View.VISIBLE);
//                        Hcall_btn.setVisibility(View.VISIBLE);
//                        mActionbarBtn.setImageResource(R.drawable.icon_write_new);

                        setTitle(getString(R.string.main_brn_txt_10));
                        break;
                    case 4 :
//                        mActionbarBtn.setVisibility(View.VISIBLE);
                        mActionbarTipBtn.setVisibility(View.VISIBLE);
//                        Hcall_btn.setVisibility(View.VISIBLE);
//                        mActionbarBtn.setImageResource(R.drawable.icon_write_new);
                        setTitle(getString(R.string.main_brn_txt_11));
                        break;
                }

                for (int i = 0; i < contentLayouts.length; i++) {
                    contentLayouts[i].setVisibility(i == id ? View.VISIBLE : View.GONE);
                }

                replaceFragment(fragments[id], true, false, null);
            }
        });

        switch (mFragmentNum){
            case 0 :
                findViewById(R.id.radio0).performClick();
                // 임신중일경우 목표 체중 안 띄움
//                if ("N".equals(commonData.getbirth_chl_yn())) {
//                    mActionbarBtn.setVisibility(View.GONE);
//                } else {
//                    mActionbarBtn.setVisibility(View.VISIBLE);
//                }
                break;
            case 1 :
                findViewById(R.id.radio1).performClick();
                break;
            case 2 :
                findViewById(R.id.radio2).performClick();
                break;
            case 3 :
                findViewById(R.id.radio3).performClick();
                break;
            case 4 :
                findViewById(R.id.radio4).performClick();
                break;
            default:
                findViewById(R.id.radio0).performClick();
                break;

        }


        replaceFragment(fragments[mFragmentNum], true, false, null);

        alertMother(mFragmentNum);

//        if(mFragmentNum == 0){
//            if ("N".equals(commonData.getbirth_chl_yn())) {
//                mActionbarBtn.setVisibility(View.GONE);
//            } else {
//                mActionbarBtn.setVisibility(View.VISIBLE);
//            }
//        }
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int vId = v.getId();
//            if (R.id.action_btn == vId) {
//                actionBtnClick();
//            } else
            if(R.id.actionbar_tip_btn == vId){
                actionTipBtnClick();
            } else if(R.id.Hcall_btn == vId){
                hCall();
            }
        }
    };

    /**
     * 상담연결하기
     */
    public void hCall() {
        mDialog = new CustomAlertDialog(MotherHealthMainActivity.this, CustomAlertDialog.TYPE_B);
        mDialog.setTitle(getString(R.string.popup_dialog_a_type_title));
        mDialog.setContent(getString(R.string.do_call_center));
        mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), null);
        mDialog.setPositiveButton(getString(R.string.popup_dialog_button_confirm), (dialog, button) -> {
            String tel = "tel:" + getString(R.string.call_center_number);
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(tel));
            startActivity(intent);
//                    startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
            dialog.dismiss();
        });
        mDialog.show();
    }

    // 액션바 버튼 클릭 처리
    public void actionBtnClick() {
        if(mRadiobtn0.isChecked()) {
            WeightManageFragment tmp = (WeightManageFragment)fragments[0];
            tmp.showGoalDialog();
        } else if(mRadiobtn1.isChecked()) {
            DummyActivity.startActivityForResult(MotherHealthMainActivity.this, 1111, StepInputFragment.class, new Bundle());
        } else if(mRadiobtn3.isChecked()){
            DummyActivity.startActivityForResult(MotherHealthMainActivity.this, 1111, PressureInputMainFragment.class, new Bundle());
        } else if(mRadiobtn4.isChecked()){
            DummyActivity.startActivityForResult(MotherHealthMainActivity.this, 1111, SugarInputMainFragment.class, new Bundle());
        }
    }

    // 액션바 관리팁 버튼 클릭 처리
    public void actionTipBtnClick() {
        String Url = "http://www.higngkids.co.kr/auth/HL_TIP_contents_view.asp?wkey=";
        Intent intent = intent = new Intent(MotherHealthMainActivity.this, TipWebViewActivity.class);
        intent.putExtra("Title", getString(R.string.psy_tip));
        if(mRadiobtn0.isChecked()) {
            intent.putExtra(CommonData.EXTRA_URL_POSITION, 0);
        } else if(mRadiobtn1.isChecked()) {
            intent.putExtra(CommonData.EXTRA_URL_POSITION , 1);
        } else if(mRadiobtn2.isChecked()) {
            intent.putExtra(CommonData.EXTRA_URL_POSITION , 2);
        } else if(mRadiobtn3.isChecked()){
            intent.putExtra(CommonData.EXTRA_URL_POSITION , 3);
        } else if(mRadiobtn4.isChecked()){
            intent.putExtra(CommonData.EXTRA_URL_POSITION , 4);
        }

        startActivity(intent);
    }


    //    @Override
    public void onClick(View v) {
    }
    public void replaceFragment(final Fragment fragment, final boolean isReplace, boolean isAnim, Bundle bundle) {

        android.util.Log.i("MotherMainA", "replaceFragment: " + fragment);

        BaseFragment.newInstance(this);
//        if (isReplace)
//            removeAllFragment();

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

//        if (isAnim)
//            transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        if (bundle != null)
            fragment.setArguments(bundle);

        if (fragment instanceof WeightManageFragment) {
            if (contentLayouts[0].getChildCount() == 0) {
                transaction.replace(R.id.mother_health_main_content_layout, fragment, fragment.getClass().getSimpleName());
            }
        } else if (fragment instanceof StepManageFragment) {
            if (contentLayouts[1].getChildCount() == 0) {
                transaction.replace(R.id.mother_health_main_content_layout2, fragment, fragment.getClass().getSimpleName());
            }
        } else if (fragment instanceof FoodManageFragment) {
            if (contentLayouts[2].getChildCount() == 0){
                transaction.replace(R.id.mother_health_main_content_layout3, fragment, fragment.getClass().getSimpleName());
            }
        } else if (fragment instanceof PressureManageFragment) {
            if (contentLayouts[3].getChildCount() == 0){
                transaction.replace(R.id.mother_health_main_content_layout4, fragment, fragment.getClass().getSimpleName());
            }
        } else if (fragment instanceof SugarManageFragment) {
            if (contentLayouts[4].getChildCount() == 0){
                transaction.replace(R.id.mother_health_main_content_layout5, fragment, fragment.getClass().getSimpleName());
            }
        }

        Ori_fragment = fragment;

        if (!isFinishing()) {
            if (isReplace == false)
                transaction.addToBackStack(null);

            transaction.commit();
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isReplace == false)
                        transaction.addToBackStack(null);

                    transaction.commitAllowingStateLoss();
                }
            }, 100);
        }
    }

//    public void superBackPressed() {
//        super.onBackPressed();
//    }


    private void alertMother(int tabpos){
        //getMotherIsPregnancy Y  임신중 N: 출산후
        //getbirth_chl_yn Y  출산후 N: 임신중
        //exsit_aft_baby 태아가 출산일이 지났는지 여부

//        exsit_aft_baby = false;
//        for (int i = 0; i < MainActivity.mChildMenuItem.size(); i++){
//            try {
//                if(MainActivity.mChildMenuItem.get(i).getmChlExistYn().equals(CommonData.NO)){
//
//                    SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
//                    Date today = new Date();
//
//                    Date AftDate = format.parse(MainActivity.mChildMenuItem.get(i).getmChldrnAftLifyea());
//
//                    if(today.compareTo(AftDate) > 0){
//                        exsit_aft_baby = true;
//                        break;
//                    }
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }

//        if(exsit_aft_baby){
//            if(commonData.getbirth_chl_yn().equals("N")){
//                if(commonData.getMberGrad().equals("10")) {
//                    CustomAlertDialog mDialog = new CustomAlertDialog(MotherHealthMainActivity.this, CustomAlertDialog.TYPE_C);
//                    View view = LayoutInflater.from(MotherHealthMainActivity.this).inflate(R.layout.popup_dialog_passmother, null);
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//                    params.gravity = Gravity.CENTER;
//                    mDialog.setContentView(view, params);
//                    mDialog.setNegativeButton(getString(R.string.popup_dialog_button_cancel), (dialog, button) -> {
//                        dialog.dismiss();
//
//                    });
//                    mDialog.setPositiveButton(getString(R.string.popup_dialog_button_moveto), (dialog, button) -> {
//                        Intent intent = new Intent(MotherHealthMainActivity.this, MotherHealthRegActivity.class);
//                        intent.putExtra("Num", tabpos);
//                        startActivity(intent);
//                        dialog.dismiss();
//                        finish();
//                    });
//                    mDialog.show();
//                }
//            }
//        }
    }


    @Override
    public void onBackPressed() {
        if (isLandScape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }
//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mother_health_main_content_layout);
//        if (fragment instanceof BaseFragment) {
//            ((BaseFragment)fragment).onBackPressed();
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private boolean isLandScape = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Ori_fragment.onConfigurationChanged(newConfig);
        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                //가로 모드
                isLandScape = true;
                mActionbar.setVisibility(View.GONE);
                mMenuRadioGroup.setVisibility(View.GONE);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                //세로 모드
                isLandScape = false;
                mActionbar.setVisibility(View.VISIBLE);
                mMenuRadioGroup.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void showProgress() {
//        super.showProgress();
        if (mProgress != null)
            mProgress.show();
    }

    @Override
    public void hideProgress() {
//        super.hideProgress();
        if (mProgress != null)
            mProgress.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Ori_fragment != null)
            Ori_fragment.onActivityResult(requestCode, resultCode, data);
    }
}
