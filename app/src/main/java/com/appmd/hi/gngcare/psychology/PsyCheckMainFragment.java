package com.appmd.hi.gngcare.psychology;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.base.BaseFragment;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.greencare.component.OnClickListener;
import com.appmd.hi.gngcare.util.Util;
import com.appmd.hi.gngcare.webview.BackWebViewActivity;

/**
 * Created by CP on 2018. 4. 3..
 */

public class PsyCheckMainFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, PsyMainActivity.onKeyBackPressedListener {

    private PsyMainActivity activity;
    private LinearLayout tabSchLy;
    private LinearLayout tabEduLy;
    private LinearLayout tabEdult;
    private Button tabSchBtn;
    private Button tabEduBtn;
    private Button tabEdultBtn;
    private View view;
    private Button shcBtn01;
    private Button shcBtn02;
    private Button shcBtn03;
    private Button shcBtn04;

    private Button eduBtn01;
    private Button eduBtn02;
    private Button eduBtn03;
    private Button eduBtn04;

    private Button edultBtn01;
    private Button edultBtn02;
    private Button edultBtn03;

    public static String mCode = "";
    public static String mCheckTitle = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (PsyMainActivity)getActivity();
        view = inflater.inflate(R.layout.psy_check_main_fragment, null);
        init(view);
        setEvent(view);

        return view;
    }

    /**
     * 객체 초기화
     *
     * @param view view
     */
    public void init(View view) {

        tabSchLy = (LinearLayout) view.findViewById(R.id.tabSchLy);
        tabEduLy = (LinearLayout) view.findViewById(R.id.tabEduLy);
        tabEdult = (LinearLayout) view.findViewById(R.id.tabEdult);
        tabSchBtn = (Button) view.findViewById(R.id.tabSchBtn);
        tabEduBtn = (Button) view.findViewById(R.id.tabEduBtn);
        tabEdultBtn = (Button) view.findViewById(R.id.tabEdultBtn);

        tabSchBtn.setSelected(true);
        tabEduBtn.setSelected(false);
        tabEdultBtn.setSelected(false);


        shcBtn01 = view.findViewById(R.id.shcBtn01);
        shcBtn02 = view.findViewById(R.id.shcBtn02);
        shcBtn03 = view.findViewById(R.id.shcBtn03);
        shcBtn04 = view.findViewById(R.id.shcBtn04);

        eduBtn01 = view.findViewById(R.id.eduBtn01);
        eduBtn02 = view.findViewById(R.id.eduBtn02);
        eduBtn03 = view.findViewById(R.id.eduBtn03);
        eduBtn04 = view.findViewById(R.id.eduBtn04);

        edultBtn01 = view.findViewById(R.id.edultBtn01);
        edultBtn02 = view.findViewById(R.id.edultBtn02);
        edultBtn03 = view.findViewById(R.id.edultBtn03);

        tabSchLy.setVisibility(View.VISIBLE);
        tabEduLy.setVisibility(View.GONE);
        tabEdult.setVisibility(View.GONE);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(View view) {
        tabSchBtn.setOnClickListener(this);
        tabEduBtn.setOnClickListener(this);
        tabEdultBtn.setOnClickListener(this);

        shcBtn01.setOnClickListener(this);
        shcBtn02.setOnClickListener(this);
        shcBtn03.setOnClickListener(this);
        shcBtn04.setOnClickListener(this);

        eduBtn01.setOnClickListener(this);
        eduBtn02.setOnClickListener(this);
        eduBtn03.setOnClickListener(this);
        eduBtn04.setOnClickListener(this);

        edultBtn01.setOnClickListener(this);
        edultBtn02.setOnClickListener(this);
        edultBtn03.setOnClickListener(this);

        // XXXMrSohn GC밸런스심리케어센터​
        view.findViewById(R.id.gc_balance_care_center).setOnClickListener(this);
        view.findViewById(R.id.gc_balance_care_center2).setOnClickListener(this);
        view.findViewById(R.id.gc_balance_care_center3).setOnClickListener(this);



        //click 저장
        OnClickListener mClickListener = new OnClickListener(this,view, getContext());

        //아이 심리
        tabSchBtn.setOnTouchListener(mClickListener);
        shcBtn01.setOnTouchListener(mClickListener);
        shcBtn02.setOnTouchListener(mClickListener);
        shcBtn03.setOnTouchListener(mClickListener);
        shcBtn04.setOnTouchListener(mClickListener);
        tabEduBtn.setOnTouchListener(mClickListener);
        eduBtn01.setOnTouchListener(mClickListener);
        eduBtn02.setOnTouchListener(mClickListener);
        eduBtn03.setOnTouchListener(mClickListener);
        eduBtn04.setOnTouchListener(mClickListener);
        tabEdultBtn.setOnTouchListener(mClickListener);
        edultBtn01.setOnTouchListener(mClickListener);
        edultBtn02.setOnTouchListener(mClickListener);
        edultBtn03.setOnTouchListener(mClickListener);

        view.findViewById(R.id.gc_balance_care_center).setOnTouchListener(mClickListener);
        view.findViewById(R.id.gc_balance_care_center2).setOnTouchListener(mClickListener);
        view.findViewById(R.id.gc_balance_care_center3).setOnTouchListener(mClickListener);

        //코드 부여(아이 심리)
        tabSchBtn.setContentDescription(getString(R.string.tabSchBtn));
        shcBtn01.setContentDescription(getString(R.string.shcBtn01));
        shcBtn02.setContentDescription(getString(R.string.shcBtn02));
        shcBtn03.setContentDescription(getString(R.string.shcBtn03));
        shcBtn04.setContentDescription(getString(R.string.shcBtn04));
        tabEduBtn.setContentDescription(getString(R.string.tabEduBtn));
        eduBtn01.setContentDescription(getString(R.string.eduBtn01));
        eduBtn02.setContentDescription(getString(R.string.eduBtn02));
        eduBtn03.setContentDescription(getString(R.string.eduBtn03));
        eduBtn04.setContentDescription(getString(R.string.eduBtn04));
        tabEdultBtn.setContentDescription(getString(R.string.tabEdultBtn));
        edultBtn01.setContentDescription(getString(R.string.edultBtn01));
        edultBtn02.setContentDescription(getString(R.string.edultBtn02));
        edultBtn03.setContentDescription(getString(R.string.edultBtn03));

        view.findViewById(R.id.gc_balance_care_center).setContentDescription(getString(R.string.gc_center));
        view.findViewById(R.id.gc_balance_care_center2).setContentDescription(getString(R.string.gc_center));
        view.findViewById(R.id.gc_balance_care_center3).setContentDescription(getString(R.string.gc_center));

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitleTxt("심 리 체 크");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tabSchBtn:
                tabSchBtn.setSelected(true);
                tabEduBtn.setSelected(false);
                tabEdultBtn.setSelected(false);
                tabSchLy.setVisibility(View.VISIBLE);
                tabEduLy.setVisibility(View.GONE);
                tabEdult.setVisibility(View.GONE);
                break;
            case R.id.tabEduBtn:
                tabSchBtn.setSelected(false);
                tabEduBtn.setSelected(true);
                tabEdultBtn.setSelected(false);
                tabSchLy.setVisibility(View.GONE);
                tabEduLy.setVisibility(View.VISIBLE);
                tabEdult.setVisibility(View.GONE);
                break;

            case R.id.tabEdultBtn:
                tabSchBtn.setSelected(false);
                tabEduBtn.setSelected(false);
                tabEdultBtn.setSelected(true);
                tabSchLy.setVisibility(View.GONE);
                tabEduLy.setVisibility(View.GONE);
                tabEdult.setVisibility(View.VISIBLE);
                break;

            case R.id.shcBtn01:
                startPsyCheckActivity("LK01001", getString(R.string.psy_btn_text_tab_01_01));
                break;
            case R.id.shcBtn02:
                startPsyCheckImgActivity("LK01002", getString(R.string.psy_btn_text_tab_01_02));
                break;
            case R.id.shcBtn03:
                startPsyCheckActivity("LK01003", getString(R.string.psy_btn_text_tab_01_03));
                break;
            case R.id.shcBtn04:
                startPsyCheckActivity("LK01004", getString(R.string.psy_btn_text_tab_01_04));
                break;
            case R.id.eduBtn01:
                startPsyCheckActivity("LK02001", getString(R.string.psy_btn_text_tab_02_01));
                break;
            case R.id.eduBtn02:
                startPsyCheckActivity("LK02002", getString(R.string.psy_btn_text_tab_02_02));
                break;
            case R.id.eduBtn03:
                startPsyCheckActivity("LK02003",getString(R.string.psy_btn_text_tab_02_03));
                break;
            case R.id.eduBtn04:
                startPsyCheckActivity("LK02004",getString(R.string.psy_btn_text_tab_02_04));
                break;
            case R.id.edultBtn01:
                startPsyCheckActivity("LK03001", getString(R.string.psy_btn_text_tab_03_01));
                break;
            case R.id.edultBtn02:
                startPsyCheckActivity("LK03002", getString(R.string.psy_btn_text_tab_03_02));
                break;
            case R.id.edultBtn03:
                startPsyCheckActivity("LK03003", getString(R.string.psy_btn_text_tab_03_03));
                break;

//            LK03001 LK03 교육 강박 검사 1 부모가 자녀에게 갖고 있는 학습 애착도와 교육 강박에 대해 알아볼까요?
//            LK03002 LK03 엄마 우울증 검사 2 엄마의 심리적 상황과 우울증 정도를 알아볼까요?
//            LK03003 LK03 상황 판단 검사 3 엄마의 상황 판단 능력에 대해 알아볼까요?
//            LK03004 LK03 자아 교감 검사 4 아빠의 마음 상태와 스트레스에 대해 알아볼까요?


            case R.id.gc_balance_care_center:
            case R.id.gc_balance_care_center2:
            case R.id.gc_balance_care_center3:
                Intent intent = new Intent(getActivity(), BackWebViewActivity.class);
                intent.putExtra(CommonData.EXTRA_URL, commonData.URL_GCBALANCECENTER);
                intent.putExtra(CommonData.EXTRA_ACTIVITY_TITLE, getString(R.string.gc_balance_care_center_title));
                startActivity(intent);
                Util.BackAnimationStart(getActivity());
                break;

        }
    }

    /**
     * 검사 Activity 시작
     * @param mCode
     * @param extraTitle
     */
    private void startPsyCheckActivity(String mCode, String extraTitle) {
        switchFragment(new PsyMainFragment());

        Intent intent = new Intent(getActivity(), PsyCheckStartActivity.class);
        intent.putExtra(CommonData.JSON_M_CODE_F, mCode);
        intent.putExtra(CommonData.EXTRA_TITLE, extraTitle);
        startActivity(intent);
    }

    /**
     * 검사 Activity 시작
     * @param mCode
     * @param extraTitle
     */
    private void startPsyCheckImgActivity(String mCode, String extraTitle) {
        switchFragment(new PsyMainFragment());
        Intent intent = new Intent(getActivity(), PsyCheckStartImgActivity.class);
        intent.putExtra(CommonData.JSON_M_CODE_F, mCode);
        intent.putExtra(CommonData.EXTRA_TITLE, extraTitle);
        startActivity(intent);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();

        switch (id) {
            case R.id.search_edit:    // 검색
                //     CommonView.getInstance().setClearImageBt(mSearchDelBtn, hasFocus);
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        ((PsyMainActivity) getContext()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
        PsyMainActivity activity = (PsyMainActivity) getActivity();
        switchFragment(new PsyMainFragment());
    }
}