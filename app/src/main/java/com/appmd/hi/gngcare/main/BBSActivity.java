package com.appmd.hi.gngcare.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.appmd.hi.gngcare.R;
import com.appmd.hi.gngcare.adapter.BBSListAdapter;
import com.appmd.hi.gngcare.collection.BBSItem;
import com.appmd.hi.gngcare.common.CommonData;
import com.appmd.hi.gngcare.common.CustomAlertDialog;
import com.appmd.hi.gngcare.common.CustomAsyncListener;
import com.appmd.hi.gngcare.common.MakeProgress;
import com.appmd.hi.gngcare.common.NetworkConst;
import com.appmd.hi.gngcare.network.RequestApi;
import com.appmd.hi.gngcare.util.GLog;
// import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class BBSActivity extends AppCompatActivity {

    BBSListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mBbsListLay;
    TextView mTxtNullData;

    ImageButton mCommonLeftBtn;

    Spinner mSpinner;

    ArrayList<BBSItem> mBBSList;

    int mPageNum;
    int mMaxPageNum;
    int mContentsType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbs);

        initView();
        init();
        initEvent();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        GLog.i("onResume", "dd");

        int push_type = getIntent().getIntExtra(CommonData.EXTRA_PUSH_TYPE, 0);
        String info_sn = getIntent().getStringExtra(CommonData.EXTRA_INFO_SN);

        if(push_type > 0){
            requestBBSViewApi(push_type, info_sn);
        }


        getIntent().removeExtra(CommonData.EXTRA_PUSH_TYPE);
        getIntent().removeExtra(CommonData.EXTRA_INFO_SN);
    }

    protected void initView(){
        mCommonLeftBtn = (ImageButton)findViewById(R.id.common_left_btn);
        mBbsListLay = (RecyclerView)findViewById(R.id.bbs_list_lay);
        mBbsListLay.setHasFixedSize(true);
        mTxtNullData = (TextView)findViewById(R.id.txt_null_data);
        mSpinner = (Spinner)findViewById(R.id.spinner);
    }

    protected void initEvent(){
        mCommonLeftBtn.setOnClickListener(v -> finish());

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPageNum = 1;
                mContentsType = position;
                requestBBSListApi(mPageNum,mContentsType);

                mBbsListLay.smoothScrollToPosition(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    protected void init(){
        mPageNum = 0;
        mContentsType = 0;
        mBBSList = new ArrayList<BBSItem>();

        mLayoutManager = new GridLayoutManager(BBSActivity.this,2);
        mBbsListLay.setLayoutManager(mLayoutManager);
        mAdapter = new BBSListAdapter(BBSActivity.this, mBBSList, mBbsListLay);
        mBbsListLay.setAdapter(mAdapter);

        mAdapter.setOnLoadMoreListener(() -> {

            if(mBBSList.size() < mMaxPageNum){
                mPageNum++;
                requestBBSListApi(mPageNum, mContentsType);
            }
        });
    }

    /**
     * 최근 게시판 목록
     */
    public void requestBBSViewApi(int pushType, String infoSn) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_PUSH_HEALTH_VIEW);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_APP_CODE, CommonData.APP_CODE_ANDROID);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN,  CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_PUSH_TYP, String.valueOf(pushType));               //  푸쉬 번호
            object.put(CommonData.JSON_INFO_SN, infoSn);               //  게시물 코드

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(BBSActivity.this, NetworkConst.NET_GET_BBS_VIEW, NetworkConst.getInstance().getDefDomain(), networkListener, params,new MakeProgress(this));
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 최근 게시판 목록
     */
    public void requestBBSListApi(int pageNum, int contentsType) {
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONObject object = new JSONObject();
            object.put(CommonData.JSON_API_CODE, CommonData.METHOD_CONTENT_SPECIAL_BBSLIST);    //  api 코드명
            object.put(CommonData.JSON_INSURES_CODE, CommonData.INSURE_CODE);          //  insures 코드
            object.put(CommonData.JSON_MBER_SN,  CommonData.getInstance().getMberSn());             //  회원고유값
            object.put(CommonData.JSON_PAGENUMBER, ""+pageNum);               //  페이지 번호
            object.put(CommonData.JSON_CONTENT_TYP, ""+contentsType);               //  게시판 분류 번호 0: 전체  1: 질병예방 2:영양 3:운동 4: 심리

            params.add(new BasicNameValuePair(CommonData.JSON_JSON, object.toString()));

            RequestApi.requestApi(BBSActivity.this, NetworkConst.NET_GET_BBS_LIST, NetworkConst.getInstance().getDefDomain(), networkListener, params,new MakeProgress(this));
        } catch (Exception e) {
            GLog.i(e.toString(), "dd");
        }
    }

    /**
     * 네트워크 리스너
     */
    public CustomAsyncListener networkListener = new CustomAsyncListener() {

        @Override
        public void onPost(Context context, int type, int resultCode, JSONObject resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub

            switch ( type ) {
                case NetworkConst.NET_GET_BBS_LIST:	// 게시판 리스트 가져오기
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_BBS_LIST", "dd");

                            try {
                                mMaxPageNum = resultData.getInt(CommonData.JSON_MAXPAGENUMBER);
                                if(mMaxPageNum > 0){
                                    int pageNum = resultData.getInt(CommonData.JSON_PAGENUMBER);
                                    JSONArray bbsList = resultData.getJSONArray(CommonData.JSON_BBSLIST);

                                    if(bbsList.length() > 0){
                                        if(pageNum == 1)
                                            mBBSList.clear();

                                        // bbsListJSONObject.getInt(CommonData.JSON_CONTENT_TYP)

                                        for (int i= 0; i < bbsList.length(); i++){
                                            JSONObject bbsListJSONObject = bbsList.getJSONObject(i);
                                            BBSItem newItem;
                                            try {
                                                newItem = new BBSItem(bbsListJSONObject.getString(CommonData.JSON_INFO_SUBJECT), bbsListJSONObject.getInt(CommonData.JSON_CONTENT_TYP),
                                                        bbsListJSONObject.getString(CommonData.JSON_INFO_TITLE_URL), bbsListJSONObject.getString(CommonData.JSON_CONTENT_TYP_MOBILE_IMG),
                                                        bbsListJSONObject.getString(CommonData.JSON_INFO_DAY));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                                newItem = new BBSItem(bbsListJSONObject.getString(CommonData.JSON_INFO_SUBJECT), 1,
                                                        bbsListJSONObject.getString(CommonData.JSON_INFO_TITLE_URL), bbsListJSONObject.getString(CommonData.JSON_CONTENT_TYP_MOBILE_IMG),
                                                        bbsListJSONObject.getString(CommonData.JSON_INFO_DAY));
                                            }

                                            mBBSList.add(newItem);
                                            mAdapter.notifyItemInserted(mBBSList.size());
                                        }
                                        mAdapter.setLoaded();

                                        mAdapter.notifyDataSetChanged();
                                        mTxtNullData.setVisibility(View.GONE);
                                        mBbsListLay.setVisibility(View.VISIBLE);
                                    }
                                }else {
                                    mBBSList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    mTxtNullData.setVisibility(View.VISIBLE);
                                    mBbsListLay.setVisibility(View.GONE);
                                }
                            }catch(Exception e){
                                GLog.e(e.toString());
                            }

                            break;

                        default:
                            GLog.i("JOIN FAIL", "dd");

                            break;
                    }
                    break;
                case NetworkConst.NET_GET_BBS_VIEW:
                    switch ( resultCode ) {
                        case CommonData.API_SUCCESS:
                            GLog.i("NET_GET_BBS_VIEW", "dd");

                            try {
                                    JSONArray bbsView = resultData.getJSONArray(CommonData.JSON_HEALTH_LIST);

                                    if(bbsView.length() > 0){
                                        Intent intent = new Intent(BBSActivity.this, BBSViewActivity.class);
                                        intent.putExtra(CommonData.JSON_INFO_TITLE_URL, bbsView.getJSONObject(0).getString(CommonData.JSON_INFO_TITLE_URL));
                                        startActivity(intent);
                                    }
                            }catch(Exception e){
                                GLog.e(e.toString());
                            }
                            break;
                        default:
                            GLog.i("JOIN FAIL", "dd");
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onNetworkError(Context context, int type, int httpResultCode, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            dialog.show();
        }

        @Override
        public void onDataError(Context context, int type, String resultData, CustomAlertDialog dialog) {
            // TODO Auto-generated method stub
            // 데이터에 문제가 있는 경우 다이얼로그를 띄우고 인트로에서는 종료하도록 한다.
            dialog.show();

        }
    };

    @Override protected void attachBaseContext(Context newBase) {
        // super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }
}
