package com.appmd.hi.gngcare.greencare.charting.interfaces.dataprovider;

import com.appmd.hi.gngcare.greencare.charting.data.SticData;

public interface SticDataProvider extends BarLineScatterCandleBubbleDataProvider {

    SticData getCandleData();
}
